'use strict';
var projectApp = angular.module('projectApp');
projectApp.directive('formInput', function () {
  return {
    restrict: 'A',
    scope: {
      ngModel: '=',
      formError: '=',
      divClass: '='
    },
    transclude: true,
    template: '<div ng-transclude class={{divClass}}></div><span class="help-block">{{formError[0]}}</span>'
  };
});
