'use strict';
var projectApp = angular.module('projectApp');

projectApp.directive('iconClientDisabilitiesAllergies', function ($compile,gettextCatalog ) {
    return {
            restrict: 'E',
            scope: { 
                     client:"=ngClient"
        },
        replace:true,
        template:'<div class="pull-left">'+
                     '<button ng-if="disabilities.length > 0" tooltips tooltips    tooltip-html="{{disabilitiesHtml}}" tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top" class="btn  withTooltip"><i class="fa fa-wheelchair blue"></i></button>'+
                     '<button class="btn withTooltip" ng-if="allergies.length > 0" tooltips    tooltip-html="{{allergiesHtml}}" tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top"  type="button"><i class="fa fa-medkit blue"></i></button>'+
                  '</div>',
        controller : function($scope, $element, $attrs){
            


        },
        link : function($scope, $element, $attrs){
            var beginHtml='<table class="aui aui-tooltip">'+
                                '<thead>'+
                                    '<tr>'+
                                    '<th translate class="text-center">Type</th>'+
                                    '<th translate class="text-center">Detail</th>'+
                                    '</tr>'+
                                '</thead>'+
                                    '<tbody>';
            var endHtml ='</tbody>'+
                                '</table>';
            $scope.disabilitiesHtml= beginHtml;            
            $scope.allergiesHtml= beginHtml;                      
            $scope.disabilities = [];
            $scope.allergies = [];
            for (var i = 0; i < $scope.client.clientSpecialComments.length; i++) {
                if($scope.client.clientSpecialComments[i].specialCategoryId==1){
                    $scope.allergies.push($scope.client.clientSpecialComments[i]);
                    $scope.allergiesHtml+= '<tr>'+
                                                '<td class="text-center" >'+gettextCatalog.getString($scope.client.clientSpecialComments[i].specialSubCategoryName)+'</td>'+
                                                '<td class="text-center">'+gettextCatalog.getString($scope.client.clientSpecialComments[i].specialSubCategoryDetailName)+'</td>'+
                                              '</tr>';
                }
                if($scope.client.clientSpecialComments[i].specialCategoryId==2){
                    $scope.disabilities.push($scope.client.clientSpecialComments[i]);
                    $scope.disabilitiesHtml+= '<tr>'+
                                                '<td class="text-center" >'+gettextCatalog.getString($scope.client.clientSpecialComments[i].specialSubCategoryName)+'</td>'+
                                                '<td class="text-center"> '+gettextCatalog.getString($scope.client.clientSpecialComments[i].specialSubCategoryDetailName)+'</td>'+
                                              '</tr>';
                }
            }
            $scope.disabilitiesHtml += endHtml;
            $scope.allergiesHtml += endHtml;
       }
    }
});

projectApp.directive('sheet', function ($compile,$window,$document,Client, $timeout,$state, ReserveSystem, SheetDetailNote,Sheet, SheetDetail) {
    return {
            restrict: 'E',
            scope: { sheet:"=ngSheet"
        },
        template:'<div > '+ 
                    '<edit-sheet ng-sheet-detail="sheetDetail" ng-confirm-client-note="confirmClientNote" ng-confirm-service-note="confirmServiceNote" ng-sheet="sheet" ng-sheet-options="sheetOptions" ng-panel-opened="sheet.panelOpened" ng-class="{\'col-md-7\':sheet.panelOpened==true,\'col-md-12\':!sheet.panelOpened}"></edit-sheet>'+
                    '<div ng-class="{\'col-md-5\':sheet.panelOpened==true,\'hidden\':!sheet.panelOpened}" class="prsht">'+
                        '<div>'+
                            '<div class="prsht-controls">'+
                                '<div class="prsht-controls-close"><span class="fa  fa-times" ng-click="sheet.panelOpened=false"></span></div>'+
                            '</div>'+
                            '<div class="prsht-view"></div>'+
                        '</div>'+
                    '</div>'+
                 '</div>',
        controller : function($scope, $element, $attrs){
            $scope.sheet.panelOpened=false;
            $scope.creating=($scope.sheet.id==undefined);
            if ($scope.creating){
              $scope.sheetDetail = SheetDetail.$build({
                title: "Notes",
              });
            }

            if($scope.sheetOptions===undefined)
                Sheet.options().then(function (_option) {
                        $scope.sheetOptions= _option.data;
                      });
            $scope.showOrCreate=false;
            $scope.confirmClientNote = SheetDetailNote.$build({});
            $scope.confirmServiceNote = SheetDetailNote.$build({});

            $scope.openClient = function(client){
                $scope.sheet.panelOpened=true;
                if(client===undefined){
                    client = Client.$build({email:""});
                }
                $scope.client = client;
                if($scope.sheet.id==undefined){
                    $scope.sheet.$save().$then(function(_sheet){
                        $scope.sheet =_sheet;
                        $scope.sheetDetail.sheetId = _sheet.id;
                        $scope.sheetDetail.$save().$then(function(_sheet_detail){
                          $scope.sheet.sheetDetails.$add(_sheet_detail);
                          $element.find(".prsht-view").html($compile('<client-panel ng-delete-sheet-detail-id="sheet.deleteSheetDetailId" ng-delete-sheet-detail-note-id="sheet.deleteSheetDetailNoteId" ng-confirm-client-note="confirmClientNote" ng-sheet="sheet" ng-client="client"></client-panel>')($scope));
                        });
                    });
                }else{
                        $element.find(".prsht-view").html($compile('<client-panel ng-delete-sheet-detail-id="sheet.deleteSheetDetailId" ng-delete-sheet-detail-note-id="sheet.deleteSheetDetailNoteId" ng-confirm-client-note="confirmClientNote" ng-sheet="sheet" ng-client="client"></client-panel>')($scope));
                }
            };
            $scope.openService = function(service){
                $scope.service = service;
                $scope.showDirectiveClient = true;
                $scope.sheet.panelOpened=true;
                if ($scope.service != undefined)
                  $scope.showOrCreate = false;
                else $scope.showOrCreate = true;
                if($scope.sheet.id==undefined){
                    $scope.sheet.$save().$then(function(_sheet){

                        $scope.sheet =_sheet;
                        $scope.sheetDetail.sheetId = _sheet.id;
                        $scope.sheetDetail.$save().$then(function(_sheet_detail){
                          $scope.sheet.sheetDetails.$add(_sheet_detail);
                          $element.find(".prsht-view").html($compile('<service-panel ' + (($scope.service != undefined) ? 'ng-service="service"' : '') + ' ng-delete-sheet-detail-id="sheet.deleteSheetDetailId" ng-delete-sheet-detail-note-id="sheet.deleteSheetDetailNoteId" ng-confirm-service-note="confirmServiceNote" ng-show-create="showOrCreate" ng-service="service" ng-sheet="sheet" ></service-panel>')($scope));
                        });
                    });
                }else{
                        $element.find(".prsht-view").html($compile('<service-panel ' + (($scope.service != undefined) ? 'ng-service="service"' : '') + ' ng-delete-sheet-detail-id="sheet.deleteSheetDetailId" ng-delete-sheet-detail-note-id="sheet.deleteSheetDetailNoteId" ng-confirm-service-note="confirmServiceNote" ng-show-create="showOrCreate" ng-service="service" ng-sheet="sheet" ></service-panel>')($scope));
                }
            };
            $scope.refreshServices = function() {
                if($scope.sheet.$pk!==undefined){
                    $scope.sheet.$refresh().$then(function(){
                        $scope.sheet.servicesList =
                          $.map( $scope.sheet.airServices, function(elem) {
                            elem.type = 'air'; 
                            return elem; 
                            }
                          ).concat(
                            $.map( $scope.sheet.accomodationServices, function(elem) { 
                              elem.type = 'accomodation'; 
                              elem.date = elem.initDate; 
                              return elem; 
                            })).concat(
                              $.map( $scope.sheet.vehicleServices, function(elem) { 
                                elem.type = 'car'; 
                                elem.date = elem.startDate; 
                                return elem; 
                              })
                          ); 
                  });
                }else{
                    $scope.sheet.servicesList = [];
                }
            };

            
                        /*Bootstrap alert BEGIN $scope.addAlert("prueba1","warning,success,danger,info");*/
            $scope.addAlert = function(msg,type,translate,close,timeout){
                $element.find(".prsht-header").append($compile('<bootstrap-alert '+(close===undefined?'':'ngf-close="'+close+'"')+' '+(timeout===undefined?'':'ngf-timeout="'+timeout+'"')+' ngf-type="'+type+'">'+(translate?'<translate>':'')+msg+(translate?'</translate>':'')+'</bootstrap-alert>')($scope));
            }

            //monitoreo sheet.panelOpened si se cierra el panel la nota
            // se limpia de memoria   
            $scope.$watch('sheet.panelOpened', function() {
                if($scope.sheet.panelOpened==false){
                    $scope.confirmServiceNote = SheetDetailNote.$build({});
                } 
                  
            }, true);
            $scope.refreshServices();
        },
        link : function ($scope, $element, $attrs) {
            
            $element.children('div').css({
                          'float': 'left',
                          'display': 'table',
                          'table-layout': 'fixed',
                          'width': '100%'
                        });
            $element.css({
                          'display':'block',
                          'float':'left',
                          'position':'relative',
                          'overflow':'hidden',
                          'border-top': '1px solid #ccc'
            });

            var w = angular.element($window);
            $scope.$watch(function () {
                return {
                    'h': w.height(), 
                    'w': w.width()
                };
            }, function (newValue, oldValue) {
                $element.css("height",newValue.h-105);
                $element.children("div").css("height",newValue.h-105);
            }, true);
            w.bind('resize', function () {
                $scope.$apply();
            });
        }
    }

});

projectApp.directive('editSheet', function ($compile,MessageUtils,gettextCatalog,$state,$timeout, SheetDetailNote) {
    return {
        restrict: 'E',
        scope :{
            sheet:"=ngSheet",
            panelOpened:"=ngPanelOpened",
            sheetOptions:"=ngSheetOptions",
            confirmClientNote:"=ngConfirmClientNote",
            confirmServiceNote:"=ngConfirmServiceNote",
            sheetDetail:"=ngSheetDetail"
        },

        template: '<div class="sht">'+
                    '<div class="row-fluid"><h5  class="titulo"><strong translate>Title:</strong></h5></div>'+
                    '<div  class="container-fluid"><div class="row-fluid"><input class="col-md-4 sht-input-title" ng-model="sheet.title" ng-enter="saveSheet()" ng-blur="saveSheet()"  type="text"><div class="col-md-3 sht-identifier"><span class="pull-left">{{"Identifier" | translate}}:</span><span class="pull-left"><strong>{{ sheet.ref }}</strong></span></div> <div class="col-md-5 sht-more-detail" collapse="detailCollapse"><div class="pull-left"><span><translate>Status</translate>:</span><span><strong>{{ sheet.status | uppercase | translate }}</strong></div><span class="separator pull-left" ng-show="panelOpened==false" ></span><div class="pull-left"></span><span><translate>Created at</translate>:</span><span><strong>{{sheet.createdAt|date:"yyyy-MM-dd"}}</strong></span></div><div class="pull-left"><span><translate>Created by</translate>:</span><span><strong>{{sheet.agent.name}} {{sheet.agent.lastName}}</strong></span></div><div class="pull-left sht-more-detail-hide"> <span ng-click="detailCollapse = !detailCollapse" ng-show="!detailCollapse" translate>hide...</span></div></div><div class="sht-more-detail-show col-md-5"><span  ng-click="detailCollapse = !detailCollapse" ng-show="detailCollapse" translate>more...</span></div></div></div>'+
                    '<div class="container-fluid" style="position: relative; margin-bottom: 10px;"><div class="row-fluid sht-detail-reason-privacy">'+ 
                        '<div class="col-md-4 sht-reason">'+
                            '<label class="control-label" style="margin-right: 5px;"><strong translate>Reason</strong></label>'+
                            '<select ng-options="key as value | translate for (key, value) in sheetOptions.reasons" class="form-control nopaddingH"  ng-model="sheet.reason">'+
                                '<option value="" translate>Select</option>'+
                            '</select>'+
                        '</div>'+
                        '<div class="col-md-4 sht-privacy">'+
                            '<label class="control-label" style="margin-right: 5px;"><strong translate>Privacy</strong></label>'+
                            '<toggle-switch ng-model="sheet.public"  class="switch-blue" on-label="{{\'Public\'|translate}}" off-label="{{\'Private\'|translate}}"></toggle-switch>'+
                        '</div></div>'+
                            '<a  ng-show="sheet.status == \'QUOTE\' && sheet.id!=undefined" loading-no-restmod="true"  loading-click="confirmSheet();" class="btn btn-default blue  sht-confirm"><translate>Confirm sheet</translate></a>'+
                        '</div>'+
                    '<div class="row-fluid">'+
                        '<div class="portlet box project">'+
                            '<div class="portlet-title">'+
                                '<div class="caption" >'+
                                    '<i class="fa fa-users"></i><translate>Clients</translate>'+
                                    '<span class="badge badge-info badge-sheet">{{sheet.clients.length}} </span>'+
                                '</div>'+
                                '<div class="tools">'+
                                    '<div class="option" ng-click="setSheetDetailNote(); openClient()">'+
                                        '<i class="fa fa-user"></i>'+
                                        '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                        '<translate ng-show="!panelOpened">Add Client</translate>'+
                                    '</div>'+
                                    '<div class="option" ng-click="sheet.$refresh()">'+
                                        '<span aria-hidden="true" class="glyphicon glyphicon-refresh" ></span>'+
                                    '</div>'+
                                    '<div class="option" ng-click="clientsTableCollapse = !clientsTableCollapse">'+
                                        '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !clientsTableCollapse, \'glyphicon-chevron-up\': clientsTableCollapse}" class="glyphicon"></span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="clientsTableCollapse">'+
                                '<div class="btn-toolbar">'+
                                    '<loading-message ng-model="sheet"></loading-message>'+
                                '</div>'+
                                '<div class="table-responsive">'+
                                    '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th></th>'+
                                                '<th translate>Name</th>'+
                                                '<th translate>Email</th>'+
                                                '<th translate>Actions</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody id="sheet_table_client">'+
                                            '<tr id="sheet_client_{{$index}}" ng-repeat="client in sheet.clients" >'+
                                                '<td class="col-sm-1" >'+
                                                    '<div class="btn-group btn-group-xs btn-group-solid sht-btn-table-client">'+
                                                        '<button tooltips tooltips  tooltip-html="<translate>Select</translate>&nbsp;<translate>Contact Person</translate>"  tooltip-lazy="false" tooltip-show-trigger="mouseover" class="btn" ng-class="{\'selected\':client.isContact}" ng-click="toggleContact(client)"  type="button"><i class="fa fa-phone"></i></button>'+
                                                        '<icon-client-disabilities-allergies ng-client="client"></icon-client-disabilities-allergies>'+
                                                        '<button tooltips tooltips  tooltip-html="<translate>Minor</translate>"  tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top" class="btn withTooltip "  ng-if="isMinor(client.birthDate)" type="button"><i class="fa fa-child"></i></button>'+
                                                    '</div>'+
                                                '</td>'+
                                                '<td ng-click="setSheetDetailNote(); openClient(client)">'+
                                                    '<span >{{ client.firstName }}&nbsp;{{ client.middleName }}&nbsp;{{ client.lastName }}&nbsp;{{ client.secondLastName }}</span>'+
                                                '</td>'+
                                                '<td ng-click="setSheetDetailNote(); openClient(client)">'+
                                                    '<span >{{ client.email }}</span>'+
                                                '</td>'+
                                                '<td class="text-center">'+
                                                    '<a href="javascript:;" loading-click="removeClient(client)" loading-icon="true" class="btn btn-table red ">'+
                                                        '<span class="glyphicon glyphicon-trash"></span>'+
                                                    '</a>'+
                                                '</td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                        '<tbody ng-show="(sheet.sheetDetails[$index].sheetDetailNotes | filter: {classification: \'clients\'}).length > 0" ng-repeat="sheetDetail in sheet.sheetDetails track by $index">'+
                                          '<tr style="color:#CD5C5C;" class="no-processed-command">'+
                                              '<td colspan="5"><strong>{{sheetDetail.title}}</strong></td>'+    
                                          '</tr>'+
                                          '<tr class="no-processed-command" ng-repeat="sheetDetailNote in sheetDetail.sheetDetailNotes | filter: {classification: \'clients\'} track by $index">'+
                                            '<td colspan="5" ng-click="setSheetDetailNote(sheetDetailNote); openClient()" style="padding-left:20px;">{{sheetDetailNote.commandText}}</td>'+
                                          '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</div>'+   
                            '</div>'+   
                        '</div>'+
                    '</div>'+
                    
                    '<div class="row-fluid" style="float:right;>'+
                            '<ul>'+
                                '<li ng-class="{\'filter-service-selected\':search.type==\'\'}" ng-if="listServiceType.length>0" class="keypad-service">'+
                                    '<a  ng-click="search.type = \'\'" tooltips tooltips  tooltip-html="<translate>  All </translate>"   tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top" class="btn withTooltip"  style="padding:0"><i class="fa fa-bars"></i></a>'+
                                '</li>'+
                                '<li ng-if="searchServiceType(\'car\')" ng-class="{\'filter-service-selected\':search.type==\'car\'}" class="keypad-service" >'+
                                    '<a  ng-click="search.type = \'car\'" tooltips tooltips   tooltip-html=" <translate> Car  </translate>"   tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top" class="btn withTooltip" style="padding:0"><i class="fa fa-car"></i></a>'+
                                '</li>'+
                                '<li ng-if="searchServiceType(\'air\')" ng-class="{\'filter-service-selected\':search.type==\'air\'}" class="keypad-service" >'+ 
                                    '<a  ng-click="search.type = \'air\'" tooltips tooltips tooltip-html=" <translate> Air  </translate>"   tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top" class="btn withTooltip" style="padding:0"><i class="fa fa-plane"></i></a>'+
                                '</li>'+
                                '<li ng-if="searchServiceType(\'accomodation\')" ng-class="{\'filter-service-selected\':search.type==\'accomodation\'}" class="keypad-service">'+ 
                                    '<a  ng-click="search.type = \'accomodation\'" tooltips tooltips  tooltip-html="<translate> Accomodation  </translate>"  tooltip-lazy="false" tooltip-show-trigger="mouseover" tooltip-side="top" class="btn withTooltip"  style="padding:0"><i class="fa fa-home"></i></a>'+
                                '</li>'+
                            '</ul>'+
                    '</div>'+
                    '<br>'+

                    '<div class="row-fluid">'+
                       '<div class="portlet box project">'+
                            '<div class="portlet-title">'+
                                '<div class="caption" >'+
                                    '<i class="fa fa-users"></i><translate>Tickets and Services</translate>'+
                                    '<span class="badge badge-info badge-sheet">{{filtered.length}} </span>'+
                                '</div>'+
                                '<div class="tools">'+
                                    '<div class="option" ng-click="$parent.openService(); newServiceType();">'+
                                        '<i class="fa fa-plane"></i>'+
                                        '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                        '<translate ng-show="!panelOpened">Add Service</translate>'+
                                    '</div>'+
                                    '<div class="option" ng-click="$parent.refreshServices()">'+
                                        '<span aria-hidden="true" class="glyphicon glyphicon-refresh" ></span>'+
                                    '</div>'+
                                    '<div class="option" ng-click="servicesTableCollapse = !servicesTableCollapse">'+
                                        '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !servicesTableCollapse, \'glyphicon-chevron-up\': servicesTableCollapse}" class="glyphicon"></span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="servicesTableCollapse">'+
                                '<div class="btn-toolbar">'+
                                    '<loading-message ng-model="sheet" one-time-loading="true"></loading-message>'+
                                    '<loading-message ng-model="sheet.airServices"></loading-message>'+
                                '</div>'+
                                '<div class="table-responsive" ng-show="(sheet.servicesList && sheet.servicesList.length > 0) || sheet.sheetDetails.length > 0">'+
                                    '<table name="tablesServices" class="table-project table table-bordered table-condensed flip-content ">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th translate>Kind</th>'+
                                                '<th translate>Date</th>'+
                                                '<th translate>Description</th>'+
                                                '<th translate>Actions</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                            '<tr ng-repeat="service in filtered = (sheet.servicesList | orderBy:\'date\' | filter:search )" >'+
                                                '<td ng-click="openService(service)">'+
                                                    '{{ service.type | translate | capitalize }}'+
                                                '</td>'+
                                                '<td ng-if="service.type==\'air\'" ng-click="openService(service)">'+
                                                    '{{ service.date}}'+
                                                '</td>'+
                                                '<td  ng-if="service.type==\'accomodation\'" ng-click="openService(service)">'+
                                                    '{{ service.initDate }} -- {{service.endDate}}'+
                                                '</td>'+
                                                '<td  ng-if="service.type==\'car\'" ng-click="openService(service)">'+
                                                    '{{ service.startDate }} -- {{service.endDate}}'+
                                                '</td>'+
                                                '<td  ng-if="service.type==\'air\'" ng-click="openService(service)">'+
                                                    '<strong translate>Origin</strong>: {{service.originLabel}} /'+
                                                    '<strong translate>Destination</strong>: {{service.destinationLabel}}'+
                                                '</td>'+
                                                '<td  ng-if="service.type==\'accomodation\'" ng-click="openService(service)">'+
                                                    '{{service.city}} <span ng-show="service.accomodationType.length > 0">/</span> {{service.accomodationType}}'+
                                                '</td>'+
                                                '<td  ng-if="service.type==\'car\'" ng-click="openService(service)">'+
                                                    '{{service.cityLabel}} / {{service.typesCar}} / {{service.quantity}}'+
                                                '</td>'+
                                                '<td  class="text-center">'+
                                                    '<a href="javascript:;" loading-click="removeService(service)" loading-icon="true" class="btn btn-table red ">'+
                                                        '<span class="glyphicon glyphicon-trash"></span>'+
                                                    '</a>'+
                                                '</td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                        '<tbody ng-show="(sheet.sheetDetails[$index].sheetDetailNotes  | filter: {classification: \'services\'}).length > 0" ng-repeat="sheetDetail in sheet.sheetDetails track by $index">'+
                                          '<tr style="color:#CD5C5C;" class="no-processed-command">'+
                                              '<td colspan="5"><strong>{{sheetDetail.title}}</strong></td>'+    
                                          '</tr>'+
                                          '<tr class="no-processed-command" ng-repeat="sheetDetailNote in sheetDetail.sheetDetailNotes | filter: {classification: \'services\'} track by $index">'+
                                            '<td colspan="5" ng-click="setSheetDetailNote(sheetDetailNote); openService();" style="padding-left:20px;">{{sheetDetailNote.commandText}}</td>'+
                                          '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</div>'+   
                                '<p ng-show="(!sheet.servicesList || sheet.servicesList.length==0) && sheet.sheetDetails.length==0" translate>'+
                                    'There aren\'t any services added yet'+
                                '</p>'+
                            '</div>'+   
                       '</div>'+
                    '</div>'+

                 '</div>',
        controller : function($scope, $element, $attrs, $transclude){
            $scope.search = {};
            $scope.search.type= '';
            $scope.listServiceType = [];

            $scope.$watch('sheet.public', function (newvalue, oldvalue) {
                if(newvalue !=oldvalue){
                    $scope.saveSheet();
                }
            });
            $scope.$watch('sheet.reason', function (newvalue, oldvalue) {
                 if(newvalue != oldvalue && newvalue!='' && newvalue!=null){
                    $scope.saveSheet();
                 }
            });

             $scope.$watch('sheet.servicesList', function (newvalue, oldvalue) {
                if ($scope.sheet.servicesList!=undefined){
                    if ($scope.sheet.servicesList.length>0)
                        $scope.loadServiceType();
                    else
                        $scope.listServiceType = [];

                }

            });

            $scope.newServiceType = function(){
                $scope.confirmServiceNote = SheetDetailNote.$build({});

            };

            $scope.searchServiceType = function(type){
                if ($scope.listServiceType.indexOf(type)==-1)
                    return false;
                else
                    return true;
            };

            $scope.loadServiceType = function(){
                $scope.listServiceType = [];
                for(var i=0; i<$scope.sheet.servicesList.length; i++){
                    var type = $scope.sheet.servicesList[i].type;
                    if ($scope.listServiceType.indexOf(type) == -1) {
                        $scope.listServiceType.push(type);
                    }

                }
            };
    

            $scope.toggleContact = function(_client) {
                $scope.sheet.setContact(_client.id, !_client.isContact).error(
                    function() {Message.error({ 'error': 'El email no puede estar vacío para los clientes contacto.'});}).then(
                    function (){
                      _client.isContact = !_client.isContact;
                    });
            };

            $scope.isMinor = function (birthDate) {
              birthDate = new Date(birthDate);
              var today = new Date();
              today.setFullYear(today.getFullYear() - 18);
              return birthDate > today;
            };

            $scope.removeClient = function (_client) {
              return $scope.sheet.deleteClient(_client.id).then(function () {
                $scope.sheet.clients.$remove(_client);
              });
            };
            $scope.removeService = function(_service){
                
                return _service.$destroy().$then(function() {
                    $scope.$parent.refreshServices();
                    $scope.loadServiceType();
                });
            };
            $scope.saveSheet = function(){
                var sheet_id = $scope.sheet.id;
                $scope.sheet.$save().$then(function(_sheet){
                    $scope.sheet = _sheet;
                    if (sheet_id == undefined){
                      $scope.sheetDetail.sheetId = _sheet.id;
                      $scope.sheetDetail.$save().$then(function(_sheet_detail){
                        $scope.sheet.sheetDetails.$add(_sheet_detail);
                        $scope.$parent.refreshServices();
                        if($scope.$parent.creating){
                            //$state.go('sheet.edit', { "sheetId": _sheet.id});
                        }
                      });
                    }
                    else{
                      if($scope.$parent.creating){
                        //$state.go('sheet.edit', { "sheetId": _sheet.id});
                      }
                    }
                });
            };
            $scope.confirmSheet = function(){
                return $scope.sheet.confirm().then(function(_response){
                    $scope.$parent.sheet.$refresh();
                    MessageUtils.success(gettextCatalog.getString(_response.data.status));
                    
                 });
            };
            $scope.openClient = function(_client){
                $scope.$parent.openClient(_client);
            };
            $scope.openService = function(_service){
                $scope.$parent.openService(_service);
            }
            $scope.setSheetDetailNote = function(_sheetDetailNote){
              if (_sheetDetailNote != undefined){
                if(_sheetDetailNote.classification=='clients') 
                    $scope.confirmClientNote = _sheetDetailNote;
                else 
                    $scope.confirmServiceNote = _sheetDetailNote;
              } else { 
                $scope.confirmClientNote = SheetDetailNote.$build({});
                $scope.confirmServiceNote = SheetDetailNote.$build({});
              }

            };

           
            
        },
        link : function ($scope, $element, $attrs) {
            $element.css({
                          'display':'table-cell',
                          'height': 'inherit',
                          'vertical-align': 'top',
                          'padding-right': '0px'
                        });

        }
    }
});
projectApp.directive('clientPanel', function ($compile, $modal,MessageUtils,gettextCatalog,Client,Phone,CustomerDate,Address,Education,Profession,SocialMedia,Passport,Visa,SpecialCategory,SpecialSubCategory,ClientSpecialComment, HotelOption, PillowType, BedType, BedSheetType, TripStyle, TripGroup, SeatingPreference, SpecialAssistance, HotelService, AirPort, ClientPreferences, ClientLoyaltyProgram, LoyaltyProgram, SheetDetailNote) {
    return {
        restrict: 'E',
        scope :{
            sheet:"=ngSheet",
            client:"=ngClient",
            confirmClientNote:"=ngConfirmClientNote",
            deleteSheetDetailId:"=ngDeleteSheetDetailId",
            deleteSheetDetailNoteId:"=ngDeleteSheetDetailNoteId"
        },
        template:   '<div class="prsht-header">'+
                        '<h4 class="prsht-header-name-client" ng-show="client.id">{{client.firstName}}&nbsp;{{client.middleName}}&nbsp;{{client.lastName}}&nbsp;{{client.secondLastName}}</h4>'+
                        '<script type="text/ng-template" id="customTemplate2.html"><a><span class="action-types" bind-html-unsafe="match.label | typeaheadHighlight:query"></span></a>'+
                        '</script>'+
                        '<div class="input-icon right input-search prsht-header-search" ng-show="!client.id">'+
                            '<i class="fa fa-search"></i>'+
                            '<input class="pull-right input-search form-control" typeahead-input-formatter="this.submitSearch($model)" type="text"'+
                            'ng-model="this.clientSelected" type="text" placeholder="{{\'Search Client\' | translate}}"'+
                            'typeahead="name as ((name.identifier !== null ? name.identifier + \' - \' : \'\') + (name.first_name !== null ? name.first_name : \'\') + \' \'+(name.middle_name !== null ? name.middle_name : \'\') + \' \'+(name.last_name !== null ? name.last_name : \'\')+\' \'+(name.second_last_name !== null ? name.second_last_name : \'\')) for name in this.searchClient($viewValue)"'+
                            'typeahead-loading="loadingLocations" typeahead-template-url="customTemplate2.html">'+
                            ''+
                            '<div ng-show="loadingLocations" class="loading-25 pull-right"></div>'+
                        '</div>'+
                        '<label ng-show="confirmClientNote.id != undefined" class="col-md-12 labelCommand">{{confirmClientNote.commandText}}</label>'+
                        '<label ng-show="clientView.commandText != undefined" class="col-md-12 labelCommand">{{clientView.commandText}}</label>'+
                    '</div>'+
                    '<div class="prsht-panel-menu">'+
                        '<ul>'+
                            '<li class="prsht-detail-nav-item" ng-class="{\'selected\':clientPanelNav==1}">'+
                                '<a ng-click="clientPanelNav = 1 ; activeAndDesactiveEditButtonInTabs(\'personalInformation\') "><i class="fa fa-user"></i></a>'+
                            '</li>'+
                            '<li class="prsht-detail-nav-item" ng-class="{\'selected\':clientPanelNav==2, \'disabled\':client.id==undefined}">'+
                                '<a ng-click="setTabVerticalClientCreated(2)"><i class="fa fa-book"></i></a>'+
                            '</li>'+
                            '<li class="prsht-detail-nav-item" ng-class="{\'selected\':clientPanelNav==3, \'disabled\':client.id==undefined}">'+
                                '<a ng-click="setTabVerticalClientCreated(3)"><i class="fa fa-thumbs-o-up"></i></a>'+
                            '</li>'+
                            '<li class="prsht-detail-nav-item" ng-class="{\'selected\':clientPanelNav==4, \'disabled\':client.id==undefined}">'+
                                '<a ng-click="setTabVerticalClientCreated(4)"><i class="fa fa-pencil-square-o"></i></a>'+
                            '</li>'+
                            '<li class="prsht-detail-nav-item" ng-class="{\'selected\':clientPanelNav==5, \'disabled\':client.id==undefined}">'+
                                '<a ng-click="setTabVerticalClientCreated(5)"><i class="fa fa-history"></i></a>'+
                            '</li>'+
                        '</ul>'+
                    '</div>'+
                    '<div class="prsht-container-nav">'+
                        '<div class="prsht-nav-1" ng-show="clientPanelNav==1" >'+
                            '<tabset>'+
                                '<tab ng-click="activeAndDesactiveEditButtonInTabs(\'personalInformation\')">'+
                                    '<tab-heading>'+
                                        '<i class="fa fa-user"></i><span translate class="hidden">Personal details</span>'+
                                    '</tab-heading>'+
                                    '<div class="row-fluid ">'+
                                        '<form name="client_personalInformation" id="client_personalInformation" >'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Personal Information</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                        '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;"'+ 
                                            'ng-click="enabledEditFields()"'+ 
                                            'ng-show="showButtonEdit()"'+
                                            'translate>Edit</label>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"  translate>CI</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.identifier}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" ng-change="changePersonalInformation()" class="form-control" placeholder="{{ \'Identifier\' | translate }}" ng-model="clientView.identifier"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group" ng-class="{\'has-error\':client_personalInformation.email.$error.email}">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Email</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.email}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="email" name="email"  required=""  ng-change="changePersonalInformation()" class="form-control" placeholder="{{ \'Email\' | translate }}" ng-model="clientView.email"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group" ng-class="{\'has-error\':client_personalInformation.firstName.$error.required && client_personalInformation.firstName.$dirty}">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>First Name</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.firstName}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" name="firstName"  required=""  class="form-control" ng-change="changePersonalInformation()" placeholder="{{ \'First Name\' | translate }}" ng-model="clientView.firstName">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Middle Name</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.middleName}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" class="form-control" ng-change="changePersonalInformation()" placeholder="{{ \'Middle Name\' | translate }}" ng-model="clientView.middleName">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group" ng-class="{\'has-error\':client_personalInformation.lastName.$error.required && client_personalInformation.lastName.$dirty}">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Last Name</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.lastName}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" name="lastName"   required="" class="form-control" ng-change="changePersonalInformation()" placeholder="{{ \'Last Name\' | translate }}" ng-model="clientView.lastName">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Second Last Name </label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.secondLastName}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" class="form-control" ng-change="changePersonalInformation()" placeholder="{{ \'Second Last Name\' | translate }}" ng-model="clientView.secondLastName">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Birth date</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.birthDate}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<div sb-date-select sb-select-class="form-control " ng-change="changePersonalInformation()" class="prsht-sb-date-select" ng-model="clientView.birthDate"></div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Sex</label>'+
                                                '<div class="col-md-12">'+
                                                    '<div class="radio-list">'+
                                                        '<label class="radio-inline">'+
                                                            '<div class="radio" id="uniform-optionsRadios25">'+
                                                                '<span ng-class="{\'checked\':clientView.sex==\'M\'}">'+
                                                                    '<input ng-disabled="showButtonEdit()" type="radio" ng-click="clientView.sex=\'M\'; changePersonalInformation(); "   value="M" ></span>'+
                                                            '</div> <translate>Male</translate> '+
                                                        '</label>'+
                                                        '<label class="radio-inline">'+
                                                            '<div class="radio" id="uniform-optionsRadios26">'+
                                                                '<span ng-class="{\'checked\':clientView.sex==\'F\'}">'+
                                                                    '<input ng-disabled="showButtonEdit()" type="radio" ng-click="clientView.sex=\'F\'; changePersonalInformation();"  value="F" ></span>'+
                                                            '</div> <translate>Female</translate> '+
                                                        '</label>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 cont-buttons-client prsht-row-button"><!--{{client_personalInformation}} {{client_personalInformation.$valid}}-->'+
                                                '<button ng-show="!showButtonEdit()" type="button" class="btn blue pull-right" ng-class="{\'disabled\':(!client_personalInformation.$valid  || !clientEditing_personalInformation)}"  loading-click="savePersonalInformation(client_personalInformation)" ><translate ng-show="client.id!=undefined">Save and add to trip</translate><translate ng-show="client.id==undefined">Save and add to trip</translate></button> '+
                                                '<button ng-show="!showButtonEdit()" class="btn default pull-right" ng-click="undoPersonalBasicDetails()" translate>Cancel</button>'+
                                        '</div>'+
                                        '</form>'+
                                    '</div>'+
                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Phone numbers</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button ng-class="{\'disabled\':client.id==undefined}" class="btn blue icons pull-right" ng-click="openPhone()"><i class="fa fa-phone-square"></i><translate>Add number</translate></button> '+
                                        '</div>'+
                                        '<div class="col-md-12" ng-show="client.phones.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no phone numbers</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="client.phones.length>0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th translate>Number</th>'+
                                                        '<th translate>Type</th>'+
                                                        '<th translate>Country</th>'+
                                                        '<th translate>Actions</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="phone in client.phones">'+
                                                        '<td ng-click="openPhone(phone)">{{phone.country.dialCode}} {{phone.number}}</td>'+
                                                        '<td ng-click="openPhone(phone)">{{phone.type}}</td>'+
                                                        '<td ng-click="openPhone(phone)">{{phone.country.name | translate }}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removePhone(phone)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Customer Dates</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button ng-class="{\'disabled\':client.id==undefined}" class="btn blue icons pull-right" ng-click="openCustomerDate()"><i class="fa fa-calendar"></i><translate>Add date</translate></button> '+
                                        '</div>'+
                                        '<div class="col-md-12" ng-show="client.customerDates.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no customer dates</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="client.customerDates.length>0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th translate>Type</th>'+
                                                        '<th translate>Date</th>'+
                                                        '<th translate>Actions</th>'+
                                                        '<th ></th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="customer_dates in client.customerDates">'+
                                                        '<td ng-click="openCustomerDate(customer_dates)">{{customer_dates.typeDate}}</td>'+
                                                        '<td ng-click="openCustomerDate(customer_dates)">{{customer_dates.date}}</td>'+
                                                        '<td ng-click="openCustomerDate(customer_dates)">{{getActionTypesString(customer_dates.actionTypes)}}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removeCustomerDate(customer_dates)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+

                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Addresses</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button ng-class="{\'disabled\':client.id==undefined}"  class="btn blue icons pull-right" ng-click="openAddress()"><i class="fa fa-building-o"></i><translate>Add address</translate></button> '+
                                        '</div>'+
                                        '<div class="col-md-12" ng-show="client.addresses.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no address</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="client.addresses.length>0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th translate>Address</th>'+
                                                        '<th translate>City</th>'+
                                                        '<th translate>Country</th>'+
                                                        '<th translate>Actions</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="address in client.addresses">'+
                                                        '<td ng-click="openAddress(address)">{{address.address1}}</td>'+
                                                        '<td ng-click="openAddress(address)">{{address.city}}</td>'+
                                                        '<td ng-click="openAddress(address)">{{address.country.name}}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removeAddress(address)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                '</tab>'+
                                '<tab disabled="client.id==undefined" ng-click="setTabClientCreated(); activeAndDesactiveEditButtonInTabs(\'professionalInformation\')">'+
                                    '<tab-heading>'+
                                        '<i class="fa fa-mortar-board"></i><span translate class="hidden">Professional Information</span>'+
                                    '</tab-heading>'+
                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Professional Information</translate></h4>'+
                                        '</div>'+
                                          '<div class="col-md-12">'+
                                        '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;"'+ 
                                            'ng-click="enabledEditFields()"'+ 
                                            'ng-show="showButtonEdit()"'+
                                            'translate>Edit</label>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"  translate>Education Level</label>'+
                                                '<div class="col-md-12">'+
                                                    '<select ng-disabled="showButtonEdit()" class="form-control" ng-change="changeProfesionalDetails()" ng-model="clientView.education.id" ng-options="education.id as education.level  for education in educations"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"  translate>Profession</label>'+
                                                '<div class="col-md-12">'+
                                                    '<select ng-disabled="showButtonEdit()" class="form-control" ng-change="changeProfesionalDetails()" ng-model="clientView.profession.id" ng-options="profession.id as profession.name  for profession in professions"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Company</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.company}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" class="form-control" ng-change="changeProfesionalDetails()"  ng-model="clientView.company">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Office</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.office}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" class="form-control"  ng-change="changeProfesionalDetails()" ng-model="clientView.office">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht"  translate>Work Phone</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.telephoneOffice}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" class="form-control "ng-change="changeProfesionalDetails()"  ng-model="clientView.telephoneOffice" numbers-only="numbers-only" maxlength="12" model-view-value="true">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-6 prsht-row-form">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-12 control-label lb-client-bg-wht" translate>Work Address</label>'+
                                                '<label class="col-md-12 control-label "  ng-show="showButtonEdit()">{{clientView.workingAddress}}</label>'+
                                                '<div class="col-md-12" ng-show="!showButtonEdit()">'+
                                                    '<input type="text" class="form-control" ng-change="changeProfesionalDetails()"  ng-model="clientView.workingAddress">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 cont-buttons-client prsht-row-button">'+
                                                '<button class="btn blue pull-right " ng-class="{\'disabled\':!clientEditing_profesionalDetails}"  loading-click="saveProfesionalDetails()" ng-show="!showButtonEdit()" translate>Save</button> '+
                                                '<button class="btn default pull-right"  ng-click="undoProfesionalDetails()" ng-show="!showButtonEdit()" translate>Cancel</button>'+
                                        '</div>'+
                                    '</div>'+
                                '</tab>'+

                                '<tab disabled="client.id==undefined" ng-click="setTabClientCreated(); activeAndDesactiveEditButtonInTabs(\'socialMedia\')">'+
                                    '<tab-heading>'+
                                        '<i class="fa fa-share-alt"></i><span translate class="hidden">Social Media</span>'+
                                    '</tab-heading>'+
                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Social Media</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button class="btn blue icons pull-right" ng-click="openSocialMedia()"><i class="fa fa-share-alt"></i><span class="icon-plus second"></span><translate>Add</translate></button> '+
                                        '</div>'+
                                         '<div class="col-md-12" ng-show="client.socialMedias.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no social medias</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="client.socialMedias.length > 0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th translate>Social</th>'+
                                                        '<th translate>Type Social</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="social_media in client.socialMedias">'+
                                                        '<td ng-click="openSocialMedia(social_media)">{{social_media.name}}</td>'+
                                                        '<td ng-click="openSocialMedia(social_media)">{{social_media.url}}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removeSocialMedia(social_media)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                '</tab>'+

                                '<tab disabled="client.id==undefined" ng-click="setTabClientCreated() ; activeAndDesactiveEditButtonInTabs(\'specialInformation\')">'+
                                    '<tab-heading>'+
                                        '<i class="fa fa-ambulance"></i><span translate class="hidden">Social Media</span>'+
                                    '</tab-heading>'+
                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Allergies</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button class="btn blue icons pull-right" ng-click="openAllergy()"><i class="fa fa-medkit"></i><span class="icon-plus second"></span><translate>Add</translate></button> '+
                                        '</div>'+
                                        '<div class="col-md-12" ng-show="allergiesClient.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no disabilities</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="allergiesClient.length>0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th translate>Type</th>'+
                                                        '<th translate>Detail</th>'+
                                                        '<th translate>Comment</th>'+
                                                        '<th translate>Actions</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="allergy in allergiesClient">'+
                                                        '<td ng-click="openAllergy(allergy)">{{allergy.specialSubCategoryDetail.specialSubCategory.name | translate}}</td>'+
                                                        '<td ng-click="openAllergy(allergy)">{{allergy.specialSubCategoryDetail.name | translate}}</td>'+
                                                        '<td ng-click="openAllergy(allergy)">{{allergy.comment}}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removeAllergy(allergy)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                    '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Disabilities</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button class="btn blue icons pull-right" ng-click="openDisability()"><i class="fa fa-wheelchair"></i><span class="icon-plus second"></span><translate>Add</translate></button> '+
                                        '</div>'+
                                        '<div class="col-md-12" ng-show="disabilitiesClient.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no disabilities</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="disabilitiesClient.length > 0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th translate>Type</th>'+
                                                        '<th translate>Detail</th>'+
                                                        '<th translate>Comment</th>'+
                                                        '<th translate>Actions</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="disability in disabilitiesClient">'+
                                                        '<td ng-click="openDisability(disability)">{{disability.specialSubCategoryDetail.specialSubCategory.name | translate}}</td>'+
                                                        '<td ng-click="openDisability(disability)">{{disability.specialSubCategoryDetail.name | translate}}</td>'+
                                                        '<td ng-click="openDisability(disability)">{{disability.comment}}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removeDisability(disability)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                '</tab>'+   
                            '</tabset>'+    
                        '</div>'+
                        '<div class="prsht-nav-2" ng-show="clientPanelNav==2">'+
                            '<div class="row-fluid ">'+
                                '<div class="prsht-title-separator">'+
                                    '<h4><translate>Passport</translate></h4>'+
                                '</div>'+
                                    '<div class="col-md-12">'+
                                        '<button class="btn blue icons pull-right" ng-click="openPassport()"><i class="fa fa-book"></i><span class="icon-plus second"></span><translate>Add</translate></button> '+
                                    '</div>'+
                                '<div class="col-md-12" ng-show="client.passports.length==0">'+
                                        '<span class="prsth-msg-not-items" translate>There are no passports</span>'+
                                '</div>'+
                                '<div class="col-md-12 prsht-container-table" ng-show="client.passports.length > 0">'+
                                    '<table class="aui">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th translate>Name</th>'+
                                                '<th translate>Number</th>'+
                                                '<th translate>Date</th>'+
                                                '<th translate>Country</th>'+
                                                '<th translate>Actions</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                            '<tr ng-repeat="passport in client.passports">'+
                                                '<td ng-click="openPassport(passport)">{{passport.fullName}}</td>'+
                                                '<td ng-click="openPassport(passport)">{{passport.passportNumber}}</td>'+
                                                '<td ng-click="openPassport(passport)">{{passport.expirationDate}}</td>'+
                                                '<td ng-click="openPassport(passport)">{{passport.country.name}}</td>'+
                                                '<td class="text-center">'+
                                                    '<a href="javascript:;" loading-click="removePassport(passport)" loading-icon="true" class="btn btn-table red ">'+
                                                        '<span class="glyphicon glyphicon-trash"></span>'+
                                                    '</a>'+
                                                '</td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</div>'+
                            '</div>'+

                            '<div class="row-fluid ">'+
                                '<div class="prsht-title-separator">'+
                                    '<h4><translate>Visa</translate></h4>'+
                                '</div>'+
                                    '<div class="col-md-12">'+
                                        '<button class="btn blue icons pull-right" ng-click="openVisa()"><i class="fa fa-book"></i><span class="icon-plus second"></span><translate>Add</translate></button> '+
                                    '</div>'+
                                '<div class="col-md-12" ng-show="client.visas.length==0">'+
                                        '<span class="prsth-msg-not-items" translate>There are no visas</span>'+
                                '</div>'+
                                '<div class="col-md-12 prsht-container-table"  ng-show="client.visas.length > 0">'+
                                    '<table class="aui">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th translate>Name</th>'+
                                                '<th translate>Number</th>'+
                                                '<th translate>Date</th>'+
                                                '<th translate>Country</th>'+
                                                '<th translate>Actions</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                            '<tr ng-repeat="visa in client.visas">'+
                                                '<td ng-click="openVisa(visa)">{{visa.fullName}}</td>'+
                                                '<td ng-click="openVisa(visa)">{{visa.visaNumber}}</td>'+
                                                '<td ng-click="openVisa(visa)">{{visa.expirationDate}}</td>'+
                                                '<td ng-click="openVisa(visa)">{{visa.country.name}}</td>'+
                                                '<td class="text-center">'+
                                                    '<a href="javascript:;" loading-click="removeVisa(visa)" loading-icon="true" class="btn btn-table red ">'+
                                                        '<span class="glyphicon glyphicon-trash"></span>'+
                                                    '</a>'+
                                                '</td>'+
                                            '</tr>'+
                                        '</tbody>'+
                                    '</table>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="prsht-nav-3" ng-show="clientPanelNav==3">'+
                        '<tabset>'+
                        '<tab ng-click="activeAndDesactiveEditButtonInTabs(\'interests\')" class="tab-hotel-preferences">'+
                        '<tab-heading>'+
                            '<i class="fa fa-user"></i><span translate class="hidden">Interests</span>'+
                        '</tab-heading>'+
                        '<h4 class="title-section" translate>What do you like?</h4>'+
                        '<div class="choices">'+
                          '<div class="btn blue" ng-repeat="tripStyle in tripStyleOptions.tripStyles" ng-click="manageStyle($index)" ng-class="{\'no-active\': !inStyles(tripStyle.id)}">'+
                            '<i class="fa" ng-class="inStyles(tripStyle.id) ? \'fa-check-square-o\' : \'fa-square-o\'" fa-check-square-o></i>'+
                            '{{tripStyle.name | translate }}'+      
                          '</div>'+
                        '</div>'+
                        
                        '<h4 class="title-section" translate>With whom you share?</h4>'+
                        '<div class="choices">'+
                          '<div class="btn blue" ng-repeat="tripGroup in tripStyleOptions.tripGroups" ng-click="manageGroup($index)" ng-class="{\'no-active\' : !inGroups(tripGroup.id)}">'+
                            '<i class="fa" ng-class="inGroups(tripGroup.id) ? \'fa-check-square-o\' : \'fa-square-o\'" fa-check-square-o></i>'+
                            '{{tripGroup.name | translate}}'+
                          '</div>'+
                        '</div>'+
                       
                      '</tab>'+
                      
                      '<tab ng-click="activeAndDesactiveEditButtonInTabs(\'hotelPreferences\')" class="tab-hotel-preferences">'+
                        '<tab-heading>'+
                          '<i class="fa fa-h-square"></i><span translate class="hidden">Hotel</span>'+
                       ' </tab-heading>'+
                        
                    '<h4 class="title-section" translate>General Information</h4>'+

                    '<div class="row">'+
                          '<div class="col-md-12">'+
                            '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;" '+
                            'ng-click="enabledEditFields()"' +
                            'ng-show="showButtonEdit()"'+
                            'translate>Edit</label>'+
                          '</div>'+
                        '<div class="col-md-12">'+
                            '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Smoker</label>'+
                            '<div class="col-md-7">'+
                              '<input type="checkbox" ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.smoker">'+
                          '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Hotel Stars</label>'+
                        '<div class="col-md-7 hotel-stars" ng-show="!showButtonEdit()">'+
                            '<span style="margin-right:10px;" ng-class="{\'selected\':tempData.starHover==0}" ng-mouseover="tempData.starHover=0" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=0" class="glyphicon glyphicon-ban-circle"></span>'+

                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=1,\'glyphicon-star-empty\':(tempData.starHover<1 || !clientView.clientPreferences.stars) }" ng-mouseover="tempData.starHover=1" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=1" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=2,\'glyphicon-star-empty\':(tempData.starHover<2 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=2" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=2" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=3,\'glyphicon-star-empty\':(tempData.starHover<3 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=3" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=3" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=4,\'glyphicon-star-empty\':(tempData.starHover<4 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=4" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=4" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=5,\'glyphicon-star-empty\':(tempData.starHover<5 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=5" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=5" class="glyphicon "></span>'+
                        '</div>'+

                        ' <div class="col-md-7 hotel-stars" ng-show="showButtonEdit()">'+
                            '<span style="margin-right:10px;" ng-class="{\'selected\':tempData.starHover==0}" class="glyphicon glyphicon-ban-circle"></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=1,\'glyphicon-star-empty\':(tempData.starHover<1 || !clientView.clientPreferences.stars) }" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=2,\'glyphicon-star-empty\':(tempData.starHover<2 || !clientView.clientPreferences.stars)}"  class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=3,\'glyphicon-star-empty\':(tempData.starHover<3 || !clientView.clientPreferences.stars)}"  class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=4,\'glyphicon-star-empty\':(tempData.starHover<4 || !clientView.clientPreferences.stars)}" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=5,\'glyphicon-star-empty\':(tempData.starHover<5 || !clientView.clientPreferences.stars)}" class="glyphicon "></span>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                      '<div class="form-group">'+
                        '<label class="col-md-12 control-label lb-client-bg-wht" ><translate>Facilities and Services</translate>:</label>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-9">'+
                      '<label class="tag-various" ng-repeat="hotel_service_tag in tempData.arrayFacilities">{{hotel_service_tag.name | translate}}</label>'+
                      '<label ng-class="{\'text-center\':tempData.arrayFacilities.length>0,\'text-left\':tempData.arrayFacilities.length==0}" class="label-edit-facilitiesAndService" ng-show="!tempData.editingFacilities" href="javascript:;" ><span ng-disabled="showButtonEdit()" ng-click="editFacilitiesAndServices()" class="btn btn-default"><i class="fa fa-edit"></i> <translate>Edit</translate></span> '+
                    '  </label>'+
                    '</div>'+
                    '<div class="col-md-12" style="margin-bottom: 15px;">'+
                        '<div class="form-group">'+
                            '<div class="col-md-12">'+
                               '<div ng-show="tempData.editingFacilities" class="container-facilities-and-services">'+
                                    '<div class="col-md-6" ng-repeat="hotel_service in hotel_services">'+
                                        '<label>'+
                                          '<input type="checkbox" checklist-model="tempData.arrayFacilities" checklist-value="hotel_service"> {{hotel_service.name | translate}}'+
                                      '</label>'+
                                  '</div>'+
                                  '<div class="col-md-12 cont-buttons-client">'+
                                      '<div class="col-md-12">'+
                                        '<button class="btn blue pull-right " ng-click="closeClientPreferenceFacilitiesAndServices()" translate>Close</button>'+
                                        '<button class="btn default pull-right" ng-click="undoClientPreferenceFacilitiesAndServices()"  translate>Cancel</button>'+
                                    '</div>'+
                               ' </div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Bed Type</label>'+
                        '<div class="col-md-7">'+
                          '<select ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.bedTypeId" class="form-control" ng-options="bed.id as bed.name | translate for bed in bed_types"></select>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Bed Sheets</label>'+
                        '<div class="col-md-7">'+
                         '<select ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.bedSheetTypeId" class="form-control" ng-options="sheet.id as sheet.name | translate for sheet in bed_sheet_types"></select>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-12">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Pillow Type</label>'+
                        '<div class="col-md-7">'+
                          '<select ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.pillowTypeId" class="form-control" ng-options="pillow.id as pillow.name | translate for pillow in pillow_types"></select>'+
                      '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div ng-show="!showButtonEdit()" class="col-md-12 cont-buttons-client" style="border:none;">'+
                      '<div class="col-md-12">'+
                        '<button class="btn blue pull-right " loading-click="saveClientPreference()" translate>Save</button>'+
                        '<button class="btn default pull-right" ng-click="undoClientPreference()"  translate>Cancel</button>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="row-fluid" style="overflow:auto; margin-bottom:10px;">'+
                        '<div class="portlet box gray">'+
                            '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="hotelOptionCollapse" >'+
                              '<div class="row-fluid ">'+
                                        '<div class="prsht-title-separator">'+
                                            '<h4><translate>Prefered Hotels</translate></h4>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<button ng-class="{\'disabled\':client.id==undefined}" class="btn blue icons pull-right" ng-click="openHotel()"><i class="fa fa-h-square"></i><translate>Add Prefered Hotel</translate></button> '+
                                        '</div>'+
                                        '<div class="col-md-12" ng-show="client.hotelOptions.length==0">'+
                                            '<span class="prsth-msg-not-items" translate>There are no prefered hotels</span>'+
                                        '</div>'+
                                        '<div class="col-md-12 prsht-container-table" ng-show="client.hotelOptions.length>0">'+
                                            '<table class="aui">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                      ' <th translate>Hotel Name</th>'+
                                                        '<th translate>Trip Type</th>'+
                                                        '<th translate>Room Type</th>'+
                                                        '<th translate>Comments</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody>'+
                                                    '<tr ng-repeat="hotel_option in client.hotelOptions">'+
                                                        '<td ng-click="openHotel(hotel_option)">{{hotel_option.name}}</td>'+
                                                        '<td ng-click="openHotel(hotel_option)">{{hotel_option.tripType.name | translate}}</td>'+
                                                        '<td ng-click="openHotel(hotel_option)">{{hotel_option.roomType.name | translate}}</td>'+
                                                        '<td ng-click="openHotel(hotel_option)">{{hotel_option.comments}}</td>'+
                                                        '<td class="text-center">'+
                                                            '<a href="javascript:;" loading-click="removeHotel(hotel_option)" loading-icon="true" class="btn btn-table red ">'+
                                                                '<span class="glyphicon glyphicon-trash"></span>'+
                                                            '</a>'+
                                                        '</td>'+
                                                    '</tr>'+
                                                '</tbody>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+   
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '</tab>'+
                    '<tab ng-click="activeAndDesactiveEditButtonInTabs(\'flyPreferences\')" class="tab-hotel-preferences">'+
                        '<tab-heading>'+
                           '<i class="fa fa-plane"></i><span translate class="hidden">Flight</span>'+
                        '</tab-heading>'+
                        '<h4 class="title-section" translate>Flight Information</h4>'+
                    
                        '<div class="col-md-12">'+
                          '<div class="form-group">'+
                            '<div class="col-md-12">'+
                             '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;"'+ 
                                'ng-click="enabledEditFields()" '+
                                'ng-show="showButtonEdit()"'+
                                'translate>Edit</label>'+
                            '</div>'+
                            '<label class="col-md-4 control-label lb-client-bg-wht" translate>Home Airport</label>'+
                            '<label class="col-md-8 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{tempData.labelOrigin}}</label>'+
                            '<div class="col-md-8" ng-show="!showButtonEdit()">'+
                                '<div class="input-icon right input-search-airPort">'+
                                    '<i class="fa fa-search"></i>'+
                                    '<script type="text/ng-template" id="customTemplateAirPort.html">'+
                                        '<a><span bind-html-unsafe="match.label | typeaheadHighlight:query"></span></a>'+
                                    '</script>'+
                                    '<input typeahead-ng-model class=" form-control"  typeahead-input-formatter="submitSearchOrigin($model)" type="text" ng-model="airportPreference" placeholder="{{tempData.labelOrigin}}" typeahead="airport as (airport.city+\',  \'+airport.countryName+\' - \'+airport.name+\'(\'+airport.fs+\')\')    for airport in searchAirport($viewValue)" typeahead-loading="loadingLocations" class="form-control" typeahead-template-url="customTemplateAirPort.html">'+
                                    '<i ng-show="loadingLocations" class="glyphicon glyphicon-refresh pull-right"></i>'+
                                '</div>'+
                                '<div form-input form-error="service.errors.origin" ng-class="{\'has-error\': service.errors.origin}">'+
                                '</div>'+
                            '</div>'+
                            '<br>'+
                         '</div>'+
                        '</div>'+
                        '<br>'+
                    '<div class="col-md-12">'+
                      '<div class="form-group">'+
                        '<label class="col-md-4 control-label lb-client-bg-wht" translate>Seating Preference</label>'+
                        '<div class="col-md-8">'+
                          '<select ng-change="changeClientFlyPreferences()" ng-disabled="showButtonEdit()" class="form-control" ng-model="clientView.clientPreferences.seatingPreferenceId" ng-options="seating.id as seating.name | translate for seating in seating_preferences"></select>'+
                      '</div>'+
                     '</div>'+
                     '</div>'+
                     '<br>'+
                      '<div class="col-md-12">'+
                      '<div class="form-group">'+
                        '<label class="col-md-4 control-label lb-client-bg-wht" translate>Special Assistance</label>'+
                        '<div class="col-md-8">'+
                          '<select ng-change="changeClientFlyPreferences()" ng-disabled="showButtonEdit()"  class="form-control" ng-model="clientView.clientPreferences.specialAssistanceId" ng-options="special.id as special.name | translate for special in special_assistances"></select>'+
                      '</div>'+
                    '</div>'+
                    '</div>' +
                    '<div ng-show="!showButtonEdit()" class="col-md-12 cont-buttons-client" style="border:none;">'+
                      '<div class="col-md-12">'+
                        '<button class="btn blue pull-right " ng-class="{\'disabled\':(!clientEditing_flyPreferences)}" loading-click="saveClientFlyPreference()" translate>Save</button>'+
                        '<button class="btn default pull-right" ng-click="undoClientFlyPreference()"  translate>Cancel</button>'+
                    '</div>'+
                    '</div>'+
                    '</tab>'+
                    '</tabset>'+
                    '</div>'+
                    '<div class="prsht-nav-4" ng-show="clientPanelNav==4">'+
                    '  <tabset>'+
                    '    <tab>'+
                    '      <tab-heading>'+
                    '        <i class="fa fa-pencil-square-o"></i>'+
                    '      </tab-heading>'+
                    '      <div class="row-fluid ">'+
                    '        <div class="prsht-title-separator">'+
                    '          <h4><translate>Loyalty Programs</translate></h4>'+
                    '        </div>'+
                    '        <div class="col-md-12">'+
                    '          <button class="btn blue icons pull-right" ng-click="openLoyaltyProgram()"><i class="fa fa-pencil-square-o"></i><span class="icon-plus second"></span><translate>Add</translate></button> '+
                    '        </div>'+
                    '        <div class="col-md-12" ng-show="client.clientLoyaltyPrograms.length==0">'+
                    '          <span class="prsth-msg-not-items" translate>There are no loyalty programs</span>'+
                    '        </div>'+
                    '        <div class="col-md-12 prsht-container-table" ng-show="client.clientLoyaltyPrograms.length>0">'+
                    '          <table class="aui">'+
                    '            <thead>'+
                    '              <tr>'+
                    '                <th translate>Name</th>'+
                    '                <th translate>Number</th>'+
                    '                <th translate>Actions</th>'+
                    '              </tr>'+
                    '            </thead>'+
                    '            <tbody>'+
                    '              <tr ng-repeat="result in client.clientLoyaltyPrograms" >'+
                    '                <td ng-click="openLoyaltyProgram(result)">{{result.loyaltyProgram.name}}</td>'+
                    '                <td ng-click="openLoyaltyProgram(result)">{{result.number}}</td>'+
                    '                <td class="text-center">'+
                    '                  <a href="javascript:;" loading-click="removeClientLoyaltyProgram(result)" loading-icon="true" class="btn btn-table red ">'+
                    '                    <span class="glyphicon glyphicon-trash">'+
                    '                    </span>'+
                    '                  </a>'+
                    '                </td>'+
                    '              </tr>'+
                    '            </tbody>'+
                    '          </table>'+
                    '        </div>'+
                    '      </div>'+
                    '    </tab>'+
                    '  </tabset>'+
                    '</div>'+
                    '<div class="prsht-nav-5" ng-show="clientPanelNav==5">'+
                    '  <tabset>'+
                    '    <tab class="tab-hotel-preferences">'+
                    '      <tab-heading>'+
                    '        <i class="fa fa-user"></i><span translate>Travels</span>'+
                    '      </tab-heading>'+
                    '      <div class="row-fluid">'+
                    '        <div class="col-md-12" ng-show="client.sheets2.length==0">'+
                    '          <span class="prsth-msg-not-items" translate>There are no trips</span>'+
                    '        </div>'+
                    '          <div class="col-md-12 prsht-container-table" ng-show="client.sheets2.length > 0">'+
                    '            <table class="aui">'+
                    '              <thead>'+
                    '                <tr>'+
                    '                  <th translate>ID</th>'+
                    '                  <th translate>Name</th>'+
                    '                  <th translate>Persons</th>'+
                    '                  <th translate>Status</th>'+
                    '                  <th translate>Begin At</th>'+
                    '                  <th translate>Ends At</th>'+
                    '                </tr>'+
                    '              </thead>'+
                    '              <tbody>'+
                    '                <tr ng-repeat="sheet in client.sheets2" ng-click="openHistory(sheet.id)">'+
                    '                  <td>{{sheet.ref}}</td>'+
                    '                  <td>{{sheet.title}}</td>'+
                    '                  <td>{{sheet.clients_count}}</td>'+
                    '                  <td>{{sheet.status}}</td>'+
                    '                  <td>{{sheet.begins_at}}</td>'+
                    '                  <td>{{sheet.ends_at}}</td>'+
                    '                </tr>'+
                    '              </tbody>'+
                    '            </table>'+
                    '          </div>'+
                    '      </div>'+
                    '    </tab>'+
                    '  </tabset>'+
                    '</div>',
        controller : function($scope, $element, $attrs){
            $scope.setTabClientCreated = function(){
                if($scope.client.id==undefined){
                    $scope.$parent.addAlert("You have not registered the minimum fields: email , first name, last name.","warning",true,true);
                }
            };
            $scope.setTabVerticalClientCreated = function(option){
                if($scope.client.id==undefined){
                    $scope.$parent.addAlert("You have not registered the minimum fields: email , first name, last name.","warning",true,true);
                }else{
                    $scope.clientPanelNav=option;
                }
            };
            $scope.isClientNew = ($scope.client.id==undefined);


/* START FUNCTIONALITY FOR EDIT BUTTON*/
            
            $scope.tabPrevious = 'personalInformation';
            $scope.editButton= true;

            $scope.enabledEditFields = function(){
                $scope.editButton= false;
            };

            $scope.showButtonEdit = function(){
                return $scope.editButton;
            };

            $scope.activeAndDesactiveEditButtonInTabs = function(val){
                
                $scope.tabPrevious = val;
                
                if ($scope.tabPrevious!=='personalInformation'){
                    $scope.undoPersonalBasicDetails();
                }

                if ($scope.tabPrevious!=='professionalInformation'){
                    $scope.undoProfesionalDetails();
                }
                
                if ($scope.tabPrevious!=='hotelPreferences'){
                    $scope.undoClientPreference();
                    $scope.undoClientPreferenceFacilitiesAndServices();
                }
                if ($scope.tabPrevious!=='flyPreferences'){
                    $scope.undoClientFlyPreference();
                }

                    $scope.editButton = true;

            };

/* END FUNCTIONALITY FOR EDIT BUTTON*/


/*Bootstrap alert END*/       
        
            $scope.isClientSelected = false;
            $scope.clientView = angular.copy($scope.client);

            $scope.tempData={
                starHover:$scope.clientView.clientPreferences.stars==undefined ? 0 : angular.copy($scope.clientView.clientPreferences.stars),
                editingFacilities:false,
                arrayFacilities:$scope.clientView.clientPreferences.id==undefined ? [] : angular.copy($scope.clientView.clientPreferences.hotelServices),
                labelOrigin:$scope.clientView.clientPreferences.id==undefined ? [] : angular.copy($scope.clientView.clientPreferences.originLabel),
                airPortOriginId:$scope.clientView.clientPreferences.origin==undefined ? [] : angular.copy($scope.clientView.clientPreferences.origin),
                airPortOriginLabel:$scope.clientView.clientPreferences.originLabel==undefined ? [] : angular.copy($scope.clientView.clientPreferences.originLabel),
            };

            $scope.clientView.clientPreferences.stars = $scope.tempData.starHover;

           
            $scope.pillow_types = PillowType.$search({});
            $scope.bed_types    = BedType.$search({});
            $scope.bed_sheet_types = BedSheetType.$search({});
            $scope.special_assistances    = SpecialAssistance.$search({});
            $scope.seating_preferences = SeatingPreference.$search({});
            $scope.educations = Education.$search({});
            $scope.professions = Profession.$search({});
            $scope.hotel_services = HotelService.$search({});

            SpecialCategory.$search({}).$then(function(_categories){
              $scope.catAllergy = _categories[0].id;
              $scope.catDisability = _categories[1].id;
            $scope.allergiesClient = ClientSpecialComment.$search({
                client_id: $scope.client.id, category_id: $scope.catAllergy
            });
            $scope.disabilitiesClient = ClientSpecialComment.$search({
                client_id: $scope.client.id, category_id: $scope.catDisability
            });
            $scope.subAllergyCategories =  SpecialSubCategory.$search({ category_id: $scope.catAllergy });
            $scope.subDisabilityCategories =  SpecialSubCategory.$search({ category_id: $scope.catDisability });
           });


            //monitoreo el valor del id del cliente   
            $scope.$watch('client.id', function() {
                if($scope.client.id==undefined){
                    $scope.editButton = false;
                } else {
                    if ($scope.isClientSelected && $scope.tabPrevious==='personalInformation') {
                        $scope.editButton = false;
                    } else {
                        $scope.editButton = true;
                    }
                }
                  
            }, true);

            //monitoreo el valor del tabPrevious 
            $scope.$watch('tabPrevious', function() {
                if($scope.client.id==undefined){
                   $scope.editButton = false;
                }else {
                    if ($scope.isClientSelected && $scope.tabPrevious==='personalInformation'){
                        $scope.editButton = false;
                    } else {
                        $scope.editButton = true;
                    }
                }  
            }, true);


/* Trip Style */
        $scope.tripStyleOptions = {
            tripStyles : TripStyle.$search({}), 
            tripGroups : TripGroup.$search({})
        };

        $scope.inStyles = function (id) {
            var res = false;

            if (id && $scope.client.id!=undefined &&  $scope.client.tripStyleIds.indexOf(id) >= 0) {
                res = true;
            }
            return res;
        };

        $scope.inGroups = function (id) {
            var res = false;
            if (id && $scope.client.id!=undefined && $scope.client.tripGroupIds.indexOf(id) >= 0) {
                res = true;
            }
            return res;
        };
        
        $scope.manageStyle = function (index) {
            if($scope.client.id!=undefined ){
                var tripStyle = $scope.tripStyleOptions.tripStyles[index];
                var pos = $scope.client.tripStyleIds.indexOf(tripStyle.id);
                if (pos < 0) {
                    $scope.client.addStyle(tripStyle.id);
                    $scope.client.tripStyleIds.push(tripStyle.id);
                } else {
                    $scope.client.deleteStyle(tripStyle.id);
                    $scope.client.tripStyleIds.splice(pos,1);
                }
            }
        };
        
        $scope.manageGroup = function (index) {
            if($scope.client.id!=undefined ){ 
                var tripGroup = $scope.tripStyleOptions.tripGroups[index];
                var pos = $scope.client.tripGroupIds.indexOf(tripGroup.id);
                if (pos < 0) {
                    $scope.client.addGroup(tripGroup.id);
                    $scope.client.tripGroupIds.push(tripGroup.id);
                } else {
                    $scope.client.deleteGroup(tripGroup.id);
                    $scope.client.tripGroupIds.splice(pos,1);
                }
            }
        };

/* End Trip Style */  


/*Start search airport*/

        //monitoreo el valor del aeropuerto de origen   
        $scope.$watch('tempData.labelOrigin', function() {
            if($scope.tempData !== undefined){

                if ($scope.tempData.labelOrigin===null){
                    $scope.tempData.labelOrigin = gettextCatalog.getString('Search AirPort');
                }

                if ($scope.tempData.labelOrigin !==undefined){
                    if ($scope.tempData.labelOrigin.length===0){
                        $scope.tempData.labelOrigin = gettextCatalog.getString('Search AirPort');
                    }

                }

            }

       }, true);

        $scope.searchAirport = function(val){
            $scope.airport = AirPort.$build();
            return $scope.airport.searchAirPort(val).then(function(_airports){
                return _airports.data.air_ports;
            });
        };


        $scope.submitSearchOrigin = function(airport){
         
         if(airport!==undefined){
          $scope.tempData.labelOrigin = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
          $scope.clientView.clientPreferences.origin= airport.id;
          $scope.clientView.clientPreferences.originLabel = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
          $scope.changeClientFlyPreferences();
        }
    };

/*End search airport*/

/*start hotel services*/
        $scope.editFacilitiesAndServices = function(){
            $scope.tempData.editingFacilities=true;
        };
        
        $scope.closeClientPreferenceFacilitiesAndServices = function(){
            $scope.tempData.editingFacilities=false;
        };
        
        $scope.undoClientPreferenceFacilitiesAndServices = function(){
            if($scope.client.clientPreferences.id==undefined){
                $scope.tempData.arrayFacilities=[];
            }else{
                $scope.tempData.arrayFacilities= angular.copy($scope.client.clientPreferences.hotelServices);
            }
        };

/*end hotel services*/      

/*  Methods Validations BEGIN */
            $scope.clientEditing_personalInformation=true;
            $scope.changePersonalInformation = function(){
                if( $scope.clientView.identifier != $scope.client.identifier ||
                    $scope.clientView.email != $scope.client.email ||
                    $scope.clientView.firstName != $scope.client.firstName ||
                    $scope.clientView.middleName != $scope.client.middleName ||
                    $scope.clientView.lastName != $scope.client.lastName ||
                    $scope.clientView.secondLastName != $scope.client.secondLastName ||
                    $scope.clientView.birthDate != $scope.client.birthDate ||
                    $scope.clientView.sex != $scope.client.sex){
                    $scope.clientEditing_personalInformation=true;
                }else{
                    if($scope.isClientNew)
                        $scope.clientEditing_personalInformation=true;
                    else
                        $scope.clientEditing_personalInformation=false;
                }
            };

            $scope.clientEditing_profesionalDetails=false;
            $scope.changeProfesionalDetails = function(){
                if( $scope.clientView.company != $scope.client.company ||
                    $scope.clientView.office != $scope.client.office ||
                    $scope.clientView.telephoneOffice != $scope.client.telephoneOffice ||
                    $scope.clientView.workingAddress != $scope.client.workingAddress ||
                    ($scope.client.profession != null &&  $scope.clientView.profession != null && $scope.clientView.profession.id !=  $scope.client.profession.id) ||
                    ($scope.client.education != null &&  $scope.clientView.education != null && $scope.clientView.education.id !=  $scope.client.education.id) ||
                    ($scope.client.profession == null && $scope.clientView.profession != null) ||
                    ($scope.client.education == null && $scope.clientView.education != null)){
                         $scope.clientEditing_profesionalDetails=true;
                }else{
                        $scope.clientEditing_profesionalDetails=false;
                }

            };

        
            $scope.clientEditing_flyPreferences=false;
            $scope.changeClientFlyPreferences = function(){
             if($scope.clientView.clientPreferences.originLabel != $scope.client.clientPreferences.originLabel ||
                    ($scope.client.clientPreferences.seatingPreferenceId != null &&  $scope.clientView.clientPreferences.seatingPreferenceId != null && $scope.clientView.clientPreferences.seatingPreferenceId !=  $scope.client.clientPreferences.seatingPreferenceId) ||
                    ($scope.client.clientPreferences.specialAssistanceId != null &&  $scope.clientView.clientPreferences.specialAssistanceId != null && $scope.clientView.clientPreferences.specialAssistanceId !=  $scope.client.clientPreferences.specialAssistanceId) ||
                    ($scope.client.clientPreferences.seatingPreferenceId == null && $scope.clientView.clientPreferences.seatingPreferenceId != null) ||
                    ($scope.client.clientPreferences.specialAssistanceId == null && $scope.clientView.clientPreferences.specialAssistanceId != null)){
                         $scope.clientEditing_flyPreferences=true;
                }else{
                         $scope.clientEditing_flyPreferences=false;
                }

            };
/*  Methods Validations  END*/ 
/*  Saves BEGIN */
            $scope.openClient = function(_client){
                $scope.$parent.openClient(_client);
            };

            $scope.savePersonalInformation = function(form){
                if(form.$valid){
                    $scope.client.identifier = $scope.clientView.identifier;
                    $scope.client.email = $scope.clientView.email;
                    $scope.client.firstName =  $scope.clientView.firstName;
                    $scope.client.middleName = $scope.clientView.middleName;
                    $scope.client.lastName = $scope.clientView.lastName;
                    $scope.client.secondLastName = $scope.clientView.secondLastName;
                    $scope.client.birthDate = $scope.clientView.birthDate;
                    $scope.client.sex = $scope.clientView.sex;
                    return $scope.client.$save().$then(function (_client) {
                        $scope.editButton = true;
                        $scope.clientEditing_personalInformation=false;
                        

                        if($scope.isClientNew && !$scope.isClientSelected){
                            $scope.client.clientPreferences = ClientPreferences.$build({
                                clientId : _client.id,
                                stars:$scope.tempData.starHover
                            });

                            $scope.client.clientPreferences.$save().$then(function () {
                                    $scope.client.$refresh();
                            });


                        }

                      
                      return $scope.sheet.addClients([_client.id], $scope.confirmClientNote.commandText).then(function () {
                        if ($scope.confirmClientNote.id != undefined){
                          var id_sheet_detail = $scope.confirmClientNote.sheetDetailId;
                          var id_sheet_detail_note = $scope.confirmClientNote.id;
                          $scope.confirmClientNote.$destroy().$then(function(_sheet_detail_note){
                            $scope.removeConfirmClient(id_sheet_detail, _sheet_detail_note);
                            $scope.sheet.clients.$add(_client);
                            $scope.sheet.$refresh();
                            $scope.$parent.addAlert('saved successful','success',true,true,10);
                            $scope.isClientNew=false;
                            $scope.isClientSelected = false;
                            $scope.clientView.commandText = $scope.confirmClientNote.commandText;
                            $scope.confirmClientNote = SheetDetailNote.$build({});
                            $scope.deleteSheetDetailId = id_sheet_detail;
                            $scope.deleteSheetDetailNoteId = id_sheet_detail_note;
                          });
                        }
                        else{
                          $scope.sheet.clients.$add(_client);
                          $scope.sheet.$refresh();
                          $scope.$parent.addAlert('saved successful','success',true,true,10);
                          $scope.isClientNew=false;
                          $scope.isClientSelected = false;
                        }
                      });
                    },function(_client){
                        $scope.$parent.addAlert(MessageUtils.getStringError(_client.errors),'warning',false,true,10);
                    }); 
                }
            }
            $scope.removeConfirmClient = function(id, sheetDetailNote){
              for (var i = 0; i < $scope.sheet.sheetDetails.length; ++i){
                if ($scope.sheet.sheetDetails[i].id == id){
                  $scope.sheet.sheetDetails[i].sheetDetailNotes.$remove(sheetDetailNote);
                  break;
                }
              }
            };

            $scope.saveProfesionalDetails = function(){
                if($scope.clientView.education)$scope.client.educationId = $scope.clientView.education.id;
                if($scope.clientView.profession)$scope.client.professionId = $scope.clientView.profession.id;
                $scope.client.company = $scope.clientView.company;
                $scope.client.office = $scope.clientView.office;
                $scope.client.telephoneOffice = $scope.clientView.telephoneOffice;
                $scope.client.workingAddress = $scope.clientView.workingAddress;
                return $scope.client.$save().$then(function (_client) {
                        $scope.editButton = true;
                        $scope.sheet.clients.$add(_client);
                        $scope.clientEditing_profesionalDetails=false;
                        $scope.$parent.addAlert('saved successful','success',true,true,10);
                },function(_client){
                    $scope.$parent.addAlert(MessageUtils.getStringError(_client.errors),'warning',false,true,10);
                });
            };

            $scope.saveClientPreference = function(){

                $scope.tempData.editingFacilities=false;

                $scope.client.clientPreferences.smoker = $scope.clientView.clientPreferences.smoker;
                $scope.client.clientPreferences.stars = $scope.clientView.clientPreferences.stars;
                $scope.client.clientPreferences.bedTypeId = $scope.clientView.clientPreferences.bedTypeId;
                $scope.client.clientPreferences.bedSheetTypeId = $scope.clientView.clientPreferences.bedSheetTypeId;
                $scope.client.clientPreferences.pillowTypeId = $scope.clientView.clientPreferences.pillowTypeId;
                return $scope.client.clientPreferences.$save().$then(function(_preference){
                    $scope.editButton = true;
                    if($scope.tempData.arrayFacilities != $scope.clientView.clientPreferences.hotelServices){
                        $scope.newArrayPreferencesFacilities = [];
                        angular.forEach($scope.tempData.arrayFacilities, function(value, key) {
                            $scope.newArrayPreferencesFacilities.push(value.id);
                        });
                        
                        $scope.client.clientPreferences.addFacilitiesService($scope.newArrayPreferencesFacilities).then(function(_result){
                                $scope.client.clientPreferences.hotelServices = $scope.tempData.arrayFacilities = _result.data.client_preferences.hotel_services;
                        });
                    }
                    $scope.$parent.addAlert('saved successful','success',true,true,10);
                });
            };

            $scope.saveClientFlyPreference = function(){
                
                $scope.client.clientPreferences.seatingPreferenceId = $scope.clientView.clientPreferences.seatingPreferenceId;
                $scope.client.clientPreferences.specialAssistanceId = $scope.clientView.clientPreferences.specialAssistanceId;
                $scope.client.clientPreferences.origin = $scope.clientView.clientPreferences.origin;
                $scope.client.clientPreferences.originLabel = $scope.clientView.clientPreferences.originLabel;
                $scope.tempData.airPortOriginId = $scope.clientView.clientPreferences.origin;
                $scope.tempData.airPortOriginLabel = $scope.clientView.clientPreferences.originLabel;
                
                return $scope.client.clientPreferences.$save().$then(function(_preference){
                    $scope.editButton = true;
                    $scope.$parent.addAlert('saved successful','success',true,true,10);

                });

         };


/*  Saves END*/           
/*  Search Client BEGIN */
            $scope.searchClient = function(_value){
                return $scope.client.searchClients(_value).then(function(_clients){
                    return _clients.data.clients;
                });
            };
            $scope.submitSearch = function(client){
                if(client!==undefined){
                     Client.$find(client.id).$then(function(_client){
                        $scope.client =_client;
                        $scope.clientView = angular.copy(_client);
                        $scope.isClientSelected = true;
                        $scope.editButton = false;
                    });
                }
            };
/*  Search Client END */

/*  Undo BEGIN */

            $scope.undoPersonalBasicDetails = function(){
                $scope.clientView.identifier = $scope.client.identifier;
                $scope.clientView.firstName = $scope.client.firstName;
                $scope.clientView.middleName = $scope.client.middleName;
                $scope.clientView.email = $scope.client.email;
                $scope.clientView.lastName = $scope.client.lastName;
                $scope.clientView.secondLastName = $scope.client.secondLastName;
                $scope.clientView.birthDate = $scope.client.birthDate;
                $scope.clientView.sex = $scope.client.sex;
                $scope.editButton = true;
            };

            $scope.undoProfesionalDetails = function(){
                $scope.clientView.education.id = $scope.client.education? $scope.client.education.id :undefined 
                $scope.clientView.profession.id = $scope.client.profession? $scope.client.profession.id :undefined 
                $scope.clientView.company = $scope.client.company;
                $scope.clientView.office = $scope.client.office;
                $scope.clientView.telephoneOffice = $scope.client.telephoneOffice;
                $scope.clientView.workingAddress = $scope.client.workingAddress;
                $scope.editButton = true;
            };

            $scope.undoClientPreference = function(){

                $scope.clientView.clientPreferences.smoker = $scope.client.clientPreferences.smoker;
                $scope.clientView.clientPreferences.stars = $scope.client.clientPreferences.stars;
                $scope.clientView.clientPreferences.bedTypeId = $scope.client.clientPreferences.bedTypeId;
                $scope.clientView.clientPreferences.bedSheetTypeId = $scope.client.clientPreferences.bedSheetTypeId;
                $scope.clientView.clientPreferences.pillowTypeId = $scope.client.clientPreferences.pillowTypeId;
                $scope.editButton = true;

                $scope.tempData.starHover = $scope.client.clientPreferences.stars;
                    if($scope.client.clientPreferences.id==undefined){
                            $scope.tempData.arrayFacilities=[];
                    }else{
                            $scope.tempData.arrayFacilities= $scope.client.clientPreferences.hotelServices;
                    }
            };

            $scope.undoClientFlyPreference = function(){

                $scope.clientView.clientPreferences.seatingPreferenceId = $scope.client.clientPreferences.seatingPreferenceId;
                $scope.clientView.clientPreferences.specialAssistanceId = $scope.client.clientPreferences.specialAssistanceId;
                $scope.clientView.clientPreferences.origin = $scope.tempData.airPortOriginId;
                $scope.clientView.clientPreferences.originLabel = $scope.tempData.airPortOriginLabel;
                $scope.tempData.labelOrigin = $scope.tempData.airPortOriginLabel;
                $scope.editButton = true;
            };

/*  Undo End */

/* PHONE*/
            $scope.removePhone = function(_phone){
                return _phone.$destroy().$then(function(_phone){
                    $scope.client.phones.$remove(_phone);
                });
            };



            $scope.openPhone = function(_phone){
                if(_phone==undefined){
                    _phone = Phone.$build({
                        clientId : $scope.client.id
                    });
                }

                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Telephone</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<form name="client_phone_modal" id="client_phone_modal" >'+
                                        '<div class="col-md-12 hidden" >'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Importance</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select class="form-control nopaddingH" name="importance_{{$index}}" ng-model="phone.importance">'+
                                                        '<option value="IMPORTANCE_1" >1</option>'+
                                                        '<option value="IMPORTANCE_2" >2</option>'+
                                                        '<option value="IMPORTANCE_3" >3</option>'+
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Country</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<select required="" class="form-control" ng-change="changePhone()"  ng-model="phone.countryId" ng-options="country.id as country.name | translate for country in countries"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+  
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Number</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-2"><span class="form-control" style="border:none; padding-left:0; padding-right:0;  font-size: float:right; text-align:right;">{{countrySelected}}</span></div>'+
                                                '<div class="col-md-5">'+
                                                    '<input required="" type="text" ui-mask="(999) 999-9999" class="form-control " ng-change="changePhone()" ng-model="phone.number" numbers-only="numbers-only" maxlength="12" model-view-value="true">'+   
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Type</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<select required="" ng-change="changePhone()" class="form-control nopaddingH" name="type_{{$index}}" ng-model="phone.type">'+
                                                        '<option value="trabajo" translate>Work</option>'+
                                                        '<option value="Personal" translate>Personal</option>'+
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '</form>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " ng-class="{\'disabled\':(!phoneEditing || !client_phone_modal.$valid)}" loading-click="savePhone()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'PhoneCtrl',
                  size: 'lg',
                  resolve: {
                    phone: function () {
                      return _phone;
                    },
                    rslv_countries: ['Country',function(Country) { return Country.$search(); }]
                  }
                });
                modalInstance.result.then(function (_phone){
                    $scope.client.phones.$add(_phone);
                }, function () {});
            };

/*METHODS HOTEL OPTIONS*/

            $scope.removeHotel = function(_hotel){
                return _hotel.$destroy().$then(function(_hotel){
                    $scope.client.hotelOptions.$remove(_hotel);
                });
            };

            $scope.openHotel = function(_hotel){
                if(_hotel==undefined){
                    _hotel = HotelOption.$build({
                        clientId : $scope.client.id
                    });
                }

                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Prefered Hotel</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<form name="client_hotel_modal" id="client_hotel_modal" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Hotel Name</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<input type="text" class="form-control"  ng-change="changeHotel()" ng-model="hotelOption.name">'+   
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Trip Type</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<select required="" class="form-control" ng-change="changeHotel()"  ng-model="hotelOption.tripTypeId" ng-options="trip.id as trip.name | translate for trip in trip_types"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+  
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Room Type</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<select required="" class="form-control" ng-change="changeHotel()"  ng-model="hotelOption.roomTypeId" ng-options="room.id as room.name | translate for room in room_types"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+ 
                                         '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Comments</translate></label>'+
                                                '<div class="col-md-7">'+
                                                    '<textarea maxlength="255"  ng-model="hotelOption.comments" name="comment" class="form-control" ng-change="changeHotel()" rows="3"></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '</form>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " ng-class="{\'disabled\':(!hotelEditing || !client_hotel_modal.$valid)}" loading-click="saveHotel()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'HotelCtrl',
                  size: 'lg',
                  resolve: {
                    hotel: function () {
                      return _hotel;
                    },
                    rslv_trip_types: ['TripType',function(TripType) { return TripType.$search(); }],
                    rslv_room_types: ['RoomType',function(RoomType) { return RoomType.$search(); }]
                           }
                });
                modalInstance.result.then(function (_hotel){
                    $scope.client.hotelOptions.$add(_hotel);
                }, function () {});
            };


/*END METHODS HOTEL OPTIONS*/



/* CUSTOMER DATE*/
            $scope.getActionTypesString = function(array){
                var string = "";
                angular.forEach(array, function(value, key) {
                    string+=((key>0)?", ":"")+value.label;
                });
                return string;
             };
            $scope.removeCustomerDate = function(_customerDate){
                return _customerDate.$destroy().$then(function(){
                    
                });
            };
            $scope.openCustomerDate = function(_customerDate){
                if(_customerDate==undefined){
                    _customerDate = CustomerDate.$build({
                        clientId : $scope.client.id
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Date</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<form name="client_customer_date_modal" id="client_customer_date_modal" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" ><translate>Type</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<select required="" ng-change="changeCustomerDate()" class="form-control nopaddingH" name="typeDate_{{$index}}" ng-model="customerDate.typeDate">'+
                                                        '<option value="BIRTHDAY" translate>Birthday</option>'+
                                                        '<option value="ANNIVERSARY" translate>Anniversary</option>'+
                                                    '</select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Date</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<div required="" ng-change="changeCustomerDate()" sb-date-select sb-select-class="form-control "  class="prsht-sb-date-select" ng-model="customerDate.date"></div>'+
                                                    
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  ><translate>Action type</translate></label>'+
                                                '<div class="col-md-7">'+
                                                    '<div  ng-dropdown-multiselect="" options="actionTypes" selected-model="customerDate.actionTypes" extra-settings="newActionTypesSettings"></div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '</form>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " ng-class="{\'disabled\':(!customerDateEditing || !client_customer_date_modal.$valid)}" loading-click="saveCustomerDate()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'CustomerDateCtrl',
                  size: 'lg',
                  resolve: {
                    customerDate: function () {
                      return _customerDate;
                    }
                  }
                });
                modalInstance.result.then(function (_customerDate){
                    $scope.client.$refresh();
                }, function () {});
            };

/* ADDRESS*/
            $scope.removeAddress = function(_address){
                return _address.$destroy().$then(function(_address){
                    $scope.client.addresses.$remove(_address);
                });
            };
            $scope.openAddress = function(_address){
                if(_address==undefined){
                    _address = Address.$build({
                        clientId : $scope.client.id
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Address</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<form name="client_address_modal" id="client_address_modal" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"><translate>Address</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<input required="" ng-change="changeAddress()" type="text" class="form-control"  ng-model="address.address1">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 hidden">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Address line 2</label>'+
                                                '<div class="col-md-7">'+
                                                    '<input ng-change="changeAddress()" type="text" class="form-control"  ng-model="address.address2">'+    
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"><translate>Country</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<select required="" ng-change="changeAddress()" class="form-control"  ng-model="address.countryId" ng-options="country.id as country.name for country in countries"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+ 
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" ><translate>State/Province/Region</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<input required="" ng-change="changeAddress()" type="text" class="form-control"  ng-model="address.state">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+  
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" ><translate>City</translate><span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<input required="" ng-change="changeAddress()" type="text" class="form-control"  ng-model="address.city">'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" >ZIP<span class="required" aria-required="true">*</span></label>'+
                                                '<div class="col-md-7">'+
                                                    '<input required="" ng-change="changeAddress()" type="text" class="form-control"  ng-model="address.zip"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+  
                                        '</form>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " loading-click="saveAddress()" ng-class="{\'disabled\':(!addressEditing || !client_address_modal.$valid)}" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'AddressCtrl',
                  size: 'lg',
                  resolve: {
                    address: function () {
                      return _address;
                    },
                    rslv_countries: ['Country',function(Country) { return Country.$search(); }]
                  }
                });
                modalInstance.result.then(function (_address){
                    $scope.client.addresses.$add(_address);
                }, function () {});
            };

/* SOCIAL MEDIA*/
            $scope.removeSocialMedia = function(_socialMedia){
                return _socialMedia.$destroy().$then(function(_socialMedia){
                    $scope.client.socialMedias.$remove(_socialMedia);
                });
            };
            $scope.openSocialMedia = function(_socialMedia){
                if(_socialMedia==undefined){
                    _socialMedia = SocialMedia.$build({
                        clientId : $scope.client.id
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Social Media</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Social</label>'+
                                                '<div class="col-md-7">'+
                                                    '<input type="text" class="form-control"  ng-model="socialMedia.name"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Type Social</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select ng-model="socialMedia.url" class="form-control" ng-options="social.url as social.name for social in listSocialMedias"></select>'+
                                                    '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " loading-click="saveSocialMedia()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'SocialMediaCtrl',
                  size: 'lg',
                  resolve: {
                    socialMedia: function () {
                      return _socialMedia;
                    }
                  }
                });
                modalInstance.result.then(function (_socialMedia){
                    $scope.client.socialMedias.$add(_socialMedia);
                }, function () {});
            };

/* PASSPORT*/
            $scope.removePassport = function(_passport){
                return _passport.$destroy().$then(function(_passport){
                    $scope.client.passports.$remove(_passport);
                });
            };
            $scope.openPassport = function(_passport){
                if(_passport==undefined){
                    _passport = Passport.$build({
                        clientId : $scope.client.id
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Passport</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Upload Image</label>'+
                                                '<div class="col-md-7" style="margin-bottom:5px;">'+
                                                    '<div   ngf-drop   ng-model="imagePassport" ngf-change="fileSelected($file)" class="drop-box"   ngf-drag-over-class="dragover" ngf-multiple="false"  name="file" accept="image/*"><i class="icon-cloud-upload"></i><translate>Drop image here to upload, or</translate> <a  ngf-select  ngf-multiple="false" ngf-change="fileSelected($file)" type="file" ng-model="imagePassport" name="file" accept="image/*" translate>browse</a>.</div>'+
                                                    '<div class="text-center">'+
                                                        '<img-view  ngf-image="passport" ></img-view>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Full Name</label>'+
                                                '<div class="col-md-7">'+
                                                    '<input type="text" class="form-control"  ng-model="passport.fullName"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Passport Number</label>'+
                                                '<div class="col-md-7">'+
                                                    '<input type="text" class="form-control"  ng-model="passport.passportNumber"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 ">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Expiration Date</label>'+
                                                '<div class="col-md-7">'+
                                                    '<div sb-date-select sb-select-class="form-control "  max="2030-12-31" class="prsht-sb-date-select" ng-model="passport.expirationDate"></div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Issuing Country</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select  class="form-control"  ng-model="passport.countryId" ng-options="country.id as country.name for country in countries"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " loading-click="savePassport()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'PassportCtrl',
                  size: 'lg',
                  resolve: {
                    passport: function () {
                      return _passport;
                    },
                    rslv_countries: ['Country',function(Country) { return Country.$search(); }]
                  }
                });
                modalInstance.result.then(function (_passport){
                    $scope.client.passports.$add(_passport);
                }, function () {});
            };

/* VISA*/
            $scope.removeVisa = function(_visa){
                return _visa.$destroy().$then(function(_visa){
                    $scope.client.visas.$remove(_visa);
                });
            };
            $scope.openVisa = function(_visa){
                if(_visa==undefined){
                    _visa = Visa.$build({
                        clientId : $scope.client.id
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Visa</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Upload Image</label>'+
                                                '<div class="col-md-7" style="margin-bottom:5px;">'+
                                                    '<div   ngf-drop   ng-model="imageVisa" ngf-change="fileSelected($file)" class="drop-box"   ngf-drag-over-class="dragover" ngf-multiple="false"  name="file" accept="image/*"><i class="icon-cloud-upload"></i><translate>Drop image here to upload, or</translate> <a  ngf-select  ngf-multiple="false" ngf-change="fileSelected($file)" type="file" ng-model="imageVisa" name="file" accept="image/*" translate>browse</a>.</div>'+
                                                    '<div class="text-center">'+
                                                    '<img-view  ngf-image="visa" ><img-view>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Full Name</label>'+
                                                '<div class="col-md-7">'+
                                                    '<input type="text" class="form-control"  ng-model="visa.fullName"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Visa Number</label>'+
                                                '<div class="col-md-7">'+
                                                    '<input type="text" class="form-control"  ng-model="visa.visaNumber"  >'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12 ">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Expiration Date</label>'+
                                                '<div class="col-md-7">'+
                                                    '<div sb-date-select sb-select-class="form-control "  max="2030-12-31" class="prsht-sb-date-select" ng-model="visa.expirationDate"></div>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Issuing Country</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select  class="form-control"  ng-model="visa.countryId" ng-options="country.id as country.name for country in countries"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div> '+ 
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " loading-click="saveVisa()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'VisaCtrl',
                  size: 'lg',
                  resolve: {
                    visa: function () {
                      return _visa;
                    },
                    rslv_countries: ['Country',function(Country) { return Country.$search(); }]
                  }
                });
                modalInstance.result.then(function (_visa){
                    $scope.client.visas.$add(_visa);
                }, function () {});
            };





/* ALLERGY*/
            $scope.removeAllergy = function(_allergy){
                return _allergy.$destroy().$then(function(_allergy){
                    $scope.allergiesClient.$remove(_allergy);
                    $scope.sheet.$refresh();
                });
            };
            $scope.openAllergy = function(_allergy){
                if(_allergy==undefined){
                    _allergy = ClientSpecialComment.$build({
                        clientId: $scope.client.id,
                        specialCategoryId: $scope.catAllergy
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Allergies</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Type</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select  class="form-control"  ng-model="allergy.specialSubCategoryId" ng-options="subAllergyCategory.id as subAllergyCategory.name | translate for subAllergyCategory in subAllergyCategories"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Detail</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select class="form-control"  ng-model="allergy.specialSubCategoryDetailId" ng-options="subAllergyCategoryDetail.id as subAllergyCategoryDetail.name | translate for subAllergyCategoryDetail in subAllergyCategoryDetails"></select>'+ 
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Comment</label>'+
                                                '<div class="col-md-7">'+
                                                    '<textarea maxlength="255"  ng-model="allergy.comment" name="comment" class="form-control inputComment" rows="3"></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " loading-click="saveAllergy()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'AllergyCtrl',
                  size: 'lg',
                  resolve: {
                    allergy: function () {
                      return _allergy;
                    },
                    catAllergy : function(){
                      return $scope.catAllergy;
                    },
                    subAllergyCategories : function(){
                        return $scope.subAllergyCategories;
                    }
                  }
                });
                modalInstance.result.then(function (_allergy){
                    $scope.allergiesClient.$add(_allergy);
                    $scope.sheet.$refresh();
                }, function () {});
            };

/* DISABILITY*/
            $scope.removeDisability = function(_disability){
                return _disability.$destroy().$then(function(_disability){
                    $scope.disabilitiesClient.$remove(_disability);
                    $scope.sheet.$refresh();
                });
            };
            $scope.openDisability = function(_disability){
                if(_disability==undefined){
                    _disability = ClientSpecialComment.$build({
                        clientId: $scope.client.id,
                        specialCategoryId: $scope.catDisability
                    });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Disabilities</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Type</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select  class="form-control"  ng-model="disability.specialSubCategoryId" ng-options="subDisabilityCategory.id as subDisabilityCategory.name | translate for subDisabilityCategory in subDisabilityCategories"></select>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Detail</label>'+
                                                '<div class="col-md-7">'+
                                                    '<select class="form-control"  ng-model="disability.specialSubCategoryDetailId" ng-options="subDisabilityCategoryDetail.id as subDisabilityCategoryDetail.name | translate for subDisabilityCategoryDetail in subDisabilityCategoryDetails"></select>'+ 
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                                '<label class="col-md-5 control-label lb-client-bg-wht"  translate>Comment</label>'+
                                                '<div class="col-md-7">'+
                                                    '<textarea maxlength="255"  ng-model="disability.comment" name="comment" class="form-control inputComment" rows="3"></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " loading-click="saveDisability()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'DisabilityCtrl',
                  size: 'lg',
                  resolve: {
                    disability: function () {
                      return _disability;
                    },
                    catDisability : function(){
                      return $scope.catDisability;
                    },
                    subDisabilityCategories : function(){
                        return $scope.subDisabilityCategories;
                    }
                  }
                });
                modalInstance.result.then(function (_disability){
                    $scope.disabilitiesClient.$add(_disability);
                    $scope.sheet.$refresh();
                }, function () {});
            };

/*LOYALTY PROGRAMS*/

            $scope.removeClientLoyaltyProgram = function(_clientLoyaltyProgram){
                return _clientLoyaltyProgram.$destroy().$then(function(_client_loyalty_program){
                    $scope.client.clientLoyaltyPrograms.$remove(_client_loyalty_program);
                });
             };

            $scope.openLoyaltyProgram = function(_clientLoyaltyProgram){

                if(_clientLoyaltyProgram==undefined){
                  _clientLoyaltyProgram = ClientLoyaltyProgram.$build({
                    clientId: $scope.client.id,
                  });
                }
                var modalInstance = $modal.open({
                  animation: false,
                  template: '<div class="modal-content">'+
                                '<div class="modal-header">'+
                                    '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 translate>Loyalty Programs</h5>'+
                                '</div>'+
                                '<div class="modal-body body-details-client ">'+
                                    '<div class="container" style="padding-top: 25px;" >'+
                                      '<form name="client_loyalty_program_modal" id="client_loyalty_program_modal">'+
                                         '<div class="row-fluid container-detail-passport">'+
                                          '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                              '<label class="col-md-5 control-label lb-client-bg-wht"><translate>Program Name</translate><span class="required" aria-required="true">*</span></label>'+
                                              '<div class="col-md-7">'+
                                                '<ui-select required="true" ng-change="changeClientLoyaltyProgram()" ng-model="clientLoyaltyProgram.loyaltyProgram.id" theme="bootstrap" ng-disabled="disabled" reset-search-input="false" title="Selecciona">'+
                                                '<ui-select-match placeholder="{{ \'Select\' | translate}}">{{$select.selected.name}}</ui-select-match>'+
                                                '<ui-select-choices repeat="loyaltyProgram.id as loyaltyProgram in loyalty_programs | filter: $select.search"'+
                                                  'refresh="refreshLoyaltyPrograms($select.search)"'+
                                                  'refresh-delay="0">'+
                                                '<div ng-bind-html="loyaltyProgram.name | highlight: $select.search"></div>'+
                                                  '</ui-select-choices>'+
                                                  '</ui-select>'+
                                              '</div>'+
                                            '</div>'+
                                          '</div>'+
                                          '<div class="col-md-12">'+
                                            '<div class="form-group">'+
                                              '<label class="col-md-5 control-label lb-client-bg-wht"><translate>Program Number</translate><span class="required" aria-required="true">*</span></label>'+
                                              '<div class="col-md-7">'+
                                                '<input type="text" required="true" class="form-control" ng-change="changeClientLoyaltyProgram()" ng-model="clientLoyaltyProgram.number" />'+
                                              '</div>'+
                                            '</div>'+
                                          '</div>'+
                                        '</div>'+
                                      '</form>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="modal-footer">'+
                                    '<button class="btn default" ng-click="cancel()"  translate>Cancel</button>'+
                                    '<button class="btn blue " ng-class="{\'disabled\':(!editingClientLoyaltyProgram || !client_loyalty_program_modal.$valid)}" loading-click="saveClientLoyaltyProgram()" translate>Save</button>'+
                                '</div>'+
                            '</div>',
                  controller: 'ClientLoyaltyProgramCtrl',
                  size: 'lg',
                  resolve: {
                    clientLoyaltyPrograms: function(){
                        return $scope.client.clientLoyaltyPrograms;
                    } ,
                    clientLoyaltyProgram: function () {
                      return _clientLoyaltyProgram;
                    },
                    rslv_loyalty_programs: ['LoyaltyProgram',function(Country) { return LoyaltyProgram.$search(); }]
                  }
                });
                modalInstance.result.then(function (_clientLoyaltyProgram){
                    $scope.client.clientLoyaltyPrograms.$add(_clientLoyaltyProgram);
                    $scope.sheet.$refresh();
                }, function () {});
            };

/*HISTORY*/  
            $scope.client.getSheets().then(function (data) {
              $scope.client.sheets2 = data.data.clients;
            });
            $scope.openHistory = function (id) {
                var modalInstance = $modal.open({
                    controller: 'ClientSheetCtrl',
                    size:'lg',
                    resolve: {
                        sheetId: function () {
                            return id;
                        }
                    },
                    template: '<div class="modal-scrollable">'+
                        '  <div class="modal-header"> '+
                        '    <a ng-click="close()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                        '    <h5 >{{sheet.title}}</h5>'+
                        '  </div>'+
                        '  <div class="modal-body2">'+
                        '    <loading-message ng-model="sheet"></loading-message>'+
                        '    <div class="col-sm-12">'+
                        '      <span class="titleCollapse-less pull-right" ng-click="titleCollapse = !titleCollapse" ng-show="titleCollapse" translate>more...</span> '+
                        '      <div class="container-titles-sheet pull-right" collapse="titleCollapse">'+
                        '        <div class="sheet-title-container-b">'+
                        '          <span><translate>Status:</translate></span><strong>{{ sheet.status | uppercase | translate }}</strong>      '+
                        '        </div>'+
                        '        <div class="sheet-title-container-b separador-left">'+
                        '          <span><translate>Created at:</translate></span><strong>{{sheet.createdAt|date:"yyyy-MM-dd"}}</strong>      '+
                        '        </div>'+
                        '        <div class="sheet-title-container-b">'+
                        '          <span><translate>Created by:</translate></span><strong>{{sheet.agent.name}} {{sheet.agent.lastName}}</strong>     '+
                        '          <span class="titleCollapse-less" ng-click="titleCollapse = !titleCollapse" ng-show="!titleCollapse" translate>hide...</span> '+
                        '        </div>'+
                        '      </div>'+
                        '    </div>'+
                        '    <div class="col-sm-12">'+
                        '      <tabset>'+
                        '        <tab>'+
                        '          <tab-heading>'+
                        '            <i class="fa fa-calendar"></i><span translate>Timeline</span>'+
                        '          </tab-heading>'+
                        '          <!-- Timeline -->'+
                        ''+
                        '          <div class="row">'+
                        '            <div class="col-sm-12">    '+
                        '              <timeline>'+
                        '                <ul class="timeline">'+
                        '                  <div ng-repeat="(date, events) in sheet.timeline">'+
                        '                    <li class="timeline-yellow">'+
                        '                 <div class="timeline-time">'+
                        '               <span class="date">{{date}}</span>'+
                        '               <span class="time">&nbsp;</span>'+
                        '                 </div>'+
                        '                    </li>'+
                        '                    <li ng-repeat="event in events" ng-class="timelineClass(event.color,$parent.$last,$last)">'+
                        '                      <div class="timeline-time">'+
                        '               <span class="date hidden-xs">&nbsp</span>'+
                        '               <span class="time hidden-xs">&nbsp</span>'+
                        '                 </div>'+
                        '                 <div class="timeline-icon">'+
                        '               <i ng-class="event.icon"></i>'+
                        '                 </div>'+
                        '                 <div class="timeline-body">'+
                        '               <h2>'+
                        '                          <span ng-hide="event.title">{{event.origin_label}} - {{event.destination_label}}</span>'+
                        '                          <span ng-show="event.title">{{event.title}}</span>'+
                        '                        </h2>'+
                        '             </div>'+
                        '                    </li>'+
                        '                  </div>'+
                        '                </ul>'+
                        '              </timeline>'+
                        '            </div>'+
                        '          </div>'+
                        '        </tab>'+
                        '        <tab>'+
                        '          <tab-heading>'+
                        '            <i class="fa fa-tags"></i><span translate>Tickets And Services</span>'+
                        '          </tab-heading>'+
                        ''+
                        '          <div class="row">'+
                        '            <div class="col-sm-12">'+
                        '              <div class="portlet box gray">'+
                        '                <div class="portlet-title">'+
                        '                  <div class="caption" >'+
                        '                    <i class="icon-note"></i><translate>Tickets and Services</translate>'+
                        '                    <span class="badge badge-info badge-sheet">'+
                        '                      {{sheet.servicesList.length}} </span>'+
                        '                  </div>'+
                        '                </div>'+
                        '                <div class="portlet-body sheet-table sheet-table-client nopadding" collapse="serviceCollapse">'+
                        '                  <div class="btn-toolbar">'+
                        '                    <loading-message ng-model="sheet" one-time-loading="true"></loading-message>'+
                        '                    <loading-message ng-model="sheet.airServices"></loading-message>'+
                        '                    <loading-message ng-if="!sheet.airServices.$pending || sheet.airServices.$pending.length == 0"  ng-model="sheet.accomodationServices"></loading-message>'+
                        '                  </div>'+
                        '                  <div class="table-responsive" ng-show="(sheet.servicesList && sheet.servicesList.length > 0) || note_command_services.length > 0">'+
                        '                    <table class="table-project table table-bordered table-condensed flip-content ">'+
                        '                      <thead>'+
                        '                        <tr>'+
                        '                          <th class="col-sm-1"translate>Kind</th>'+
                        '                          <th class="col-sm-3"translate>Date</th>'+
                        '                          <th translate>Description</th>'+
                        '                        </tr>'+
                        '                      </thead>'+
                        '                      <tbody>'+
                        '                        <tr ng-repeat="service in sheet.servicesList | orderBy:\'date\'">'+
                        '                          <td ng-click="editService(service)">{{ service.type | translate | capitalize }}'+
                        '                          </td>'+
                        '                          <td ng-if="service.type==\'air\'">'+
                        '                            {{ service.date}}'+
                        '                          </td>'+
                        '                          <td ng-if="service.type==\'accomodation\'" ng-click="editService(service)">'+
                        '                            {{ service.initDate }} -- {{service.endDate}}'+
                        '                          </td>'+
                        '                          <td ng-if="service.type==\'air\'" ng-click="editService(service)">'+
                        '                            <strong translate>Origin</strong>: {{service.originLabel}} /'+
                        '                            <strong translate>Destination</strong>: {{service.destinationLabel}}'+
                        '                          </td>'+
    '                          <td ng-if="service.type==\'accomodation\'" ng-click="editService(service)">'+
                        '                            {{service.city}} / {{service.accomodationType}}'+
                        '                          </td>'+
                        '                        </tr>'+
                        '                        <tr  class="no-processed-command" ng-repeat="note in note_command_services">'+
                        '                          <td  colspan="5">{{note.command_text}}</td>'+
                        '                        </tr>'+
                        '                      </tbody>'+
                        '                    </table>'+
                        '                  </div>'+
                        '                  <p ng-show="(!sheet.servicesList || sheet.servicesList.length==0) && note_command_services.length==0" translate>'+
                        '                    There aren\'t any services added yet'+
                        '                  </p>'+
                        '                </div>'+
                        '              </div>'+
                        '            </div>'+
                        '          </div>    '+
                        '        </tab>'+
                        '        <tab>'+
                        '          <tab-heading>'+
                        '            <i class="fa fa-users"></i><span translate>Clients</span>'+
                        '          </tab-heading>'+
                        '          <div class="row-fluid">'+
                        '            <div class="portlet box gray">'+
                        '              <div class="portlet-title">'+
                        '                <div class="caption" >'+
                        '                  <i class="fa fa-users"></i><translate>Clients</translate>'+
                        '                  <span class="badge badge-info badge-sheet">'+
                        '                    {{sheet.clients.length}} </span>'+
                        '                </div>'+
                        '              </div>'+
                        '              <div class="portlet-body sheet-table sheet-table-client nopadding" collapse="clientCollapse">'+
                        '                '+
                        '                <div class="btn-toolbar">'+
                        '                  <loading-message ng-model="sheet"></loading-message>'+
                        '                </div>'+
                        '                <div class="table-responsive">'+
                        '                  <table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                        '                    <thead>'+
                        '                      <tr>'+
                        '                        <th translate>ID</th>'+
                        '                        <th translate>Name</th>'+
                        '                        <th translate>Email</th>'+
                        '                      </tr>'+
                        '                    </thead>'+
                        '                    <tbody>'+
                        '                      <tr ng-repeat="client in sheet.clients" >'+
                        '                        <td ng-click="addDetailClient(sheet,client,null)">'+
                        '                          <span >{{ client.id }}</span>'+
                        '                        </td>'+
                        '                        <td ng-click="addDetailClient(sheet,client,null)">'+
                        '                          <span >{{ client.firstName }}&nbsp;{{ client.middleName }}&nbsp;{{ client.lastName }}&nbsp;{{ client.secondLastName }}</span>'+
                        '                        </td>'+
                        '                        <td ng-click="addDetailClient(sheet,client,null)">'+
                        '                          <span >{{ client.email }}</span>'+
                        '                        </td>'+
                        '                      </tr>'+
                        '                      <tr class="no-processed-command" ng-repeat="note in note_command_clients">'+
                        '                        <td  colspan="5">{{note.command_text}}</td>'+
                        '                      </tr>'+
                        '                    </tbody>'+
                        ''+
                        '                    '+
                        '                  </table>'+
                        '                </div>'+
                        '              </div>'+
                        '            </div>'+
                        '          </div>'+
                        '        </tab>'+
                        '      </tabset>'+
                        '    </div>'+
                        '  </div>'+
                        '</div>'
                });
            };
        },
        link : function ($scope, $element, $attrs) {
          
            $scope.clientPanelNav = 1;
            $element.css({
                          'height':'inherit'
                        });
             
        }
    }
});

angular.module('projectApp').controller('PhoneCtrl', [
  '$scope',
  '$modalInstance',
  'phone',
  'rslv_countries',
  function ($scope, $modalInstance,phone,rslv_countries) {
    $scope.phone = phone;
    $scope.phone.number= $scope.phone.number==null ? '':$scope.phone.number;

    $scope.phoneCopy = angular.copy($scope.phone);
    $scope.countries = rslv_countries;
    if($scope.phoneCopy.country!=null){
        $scope.phone.countryId = $scope.phoneCopy.country.id;
        $scope.countrySelected = $scope.phoneCopy.country.dialCode;
    }
     
    $scope.phoneEditing=false;

    //monitoreo el valor del id de pais   
    $scope.$watch('phone.countryId', function() {
      if($scope.phone.countryId!==undefined){
        $scope.index_result = $scope.findIndexByKeyValue($scope.countries, "id", $scope.phone.countryId);
        if($scope.index_result!==null){
            $scope.countrySelected = $scope.countries[$scope.index_result].dialCode;
        } 
      } 
    }, true);


    $scope.findIndexByKeyValue = function(array, key, value) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] == value) {
                return i;
            }
        }
        return null;
    }

    

    $scope.changePhone = function(){
        if( $scope.phone.number != $scope.phoneCopy.number ||
            $scope.phone.type != $scope.phoneCopy.type || 
            $scope.phone.country != $scope.phoneCopy.country){
            $scope.phoneEditing=true;
        }else{
            $scope.phoneEditing=false;
        }
    }

    $scope.savePhone = function(){
        return $scope.phone.$save().$then(function (_phone) {
                $modalInstance.close(_phone);
        });
    };
    $scope.cancel = function () {
        $scope.phone.importance = $scope.phoneCopy.importance;
        $scope.phone.number = $scope.phoneCopy.number;
        $scope.phone.type = $scope.phoneCopy.type;
        $scope.phone.country = $scope.phoneCopy.country;
        $modalInstance.dismiss();
      };
  }]);


angular.module('projectApp').controller('HotelCtrl', [
  '$scope',
  '$modalInstance',
  'hotel',
  'rslv_trip_types',
  'rslv_room_types',
  function ($scope, $modalInstance,hotel,rslv_trip_types, rslv_room_types) {
    $scope.hotelOption =hotel;
    $scope.hotelOption.name = $scope.hotelOption.name==null ? '':$scope.hotelOption.name;

    $scope.hotelOptionCopy = angular.copy($scope.hotelOption);
    $scope.trip_types = rslv_trip_types;
    $scope.room_types = rslv_room_types;
      
     
    $scope.hotelEditing=false;

    $scope.changeHotel = function(){
        if( $scope.hotelOption.name != $scope.hotelOptionCopy.name ||
            $scope.hotelOption.tripTypeId != $scope.hotelOptionCopy.tripTypeId || 
            $scope.hotelOption.roomTypeId != $scope.hotelOptionCopy.roomTypeId ||
            $scope.hotelOption.comments != $scope.hotelOptionCopy.comments){
            $scope.hotelEditing=true;
        }else{
            $scope.hotelEditing=false;
        }
    }

    $scope.saveHotel = function(){
        return $scope.hotelOption.$save().$then(function (_hotel) {
                $modalInstance.close(_hotel);
        });
    };
    $scope.cancel = function () {
        $scope.hotelOption.name = $scope.hotelOptionCopy.name;
        $scope.hotelOption.tripTypeId  = $scope.hotelOptionCopy.tripTypeId ;
        $scope.hotelOption.roomTypeId = $scope.hotelOptionCopy.roomTypeId;
        $scope.hotelOption.comments = $scope.hotelOptionCopy.comments;
        $modalInstance.dismiss();
      };
  }]);


angular.module('projectApp').controller('CustomerDateCtrl', [
  '$scope',
  '$modalInstance',
  'customerDate',
  'ActionType',
  '$compile',
  function ($scope, $modalInstance,customerDate,ActionType,$compile) {
    $scope.customerDate = customerDate;
    $scope.customerDateCopy = angular.copy($scope.customerDate);
    $scope.actionTypes = ActionType.$search({});
    $scope.newActionTypes = [];
    $scope.newActionTypesSettings = {externalIdProp: ''};

    $scope.customerDateEditing=false;


    $scope.changeCustomerDate = function(){
        if( $scope.customerDate.typeDate != $scope.customerDateCopy.typeDate ||
            $scope.customerDate.date != $scope.customerDateCopy.date ){
            $scope.customerDateEditing=true;
        }else{
            $scope.customerDateEditing=false;
        }
    }
    $scope.$watchCollection('customerDate.actionTypes',function(newvalue){
        if(newvalue.length==0){
         $scope.customerDateEditing=false; 
        }else{
            $scope.changeCustomerDate();
        }
    });
    $scope.saveCustomerDate = function(){
            $scope.newArrayActionTypes = [];
            angular.forEach($scope.customerDate.actionTypes, function(value, key) {
                $scope.newArrayActionTypes.push(value.id);
            });
            return $scope.customerDate.$save().$then(function (_customerDate) {
                    _customerDate.addActionType($scope.newArrayActionTypes).then(function(_customerDate){
                        $modalInstance.close(_customerDate);
                    });
                    
            });
     };
    $scope.cancel = function () {
        $scope.customerDate.typeDate = $scope.customerDateCopy.typeDate;
        $scope.customerDate.date = $scope.customerDateCopy.date;
        $scope.customerDate.actionTypes = $scope.customerDateCopy.actionTypes;
        $modalInstance.dismiss();
      };
  }]);

angular.module('projectApp').controller('AddressCtrl', [
  '$scope',
  '$modalInstance',
  'address',
  'rslv_countries',
  function ($scope, $modalInstance,address,rslv_countries) {
    $scope.address = address;
    $scope.countries = rslv_countries;
    $scope.addressCopy = angular.copy($scope.address);

    $scope.addressEditing=false;


    $scope.changeAddress = function(){
        if( $scope.address.address1 != $scope.addressCopy.address1 ||
            $scope.address.city != $scope.addressCopy.city ||
            $scope.address.state != $scope.addressCopy.state ||
            $scope.address.zip != $scope.addressCopy.zip ||
            $scope.address.country != $scope.addressCopy.country 
            ){
            $scope.addressEditing=true;
        }else{
            $scope.addressEditing=false;
        }
    }


    if($scope.addressCopy.country!=null)
        $scope.address.countryId = $scope.addressCopy.country.id;
    $scope.saveAddress = function(){
        return $scope.address.$save().$then(function (_address) {
                $modalInstance.close(_address);
        });
    };
    $scope.cancel = function () {
        $scope.address.address1 = $scope.addressCopy.address1;
        $scope.address.address2 = $scope.addressCopy.address2;
        $scope.address.city = $scope.addressCopy.city;
        $scope.address.state = $scope.addressCopy.state;
        $scope.address.zip = $scope.addressCopy.zip;
        $scope.address.country = $scope.addressCopy.country;
        $modalInstance.dismiss();
      };
  }]);

angular.module('projectApp').controller('SocialMediaCtrl', [
  '$scope',
  '$modalInstance',
  'socialMedia',
  function ($scope, $modalInstance,socialMedia) {
    $scope.socialMedia = socialMedia;
    $scope.socialMediaCopy = angular.copy($scope.socialMedia);
    $scope.listSocialMedias = [
             {name: 'Facebook',  url: 'http://facebook.com'},
             {name: 'Instagram', url: 'http://instagram.com'},
             {name: 'Twitter', url: 'http://twitter.com'},
             {name: 'linkedin', url: 'http://linkedin.com'}

    ];
    $scope.saveSocialMedia = function(){
        return $scope.socialMedia.$save().$then(function (_socialMedia) {
                $modalInstance.close(_socialMedia);
        });
    };
    $scope.cancel = function () {
        $scope.socialMedia.name = $scope.socialMediaCopy.name;
        $scope.socialMedia.url = $scope.socialMediaCopy.url;
        $modalInstance.dismiss();
      };
  }]);

angular.module('projectApp').controller('PassportCtrl', [
  '$scope',
  '$modalInstance',
  'UploadFiles',
  'passport',
  'MessageUtils',
  'rslv_countries',
  function ($scope, $modalInstance,UploadFile,passport,Message,rslv_countries) {
    $scope.passport = passport;
    $scope.countries = rslv_countries;
    $scope.passportCopy = angular.copy($scope.passport);
    if($scope.passportCopy.country!=null)
        $scope.passport.countryId = $scope.passportCopy.country.id;
    
    $scope.savePassport = function(){
        return $scope.passport.$save().$then(function (_passport) {
                $modalInstance.close(_passport);
        });
    };
    $scope.cancel = function () {
        $scope.passport.fullName = $scope.passportCopy.fullName;
        $scope.passport.passportNumber = $scope.passportCopy.passportNumber;
        $scope.passport.expirationDate = $scope.passportCopy.expirationDate;

        $modalInstance.dismiss();
      };
    $scope.fileSelected = function(file){
        if (file != null) {
            UploadFile.uploadFile(file).success(function(data, status, headers, config){
            $scope.passport.imageId = data['temp_file']['id'];
            if($scope.passport.id){
              $scope.passport.$save().$then(function (_passport) {
                $scope.imageView = _passport;
                },function(_errors){
                    Message.error(_errors);
                });
            }
          }).progress(function (evt) {
                $scope.progressUploadPassport = parseInt(100.0 * evt.loaded / evt.total);
          });
        }
    };
  }]);

angular.module('projectApp').controller('VisaCtrl', [
  '$scope',
  '$modalInstance',
  'UploadFiles',
  'visa',
  'MessageUtils',
  'rslv_countries',
  function ($scope, $modalInstance,UploadFile,visa,MessageUtils,rslv_countries) {
    $scope.visa = visa;
    $scope.countries = rslv_countries;
    $scope.visaCopy = angular.copy($scope.visa);
    if($scope.visaCopy.country!=null)
        $scope.visa.countryId = $scope.visaCopy.country.id;
    $scope.saveVisa = function(){
        return $scope.visa.$save().$then(function (_visa) {
                $modalInstance.close(_visa);
        });
    };
    $scope.cancel = function () {
        $scope.visa.fullName = $scope.visaCopy.fullName;
        $scope.visa.visaNumber = $scope.visaCopy.visaNumber;
        $scope.visa.expirationDate = $scope.visaCopy.expirationDate;
        $modalInstance.dismiss();
      };
      $scope.fileSelected = function(file){
        if (file != null) {
            UploadFile.uploadFile(file).success(function(data, status, headers, config){
            $scope.visa.imageId = data['temp_file']['id'];
            if($scope.visa.id){
                $scope.visa.$save().$then(function (_visa) {
                    $scope.imageView = _visa;
                    },function(_errors){
                        Message.error(_errors);
                    });
            }
          }).progress(function (evt) {
                $scope.progressUploadPassport = parseInt(100.0 * evt.loaded / evt.total);
          });
        }
    };
  }]);

angular.module('projectApp').controller('AllergyCtrl', [
  '$scope',
  '$modalInstance',
  'allergy',
  'catAllergy',
  'subAllergyCategories',
  'SpecialSubCategoryDetail',
  function ($scope, $modalInstance,allergy,catAllergy, subAllergyCategories,SpecialSubCategoryDetail) {
    $scope.allergy = allergy;
    $scope.allergyCopy = angular.copy($scope.allergy);
    $scope.catAllergy = catAllergy;
    $scope.subAllergyCategories = subAllergyCategories;

    $scope.saveAllergy = function(){
        return $scope.allergy.$save().$then(function(_clientAllergy){
            $modalInstance.close(_clientAllergy);
        });
    };
    $scope.$watch('allergy.specialSubCategoryId', function(){
        if ($scope.allergy!=null){
            if ($scope.allergy.specialSubCategoryId != null){
                SpecialSubCategoryDetail.$search({
                    category_id: $scope.catAllergy, sub_category_id: $scope.allergy.specialSubCategoryId
                }).$then(function(_subAllergyCategories){
                    $scope.subAllergyCategoryDetails = _subAllergyCategories;
                });
            }
            else{
                $scope.subcategoryAllergyLoading = false;
            }
        }
    });
    $scope.cancel = function () {
        $modalInstance.dismiss();
      };
  }]);

angular.module('projectApp').controller('DisabilityCtrl', [
  '$scope',
  '$modalInstance',
  'disability',
  'catDisability',
  'subDisabilityCategories',
  'SpecialSubCategoryDetail',
  function ($scope, $modalInstance,disability,catDisability, subDisabilityCategories,SpecialSubCategoryDetail) {
    $scope.disability = disability;
    $scope.disabilityCopy = angular.copy($scope.disability);
    $scope.catDisability = catDisability;
    $scope.subDisabilityCategories = subDisabilityCategories;

    $scope.saveDisability = function(){
        return $scope.disability.$save().$then(function(_clientDisability){
            $modalInstance.close(_clientDisability);
        });
    };
    $scope.$watch('disability.specialSubCategoryId', function(){
        if ($scope.disability!=null){
            if ($scope.disability.specialSubCategoryId != null){
                SpecialSubCategoryDetail.$search({
                    category_id: $scope.catDisability, sub_category_id: $scope.disability.specialSubCategoryId
                }).$then(function(_subDisabilityCategories){
                    $scope.subDisabilityCategoryDetails = _subDisabilityCategories;
                });
            }
            else{
                $scope.subcategoryAllergyLoading = false;
            }
        }
    });
    $scope.cancel = function () {
        $modalInstance.dismiss();
      };
  }]);

angular.module('projectApp').controller('ClientLoyaltyProgramCtrl', [
  '$scope',
  '$modalInstance',
  'clientLoyaltyPrograms',
  'LoyaltyProgram',
  'clientLoyaltyProgram',
  'rslv_loyalty_programs',
  function ($scope, $modalInstance, clientLoyaltyPrograms, LoyaltyProgram, clientLoyaltyProgram, rslv_loyalty_programs) {
    $scope.clientLoyaltyProgram = clientLoyaltyProgram;
    $scope.clientLoyaltyPrograms = clientLoyaltyPrograms;
    $scope.loyalty_programs = rslv_loyalty_programs;
    $scope.editingClientLoyaltyProgram = false;
    $scope.clientLoyaltyProgramCopy = angular.copy($scope.clientLoyaltyProgram);
    
    $scope.refreshLoyaltyPrograms = function(name) {
      $scope.loyaltyPrograms = LoyaltyProgram.$build();
        return $scope.loyaltyPrograms.searchLoyaltyPrograms(name).then(function(response) {
          return $scope.loyalty_programs = response.data.loyalty_programs;
        });
    };
    $scope.saveClientLoyaltyProgram = function(){
        if ($scope.validateUniqueClientLoyaltyProgram($scope.clientLoyaltyProgram.loyaltyProgram.id)){    
          $scope.clientLoyaltyProgram.loyaltyProgramId = $scope.clientLoyaltyProgram.loyaltyProgram.id;
        }

      return $scope.clientLoyaltyProgram.$save().$then(function (_client_loyalty_program) {
        $modalInstance.close(_client_loyalty_program);
      });
    };

    $scope.validateUniqueClientLoyaltyProgram = function(id){
        var response = true;
        for (var i=0; i<$scope.clientLoyaltyPrograms.length; i++) {
            if (id==$scope.clientLoyaltyPrograms[i].loyaltyProgram.id) 
            {
             $scope.clientLoyaltyPrograms[i].number = $scope.clientLoyaltyProgram.number;
             $scope.clientLoyaltyProgram = $scope.clientLoyaltyPrograms[i];
             response=false;
             break;
            }
          }       
           return response;
    }
    
    $scope.changeClientLoyaltyProgram = function(){
      if ($scope.clientLoyaltyProgramCopy.loyaltyProgram != undefined){
        if ($scope.clientLoyaltyProgram.loyaltyProgram.id != $scope.clientLoyaltyProgramCopy.loyaltyProgram.id ||
          $scope.clientLoyaltyProgram.number != $scope.clientLoyaltyProgramCopy.number){
          $scope.editingClientLoyaltyProgram = true;
        }
        else{
          $scope.editingClientLoyaltyProgram = false;
        }
      }
      else{
        $scope.editingClientLoyaltyProgram = true;
      }
    };

    $scope.cancel = function () {
      if ($scope.clientLoyaltyProgramCopy.loyaltyProgram != undefined){
        $scope.clientLoyaltyProgram.loyaltyProgram.id = $scope.clientLoyaltyProgramCopy.loyaltyProgram.id;
        $scope.clientLoyaltyProgram.number = $scope.clientLoyaltyProgramCopy.number;
      }
      $modalInstance.dismiss();
    };
  }]);

angular.module('projectApp').controller('ClientSheetCtrl', [
    '$scope',
    '$modalInstance',
    'sheetId',
    'Sheet',
    function ($scope, $modalInstance, sheetId, Sheet) {
        var today = function() {
            var now = new Date();
            var day = now.getDate() < 10 ? '0'+now.getDate() : now.getDate();
            var month = (now.getMonth()+1) < 10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1);
            var year = now.getFullYear();
            return year+'-'+month+'-'+day;
        };

        var getColor = function(date) {
            var res = 'timeline-sp1';
            if (new Date(date) < new Date(today())) {
                res = 'timeline-blue';
            } else if (new Date(date) > new Date(today())){
                res = 'timeline-sp2';
            }
            return res;
        };
        
        var addEvent = function (element, date) {
            if ($scope.sheet.timeline[date] == undefined){
                $scope.sheet.timeline[date] = [element];
            } else {
                $scope.sheet.timeline[date] = $scope.sheet.timeline[date].concat(element);
            };
        };

        function buildTimeline() {
            $scope.sheet.timeline = {};
            addEvent({
                title: 'Hoy',
                badgeClass: 'warning',
                badgeIconClass: 'fa fa-thumb-tack',
                color: 'timeline-sp1',
                icon: 'fa fa-thumb-tack'
            }, today());
            $.map($scope.sheet.airServices, function(elem){
                var newElem =   {
                    type: 'air',
                    passengers: elem.passengers,
                    origin_label: elem.originLabel,
                    destination_label: elem.destinationLabel,
                    badgeClass: 'info',
                    badgeIconClass: 'fa fa-plane',
                    color: getColor(elem.date),
                    icon: 'fa fa-plane'
                };
                addEvent(newElem,elem.date);
            });
            $.map($scope.sheet.accomodationServices, function(elem){
                var elementIn =  {
                    type: 'in',
                    people_number: elem.peopleNumber,
                    title: 'Check In: '+elem.accomodationType+' - '+elem.city,
                    badgeClass: 'success',
                    badgeIconClass: 'fa fa-h-square',
                    color: getColor(elem.initDate),
                    icon: 'fa fa-h-square'
                };
                var elementOut =  {
                    type: 'out',
                    people_number: elem.peopleNumber,
                    title: 'Check Out: '+elem.accomodationType+' - '+elem.city,
                    badgeClass: 'danger',
                    badgeIconClass: 'fa fa-h-square',
                    color: getColor(elem.endDate),
                    icon: 'fa fa-h-square'
                };
                addEvent(elementIn,elem.initDate);
                addEvent(elementOut,elem.endDate);
            });
        };
        
        function generateList() {
            $scope.sheet.servicesList =
                $.map( $scope.sheet.airServices, function(elem) {
                    elem.type = 'air'; 
                    return elem; }
                ).concat(
                    $.map( $scope.sheet.accomodationServices, function(elem) { 
                        elem.type = 'accomodation'; 
                        elem.date = elem.initDate; 
                        return elem; }
                         )).concat(
                          $.map( $scope.sheet.vehicleServices, function(elem) { 
                            elem.type = 'car'; 
                            elem.date = elem.startDate; 
                            return elem; 
                          })
                        );
        };
        
        $scope.sheet = Sheet.$find(sheetId).$then(function () {
            buildTimeline();            
            generateList();
        });

        $scope.close = function () {
            $modalInstance.close();
        };

        $scope.timelineClass = function (colorClass, lastDate, lastEvent) {
            var res = colorClass;
            
            if (lastDate && lastEvent) {
                res = res+' timeline-noline';
            }
            
            return res;
        };
    }]);

angular.module('projectApp').controller('CreateClientCtrl', [
  '$scope',
  '$modalInstance',
  'Client',
  'clientSabre',
  function ($scope, $modalInstance, Client, clientSabre) {
    $scope.client = Client.$build({});
    $scope.text = clientSabre.name + ' ' + clientSabre.lastname;
    $scope.textSave = "Register and Link";
    $scope.textExist = "Link";

    $scope.createClient = function(){
      if ($scope.client.id != undefined){
        $modalInstance.close($scope.client);
      }
      else{
        return $scope.client.$save().$then(function(_client){
          $modalInstance.close(_client);
        });
      }
    };
    $scope.cancel = function () {
      $modalInstance.dismiss();
    };
  }]);

projectApp.directive('servicePnrPanel', function ($compile, ReserveSystem, $modal, TravelItinerary, Client, Pnr, PnrClient, $q) {
  return {
    restrict: 'E',
    scope: {
      sheet:"=ngSheet",
      service:"=ngService",
      showOrCreate:"=ngShowCreate"
    },
    replace: true,
    template: '<div class="container-service-mode" ng-class="{\'container-service-mode-hidden\':showSpinner==true}">'+
                '<div class="prsht-panel-menu prsht-panel-menu-sabre">'+
                    '<ul>'+
                        '<li ng-show="thereAreClients==true" class="prsht-detail-nav-item" ng-class="{\'selected\':panelSabre==1, \'disabled\':showSpinner==true}">'+
                            '<a ng-click="panelSabre = 1"><i class="fa fa-users"></i></a>'+
                        '</li>'+
                        '<li ng-show="thereAreServices==true" class="prsht-detail-nav-item" ng-class="{\'selected\':panelSabre==2, \'disabled\':showSpinner==true}">'+
                            '<a ng-click="panelSabre = 2"><i class="fa fa-bars"></i></a>'+
                        '</li>'+
                        '<li ng-show="thereAreFlights==true" class="prsht-detail-nav-item" ng-class="{\'selected\':panelSabre==3, \'disabled\':showSpinner==true}">'+
                            '<a ng-click="panelSabre = 3"><i class="fa fa-plane"></i></a>'+
                        '</li>'+
                        '<li ng-show="thereAreLodgings==true" class="prsht-detail-nav-item" ng-class="{\'selected\':panelSabre==4, \'disabled\':showSpinner==true}">'+
                            '<a ng-click="panelSabre = 4"><i class="fa fa-home"></i></a>'+
                        '</li>'+
                        '<li ng-show="thereAreCars==true" class="prsht-detail-nav-item" ng-class="{\'selected\':panelSabre==5, \'disabled\':showSpinner==true}">'+
                            '<a ng-click="panelSabre = 5"><i class="fa fa-car"></i></a>'+
                        '</li>'+
                    '</ul>'+
                '</div>'+
                '<div class="row-fluid ">'+
                '<div class="col-md-3 prsht-row-form">'+
                  '<div class="form-group" >'+
                     '<label class="col-md-12 control-label lb-client-bg-wht">PNR:</label>'+
                     '<div class="col-md-12">'+
                       '<input type="text" class="form-control" ng-change="enableSearchButton()" ng-model="pnr"/>'+
                     '</div>'+
                  '</div>'+
                '</div>'+
                '<div class="col-md-5 prsht-row-form">'+
                  '<div class="form-group" >'+
                     '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Reserve system</translate>:</label>'+
                     '<div class="col-md-12">'+
                       '<select class="form-control" ng-change="enableSearchButton()" ng-model="selectedSystem" ng-options="reserve.id as reserve.name for reserve in reserve_systems"></select>'+
                     '</div>'+
                  '</div>'+
                '</div>'+
                '<div class="col-md-2" style="padding-top:30px;">'+
                  '<button class="btn blue" ng-disabled="disabledSearchButton" loading-no-restmod="true" loading-click="searchItinerary()" translate>Search</button>'+
                '</div>'+
                '<div class="col-md-12" ng-show="showDivError==true">'+
                        '<span class="prsth-msg-not-items"><span ng-show="divError1.length > 0">{{divError1 | translate}} <span ng-show="pnrAux.length>0">{{pnrAux}}</span></span><span ng-show="divError2.length > 0"> {{divError2 | translate}} {{pnrObject.sheet.id}} - {{pnrObject.sheet.title}}</span> <span ng-show="divError3.length > 0">{{divError3 | translate}}</span></span>'+
                    '</div>'+
                '<div ng-show="panelSabre==1 && showSpinner==false">'+
                  '<div class="row-fluid ">'+
                    '<div class="prsht-title-separator">'+
                        '<h4><translate>Clients</translate></h4>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==true">'+
                        '<span class="prsth-msg-not-items">{{divError}}</span>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==false && travelItinerary == null && matchClients.length == 0">'+
                        '<span class="prsth-msg-not-items" translate>There are no clients</span>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-container-table table-customers" ng-show="showDivError==false && (travelItinerary.customers.length > 0 || matchClients.length > 0)">'+
                        '<table class="aui">'+
                        '  <thead>'+
                        '    <tr>'+
                        '      <th translate>PNR Client</th>'+
                        '      <th translate>Sheet Client</th>'+
                        '      <th translate>Email</th>'+
                        '      <th></th>'+
                        '    </tr>'+
                        '  </thead>'+
                        '    <tbody>'+
                        '    <tr ng-repeat="client in matchClients track by $index">'+
                        '      <td>'+
                        '        <span >{{ client.clientpnr.name }} {{ client.clientpnr.lastname }}</span>'+
                        '      </td>'+
                        '      <td>'+
                        '        <span >{{ client.client.firstName }} {{ client.client.lastName }}</span>'+
                        '      </td>'+
                        '      <td>'+
                        '        <span >{{ client.client.email }}</span>'+
                        '      </td>'+
                        '      <td>'+
                        '      </td>'+
                        '      </tr>'+
                        '    <tr ng-repeat="customer in travelItinerary.customers track by $index">'+
                        '      <td>'+
                        '        <span >{{ customer.name }} {{ customer.lastname }}</span>'+
                        '      </td>'+
                        '      <td>'+
                        '      </td>'+
                        '      <td>'+
                        '      </td>'+
                        '      <td>'+
                        '        <div ng-show="matchedPnr==false"><i ng-click="setItemAction($index)" class="icon-match-client fa fa-sort-desc"></i>'+
                        '      <div id="help-{{$index}}" class="match-clients-div">'+
                        '        <div class="match-client-inside-menu"><span class="text-match-client-inside-menu" translate>Match to...</span></div>'+
                        '        <ul ng-show="sheet.clients.length > 0" class="tabs-client-match">'+
                        '          <li ng-click="matchClient(client)" ng-repeat="client in sheet.clients track by $index"><span class="match-icon-li-move icon-user"></span><span class="match-text-li-move">{{client.firstName}} {{client.lastName}}</span></li>'+
                        '        </ul>'+
                        '        <div ng-click="openCreateClient(customer)" class="match-create-inside-menu"><span class="match-icon-create-inside-menu"><i class="fa fa-plus"></i></span><span class="match-text-create-inside-menu" translate>New client...</span></div>'+
                        '      </div></div>'+
                        '      </td>'+
                        '    </tr>'+
                        '  </tbody>'+
                        '</table>'+
                    '</div>'+
                '</div>'+
                '</div>'+
                '<div ng-show="panelSabre==2 && showSpinner==false">'+
                  '<div class="row-fluid ">'+
                    '<div class="prsht-title-separator">'+
                        '<h4><translate>Services</translate></h4>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==true">'+
                        '<span class="prsth-msg-not-items">{{divError}}</span>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==false && (travelItinerary == null || travelItinerary.services.length == 0)">'+
                        '<span class="prsth-msg-not-items" translate>There are no services</span>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-container-table" ng-show="showDivError==false && travelItinerary.services.length > 0">'+
                        '<table class="aui">'+
                        '  <thead>'+
                        '    <tr>'+
                        '      <th translate>Description</th>'+
                        '      <th translate>Type</th>'+
                        '    </tr>'+
                        '  </thead>'+
                        '    <tbody>'+
                        '    <tr ng-repeat="service in travelItinerary.services track by $index">'+
                        '      <td>'+
                        '        <span ng-if="service.type==\'flight\'">{{ service.origin_location.location_code }} - {{ service.destination_location.location_code }} / {{ service.departure_date_time }} - {{ service.arrival_date_time }}</span>'+
                        '        <span ng-if="service.type==\'hotel\'">{{ service.hotel_city_code }} / {{ service.hotel_name }} / {{ service.start_date }} - {{ service.end_date }}</span>'+
                        '        <span ng-if="service.type==\'vehicle\'">{{ service.vehicle_location_detail.location_code }} / {{ service.vehicle_location_detail.pick_up_date_time }} - {{ service.vehicle_location_detail.return_date_time }}</span>'+
                        '      </td>'+
                        '      <td>'+
                        '        <span >{{ service.type }}</span>'+
                        '      </td>'+
                        '    </tr>'+
                        '  </tbody>'+
                        '</table>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                '<div ng-show="panelSabre==3 && showSpinner==false">'+
                  '<div class="row-fluid ">'+
                    '<div class="prsht-title-separator">'+
                        '<h4><translate>Flights</translate></h4>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==true">'+
                        '<span class="prsth-msg-not-items">{{divError}}</span>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==false && (travelItinerary == null || (travelItinerary.services | filter:{type:\'flight\'}).length == 0)">'+
                        '<span class="prsth-msg-not-items" translate>There are no flights</span>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-container-table" ng-show="showDivError==false && (travelItinerary.services | filter:{type:\'flight\'}).length > 0">'+
                        '<table class="aui">'+
                        '  <thead>'+
                        '    <tr>'+
                        '      <th translate>Description</th>'+
                        '    </tr>'+
                        '  </thead>'+
                        '    <tbody>'+
                        '    <tr ng-repeat="service in travelItinerary.services | filter:{type:\'flight\'} track by $index">'+
                        '      <td>'+
                        '        <span>{{ service.origin_location.location_code }} - {{ service.destination_location.location_code }} / {{ service.departure_date_time }} - {{ service.arrival_date_time }}</span>'+
                        '      </td>'+
                        '    </tr>'+
                        '  </tbody>'+
                        '</table>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                '<div ng-show="panelSabre==4 && showSpinner==false">'+
                  '<div class="row-fluid ">'+
                    '<div class="prsht-title-separator">'+
                        '<h4><translate>Lodgings</translate></h4>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==true">'+
                        '<span class="prsth-msg-not-items">{{divError}}</span>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==false && (travelItinerary == null || (travelItinerary.services | filter:{type:\'hotel\'}).length == 0)">'+
                        '<span class="prsth-msg-not-items" translate>There are no lodgings</span>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-container-table" ng-show="showDivError==false && (travelItinerary.services | filter:{type:\'hotel\'}).length > 0">'+
                        '<table class="aui">'+
                        '  <thead>'+
                        '    <tr>'+
                        '      <th translate>Description</th>'+
                        '    </tr>'+
                        '  </thead>'+
                        '    <tbody>'+
                        '    <tr ng-repeat="service in travelItinerary.services | filter:{type:\'hotel\'} track by $index">'+
                        '      <td>'+
                        '        <span>{{ service.hotel_city_code }} / {{ service.hotel_name }} / {{ service.start_date }} - {{ service.end_date }}</span>'+
                        '      </td>'+
                        '    </tr>'+
                        '  </tbody>'+
                        '</table>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                '<div ng-show="panelSabre==5 && showSpinner==false">'+
                  '<div class="row-fluid ">'+
                    '<div class="prsht-title-separator">'+
                        '<h4><translate>Vehicles</translate></h4>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==true">'+
                        '<span class="prsth-msg-not-items">{{divError}}</span>'+
                    '</div>'+
                    '<div class="col-md-12" ng-show="showDivError==false && (travelItinerary == null || (travelItinerary.services | filter:{type:\'vehicle\'}).length == 0)">'+
                        '<span class="prsth-msg-not-items" translate>There are no vehicles</span>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-container-table" ng-show="showDivError==false && (travelItinerary.services | filter:{type:\'vehicle\'}).length > 0">'+
                        '<table class="aui">'+
                        '  <thead>'+
                        '    <tr>'+
                        '      <th translate>Description</th>'+
                        '    </tr>'+
                        '  </thead>'+
                        '    <tbody>'+
                        '    <tr ng-repeat="service in travelItinerary.services | filter:{type:\'vehicle\'} track by $index">'+
                        '      <td>'+
                        '        <span>{{ service.vehicle_location_detail.location_code }} / {{ service.vehicle_location_detail.pick_up_date_time }} - {{ service.vehicle_location_detail.return_date_time }}</span>'+
                        '      </td>'+
                        '    </tr>'+
                        '  </tbody>'+
                        '</table>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                '<div ng-show="showDivError==false && showSpinner==false && panelSabre!=-1" class="col-md-12" style="padding-top:30px;">'+
                  '<button class="btn blue" loading-no-restmod="true" loading-click="confirmPnr()" translate>Confirm PNR</button>'+
                '</div>'+
                '</div>'+
                '</div>'+
              '</div>',
    controller : function($scope, $element, $attrs){
      $('.prsht-container-nav').click(function(e){
        var target = $(e.target);
        if (!target.is('.container-service-mode .fa-sort-desc') &&
          !target.is('.container-service-mode .match-clients-div div') &&
          !target.is('.container-service-mode .match-clients-div div span') &&
          !target.is('.container-service-mode .match-clients-div .tabs-client-match') &&
          !target.is('.container-service-mode .match-clients-div .tabs-client-match li') &&
          !target.is('.container-service-mode .match-clients-div .tabs-client-match li span') && 
          !target.is('.container-service-mode .match-clients-div .tabs-client-match div')){
          $scope.resetView();
        }
      });
      $scope.optionMode = -1;
      $scope.reserve_systems = ReserveSystem.$search({});
      $scope.selectedSystem = "";
      $scope.pnr = "";
      $scope.disabledSearchButton = true;
      $scope.selectedName = "";
      $scope.panelSabre = -1;
      $scope.travelItinerary = null;
      $scope.showDivError = false;
      $scope.divError = "";
      $scope.showSpinner = false;
      $scope.auxTravelItinerary = TravelItinerary.$build();
      $scope.matchClients = [];
      $scope.itemAction = -1;
      $scope.origHeight = -1;
      $scope.thereAreClients = false;
      $scope.thereAreFlights = false;
      $scope.thereAreLodgings = false;
      $scope.thereAreCars = false;
      $scope.thereAreServices = false;

      $scope.enableSearchButton = function(){
        if ($scope.pnr != "" && $scope.selectedSystem != "" && $scope.selectedSystem != 2){
          $scope.disabledSearchButton = false;
        }
        else $scope.disabledSearchButton = true;
      };
      $scope.searchItinerary = function(){
        $scope.matchedPnr = false;
        for (var i = 0; i < $scope.reserve_systems.length; ++i){
          if ($scope.reserve_systems[i].id == $scope.selectedSystem)
            $scope.selectedName = $scope.reserve_systems[i].name.toLowerCase();
        }
        if ($scope.selectedName != ""){
          $scope.showSpinner = true;
          $scope.thereAreClients = false;
          $scope.thereAreFlights = false;
          $scope.thereAreLodgings = false;
          $scope.thereAreCars = false;
          $scope.thereAreServices = false;
          $scope.showDivError = false;
          $scope.divError = "";
          $scope.divError1 = "";
          $scope.divError2 = "";
          $scope.divError3 = "";
          $scope.pnrAux = "";
          $scope.panelSabre = -1;
          return $scope.auxTravelItinerary.getPNRItinerary($scope.pnr, $scope.selectedName).then(function(response){
            $scope.travelItinerary = response.data.pnrs;
            $scope.pnrObject = Pnr.$build({});
            $scope.pnrObject.getByCode($scope.pnr).then(function(response){
              $scope.pnrObject = response.data.pnr;
              if ($scope.pnrObject != undefined){
                if ($scope.pnrObject.sheet_id != $scope.sheet.id){
                  $scope.matchedPnr = true;
                  $scope.divError1 = 'The PNR';
                  $scope.pnrAux = $scope.pnr;
                  $scope.divError2 = 'is already in use by the sheet';
                  $scope.divError3 = '. Please, try with another one.';
                  $scope.showDivError = true;
                  $scope.showSpinner = false;
                  $scope.matchClients = [];
                  $scope.panelSabre = -1;
                }
                else if ($scope.pnrObject.sheet_id == $scope.sheet.id){
                  $scope.$parent.$parent.addAlert('PNR has been already associated to this sheet.','danger',true,true,10);
                  $scope.checkClientsServices();
                  $scope.showDivError = false;
                  $scope.panelSabre = 1;
                  $scope.showSpinner = false;
                  $scope.matchClients = [];
                  $scope.divError = "";
                }
              }
              else{
                $scope.checkClientsServices();
                $scope.showDivError = false;
                $scope.panelSabre = 1;
                $scope.showSpinner = false;
                $scope.matchClients = [];
                $scope.divError = "";
              }
            });
            }, function(response){
              $scope.divError1 = "Internal Server Error";
              $scope.showDivError = true;
              $scope.panelSabre = -1;
              $scope.showSpinner = false;
          });
        }
      };
      $scope.checkClientsServices = function(){
        if ($scope.travelItinerary.customers.length > 0)
          $scope.thereAreClients = true;
        for(var i = 0; i < $scope.travelItinerary.services.length; ++i){
          $scope.thereAreServices = true;
          if ($scope.travelItinerary.services[i].type == "flight")
            $scope.thereAreFlights = true;
          if ($scope.travelItinerary.services[i].type == "hotel")
            $scope.thereAreLodgings = true;
          if ($scope.travelItinerary.services[i].type == "vehicle")
            $scope.thereAreCars = true;
          if ($scope.thereAreClients == true && $scope.thereAreCars == true && $scope.thereAreLodgings == true
            && $scope.thereAreFlights == true && $scope.thereAreServices == true)
            break;
        }
      };
      $scope.matchClient = function(_client){
        if ($scope.itemAction != -1){
          $scope.matchClients.push({'client':'','clientpnr':''});
          $scope.matchClients[$scope.matchClients.length - 1].client = _client;
          $scope.matchClients[$scope.matchClients.length - 1].clientpnr = $scope.travelItinerary.customers[$scope.itemAction];
          $scope.travelItinerary.customers.splice($scope.itemAction, 1);
          $scope.resetView();
        }
      };
      $scope.confirmPnr = function(){
        var deferred = $q.defer();
        if ($scope.pnrObject == undefined){
          if ($scope.travelItinerary.customers.length > 0){
            $scope.$parent.$parent.addAlert('There are clients PNR without association.','danger',true,true,10);
            deferred.reject();
            return deferred.promise;
          }
          else{
            if ($scope.matchClients.length > 0){
              $scope.sentClients = [];
              for (var i = 0; i < $scope.matchClients.length; ++i){
                $scope.sentClients.push({'client_id':$scope.matchClients[i].client.id,'clientpnr':$scope.matchClients[i].clientpnr});
              }
              $scope.newPnr = Pnr.$build({});
              return $scope.newPnr.associatePnrSheet($scope.sheet.id, $scope.pnr, $scope.sentClients, $scope.travelItinerary.services).then(function(response){
                $scope.pnrObject = response.data.pnr;
                $scope.$parent.$parent.refreshServices();
                $scope.$parent.$parent.addAlert('PNR successfully associated.','success',true,true,10);
              }, function(response){
                $scope.$parent.$parent.addAlert('PNR association failed. Please, try again.','danger',true,true,10);
              });
            }
          }
        }
        else{
          if ($scope.pnrObject.sheet_id == $scope.sheet.id){
            $scope.$parent.$parent.addAlert('PNR has been already associated to this sheet.','danger',true,true,10);
            deferred.reject();
            return deferred.promise;
          }
        }
      };
      $scope.setItemAction = function(_index){
        var aux = $('#help-'+_index).css('display');
        if ($scope.origHeight == -1){
            $scope.origHeight = $('.table-customers').css('height');
            $scope.origHeight = parseInt($scope.origHeight);
        }
        var newHeight = $scope.origHeight;
        newHeight += 360;
        if (aux == 'block'){
          $element.find('#help-'+_index).css('display', 'none');
          $('.table-customers').height('inherit');
          $scope.origHeight = -1;
          $scope.itemAction=-1;
        }
        else{
          $element.find('.match-clients-div').css('display','none');
          $element.find('#help-'+_index).css('display', 'block');
          $('.table-customers').height(newHeight);
          $scope.itemAction=_index;
        }
      };
      $scope.resetView = function(){
        $scope.itemAction = -1;
        $element.find('.match-clients-div').css('display','none');
        if ($scope.origHeight != -1){
          $('.table-customers').height('inherit');
          $scope.origHeight = -1;
        }
      };
      $scope.openCreateClient = function(_clientSabre){
        var modalInstance = $modal.open({
          animation: false,
          template: '<div class="modal-content">'+
                        '<div class="modal-header">'+
                            '<a ng-click="cancel()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                            '<h5 translate>New Client</h5>'+
                        '</div>'+
                        '<div class="modal-body body-details-client">'+
                          '<div class="container" style="padding-top: 25px;" >'+
                            '<client-min ng-client="client" ng-text-save="textSave" ng-text-exist="textExist" ng-text-note="text" ng-save="createClient" ng-cancel="cancel"></client-min>'+
                          '</div>'+
                        '</div>'+
                    '</div>',
          controller: 'CreateClientCtrl',
          size: 'lg',
          resolve: {
            clientSabre: function(){
              return _clientSabre;
            }
          }
        });
        modalInstance.result.then(function (_client){
          return $scope.sheet.addClients([_client.id],"").then(function(){
            var exist = false;
            for(var i = 0; i < $scope.sheet.clients.length; ++i){
               if ($scope.sheet.clients[i].id == _client.id){
                 exist = true;
                 break;
               } 
            }
            if (!exist) $scope.sheet.clients.$add(_client);
            $scope.matchClient(_client);
          });
        }, function () {});
      };
    },
    link : function($scope, $element, $attrs){
      
    }
  }
});

projectApp.directive('servicePanel', function ($compile) {
    return {
        restrict: 'E',
        scope :{
            sheet:"=ngSheet",
            serviceT:"=ngService",
            confirmServiceNote:"=ngConfirmServiceNote",
            showOrCreate:"=ngShowCreate"
        },
        template:   '<div class="prsht-header">'+
                    '<label ng-show="( (service == null|| originFromNote) && confirmServiceNote.id != undefined)" class="col-md-12 labelCommand noteCommandServices">{{confirmServiceNote.commandText}}</label>'+
                    '<label ng-show="service.commandText != undefined" class="col-md-12 labelCommand noteCommandServices">{{service.commandText}}</label>'+

                    '<div ng-show="showOrCreate==true" class="col-md-12 prsht-row-form">'+
                        '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Select service mode</translate>:</label>'+
                        '<div class="col-md-12">'+
                             '<label class="radio-services-type">'+
                                 '<span ng-class="{\'checked\':optionMode==1}">'+
                                   '<input type="radio" name="serviceMode" value="" ng-click="optionMode=1; insertModePanel()"/><translate>Manual service</translate></span>'+
                             '</label>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                             '<label class="radio-services-type">'+
                                 '<span ng-class="{\'checked\':optionMode==2}">'+
                                   '<input type="radio" name="serviceMode" value="" ng-click="optionMode=2; insertModePanel()"/><translate>GDS</translate></span>'+
                             '</label>'+
                        '</div>'+
                        '<div class="col-md-12">'+
                             '<label class="radio-services-type">'+
                                 '<span ng-class="{\'checked\':optionMode==3}">'+
                                   '<input ng-disabled="true" type="radio" name="serviceMode" value="" ng-click="optionMode=3; insertModePanel()"/><translate>Existing services</translate></span>'+
                             '</label>'+
                       '</div>'+
                    '</div>'+
                    '<div ng-show="service.id==undefined && (showOrCreate==false || showButtonOpt==true)" class="col-md-12 prsht-row-form">'+
                            '<div class="form-group" >'+
                                    '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Select service type</translate>:</label>'+
                                    '<div class="col-md-12">'+
                                        '<div class="clearfix prsht-header-service-type-list">'+
                                            '<div class="btn-group" >'+
                                                '<button type="button" ng-click="selectServiceDirective(type);" ng-class="{\'active\':typeServiceSelected===type.id}" ng-repeat="type in listTypeServices" class="btn blue3">{{type.name | translate}}</button>'+
                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="prsht-container-nav">'+
                    '</div>',
        controller : function($scope, $element, $attrs){
            $scope.service=null;
            $scope.optionMode = -1;
            $scope.showButtonOpt = false;
            $scope.listCommandServices = [];
            $scope.listCommandServices["#flight"] = 'air';
            $scope.listCommandServices["#lodging"] = 'accomodation';
            $scope.listTypeServices = [
                                        {id:'air',name:"Flight"},
                                        {id:'accomodation',name:"Accomodation"}
            ];

            if($scope.serviceT!=undefined){
                $scope.showButtonOpt = true;
                $scope.service = $scope.serviceT;
                $scope.typeServiceSelected = $scope.service.type;
                $element.find(".prsht-container-nav").html($compile('<'+$scope.service.type+'-service-panel ng-show-create="showOrCreate" ng-service="service" ng-sheet="sheet" ></'+$scope.service.type+'-service-panel>')($scope));
            
            }
            $scope.selectServiceDirective = function(type){
                $scope.typeServiceSelected = type.id;     
                $scope.service = $scope.sheet[type.id + 'Services'].$build({}); 
                $element.find(".prsht-container-nav").html($compile('<'+$scope.typeServiceSelected+'-service-panel ng-show-create="showOrCreate" ng-service="service" ng-sheet="sheet" ></'+$scope.typeServiceSelected+'-service-panel>')($scope));
            
            }
            $scope.insertModePanel = function(){
                $scope.originFromNote=false;

                if ($scope.optionMode == 1){
                  $scope.showButtonOpt = true;
                  $element.find(".prsht-container-nav").html('');
                  
                  //si viene de una nota verifica que tipo de nota es para 
                  //renderizar la vista
                  if($scope.confirmServiceNote.typeCommand!==undefined){
                    $scope.showButtonOpt = false;
                    $scope.originFromNote=true;
                    $scope.typeServiceSelected = $scope.listCommandServices[$scope.confirmServiceNote.typeCommand];
                    $scope.service = $scope.sheet[$scope.typeServiceSelected + 'Services'].$build({}); 
                    $element.find(".prsht-container-nav").html($compile('<'+$scope.typeServiceSelected+'-service-panel ng-show-create="showOrCreate" ng-delete-sheet-detail-id="sheet.deleteSheetDetailId" ng-delete-sheet-detail-note-id="sheet.deleteSheetDetailNoteId" ng-confirm-service-note="confirmServiceNote" ng-service="service" ng-sheet="sheet" ></'+$scope.typeServiceSelected+'-service-panel>')($scope));
                  }

                }
                else{
                  $scope.typeServiceSelected = -1;
                  $scope.showButtonOpt = false;
                  $element.find(".prsht-container-nav").html('');
                  $element.find(".prsht-container-nav").html($compile('<service-pnr-panel ng-show-create="showOrCreate" ng-service="service" ng-sheet="sheet" ></service-pnr-panel>')($scope));
                }
              };       
        },
        link : function ($scope, $element, $attrs) {
             $element.css({
                          'height':'inherit'
                        });
        }
    }
});

projectApp.directive('airServicePanel', function ($compile,gettextCatalog,AirPort,MessageUtils, SheetDetailNote) {
    return {
        restrict: 'E',
        scope :{
            sheet:"=ngSheet",
            service:"=ngService",
            confirmServiceNote:"=ngConfirmServiceNote",
            deleteSheetDetailId:"=ngDeleteSheetDetailId",
            deleteSheetDetailNoteId:"=ngDeleteSheetDetailNoteId",
            showOrCreate:"=ngShowCreate"
        },
        replace: true,
        template:   '<form name="airServiceForm" id="airServiceForm">'+
                    '<div class="col-md-12 prsht-row-form">'+
                        '<div class="form-group">'+
                                '<label ng-show="service.type===undefined" class="col-md-12 control-label lb-client-bg-wht" ><translate>Service Type</translate>: {{\'Air\' | translate }}</label>'+
                                '<label ng-show="service.type!==undefined" class="col-md-12 control-label lb-client-bg-wht" ><translate>Service Type</translate>: {{service.type | translate | capitalize }}</label>'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Origin</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<div class="input-icon right input-search-airPort">'+
                                        '<i class="fa fa-search"></i>'+
                                        '<script type="text/ng-template" id="customTemplateAirPort.html">'+
                                            '<a>'+
                                                '<span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>'+
                                            '</a>'+
                                        '</script>'+
                                        '<input   name="origin" class="pull-right  form-control"  '+
                                            'typeahead-input-formatter="submitSearchOrigin($model)" type="text" '+
                                            'ng-model="originSelected" placeholder="{{labelOrigin}}" typeahead="airport as (airport.city+\',  \'+airport.countryName+\' - \'+airport.name+\'(\'+airport.fs+\')\')    for airport in searchAirport($viewValue)" typeahead-loading="loadingLocations"  typeahead-template-url="customTemplateAirPort.html">'+
                                        '<i ng-show="loadingLocations" class="glyphicon glyphicon-refresh pull-right" style="right:25px;"></i>'+
                                    '</div>'+
                                    '<div form-input form-error="service.errors.origin" ng-class="{\'has-error\': service.errors.origin}">'+                                
                                    '</div>'+
                                '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-row-form">'+
                        '<div class="form-group">'+
                                '<label class="col-md-12 control-label lb-client-bg-wht" ><translate>Destination</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<div class="input-icon right input-search-airPort">'+
                                        '<i class="fa fa-search"></i>'+
                                        '<input  name="destination"  class="pull-right  form-control"  '+
                                            'typeahead-input-formatter="submitSearchDestination($model)" type="text" '+
                                            'ng-model="destinationSelected" placeholder="{{labelDestination}}" typeahead="airport as (airport.city+\',  \'+airport.countryName+\' - \'+airport.name+\'(\'+airport.fs+\')\')      for airport in searchAirport($viewValue)" typeahead-loading="loadingLocationsb"  typeahead-template-url="customTemplateAirPort.html">'+
                                        '<i ng-show="loadingLocationsb" class="glyphicon glyphicon-refresh pull-right" style="right:25px;"></i>'+
                                    '</div>'+
                                    '<div form-input form-error="service.errors.destination" ng-class="{\'has-error\': service.errors.destination}">'+                                
                                    '</div>'+
                                '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-4 prsht-row-form">'+
                        '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>N° Passengers</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<input  required="" name="passengers" ng-model="service.passengers" min="1" type="number" class="form-control">'+
                                    '<div form-input form-error="service.errors.passengers" ng-class="{\'has-error\': service.errors.passengers}">'+
                                    '</div>'+
                                '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-8 prsht-row-form">'+
                        '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Date</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<div  required="" name="date" sb-date-select sb-select-class="form-control " min="{{today}}" max="2020-12-31" class="prsht-sb-date-select" ng-model="service.date"></div>'+
                                    '<div form-input form-error="service.errors.date" ng-class="{\'has-error\': service.errors.date}">'+
                                    '</div>'+
                                '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12 prsht-row-form">'+
                        '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Observations</translate></label>'+
                                '<div class="col-md-12">'+
                                    '<textarea class="form-control" rows="3" ng-model="service.notes"></textarea>'+
                                '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-12 cont-buttons-client prsht-row-button"><!--{{airServiceForm}} {{airServiceForm.$valid}}-->'+
                        '<button type="button" class="btn blue pull-right" ng-class="{\'disabled\':(!airServiceForm.$valid || service.origin ==null || service.destination ==null) }"  loading-click="saveService(airServiceForm)" ><translate >Save</translate></button> '+
                    '</div>'+
                    '</form>',
        controller : function($scope, $element, $attrs){
            window.myscope = $scope.service;

            $scope.airport = AirPort.$build();
            $scope.today = moment().format("YYYY-MM-DD");
            var i=0;


            $scope.submitSearchOrigin = function(airport){

                if(airport!==undefined){
                      $scope.labelOrigin = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
                      $scope.service.origin= airport.id;
                      $scope.service.originLabel = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
                  }else{
                    if($scope.service.origin!==undefined){
                        $scope.labelOrigin = $scope.service.originLabel;
                    }else{
                         $scope.labelOrigin =  gettextCatalog.getString('Search AirPort');
                         $scope.service.origin= null;
                         $scope.service.originLabel = null;
                    }

                 }
            };
            $scope.searchAirport = function(val){
                return $scope.airport.searchAirPort(val).then(function(_airports){
                    return _airports.data.air_ports;
                });
            };

             $scope.submitSearchDestination = function(airport){
               
                if(airport!==undefined){
                   $scope.labelDestination = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
                   $scope.service.destination= airport.id;
                   $scope.service.destinationLabel = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";;
                }else{
                    if($scope.service.destination!==undefined){
                        $scope.labelDestination = $scope.service.destinationLabel;
                    }else{
                      $scope.labelDestination =  gettextCatalog.getString('Search AirPort');
                      $scope.service.destination= null;
                      $scope.service.destinationLabel = null;
                    }
                
                }
            };

            $scope.saveService = function(form){
                if(form.$valid){
                    
                    if($scope.confirmServiceNote!==undefined){
                        
                        $scope.service.commandText = $scope.confirmServiceNote.commandText;
                        $scope.confirmServiceNote.commandText ="";
                       
                    }
                    return $scope.service.$save().$then(
                       
                       function(_service){
                        if ($scope.confirmServiceNote != undefined){
                            var id_sheet_detail = $scope.confirmServiceNote.sheetDetailId;
                            var id_sheet_detail_note = $scope.confirmServiceNote.id;
                            $scope.confirmServiceNote.$destroy().$then(function(_sheet_detail_note){
                            $scope.removeConfirmService(id_sheet_detail, _sheet_detail_note);
                            $scope.confirmServiceNote = SheetDetailNote.$build({});
                            $scope.deleteSheetDetailId = id_sheet_detail;
                            $scope.deleteSheetDetailNoteId = id_sheet_detail_note;
                          });
                        }
                        $scope.$parent.$parent.refreshServices();
                        $scope.$parent.$parent.addAlert('saved successful','success',true,true,10);
                        $scope.$parent.$parent.panelOpened=false;
                      },function(_client){
                        $scope.$parent.$parent.addAlert(MessageUtils.getStringError(_client.errors),'warning',false,true,10);
                      });
                }else{
                    return true;
                }

            };

            $scope.removeConfirmService = function(id, sheetDetailNote){
              for (var i = 0; i < $scope.sheet.sheetDetails.length; ++i){
                if ($scope.sheet.sheetDetails[i].id == id){
                  $scope.sheet.sheetDetails[i].sheetDetailNotes.$remove(sheetDetailNote);
                  break;
                }
              }
            };
                            
        },
        link : function ($scope, $element, $attrs) {
             $element.css({
                          'overflow':'auto',
                          'padding-bottom':'160px'
                        });
        }
    }
});




projectApp.directive('accomodationServicePanel', function ($compile,MessageUtils,Address,gettextCatalog, SheetDetailNote) {
    return {
        restrict: 'E',
        scope :{
            sheet:"=ngSheet",
            service:"=ngService",
            confirmServiceNote:"=ngConfirmServiceNote",
            deleteSheetDetailId:"=ngDeleteSheetDetailId",
            deleteSheetDetailNoteId:"=ngDeleteSheetDetailNoteId",
            showOrCreate:"=ngShowCreate"
        },
        replace: true,
        template:   '<form name="accomodationServiceForm" id="accomodationServiceForm" >'+
                        '<div class="col-md-12 prsht-row-form">'+
                            '<div class="form-group">'+
                                '<label ng-show="service.type===undefined" class="col-md-12 control-label lb-client-bg-wht" ><translate>Service Type</translate>: {{\'Accomodation\' | translate }}</label>'+
                                '<label ng-show="service.type!==undefined" class="col-md-12 control-label lb-client-bg-wht" ><translate>Service Type</translate>: {{service.type | translate | capitalize }}</label>'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>City</translate><span class="required" aria-required="true">*</span></label>'+
                                    '<div class="col-md-12">'+
                                        '<ui-select name="city" id="selectCity" required="" ng-model="address.selected"'+
                                            'theme="bootstrap"'+
                                            'reset-search-input="true"'+
                                            'style="width: 100%;"  ng-enter="setValue(address.selected,$select)" ng-blur-select="alert();setValue(address.selected,$select)" on-select="setValue(address.selected,$select)">'+
                                            '<ui-select-match allow-clear="false" placeholder="{{\'Enter an city...\' | translate}}">{{$select.selected.data.formatted_address}}</ui-select-match>'+
                                            '<ui-select-choices repeat="address in addresses track by $index"'+
                                                'refresh="refreshAddresses($select.search)"'+
                                                'refresh-delay="0">'+
                                                '<div ng-bind-html="address.data.formatted_address | highlight: $select.search"></div>'+
                                            '</ui-select-choices>'+
                                        '</ui-select>'+
                                        '<div form-input form-error="service.errors.city" ng-class="{\'has-error\': service.errors.city}">'+
                                        '</div>'+
                                    '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 prsht-row-form">'+
                            '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Type</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<input required="" name="accomodationType" ng-model="service.accomodationType" type="text" class="form-control">'+
                                    '<div form-input form-error="service.errors.accomodationType" ng-class="{\'has-error\': service.errors.accomodationType}">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-6 prsht-row-form">'+
                            '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Number of People</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<input required="" name="peopleNumber" ng-model="service.peopleNumber" type="number"  min="1" class="form-control">'+
                                        '<div form-input form-error="service.errors.peopleNumber" ng-class="{\'has-error\': service.errors.peopleNumber}">'+
                                        '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 prsht-row-form">'+
                            '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Beggins At</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<div  required="" name="initDate" sb-date-select sb-select-class="form-control " min="{{today}}" max="2020-12-31" class="prsht-sb-date-select" ng-model="service.initDate"></div>'+
                                    '<div form-input form-error="service.errors.initDate" ng-class="{\'has-error\': service.errors.initDate}">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 prsht-row-form">'+
                            '<div class="form-group" >'+
                                '<label required="" class="col-md-12 control-label lb-client-bg-wht"><translate>Ends At</translate><span class="required" aria-required="true">*</span></label>'+
                                '<div class="col-md-12">'+
                                    '<div disabled="!service.initDate"  required="" name="endDate" sb-date-select sb-select-class="form-control " ng-min="service.initDate" max="2025-12-31" class="prsht-sb-date-select" ng-model="service.endDate"></div>'+
                                    '<div form-input form-error="service.errors.endDate" ng-class="{\'has-error\': service.errors.endDate}">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 prsht-row-form">'+
                            '<div class="form-group" >'+
                                '<label class="col-md-12 control-label lb-client-bg-wht"><translate>Observations</translate></label>'+
                                '<div class="col-md-12">'+
                                    '<textarea class="form-control" rows="3" ng-model="service.notes"></textarea>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-md-12 cont-buttons-client prsht-row-button"><!--{{accomodationServiceForm}} {{accomodationServiceForm.$valid}}{{service}}-->'+
                            '<button type="button" class="btn blue pull-right" ng-class="{\'disabled\':(!accomodationServiceForm.$valid) }"  loading-click="saveService(accomodationServiceForm)" ><translate >Save</translate></button> '+
                        '</div>'+
                    '</form>',
        controller : function($scope, $element, $attrs){
            $scope.today = moment().format("YYYY-MM-DD");   
            $scope.address = {};

            $scope.setValue = function(value,input){
                if(value==undefined){
                    input.search="";
                    $scope.address.selected = "";
                } else {
                    if (value.data) {
                        Address.getAddressGlobal(value.data.formatted_address).then(function(response) {
                            if (response.data.addresses == undefined || (response.data.undefined && response.data.addresses.length == 0)) {
                                input.search="";
                                $scope.address.selected = "";
                            } 
                        });
                    } else {
                        input.search="";
                        $scope.address.selected = "";
                    }
                }
             };
            $scope.refreshAddresses = function(address) {
                var params = {address: address, sensor: false};
                return Address.getAddressGlobal(address).then(function(response) {
                    $scope.addresses = response.data.addresses;
                });
            };
            if ($scope.service.city)
                $scope.address.selected= {data:{formatted_address:$scope.service.city}};
           
            $scope.saveService = function(form){
                if(form.$valid){
                    
                    if($scope.confirmServiceNote!==undefined){
                        $scope.service.commandText = $scope.confirmServiceNote.commandText;
                        $scope.confirmServiceNote.commandText ="";
                    }


                    $scope.service.city = $scope.address.selected.data.formatted_address;
                    return $scope.service.$save().$then(
                        function(_service){
                            if ($scope.confirmServiceNote != undefined){
                                var id_sheet_detail = $scope.confirmServiceNote.sheetDetailId;
                                var id_sheet_detail_note = $scope.confirmServiceNote.id;
                                $scope.confirmServiceNote.$destroy().$then(function(_sheet_detail_note){
                                $scope.removeConfirmService(id_sheet_detail, _sheet_detail_note);
                                $scope.confirmServiceNote = SheetDetailNote.$build({});
                                $scope.deleteSheetDetailId = id_sheet_detail;
                                $scope.deleteSheetDetailNoteId = id_sheet_detail_note;
                              });
                            }

                             $scope.$parent.$parent.refreshServices();
                             $scope.$parent.$parent.addAlert('saved successful','success',true,true,10);
                             $scope.$parent.$parent.panelOpened=false;
                      },function(_client){
                            $scope.$parent.$parent.addAlert(MessageUtils.getStringError(_client.errors),'warning',false,true,10);
                      });
                }else{
                    return true;
                }

            };

            $scope.removeConfirmService = function(id, sheetDetailNote){
              for (var i = 0; i < $scope.sheet.sheetDetails.length; ++i){
                if ($scope.sheet.sheetDetails[i].id == id){
                  $scope.sheet.sheetDetails[i].sheetDetailNotes.$remove(sheetDetailNote);
                  break;
                }
              }
            };

                            
        },
        link : function ($scope, $element, $attrs) {
             $element.css({
                          'overflow':'auto',
                          'padding-bottom':'200px'
                        });
             
        }
    }
});
