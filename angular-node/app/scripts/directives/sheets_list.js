projectApp.directive('sheetsList', function ($compile,$window,$document,Client, $timeout,$state,Sheet) {
    return {
            restrict: 'E',
            scope: { sheetStatus:"=ngSheetStatus"
        },
        replace:true,
        template:'<div class="row-fluid">'+
                    '<tabset class="nav-sheet-list">'+
	                    '<tab >'+
	                        '<tab-heading >'+
	                        	'<i class="fa fa-user hidden"></i><span translate>List of sheets</span>'+

	                        '</tab-heading>'+
			        		'<div  id="container-menu-secundario" class="row-fluid padding-30">'+
								'<div class="container-data">'+
									'<div class="portlet sheet-table sheet-table-{{sheetStatus | lowercase}}">'+
										'<div class="portlet-title-project-doble">'+
											'<div class="containerTitle">'+
												'<i class="icon-note"></i>'+
												'<span class="caption-subject" translate>List of sheets</span>'+
											'</div>'+
											'<div class="container-search-add">'+
												'<form name="searchForm " class="searchForm">'+
													'<div class="input-group">'+
														'<input class="form-control" type="text" name="ng-click="searchSheets();"" ng-model="sheetSearch" ng-change="searchSheets()" placeholder="{{\'Search\'|translate}}">'+
														'<span class="input-group-addon">'+
															'<a ng-click="searchSheets();"><i class="fa fa-search"></i></a>'+
														'</span>'+
													'</div>'+
												'</form>'+	
												'<a ng-click="openSheet()" class="btn btn-md blue " ><span class="ng-scope" translate>Create Sheet</span>&nbsp;<i class="fa fa-plus"></i></a>'+	
											'</div>'+	
											'<div class="menu-secundario">'+
												'<a ui-sref="sheet"><div class="opcion-menu" ng-class="{\'selected\': sheetStatus == \'QUOTE\'}"><span translate>Quote</span></div></a>'+
												'<a ui-sref="sheetNext"><div class="opcion-menu" ng-class="{\'selected\': sheetStatus == \'NEXT\'}" ><span translate>Next</span></div></a>'+
												'<a ui-sref="sheetActive"><div class="opcion-menu" ng-class="{\'selected\': sheetStatus == \'ACTIVE\'}" ><span translate>Active</span></div></a>'+
												'<a ui-sref="sheetRecent"><div class="opcion-menu" ng-class="{\'selected\': sheetStatus == \'RECENT\'}" ><span translate>Recent</span></div></a>'+
												'<a ui-sref="sheetClaim"><div class="opcion-menu" ng-class="{\'selected\': sheetStatus == \'CLAIM\'}" ><span translate>Claim</span></div></a>'+
											'</div>'+
										'</div>'+
										'<div class="portlet-body flip-scroll">'+
											'<table t-sort default-sort="id" collection="sheets" name="tablesSheets" datatable="ng" dt-options="dtSheetsOptions" dt-column-defs="dtSheetsColumnDefs" class="table-project table table-bordered table-condensed flip-content ">'+
												'<thead class="flip-content">'+
													'<tr>'+
														'<th c-sort="id"  translate>ID</th>'+
														'<th c-sort="title" translate >Name</th>'+
														'<th translate>Client</th>'+
														'<th c-sort="clients_count" translate>Persons</th>'+
														'<th c-sort="begins_at" translate>Begin At</th>'+
														'<th c-sort="ends_at" translate>Ends At</th>'+
													'</tr>'+
												'</thead>'+
												'<tbody>'+
													'<tr ng-click="openSheet(sheet)" ng-class="{\'selected\' : sheetSelected(sheet)}"  ng-repeat="sheet in sheets">'+
														'<td >{{sheet.ref}}</td>'+
														'<td >{{sheet.title}}</td>'+
														'<td >{{sheet.contactName}}</td>'+
														'<td >{{sheet.clientsCount}}</td>'+
														'<td >{{sheet.beginsAt}}</td>'+
														'<td >{{sheet.endsAt}}</td>'+
													'</tr>'+
												'</tbody>'+
											'</table>'+
											'<loading-message ng-model="sheets"></loading-message>'+
												'<div class="text-right">'+
													'<pagination next-text="{{ \'NEXT \' | translate }}" previous-text="{{ \'PREVIOUS\' | translate }}" max-size="sheets.$metadata.per_page" total-items="sheets.$metadata.total" ng-model="current.page" ng-change="refresh()"></pagination>'+
												'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</tab>'+
						'<tab ng-repeat="sheet in sheetsTab" active="sheet.active" select="selectedTab(sheet)">'+
	                        '<tab-heading >'+
	                        	'<i class="fa fa-user hidden"></i><span>{{sheet.title}}</span>'+
	                        	'<span  class="fa  fa-times sheet-list-close-icon" ng-click="closeSheet(sheet)"></span>'+
	                        '</tab-heading>'+
                          	'<sheet-panel-command ng-sheet="sheet" ></sheet-panel-command>'+
	                        '<sheet ng-sheet="sheet" > </sheet>'+
	                    '</tab>'+
					'</tabset>'+
				'</div>',
        controller : function($scope, $element, $attrs){
        	$scope.redirect=true;

        	$scope.current = {page:1};
		    $scope.sheetSearch = "";  
		    $scope.tabActive=-1;
		    $scope.sheets = Sheet.$search({
		      page: $scope.current.page,
		      status: $scope.sheetStatus,
		      order_by: 'id ASC'
		    });
		   $scope.sheetsTab =[];
		   window.a = $scope;
		   $scope.openSheet= function(sheet){
		   	   if($scope.redirect){
			   		$scope.$on('$locationChangeStart', function(event, newUrl, oldUrl) {
					  		event.preventDefault();
					});
					$scope.redirect=false;
				}
		   	if(sheet!=undefined){
			   	if($scope.sheetsTab.indexOf(sheet)==-1){
			   	  $scope.sheetsTab.push(sheet);
			   	  sheet.active=true;
			   	  sheet.deleteSheetDetailId=-1;
			   	  sheet.deleteSheetDetailNoteId=-1;
			   	}else{
			   		$scope.sheetsTab[$scope.sheetsTab.indexOf(sheet)].active=true;
			   		$scope.sheetsTab[$scope.sheetsTab.indexOf(sheet)].deleteSheetDetailId=-1;
			   		$scope.sheetsTab[$scope.sheetsTab.indexOf(sheet)].deleteSheetDetailNoteId=-1;	
			   	}
		   	}else{
		   		$scope.sheetsTab.push(Sheet.$build({
		            status: 'QUOTE',
		            title: 'DRAFT-'+(Math.random().toString(16)+"000000000").substr(2,8),
		            reason: '',
		            public: true
		          }));
		   		$scope.sheetsTab[$scope.sheetsTab.length-1].active=true;
		   		$scope.sheetsTab[$scope.sheetsTab.length-1].deleteSheetDetailId=-1;
		   		$scope.sheetsTab[$scope.sheetsTab.length-1].deleteSheetDetailNoteId=-1;		   		
		   	}
		   
		   };
		   $scope.closeSheet = function(sheet){
		   	$scope.sheetsTab.splice($scope.sheetsTab.indexOf(sheet),1);
		   }

		   $scope.sheetSelected =  function(sheet){
			    return $.inArray(sheet, $scope.Tabs) > -1;
			};
			$scope.selectedTab = function(sheet){
				sheet.panelOpened=false;
				//console.log(sheet);
			}
			$scope.searchSheets = function(){
				$scope.sheets.$refresh({
					page: $scope.current.page,
					status: $scope.sheetStatus,
					client_name: $scope.sheetSearch
				});
			};
			$scope.refresh = function () {
				var search = {
					page: $scope.current.page,
					status: $scope.sheetStatus,
					client_name: $scope.sheetSearch
				};
				$scope.sheets.$refresh(search);
			}
			
        },
        link : function (scope, $element, $attrs) {
        }

    }
 });