'use strict';
/* global $ */

var projectApp = angular.module('projectApp');

projectApp.directive('loadingClick', [function() {
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) { 
      elem.bind('click', function() {
        //Disable button and get size
        elem.attr('disabled','disabled');
        var size = elem.height();

        //Add spinner container
        if (attrs.loadingIcon == "true") {
          var spinClass = ".spin-box";
          elem.prepend('<div class="'+ spinClass.slice(1) +'"></div>');
          var target = elem.children(spinClass);
          var icon = elem.find('i');
          elem.find('i').remove();
          target.css({"width": size-6 +"px", "height": size-11+"px"});
        } else {
          var spinClass = ".text-spin-box";
          elem.prepend('<div class="'+ spinClass.slice(1) +'"></div>');
          var target = elem.children(spinClass);
          target.css({"width": size+"px", "height": size-7+"px"});
        }
        //Define spinner options
        var opts = {
          length: size/4 -1,
          width: size/6 > 0 ? size/6 -1 : 1,
          radius: size/4 -1,
          color: '#ffffff'
        };
        //Add Spinner
        var spinner = new Spinner(opts).spin();
        target.prepend(spinner.el);

        //Execute function activated by the click
        function returnToOriginal(){
          elem.removeAttr('disabled');
          elem.children(spinClass).remove();
          if (attrs.loadingIcon == "true") {
            elem.append(icon)
          }
        }
        if (attrs.loadingNoRestmod == "true") {
          scope.$apply(attrs.loadingClick).then(
            function(){
              returnToOriginal();
            }, function(){
              returnToOriginal();
            });
        } else {
          scope.$apply(attrs.loadingClick).$then(
            function(){
              returnToOriginal();
            }, function(){
              returnToOriginal();
            });
        }
      });
    }
  }
}]);


projectApp.directive('loadingMessage', ['$rootScope', 
  function ($rootScope) { 
    return {
      restrict: 'EA',
      require: ['^ngModel'],
      template: '<h3 class="center-text">{{ "LOADING" | translate }} <div class="loading-spin-box"></div></h3>',
      scope: {
        model: '=ngModel',
        oneTimeLoading: '@'
      },
      link: function (scope, elem, attrs) {
        var loadsOnce = scope.oneTimeLoading == 'true' ? true : false;
        var hasNotLoaded = true;
        
        //Define spinner options
        var size = elem.height();
        var target = elem.find('.loading-spin-box');
        var opts = {
          length: 7,
          width: 2,
          radius: 7,
          color: '#000'
        };
        //Add Spinner
        var spinner = new Spinner(opts).spin();
        target.append(spinner.el);

        scope.isLoading = function () {
          if (scope.model.$pending)
            return scope.model.$pending.length > 0;
          else 
            return false;
        };
        scope.$watch(scope.isLoading, function (newVal) {
          if(newVal){
            if (loadsOnce && hasNotLoaded) {
              elem.show();
              hasNotLoaded = false;
              if (scope.oneTimeLoadingEvent) {
                $rootScope.$broadcast(scope.oneTimeLoadingEvent);
              }
            } else if (!loadsOnce) {
              elem.show();
            } 
          }else{
            elem.hide();
          }
        });
      }
    };
}]);
