'use strict';
var projectApp = angular.module('projectApp');
projectApp.directive('clientMin', function ($compile, Client){
  return {
    scope: {
      client:"=ngClient",
      textNote:"=ngTextNote",
      save:"=ngSave",
      cancel:"=ngCancel",
      textButtonSave:"=ngTextSave",
      textButtonExist:"=ngTextExist"
    },
    restrict: 'E',
    replace: true,
    template:'<div class="editionContainer">'+
            '<pre class="hidden">Model: {{clientSelected | json}}</pre>'+
            '<script type="text/ng-template" id="customTemplate2.html">'+
            '  <a>'+
            '    <span class="action-types" bind-html-unsafe="match.label | typeaheadHighlight:query"></span>'+
            '  </a>'+
            '</script>'+
            '<div class="input-icon right input-search">'+
            '<i class="fa fa-search"></i>'+
            '<input class="pull-right input-search form-control" typeahead-input-formatter="submitSearch($model)" type="text"'+
            ' ng-model="clientSelected" type="text" placeholder="{{\'Search Client\' | translate}}"'+
            ' typeahead="name as ((name.identifier !== null ? name.identifier + \' - \' : \'\') + (name.first_name !== null ? name.first_name : \'\') + \' \'+(name.middle_name !== null ? name.middle_name : \'\') + \' \'+(name.last_name !== null ? name.last_name : \'\')+\' \'+(name.second_last_name !== null ? name.second_last_name : \'\')) for name in searchClient($viewValue)"'+
            ' typeahead-loading="loadingLocations" typeahead-template-url="customTemplate2.html">'+
            '<div ng-show="loadingLocations" class="loading-25 pull-right"></div>'+
            '</div>'+
            '<br /><br />'+
            '<label ng-show="textNote.length > 0" class="col-md-12 labelCommand">{{textNote}}</label>'+
            '<br />'+
            '<legend>{{ \'Client Details\' | translate }}</legend>'+
            ' <div class="col-md-12">'+
            '   <div class="form-group">'+
            '     <label class="col-md-4 control-label lb-client-bg-wht"  translate>CI</label>'+
            '     <div class="col-md-8">'+
            '       <input type="text" ng-disabled="client.id!==undefined" ng-model="client.identifier" class="form-control" placeholder="{{ \'Identifier\' | translate }}" >'+
            '     </div>'+
            '   </div>'+
            ' </div>'+
            ' <div class="col-md-12">'+
            '   <div class="form-group">'+
            '      <label class="col-md-4 control-label lb-client-bg-wht" translate>First Name</label>'+
            '      <div class="col-md-8">'+
            '        <input type="text" ng-change="enableButton()" ng-disabled="client.id!==undefined" ng-model="client.firstName" class="form-control" placeholder="{{ \'First Name\' | translate }}">'+
            '      </div>'+
            '    </div>'+
            ' </div>'+
            ' <div class="col-md-12">'+
            '   <div class="form-group">'+
            '     <label class="col-md-4 control-label lb-client-bg-wht"  translate>Last Name</label>'+
            '     <div class="col-md-8">'+
            '        <input type="text" ng-change="enableButton()" ng-disabled="client.id!==undefined" ng-model="client.lastName" class="form-control" placeholder="{{ \'Last Name\' | translate }}">'+
            '     </div>'+
            '   </div>'+
            ' </div>'+
            ' <div class="col-md-12">'+
            '   <div class="form-group">'+
            '     <label class="col-md-4 control-label lb-client-bg-wht"  translate>Email</label>'+
            '     <div class="col-md-8">'+
            '       <input type="email" ng-change="enableButton()" ng-disabled="client.id!==undefined" ng-model="client.email" class="form-control" ng-class="{\'has-error\':client.email.$error.email}" placeholder="{{ \'Email\' | translate }}">'+
            '     </div>'+
            '   </div>'+
            ' </div>'+
            ' <div class="col-md-12 cont-buttons-client">'+
                '<div class="col-md-12">'+
                  '<button ng-disabled="disabledSave==true" loading-click="saveClient()" class="btn blue pull-right "><span ng-show="client.id===undefined">{{textButtonSave | translate}}</span><span ng-show="client.id!==undefined">{{textButtonExist | translate}}</span></button>'+
                  '<button ng-click="cleanClient()" class="btn default pull-right"  translate>Undo</button>'+
                  '<button ng-click="cancelClient()" class="btn default pull-right"  translate>Cancel</button>'+
                '</div>'+
              '</div>'+
            '</div>',
    controller: function($scope, $element, $attrs){
      $scope.disabledSave = true;
      if ($scope.client.email == undefined){
        $scope.client.identifier = "";
        $scope.client.firstName = "";
        $scope.client.lastName = "";
        $scope.client.email = "";
      }
      $scope.searchClient = function(val){
        return $scope.client.searchClients(val).then(function(_clients){
          return _clients.data.clients;
        });
      };
      $scope.submitSearch = function(client){
        if(client!==undefined){
           Client.$find(client.id).$then(function(_client){
            $scope.client = _client;
            $scope.enableButton();
          });
        }
      };
      $scope.cleanClient = function(){
        $scope.client = Client.$build({
          identifier: "",
          firstName: "",
          lastName: "",
          email: "",
        });
        $scope.enableButton();
      };
      $scope.saveClient = function(){
        $scope.save();
      };
      $scope.cancelClient = function(){
        $scope.cancel();
      };
      $scope.enableButton = function(){
        if (($scope.client.firstName != "" && $scope.client.lastName != "" && $scope.client.email != "") || $scope.client.id !== undefined){
          $scope.disabledSave = false;
        }
        else{
          $scope.disabledSave = true;
        }
      };
    }
  }
});