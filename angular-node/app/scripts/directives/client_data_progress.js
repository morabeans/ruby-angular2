/**
 * Created by alfonso on 6/11/15.
 */

'use strict';
var projectApp = angular.module('projectApp');


projectApp.directive('clientDataProgress', function () {

  return {
    restrict: 'E',
    replace: true,
    template: '<div class="client-data-progress-container">' +
                '<span>&nbsp;</span>' +
              '</div>',
    scope: {},

    link: function(scope, element, attributes) {

      scope.text = attributes["percentage"];


      var calcEscencialData = function() {
        var percent = 0, numeroAtributos = 8, totaldatosEsenciales = 66;
        var pesoAtributo = totaldatosEsenciales / numeroAtributos;
        percent += (String(attributes["nombre"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["apellido"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["email"]).trim() != "")? pesoAtributo : 0 ;
        percent += (parseInt(attributes["telefonos"]) > 0)? pesoAtributo : 0 ;
        percent += (String(attributes["id"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["nacimiento"]).trim() != "")? pesoAtributo : 0 ;
        percent += (parseInt(attributes["pasaportes"]) > 0)? pesoAtributo : 0 ;
        percent += (parseInt(attributes["visas"]) > 0)? pesoAtributo : 0 ;
        return percent;
      };

      var calcExtraData = function() {
        var percent = 0, numeroAtributos = 2, totaldatosExtra = 34;
        var pesoAtributo = totaldatosExtra / numeroAtributos;
        percent += (parseInt(attributes["direcciones"]) > 0)? pesoAtributo : 0 ;
        percent += (parseInt(attributes["sociales"]) > 0)? pesoAtributo : 0 ;

        return percent;
      };

      var totalProgress = calcEscencialData() + calcExtraData();

      var colorClass = 'redish';
      if (totalProgress > 30) { colorClass = 'yellowish'; }  // being 30 an arbitrary value between red and yellow
      if (calcEscencialData() > 65) { colorClass = 'greenish'; }
      console.log(totalProgress);
      var span = element.children()[0];
      $(span).addClass(colorClass);
      $(span).css({ "width": String(totalProgress) + "%" });

    }
  };
});
