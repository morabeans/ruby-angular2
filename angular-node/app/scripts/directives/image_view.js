'use strict';
var projectApp = angular.module('projectApp');
projectApp.directive('imgView', function ($modal) {
    return {
        restrict: 'E',
        scope: { 
                ngImage:"=ngfImage"
        },
        replace:true,
        template:'<img ng-show="ngImage.imageUrlThumb!=undefined && ngImage.imageUrlThumb!=\'\' && ngImage.imageUrlThumb!=null" ng-click="viewImage()" src="{{ngImage.imageUrlThumb}}">',
        controller : function($scope, $element, $attrs, $transclude){
            window.ff=$scope;
            $scope.viewImage = function () {
                  var _modal = $modal.open({
                    template: '<div class="modal-content">'+
                                '<div class="modal-header"> &nbsp;'+
                                    '<a ng-click="close()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                                    '<h5 >{{image.imageFileName}}</h5>'+
                                '</div>'+
                                '<img class="fullImage" src="{{imageUrl}}">'+
                               '</div>',
                    controller: 'ImageServiceCtrl',
                    size: 'lg',
                    windowClass:'windowImage',
                    resolve: {
                        image: function() {
                        return $scope.ngImage;
                      }
                    }
                  });
                  _modal.result.then(function () {
                    //imageUrlThumb
                  });
            };
        },
        link : function ($scope, $element, $attrs) {
            window.ff=$scope;
            console.log($scope);
            var canvas = document.createElement("canvas");
                canvas.width = 24;
                canvas.height = 24;
                //document.body.appendChild(canvas);
                var ctx = canvas.getContext("2d");
                ctx.fillStyle = "#000000";
                ctx.font = "24px FontAwesome";
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                ctx.fillText("\uf002", 12, 12);
                var dataURL = canvas.toDataURL('image/png')
                $element.css('cursor', 'url('+dataURL+'), auto');
        }
    }
});
