'use strict';
var projectApp = angular.module('projectApp');
projectApp.directive('commandPanel', function ($compile, Sheet, Client,$timeout, $state, SheetNote, SheetNoteCommand, SheetDetail, SheetDetailNote) {
  return {
    restrict: 'E',
    scope: {
      executeCommand: '='
    },
    template:'<div class="commandPanelContainer top-command-panel">'+
                '<command-panel-eyelash class="eyelash eyelash-color-command-panel" >'+
                  '<span class="icon-paper-plane"></span>'+
                '</command-panel-eyelash>'+
                '<command-panel-container-tabs class="panelContainer">'+
                '</command-panel-container-tabs>'+
                '<div class="panelInsertion panel-insertion"> '+
                '</div>'+
              '</div>',
    replace: true,
  	controller: function ($scope, $element, $attrs, $transclude) {
      $('.top-command-panel').click(function(event) {
        $('html, .top-sheet-command-panel').one('click',function() {
          if($scope.state.isOpen==true)
            $scope.showHide();
        });
        event.stopPropagation();
      });
      Sheet.options().then(function (_option) {
        $scope.sheet_options = _option.data;
      });
    		$scope.state = {
      			isOpen:false,
            animating:false,
            cantTabCreated:0,
            tabActive:-1,
            commandEditingMenu:-1,
            commandEditing:-1,
            isOpenInsertion: false,
            animatingInsertion: false,
            processingCommand: -1,
            dataLoaded: false,
            isOpenPanelHelp: false,
            deletingCommand: -1,
            processingCommandAnt: -1
          };
        $scope.infoCommand = function(){
          var sheetNoteCommand;
          var sheetNoteCommandCmp;
          var clientCommand;
        };
        $scope.tabsData = {};
        $scope.client = Client.$build({});
        $scope.clientSelected = "";
        $scope.sheetNote = SheetNote.$build({});
        $scope.commandSheetNote = [];
        $scope.commandsInfo = {
          '#name':{
            'minParam': 2,
            'minParamError': 'Type at least one value after the hashtag command.',
            'classification': 'clients',
            'icon':'glyphicon-user'
          },
          '#flight':{
            'minParam': 2,
            'minParamError': 'Type at least one value after the hashtag command.',
            'classification': 'services',
            'icon':'glyphicon-plane'
          },
          '#lodging':{
            'minParam': 2,
            'minParamError': 'Type at least one value after the hashtag command.',
            'classification': 'services',
            'icon':'glyphicon-home'
          },
          '#carRental':{
            'minParam': 2,
            'minParamError': 'Type at least one value after the hashtag command.',
            'classification': 'services',
            'icon':'fa-car'
          }
        };
      	$scope.showHide = function () {
           if(!$scope.state.animating){
            if (!$scope.state.isOpen){
              if (!$scope.state.dataLoaded)
                $scope.loadTabData();
              else $scope.checkTabs();
            }
            $scope.state.animating=true;
          	$element.animate({
  				    right: (($scope.state.isOpen)?"-":"+")+($scope.state.isOpenInsertion ? "=1060" : "=560")
  				  }, 1000, function() {
              $scope.state.animating=false;
  				    $scope.state.isOpen=!$scope.state.isOpen;
  				  });
            if ($scope.state.isOpenInsertion){
              $element.children('.panelInsertion').animate({
                right: (($scope.state.isOpen)?"-=500":"")
              }, 1000, function() {
                $scope.state.isOpenInsertion=!$scope.state.isOpenInsertion;
              });
            }
          }
         };
         $scope.loadTabData = function(){
          var cont = 0;
          SheetNote.$search({}).$then(function(_sheet_note){
            if (_.size(_sheet_note) !== 0){
              for(var i = 0; i < _.size(_sheet_note); i++){
                $scope.state.cantTabCreated++;
                $scope.sheetNote = _sheet_note[i];
                for(var j = 0; j < _.size(_sheet_note[i].sheetNoteCommands); j++){
                  $scope.commandSheetNote.push(new $scope.infoCommand());
                  $scope.commandSheetNote[j].sheetNoteCommand = _sheet_note[i].sheetNoteCommands[j];
                  $scope.commandSheetNote[j].sheetNoteCommandCmp = angular.copy(_sheet_note[i].sheetNoteCommands[j]);
                  $scope.commandSheetNote[j].clientCommand = Client.$build({});
                  if ($scope.commandSheetNote[j].sheetNoteCommand.typeCommand === "#name" && $scope.commandSheetNote[j].sheetNoteCommand.processed){
                    $scope.commandSheetNote[j].clientCommand = Client.$find($scope.commandSheetNote[j].sheetNoteCommand.typeCommandId).$then(function(){});
                  }
                  else if ($scope.commandSheetNote[j].sheetNoteCommand.typeCommand === "#name" && !$scope.commandSheetNote[j].sheetNoteCommand.processed){
                    $scope.commandSheetNote[j].clientCommand = Client.$build({});
                  }
                }
                $scope.$broadcast('insertTabSheet');
                $scope.sheetNote = SheetNote.$build({});
                $scope.commandSheetNote = [];
                cont++;
              }
            }
            else $scope.checkTabs();
            if (cont === _.size(_sheet_note) || _.size(_sheet_note) === 0)
              $scope.state.dataLoaded = true;
          });
         };
         $scope.sheetNoteCmp = function(obj1, obj2){
           if (obj1.title === obj2.title && obj1.reason === obj2.reason && obj1.public === obj2.public)
            return true;
          return false;
         };
         $scope.sheetNoteCommandCmp = function(obj1, obj2){
           if (obj1.typeCommand === obj2.typeCommand && obj1.commandText === obj2.commandText && obj1.typeCommandId === obj2.typeCommandId
            && obj1.processed === obj2.processed && obj1.classification === obj2.classification)
            return true;
          return false;
         };
         $scope.saveSheetNote = function(tabId){
           if (!$scope.sheetNoteCmp($scope.tabsData[tabId].sheetNote, $scope.tabsData[tabId].sheetNoteCmp)){
             $scope.tabsData[tabId].sheetNote.$save().$then(function(_sheet_note){
               $scope.tabsData[tabId].sheetNoteCmp = angular.copy(_sheet_note);
             });
           }
         };
         $scope.saveSheetNoteCommand = function(tabId, commandIndex){
           if ($scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.commandText.replace(/^\s+|\s+$/gm,'').length !== 0){
             if ($scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.id === undefined){
               $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.sheetNoteId = $scope.tabsData[tabId].sheetNote.id;
             }
             if (!$scope.sheetNoteCommandCmp($scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand, $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommandCmp)){
               $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.$save().$then(function(_sheet_note_command){
                 $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommandCmp = angular.copy(_sheet_note_command);
               });
             }
           }
         };
         $scope.$watch('tabsData[state.tabActive].sheet.reason', function (newvalue, oldvalue){
           if ($scope.state.tabActive !== -1 && newvalue !== $scope.tabsData[$scope.state.tabActive].sheetNote.reason){
            $scope.tabsData[$scope.state.tabActive].sheetNote.reason = newvalue;
            if (oldvalue !== undefined)
              $scope.saveSheetNote($scope.state.tabActive);
           }
         });
         $scope.$watch('tabsData[state.tabActive].sheet.public', function (newvalue, oldvalue){
           if ($scope.state.tabActive !== -1 && newvalue !== $scope.tabsData[$scope.state.tabActive].sheetNote.public){
            $scope.tabsData[$scope.state.tabActive].sheetNote.public = newvalue;
            if (oldvalue !== undefined)
              $scope.saveSheetNote($scope.state.tabActive);
           }
         });
         $scope.$watch('tabsData[state.tabActive].sheet.title', function (newvalue, oldvalue){
           if ($scope.state.tabActive !== -1 && newvalue !== $scope.tabsData[$scope.state.tabActive].sheetNote.title){
            $scope.tabsData[$scope.state.tabActive].sheetNote.title = newvalue;
           }
         });
         $scope.removeTab = function(tabId, checkTab){
            if (checkTab){
              return $scope.tabsData[tabId].sheetNote.$destroy().$then(function(){
                delete $scope.tabsData[tabId];
                $element.find("#container-note-"+tabId).remove();
                $element.find("#container-tab-"+tabId).remove();
                $scope.checkTabs();
              });
            }
            else{
              delete $scope.tabsData[tabId];
              $element.find("#container-note-"+tabId).remove();
              $element.find("#container-tab-"+tabId).remove();
            }
         };
         $scope.removeCommand = function(tabId, commandIndex){
           $scope.state.deletingCommand = commandIndex;
           if ($scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.id !== undefined){
             return $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.$destroy().$then(function(){
               $scope.tabsData[tabId].commands.splice(commandIndex, 1);
               $scope.state.deletingCommand = -1;
             });
           }
           else{
             $scope.tabsData[tabId].commands.splice(commandIndex, 1);
             $scope.state.deletingCommand = -1;
           }
         };
         $scope.addNote = function(tabId, commandIndex){
          if (commandIndex === -2 || commandIndex === -1){
            $scope.tabsData[tabId].commands.push(new $scope.infoCommand());
            commandIndex = $scope.tabsData[tabId].commands.length == 0 ? 0 : $scope.tabsData[tabId].commands.length - 1;
          }
          else{
            commandIndex += 1;
            $scope.tabsData[tabId].commands.splice(commandIndex, 0, new $scope.infoCommand());
          }
          $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand = SheetNoteCommand.$build({
            typeCommand: "",
            commandText: "",
            typeCommandId: 0,
            processed: false,
            classification: "notes",
            sheetNoteId: $scope.tabsData[tabId].sheetNote.id !== undefined ? $scope.tabsData[tabId].sheetNote.id : 0,
          });
          $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommandCmp = SheetNoteCommand.$build({});
          $scope.tabsData[tabId].commands[commandIndex].clientCommand = Client.$build({});
         };
         $scope.setFocus = function(tabId, commandIndex){
          setTimeout(function(){
            if (commandIndex === -1)
              commandIndex = $scope.tabsData[tabId].commands.length - 1;
            else commandIndex += 1;
            $element.find("#container-note-"+tabId).find("#note-"+commandIndex).focus();
          });
         };
         $scope.moveFocus = function($event, tabId, commandIndex){
           if (!$scope.state.isOpenPanelHelp){
             if ($event.which == 38)
               commandIndex -= 1;
             else if ($event.which == 40)
               commandIndex += 1;
             $element.find("#container-note-"+tabId).find("#note-"+commandIndex).focus();
           }
         };
         $scope.showPanelCog = function(event,commandIndex){
          if(commandIndex ==  $scope.state.commandEditingMenu)
            $scope.state.commandEditingMenu=-1;
          else
            $scope.state.commandEditingMenu = commandIndex;
          event.stopPropagation();
        };
        $scope.showPanelInsertion = function(){
          if ($scope.tabsData[$scope.state.tabActive].commands[$scope.state.processingCommand].sheetNoteCommand.typeCommand==="#name"
            || $scope.tabsData[$scope.state.tabActive].commands[$scope.state.processingCommand].sheetNoteCommand.processed || $scope.state.processingCommand !== $scope.state.processingCommandAnt){
            if (!$scope.state.animatingInsertion){
              if (!$scope.state.isOpenInsertion){
                 $scope.state.processingCommandAnt = $scope.state.processingCommand;
                $scope.insertTabData($scope.state.tabActive, $scope.state.processingCommandAnt);
              }
              else{
                $scope.state.processingCommand = -1;
                $scope.state.processingCommandAnt = -1;
              }
              $scope.state.animatingInsertion=true;
              $element.animate({
                right: (($scope.state.isOpenInsertion)?"-":"+")+"=500"
              }, 1000);
              $element.children('.panelInsertion').animate({
                right: (($scope.state.isOpenInsertion)?"-":"+")+"=500"
              }, 1000, function() {
                $scope.state.animatingInsertion=false;
              });
              $scope.state.isOpenInsertion=!$scope.state.isOpenInsertion;
            }
          }
        };
        $scope.showErrorMessage = function(tabId, msg){
          var msgErrorHtml = $compile('<span translate>'+ msg +'</span>')($scope);
          $element.children('.panelContainer').find('#panelError-'+ tabId + ' .errorInfo').html('');
          $element.children('.panelContainer').find('#panelError-'+ tabId + ' .errorInfo').append(msgErrorHtml);
          $element.children('.panelContainer').find('#panelError-'+ tabId).css('display','block');
          $timeout(function(){
              $element.children('.panelContainer').find('#panelError-'+ tabId).css('display','none');
          },3000);
        };
        $scope.closeErrorPanel = function(tabId){
          $element.children('.panelContainer').find('#panelError-'+ tabId).css('display','none');
        };
        $scope.insertTabData = function(tabId, commandIndex){
          $scope.state.tabActive = tabId;
          $scope.state.processingCommand = commandIndex;
          if ($scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.typeCommandId !== 0){
            $scope.client = angular.copy($scope.tabsData[tabId].commands[commandIndex].clientCommand);
          }
          else $scope.client = Client.$build({});
          $scope.text = $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.commandText;
          $scope.textSave = "Register and Link";
          $scope.textExist = "Link";
          $element.children('.panelInsertion').html($compile('<client-min ng-client="client" ng-text-save="textSave" ng-text-exist="textExist" ng-text-note="text" ng-save="createClient" ng-cancel="showPanelInsertion"></client-min>')($scope));
        };
        $scope.searchClient = function(val){
          return $scope.client.searchClients(val).then(function(_clients){
            return _clients.data.clients;
          });
        };
        $scope.submitSearch = function(client){
          if(client!==undefined){
             Client.$find(client.id).$then(function(_client){
              $scope.client = _client;
            });
          }
        };
        $scope.createClient = function(){
          if ($scope.client.id!==undefined){
            $scope.tabsData[$scope.state.tabActive].commands[$scope.state.processingCommand].sheetNoteCommand.typeCommandId = $scope.client.id;
            $scope.tabsData[$scope.state.tabActive].commands[$scope.state.processingCommand].clientCommand = angular.copy($scope.client);
            $scope.processCommand($scope.state.tabActive, $scope.state.processingCommand);
            $scope.showPanelInsertion();
          }
          else{
            return $scope.client.$save().$then(function(_client){
              $scope.tabsData[$scope.state.tabActive].commands[$scope.state.processingCommand].sheetNoteCommand.typeCommandId = _client.id;
              $scope.tabsData[$scope.state.tabActive].commands[$scope.state.processingCommand].clientCommand = angular.copy(_client);
              $scope.processCommand($scope.state.tabActive, $scope.state.processingCommand);
              $scope.showPanelInsertion();
            });
          }
        };
        $scope.processCommand = function(tabId, commandIndex){
          if (!$scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.processed){
            $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.processed = true;
          }
          $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.commandText = $scope.tabsData[tabId].commands[commandIndex].clientCommand.firstName + ' ' + $scope.tabsData[tabId].commands[commandIndex].clientCommand.lastName;
          $scope.saveSheetNoteCommand(tabId, commandIndex);
        };
        $scope.createSheet = function(tabId){
          var sheet = {
            title: $scope.tabsData[tabId].sheet.title,
            status: $scope.tabsData[tabId].sheet.status,
            reason: $scope.tabsData[tabId].sheet.reason,
            public: $scope.tabsData[tabId].sheet.public
          };
          var sheet_note = {
            id: $scope.tabsData[tabId].sheetNote.id
          };
          var notes_data = [];
          for (var i = 0; i < $scope.tabsData[tabId].commands.length; ++i){
            if ($scope.tabsData[tabId].commands[i].sheetNoteCommand.commandText.replace(/^\s+|\s+$/gm,'').length !== 0){
              notes_data.push({
                typeCommand: $scope.tabsData[tabId].commands[i].sheetNoteCommand.typeCommand,
                commandText: $scope.tabsData[tabId].commands[i].sheetNoteCommand.commandText,
                typeCommandId: $scope.tabsData[tabId].commands[i].sheetNoteCommand.typeCommandId,
                processed: $scope.tabsData[tabId].commands[i].sheetNoteCommand.processed,
                classification: $scope.tabsData[tabId].commands[i].sheetNoteCommand.classification
              });
            }
          }
          $scope.newSheet = Sheet.$build({});
          return $scope.newSheet.createSheetByNotes(sheet, sheet_note, notes_data).then(function(response){
            $state.go('sheet.edit', {sheetId: response.data.sheet.id});
            $scope.removeTab(tabId, false);
            $scope.showHide();
          }, function(response){
            var msg = "Sheet not saved. Please try again.";
            $scope.showErrorMessage(tabId, msg);
          });
        };
        $scope.editCommand = function(tabId, commandIndex, add){
          var tokens = $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.commandText.split(' ');
          if (tokens[0][0] === '#' && tokens.length < $scope.commandsInfo[tokens[0]].minParam){
            $scope.showErrorMessage(tabId, $scope.commandsInfo[tokens[0]].minParamError);
          }
          else{
            $scope.state.commandEditing=-1;
            if (tokens[0][0] === '#' && $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.typeCommand !== tokens[0]){
              $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.typeCommand = tokens[0];
              $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.classification = $scope.commandsInfo[tokens[0]].classification;
            }
            else if (tokens[0][0] !== '#' && !$scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.processed){
              $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.typeCommand = "";
              $scope.tabsData[tabId].commands[commandIndex].sheetNoteCommand.classification = "notes";
            }
            $scope.saveSheetNoteCommand(tabId, commandIndex);
            $scope.$broadcast('hidePanelCommand');
            add ? $scope.addNote(tabId, commandIndex) : "";
          }
        };
        $scope.checkTabs = function(){
          if (_.size($scope.tabsData) == 0){
            $scope.$broadcast('insertTabSheet');
          }
        };
        $scope.moveTabKey = function($event){
          if ($scope.state.isOpenPanelHelp){
            $scope.$broadcast('moveTab');
            $event.preventDefault();
          }
        };
        $scope.cancelMove = function($event){
          if ($scope.state.isOpenPanelHelp){
            $event.preventDefault();
          }
        };
         return {
          getState:$scope.state,
          getTabsData: $scope.tabsData
			};
      }
     
	}
});

projectApp.directive('commandPanelEyelash', function () {
  return {
  	restrict: 'E',
  	require: '^?commandPanel',
    controller: function($scope) {
                
    },
    link: function (scope, elem, attrs, commandPanelCtrl) {
    	elem.bind('click',function(){
    		scope.showHide();
    	});
    }
  };
});
projectApp.directive('commandPanelInput', function ($compile) {
  return {
    restrict: 'A',
    require: '^?commandPanel',
    scope :{
        indexCell:"@indexCell"
    },
    controller : function($scope, $element, $attrs){
      
      $scope.commandsList = {
                        '#name':{'helpMessage':' - FirstName LastName [Email] - Add a client to the note'},
                        '#flight':{'helpMessage':' - Origin Destination Date Quantity - Add a flight to the note'},
                        '#lodging':{'helpMessage':' - City CheckIn CheckOut Quantity - Add a lodging to the note'},
                        '#carRental':{'helpMessage':' - City Pick-up-date Drop-off-date - Add a car rental to the note'},
      };
      $scope.commandTyping="";
      $scope.lastCaret=-1;
      $scope.showPanelHelp=false;
      $scope.os = 0
      $scope.oe = 0
      $scope.indexSelect = -1;
      $scope.word = '';
      $scope.showPanelUp = false;
      $scope.isHashtag =function(word){
        return word;
      };

      $scope.getLastWordCaret = function(){
        var  preCaret = $element[0].value.substr(0,$element[0].selectionStart);
        var  postCaret = $element[0].value.substr($element[0].selectionStart,1);
        var  arrayPreCaret = preCaret.split(" ");
        var  lastWord = arrayPreCaret[arrayPreCaret.length-1];
        if (preCaret.length === 0){
          $scope.commandTyping = '';
          $scope.hidePanelCommand();
        }
        return lastWord;
      };
      $scope.showHideHashtagCommand = function(){
        var pattern = new RegExp(/^#{1,2}\w*$/g);
        $scope.word = $scope.getLastWordCaret();
        if ($scope.word === '' && $scope.commandTyping !== '')
          $scope.word = $scope.commandTyping;
        if($scope.word==''){
          $scope.commandTyping="";
          $scope.$apply();
        }else
        if(pattern.test($scope.word)){
          $scope.showCommands = {};
          angular.forEach($scope.commandsList, function(value, key){
            if (key.search($scope.word) !== -1){
              $scope.showCommands[key] = value.helpMessage;
            }
          });
          $scope.commandTyping = $scope.word;
          $scope.word = '';
          $scope.showPanelCommand();
          $scope.$apply();
        }else{
          $scope.commandTyping="";
          $scope.$apply();
        }
        return $scope.showPanelHelp;
        
      }
      $scope.setCommand = function(word, call){
        var  preCaret = $element[0].value.substr(0,$element[0].selectionStart).trim();
        var  postCaret =$element[0].value.substring($element[0].selectionStart,$element[0].value.length);
        var  newPreCaret = preCaret.substring(0, preCaret.lastIndexOf(" ")+1) + word;
        $scope.commandTyping=word;
        $element[0].value=newPreCaret+" "+postCaret;
        $element[0].focus();
        $element[0].selectionStart=$element[0].selectionEnd = newPreCaret.length+1;
        if (call)
          $scope.showHideHashtagCommand();
        else $scope.indexSelect = -1;
      }
      $scope.$on('hidePanelCommand', function(event){
        $scope.hidePanelCommand();
      });
      $scope.$on('moveTab', function(event){
        $scope.moveTab();
      });
    },
    link: function ($scope, elem, attrs, commandPanelCtrl) {
      var elementHtml = $compile('<ul ng-show="this.showPanelHelp==true" class="dropdown-menu panel-command-help" >'+
                                        '<li ng-show="this.showPanelUp==true" class="title">'+
                                          '<span class="header_help"><strong>tab</strong>&nbsp; or &nbsp;<strong>↑</strong> <strong>↓</strong>&nbsp; to navigate <strong class="left_margin">↵</strong>&nbsp; to select <strong class="left_margin">esc</strong>&nbsp; to dismiss</span> '+
                                        '</li>'+
                                        '<li ng-click="this.setCommand(key, true)" ng-class="{active:this.commandTyping==key || this.indexSelect===$index}" ng-mouseover="this.setCommand(key, false)" class="command" ng-repeat="(key, value) in this.showCommands track by $index" >'+
                                          '<span><strong>{{key}}</strong>{{value | translate}}</span>'+
                                          '</a>'+
                                       '</li>'+
                                        '<li ng-show="this.showPanelUp==false" class="title">'+
                                          '<span class="header_help"><strong>tab</strong>&nbsp; or &nbsp;<strong>↑</strong> <strong>↓</strong>&nbsp; to navigate <strong class="left_margin">↵</strong>&nbsp; to select <strong class="left_margin">esc</strong>&nbsp; to dismiss</span> '+
                                        '</li>'+
                                      '</ul>')( $scope );      
      elem.parent().prepend(elementHtml);
      elem.on("keyup",function(event){
        if (event.which == 16){
          event.stopPropagation();
          return true;
        }
        if (event.which == 27){
          $scope.hidePanelCommand();
        }
        if (event.which === 38 || event.which === 40){
          if ($scope.showPanelHelp === true){
            if (event.which === 38){
              if ($scope.indexSelect !== 0 && $scope.indexSelect !== -1)
                $scope.indexSelect--;
              else if ($scope.showPanelUp == true && $scope.indexSelect == -1)
                $scope.indexSelect = _.size($scope.showCommands) - 1;
            }
            if (event.which === 40){
              if ($scope.indexSelect !== _.size($scope.showCommands) - 1 && $scope.showPanelUp == false)
                $scope.indexSelect++;
              else if ($scope.showPanelUp == true && $scope.indexSelect != -1 && $scope.indexSelect !== _.size($scope.showCommands) - 1)
                $scope.indexSelect++;
            }
          }
        }
        if (event.which === 13){
          if ($scope.showPanelHelp === true && $scope.indexSelect !== -1){
            var keys = Object.keys($scope.showCommands);
            keys = keys.sort();
            $scope.setCommand(keys[$scope.indexSelect], false);
            $scope.indexSelect = -1;
          }
        }
        $scope.showHideHashtagCommand();
      });
      $scope.hidePanelCommand = function(){
        $scope.showPanelHelp = false;
        $scope.indexSelect = -1;
        commandPanelCtrl.getState.isOpenPanelHelp = false;
        $scope.showPanelUp = false;
      };
      $scope.showPanelCommand = function(){
        var outter = $('#container-note-'+commandPanelCtrl.getState.tabActive + ' .container-notes');
        var inner = $('#note-'+$scope.indexCell);
        if (!$scope.showPanelHelp){
          $scope.showPanelHelp = true;
          setTimeout(function(){
            var panel = elem.parent().find('.panel-command-help').height();
            var offsetOutter = outter.offset();
            var offsetInner = inner.offset();
            if ((outter.height() - (offsetInner.top - offsetOutter.top)) < panel){
              $scope.showPanelUp = true;
              $scope.$apply();
              $('.panel-command-help').css('bottom','26px');
              $('.panel-command-help').css('top','auto');
            }
            else{
              $('.panel-command-help').css('bottom','auto');
              $('.panel-command-help').css('top','26px');
              $scope.showPanelUp = false;
              $scope.$apply();
            }
          });
        }
        else{
          if ($scope.showPanelUp == true){
            $('.panel-command-help').css('bottom','26px');
            $('.panel-command-help').css('top','auto');
          }
        }
        commandPanelCtrl.getState.isOpenPanelHelp = true;
      };
      $scope.moveTab = function(){
        if ($scope.indexSelect === _.size($scope.showCommands) - 1 || $scope.indexSelect === -1)
          $scope.indexSelect = 0;
        else $scope.indexSelect++;
      };
    }
  };
});

projectApp.directive('commandPanelContainerTabs', function ($compile,gettextCatalog, Sheet, SheetNote) {
  return {
    restrict: 'E',
    require: '^?commandPanel',
    template:'<i ng-show="this.state.dataLoaded===false" class="fa fa-spinner"></i><span ng-show="this.state.dataLoaded===false" translate>LOADING</span>'+
    '<div class="panelTabs" ng-show="this.state.dataLoaded===true"><div ng-show="this.state.isOpenInsertion==false && this.state.dataLoaded===true" class="addTab pull-right"><span class="glyphicon glyphicon-plus"></span></div><div class="containerTabs" ng-class="{\'pull-right\': this.state.isOpenInsertion==false}" ng-show="this.state.dataLoaded===true"><ul class="nav nav-tabs"></ul></div></div><div ng-show="this.state.dataLoaded===true" class="containerNote"></div>',

    link: function ($scope, elem, attrs, commandPanelCtrl) {

      elem.find('.addTab').bind('click',function(){
        $scope.addTabSheet();
      });

      $scope.$on('insertTabSheet', function(event){
        $scope.addTabSheet();
      });

      $scope.addTabSheet = function(){
        if ($scope.sheetNote.id === undefined){
          commandPanelCtrl.getState.cantTabCreated++;
        }
        var tabId = commandPanelCtrl.getState.cantTabCreated;
        commandPanelCtrl.getTabsData[tabId]={
          editingTitle:true,
          commands: $scope.sheetNote.id === undefined ? [] : $scope.commandSheetNote, //aqui se push cada string con el comando o notas..
          sheet: Sheet.$build({
            title: $scope.sheetNote.id === undefined ? "" : $scope.sheetNote.title,
            status: "QUOTE",
            reason: $scope.sheetNote.id === undefined ? "" : $scope.sheetNote.reason,
            public: $scope.sheetNote.id === undefined ? true : $scope.sheetNote.public,
          }),
          sheetNote: $scope.sheetNote.id === undefined ? SheetNote.$build({
            title: "",
            status: "OPEN",
            reason: "",
            public: true,
          }) : $scope.sheetNote,
          sheetNoteCmp: $scope.sheetNote.id === undefined ? SheetNote.$build({}) : angular.copy($scope.sheetNote),
        };
        if ($scope.sheetNote.id === undefined){
          commandPanelCtrl.getTabsData[tabId].sheet.title="Draft-"+tabId;
          commandPanelCtrl.getTabsData[tabId].sheetNote.title="Draft-"+tabId;
          $scope.saveSheetNote(tabId);
          $scope.addNote(tabId, -2);
        }
        else{
          if (commandPanelCtrl.getTabsData[tabId].commands.length === 0)
            $scope.addNote(tabId, -2);
        }
        var tabElem = $compile('<div id="panelError-'+ tabId +'" class="panelError Metronic-alerts alert alert-danger fade in">'+
                               '<button ng-click="this.closeErrorPanel('+ tabId +')" type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><span class="errorInfo"></span></div>'+
                               '<li id="container-tab-'+tabId+'" ng-show="(this.tabsData['+tabId+']!=undefined && this.state.isOpenInsertion==false) || (this.state.isOpenInsertion==true && this.state.tabActive=='+tabId+')" ng-class="{active: this.state.tabActive=='+tabId+'}">'+
                               '  <div class="tab"  ng-click="this.state.tabActive='+tabId+'" style="float:left;">'+
                               '    <span ng-click="this.tabsData['+tabId+'].editingTitle=!this.tabsData['+tabId+'].editingTitle" ng-show="!this.tabsData['+tabId+'].editingTitle">{{this.tabsData['+tabId+'].sheet.title}}</span>'+
                               '    <input class="inputSheetName" ng-show="this.tabsData['+tabId+'].editingTitle" ng-model="this.tabsData['+tabId+'].sheet.title"  ng-enter="this.saveSheetNote('+tabId+'); this.tabsData['+tabId+'].editingTitle=false" ng-blur="this.saveSheetNote('+tabId+')" type="text">'+
                               '  </div>'+
                               '</li>')( $scope );
        elem.find('.containerTabs').children('.nav').append(tabElem);
        elem.find('.containerTabs').children('.nav').find('.inputSheetName:last').focus();
        commandPanelCtrl.getState.tabActive=tabId;

        var noteElem = $compile('<div id="container-note-'+tabId+'" ng-show="this.state.tabActive=='+tabId+'">'+
                                  ' <div class="container-detail">'+
                                  '   <div class="container-reason col-md-6">'+ 
                                  '     <label class="control-label"><strong translate>Reason</strong></label>'+
                                  '     <select ng-disabled="this.state.isOpenInsertion==true" ng-options="key as value | translate  for (key,value) in this.sheet_options.reasons" class="form-control nopaddingH" name="reason"'+
                                  '       ng-model="this.tabsData['+tabId+'].sheet.reason">'+
                                  '       <option value="" translate>Select</option>'+
                                  '     </select>'+
                                  '   </div>'+
                                  '   <div class="container-privacy col-md-6">'+ 
                                  '     <label class="control-label"><strong translate>Privacy</strong></label>'+
                                  '     <toggle-switch ng-model="this.tabsData['+tabId+'].sheet.public" class="switch-blue" on-label="{{\'Public\'|translate}}" off-label="{{\'Private\'|translate}}"></toggle-switch>'+
                                  '   </div>'+
                                  ' </div>'+
                                  ' <div class="container-notes">'+
                                  '   <div ng-class="{\'active\': (this.state.isOpenInsertion==true && this.state.processingCommand==$index)}" class="note" ng-repeat="command in this.tabsData['+tabId+'].commands  track by $index">'+
                                  '     <div><span ng-show="command.sheetNoteCommand.typeCommand===\'#name\' && command.sheetNoteCommand.processed===true" class="icon-span-processed glyphicon glyphicon-user"></span>'+
                                  '       <span ng-show="command.sheetNoteCommand.typeCommand===\'#flight\'" class="icon-span-processed glyphicon glyphicon-plane"></span>'+
                                  '       <span ng-show="command.sheetNoteCommand.typeCommand===\'#lodging\'" class="icon-span-processed glyphicon glyphicon-home"></span>'+
                                  '       <i ng-show="command.sheetNoteCommand.typeCommand===\'#carRental\'" class="icon-center-processed fa fa-car"></i><input ng-focus="this.state.processingCommandAnt=$index; ((this.state.isOpenInsertion==true && this.state.processingCommand!=$index) ? this.showPanelInsertion() : 0)"'+
                                  '       id="note-{{$index}}" ng-keyup="(($event.which===38 || $event.which===40) ? this.moveFocus($event, '+tabId+', $index) : 0)" command-panel-input class="input-command" index-cell="{{$index}}"'+
                                  '       ui-keydown="{\'tab\':\'this.moveTabKey($event)\',38:\'this.cancelMove($event)\'}" ng-class="{\'active\': command.sheetNoteCommand.processed===true || command.sheetNoteCommand.classification===\'services\'}" ng-model="command.sheetNoteCommand.commandText"  ng-enter="this.editCommand('+tabId+', $index, true); this.setFocus('+tabId+', $index)" ng-blur="this.editCommand('+tabId+', $index, false)" type="text"></div>'+
                                  '     <span ng-show="this.state.isOpenInsertion==false && this.state.deletingCommand===-1" ng-click="this.removeCommand('+tabId+', $index)" class="icon-cog glyphicon glyphicon-remove"></span>'+
                                  '     <span ng-class="{\'active\': (this.state.isOpenInsertion==true && this.state.processingCommand==$index)}" class="icon-details icon-arrow-right" ng-click="this.state.processingCommand=$index; this.showPanelInsertion()"'+
                                  '       ng-show="this.state.isOpenInsertion==false && command.sheetNoteCommand.typeCommand===\'#name\' && this.state.deletingCommand===-1"></span>'+
                                  '     <span ng-class="{\'active\': (this.state.isOpenInsertion==true && this.state.processingCommand==$index)}" class="icon-details icon-arrow-left" ng-click="this.state.processingCommand=$index; this.showPanelInsertion()"'+
                                  '       ng-show="(this.state.isOpenInsertion==true && this.state.processingCommand==$index)"></span>'+
                                  '     <i ng-show="this.state.deletingCommand===$index" class="icon-details fa fa-spinner"></i>'+
                                  '   </div>'+
                                  '   <label ng-show="this.tabsData['+tabId+'].commands.length!==0" ng-click="this.addNote('+tabId+', -1); this.setFocus('+tabId+', -1)" class="col-md-12 addCommand" translate>Add another note to the sheet</label>'+
                                  '   <label ng-show="this.tabsData['+tabId+'].commands.length===0" ng-click="this.addNote('+tabId+', -1); this.setFocus('+tabId+', -1)" class="col-md-12 addCommand" translate>Add one note to the sheet</label>'+
                                  ' </div>'+
                                  ' <div class="col-md-12 text-center panelBottom">'+
                                  '   <button ng-disabled="this.state.isOpenInsertion==true || this.state.deletingCommand!==-1" loading-no-restmod="true" loading-click="this.createSheet('+tabId+')" class="btn blue" translate>Create Sheet</button>'+
                                  '   <button ng-disabled="this.state.isOpenInsertion==true || this.state.deletingCommand!==-1" loading-click="this.removeTab(\''+tabId+'\',true)" class="btn default" translate>Delete Sheet</button>'+
                                  ' </div>'+
                                '</div>')( $scope );
        elem.find('.containerNote').append(noteElem);
      };
    }
  };
});

