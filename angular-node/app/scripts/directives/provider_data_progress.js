
'use strict';
var projectApp = angular.module('projectApp');


projectApp.directive('providerDataProgress', function () {

  return {
    restrict: 'E',
    replace: true,
    template: '<div class="provider-data-progress-container">' +
                '<span>&nbsp;</span>' +
              '</div>',
    scope: {},

    link: function(scope, element, attributes) {

      scope.text = attributes["percentage"];


      var calcEscencialData = function() {
        var percent = 0, numeroAtributos = 5, totaldatosEsenciales = 100;
        var pesoAtributo = totaldatosEsenciales / numeroAtributos;
        percent += (String(attributes["nombreProveedor"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["nombre"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["apellido"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["email"]).trim() != "")? pesoAtributo : 0 ;
        percent += (String(attributes["id"]).trim() != "")? pesoAtributo : 0 ;
        return percent;
      };

      

      var totalProgress = calcEscencialData();

      var colorClass = 'redish';
      if (totalProgress > 30) { colorClass = 'yellowish'; }  // being 30 an arbitrary value between red and yellow
      if (calcEscencialData() > 65) { colorClass = 'greenish'; }
      var span = element.children()[0];
      $(span).addClass(colorClass);
      $(span).css({ "width": String(totalProgress) + "%" });

    }
  };
});
