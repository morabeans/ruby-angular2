'use strict';
var projectApp = angular.module('projectApp');

projectApp.directive('bootstrapAlert', function ($timeout,gettextCatalog) {
	return {
			restrict: 'E',
			scope: { type:"@ngfType",
					 close:"@ngfClose",
					 timeout:"@ngfTimeout"

		},
		replace:true,
		transclude: true,
		template:''+
					'<div id="bootstrap_alert_'+Math.floor((Math.random()*999)+1)+'" style="padding-left: 30px; clear:both;" class="Metronic-alerts alert alert-{{type}} fade in">'+
						'<button ng-show="close==undefined || close" type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>'+
							'<i class="fa-lg fa fa-{{type}}" style="margin-left: -20px;"></i>'+
							'<span ng-transclude></span>'+
					'</div>'+
				'',
		controller : function($scope, $element, $attrs, $transclude){

			$scope.closeDestroy = function(seg){
				$timeout(function(){
					$scope.$destroy();
				},seg*1000);
			};
		},
		link : function($scope, $element, $attrs,$ctrl,$transclude){
			$transclude($scope,function(clone) {
				//$element.find('span').html(gettextCatalog.getString(clone[0].textContent));
            });
			if($scope.timeout!=undefined){
				$scope.closeDestroy($scope.timeout);
			}else{
				$scope.closeDestroy(10);
			}
			$scope.$on("$destroy",function() {
                $element.remove();
            }); 
		}
	}
});
//
