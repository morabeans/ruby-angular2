'use strict';
var projectApp = angular.module('projectApp');
projectApp.directive('sheetPanelCommand', function ($compile, SheetDetail, SheetDetailNote, Client, Sheet, $timeout){
  return {
  	restrict: 'E',
    scope:{
      sheet:"=ngSheet"
    },
  	template: '<div class="commandPanelContainer top-sheet-command-panel">'+
                '<div ng-click="showHideBothPanel()" class="eyelash eyelash-color-sheet-command-panel" >'+
                '  <span class="icon-book-open"></span>'+
                '</div>'+
                '<div class="div-modal-notes" ng-show="showSpinner==true">'+
                '</div>'+
                '<div class="div-modal-new-tab" ng-show="showModal==true">'+
                '  <div class="form-new-note">'+
                '    <input class="input-new-note" ng-model="textNote" placeholder="{{\'Title\' | translate}}" type="text">'+
                '    <button ng-click="addTab(textNote, true)" ng-disabled="textNote.length <= 0" class="button-new-note-save btn blue pull-right" translate>Save</button><button ng-click="showModal=false;textNote=\'\'" class="button-new-note-cancel btn default pull-right" translate>Cancel</button>'+
                '  </div>'+
                '</div>'+
                '<div class="panelContainer">'+
                '  <i ng-show="isLoadData===false" class="fa fa-spinner"></i><span ng-show="isLoadData===false" translate>LOADING</span>'+
                '  <div ng-show="isLoadData===true && tabActive != 0" ng-class="{\'move-elements-note\':moveItems > 0, \'panel-up-search\':moveItems <= 0}">'+
                '    <span ng-click="actionNote=\'cancel\'" class="icon-back-move icon-arrow-left"></span><!--<span class="back-text-move">Back</span>-->'+
                '    <!--<span class="title-move-note">{{tabs[tabActive].title}}</span>--><span class="text-selected-move">{{moveItems}} <ng-pluralize count="moveItems" when="{\'1\': \'{{noteSingular | translate}}\',\'other\':\'{{notePlural | translate}}\'}"></ng-pluralize></span>'+
                '    <i ng-click="actionNote=\'delete\'" class="icon-trash-move fa fa-trash-o"></i><i ng-click="showListTab=!showListTab" class="icon-menu-move fa fa-ellipsis-v"></i>'+
                '    <div class="div-search-notes">'+
                '      <span class="icon-search-notes glyphicon glyphicon-search"></span><input class="text-search-notes" type="text" placeholder="{{\'Search\' | translate}}">'+
                '    </div>'+
                '    <div ng-show="showListTab==true" class="div-list-move-notes">'+
                '      <div ng-click="actionNote=\'delete\'" class="delete-inside-menu"><span><i class="icon-delete-inside-menu fa fa-trash-o"></i></span><span class="text-delete-inside-menu" translate>Delete</span></div>'+
                '      <div class="move-inside-menu"><span class="text-move-inside-menu" translate>Move selected to...</span></div>'+
                '      <ul class="list-move-notes">'+
                '        <li ng-click="setValueAction(\'move\', $index); showListTab=false" ng-show="$index != 0 && $index != tabActive" ng-repeat="tab in tabs track by $index"><span class="icon-li-move"><i class="fa fa-folder-open-o"></i></span><span class="text-li-move">{{tab.title}}</span></li>'+
                '      </ul>'+
                '      <div ng-click="createTab(); showListTab=false" class="create-inside-menu"><span class="icon-create-inside-menu"><i class="fa fa-plus"></i></span><span class="text-create-inside-menu" translate>Create new...</span></div>'+
                '    </div>'+
                '  </div>'+
                '  <div ng-show="isLoadData===true" class="panelTabs">'+
                '    <div ng-show="isOpenSecond===false" class="addTab pull-right" ng-click="addTab(\'New Note\', false)">'+
                '      <span class="glyphicon glyphicon-plus"></span>'+
                '    </div>'+
                '    <div class="containerTabs">'+
                '      <ul class="nav nav-tabs">'+
                '      <div class="panelError Metronic-alerts alert alert-danger fade in">'+
                '      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button><span class="errorInfo"></span></div>'+
                '        <li ng-show="(isOpenSecond===false || (isOpenSecond===true && tabActive===$index))" ng-repeat="tab in this.tabs track by $index" ng-class="{\'active\': tabActive==$index}"  >'+
                '          <div class="tab" style="float:left;" ng-click="setTab($index)">'+
                '            <span ng-show="tab.editable===false" ng-click="editTitle($index)">{{tab.title}}</span>'+
                '            <input class="inputSheetName" ng-model="tab.title" ng-show="tab.editable===true" ng-enter="editTitle($index); saveNote(false)" type="text" ng-blur="saveNote(false)">'+
                '          </div>'+
                '        </li>'+
                '      </ul>'+
                '    </div>'+
                '  </div>'+
                ' <div id="commandsProjectContainer-{{sheet.id}}" ng-show="isLoadData===true" class="row-fluid commandsProjectContainer containerNote" ng-class="{\'change-size-div-notes\':tabActive==0}">'+
                ' </div>'+
                '</div>'+
                '<div class="panelInsertion panel-insertion-sheet"> '+
                '</div>'+
              '</div>',
    controller: function ($scope, $element, $attrs) {
      $('.top-sheet-command-panel').click(function(event) {
        $('html, .top-command-panel').one('click',function() {
          if ($scope.isOpenFirst)
            $scope.showHideBothPanel();
        });
        event.stopPropagation();
      });
      $('.top-sheet-command-panel').click(function(e){
        if ($scope.showListTab)
          $scope.showListTab = false;
        var target = $(e.target);
        if (!target.is('.panelContainer .fa-ellipsis-v') &&
          !target.is('.panelContainer .div-list-move-notes div') &&
          !target.is('.panelContainer .div-list-move-notes div span') &&
          !target.is('.panelContainer .div-list-move-notes .list-move-notes') &&
          !target.is('.panelContainer .div-list-move-notes .list-move-notes li') &&
          !target.is('.panelContainer .div-list-move-notes .list-move-notes li span') && 
          !target.is('.panelContainer .div-list-move-notes .list-move-notes div')){
          $element.find('.commandPanelContainer').find('.panelContainer').find('.div-list-move-notes').css('display','none');
        }
        else{
          $element.find('.commandPanelContainer').find('.panelContainer').find('.div-list-move-notes').css('display','block');
        }
      });
      $scope.tabs = [];
      $scope.tabActive=0;
      $scope.showSpinner = false;
      $scope.moveItems = 0;
      $scope.actionNote = "";
      $scope.showListTab = false;
      $scope.textNote = "";
      $scope.showModal = false;
      $scope.noteSingular = "note";
      $scope.notePlural = "notes";
      $scope.addCommand = false;
      $scope.loadData = function(){
        $scope.tabs = [{'title':'Overview','editable':false,'note':'','noteCmp':'','commands':[]},{'title':'Notes','editable':false,'note':'','noteCmp':'','commands':[]}];
        var h = 0;
        var cont = 0;
        //Sheet.$find($scope.sheet.id).$then(function(_sheet){
          for (var j = 0; j < _.size($scope.sheet.sheetDetails); j++){
            h = j + 1;
            if (j >= 1)
              $scope.tabs.push({'title':'','editable':true,'note':'','noteCmp':'','commands':[]});
            $scope.tabs[h].title = (h === 1 ? 'Notes' : $scope.sheet.sheetDetails[j].title);
            $scope.tabs[h].editable = (h === 1 ? false : true);
            $scope.tabs[h].note = $scope.sheet.sheetDetails[j];
            $scope.tabs[h].noteCmp = angular.copy($scope.sheet.sheetDetails[j]);
            $scope.tabs[h].commands = [];
            if (_.size($scope.sheet.sheetDetails[j].sheetDetailNotes) > 0){
              for (var i = 0; i < _.size($scope.sheet.sheetDetails[j].sheetDetailNotes); i++){
                $scope.tabs[h].commands.push(new $scope.noteInfo());
                $scope.tabs[h].commands[i].sheetDetailNote = $scope.sheet.sheetDetails[j].sheetDetailNotes[i];
                $scope.tabs[h].commands[i].sheetDetailNoteCmp = angular.copy($scope.sheet.sheetDetails[j].sheetDetailNotes[i]);
                $scope.tabs[h].commands[i].client = Client.$build({});
              }
            }
            cont++;
          }
          if (cont === _.size($scope.sheet.sheetDetails) || _.size($scope.sheet.sheetDetails) === 0){
            $scope.isLoadData = true;
            $scope.setTab(0);
          }
        //});
      }
      $scope.isOpenFirst = false;
      $scope.isOpenSecond = false;
      $scope.isAnimate = false;
      $scope.isAnimateSecond = false;
      $scope.isLoadData = false;
      $scope.createTab = function(){
        $scope.showModal = true;
        //$element.find('.panelContainer .div-modal-new-tab').find('.input-new-note').focus();
      };
      $scope.setValueAction = function(value, _tab){
        $scope.actionNote = value;
        $scope.indexMoveTab = _tab;
      };
      $scope.editTitle = function(_command){
        if (_command !== 0 && _command !== 1)
          $scope.tabs[_command].editable = !$scope.tabs[_command].editable;
      };
      $scope.setTab = function(_index){
        if (_index != $scope.tabActive)
          $scope.actionNote = "cancel";
        $scope.tabActive=_index;
        if(_index!=0){
         $element.find(".commandsProjectContainer").html($compile('<commands-project ng-add-command-note="addCommand" ng-show-list-tab="showListTab" ng-move-tab="indexMoveTab" ng-action-note="actionNote" ng-move-items="moveItems" ng-show-spinner="showSpinner" ng-tabs="tabs" ng-tab-active="tabActive" show-right-panel="showSecondPanel" hide-right-panel="hideSecondPanel" ng-open-first="isOpenFirst" ng-open-second="isOpenSecond" ng-tab="tabs[tabActive]" ng-sheet="sheet"></commands-project >')($scope));
        }else{
          $element.find(".commandsProjectContainer").html($compile('<overview-panel-command-project ng-tab-active="tabActive" ng-tabs="tabs" ng-sheet="sheet"></overview-panel-command-project >')($scope));
        }
      };
      
      $scope.noteInfo = function(){
        var sheetDetailNote;
        var sheetDetailNoteCmp;
        var client;
        var selected;
      };
      $scope.addTab = function(tabTitle, isCreated){
        $scope.tabs.push({'title':tabTitle,'editable':true,'note':'','noteCmp':'','commands':[]});
        if (!isCreated)
          $scope.setTab($scope.tabs.length - 1);
        $scope.tabs[$scope.tabs.length - 1].note = SheetDetail.$build({
          title: tabTitle,
          sheetId: $scope.sheet.id,
        });
        $scope.tabs[$scope.tabs.length - 1].noteCmp = SheetDetail.$build({});
        $scope.saveNote(isCreated);
      };
      $scope.saveNote = function(callFunction){
        if (!callFunction){
          var update = false;
          if ($scope.tabActive !== -1 && !$scope.detailNoteCmp()){
            if ($scope.tabs[$scope.tabActive].note.id === undefined)
              update = true;
            if (update){
              $scope.tabs[$scope.tabActive].note.$save().$then(function(_sheet_detail){
                $scope.sheet.sheetDetails.push($scope.tabs[$scope.tabActive].note);
                $scope.tabs[$scope.tabActive].noteCmp = angular.copy(_sheet_detail);
              });
            }
            else{
              var index = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
              var tabValue = $scope.tabActive;
              $scope.tabs[$scope.tabActive].note.$save().$then(function(_sheet_detail){
                $scope.sheet.sheetDetails[index] = _sheet_detail;
                $scope.tabs[tabValue].noteCmp = angular.copy(_sheet_detail);
              });
            }
          }
        }
        else{
          var tab = $scope.tabs.length - 1;
          $scope.tabs[tab].note.$save().$then(function(_sheet_detail){
            $scope.sheet.sheetDetails.push($scope.tabs[tab].note);
            $scope.tabs[tab].noteCmp = angular.copy(_sheet_detail);
            $scope.textNote = "";
            $scope.showModal = false;
            $scope.actionNote = "move";
            $scope.indexMoveTab = tab;
          });
        }
      };
      $scope.detailNoteCmp = function(){
        if ($scope.tabs[$scope.tabActive].note.title === $scope.tabs[$scope.tabActive].noteCmp.title)
          return true;
        return false;
      };
      $scope.$watch('tabs[tabActive].title', function(){
        if ($scope.tabs.length > 0)
          $scope.tabs[$scope.tabActive].note.title = $scope.tabs[$scope.tabActive].title;
      });
      $scope.showHideBothPanel = function(){
      	if (!$scope.isAnimate){
      	  $scope.isAnimate = true;
          if (!$scope.isOpenFirst){
            if (!$scope.isLoadData){
              if ($scope.sheet.id == undefined){
                $scope.sheet.$save().$then(function(_sheet){
                  $scope.sheet = _sheet;
                  $scope.sheetDetailNew = SheetDetail.$build({
                    title: "Notes",
                    sheetId: _sheet.id,
                  });
                  $scope.sheetDetailNew.$save().$then(function(_sheet_detail){
                    $scope.sheet.sheetDetails.$add(_sheet_detail);
                    $scope.loadData();
                  });
                });
              }
              else{
                $scope.loadData();
              }
            }
            else{
              if ($scope.tabActive === 0)
                $scope.setTab(0);
            }
            $element.find('.commandPanelContainer').animate({
		          right: "+=560"
		        }, 1000, function() {
		          $scope.isOpenFirst=!$scope.isOpenFirst;
		          $scope.isAnimate = false;
              $scope.$apply();
		        });
          }
          else{
            $element.find('.commandPanelContainer').animate({
		          right: "-" + ($scope.isOpenSecond ? "=1060" : "=560")
		        }, 1000, function() {
		          $scope.isOpenFirst=!$scope.isOpenFirst;
		          $scope.isAnimate = false;
              $scope.$apply();
		        });
		        if ($scope.isOpenSecond){
              $element.find('.panelInsertion').animate({
		            right: "-=500"
		          }, 1000, function() {
		            $scope.isOpenSecond=!$scope.isOpenSecond;
                $scope.$apply();
		          });
		        }
          }
        }
      };

      $scope.showSecondPanel = function(scope){
        if (!$scope.isAnimateSecond){
      	  $scope.isAnimateSecond = true;
          $scope.processing = scope.processingNote;
          $scope.text = scope.textNote;
          $scope.client = Client.$build({});
          $scope.textSave = "Register and Link";
          $scope.textExist = "Link";
          $element.find(".panelInsertion").html($compile('<client-min ng-client="client" ng-text-save="textSave" ng-text-exist="textExist" ng-text-note="text" ng-save="createClient" ng-cancel="hideSecondPanel"></client-min>')($scope));
          $element.find('.panelInsertion').animate({
	          right: "+=500"
	        }, 1000, function() {
	          $scope.isOpenSecond=!$scope.isOpenSecond;
	          $scope.isAnimateSecond = false;
            $scope.$apply();
	        }.bind(scope));

          $element.find('.commandPanelContainer').animate({
            right: "+=500"
          }, 1000, function() {

          });
        }
      };

      $scope.hideSecondPanel = function(scope){
        if (!$scope.isAnimateSecond){
      	  $scope.isAnimateSecond = true;
          $element.find('.panelInsertion').animate({
	          right: "-=500"
	        }, 1000, function() {
	          $scope.isOpenSecond=!$scope.isOpenSecond;
	          $scope.isAnimateSecond = false;
            $scope.$apply();
	        }.bind(scope));
          $element.find('.commandPanelContainer').animate({
            right: "-=500"
          }, 1000, function() {
          });
        }
      };
      $scope.showError = function(msg){
        var msgErrorHtml = $compile('<span translate>'+ msg +'</span>')($scope);
        $element.find('.panelContainer').find('.panelError .errorInfo').html('');
        $element.find('.panelContainer').find('.panelError .errorInfo').html('');
        $element.find('.panelContainer').find('.panelError .errorInfo').append(msgErrorHtml);
        $element.find('.panelContainer').find('.panelError').css('display','block');
        $timeout(function(){
            $element.find('.panelContainer').find('.panelError').css('display','none');
        },3000);
      };
      $scope.$watch('sheet.deleteSheetDetailNoteId', function(){
        if ($scope.sheet.deleteSheetDetailNoteId != -1){
          $scope.removeNoteTab();
        }
      });
      $scope.removeNoteTab = function(){
        var exit = false;
        for (var i = 1; i < $scope.tabs.length; ++i){
          if ($scope.tabs[i].note.id == $scope.sheet.deleteSheetDetailId){
            for (var j = 0; j < $scope.tabs[i].commands.length; ++j){
              if ($scope.tabs[i].commands[j].sheetDetailNote.id == $scope.sheet.deleteSheetDetailNoteId){
                $scope.tabs[i].commands.splice(j, 1);
                exit = true;
                $scope.sheet.deleteSheetDetailId = -1;
                $scope.sheet.deleteSheetDetailNoteId = -1;
                if ($scope.tabs[$scope.tabActive].commands.length == 0)
                  $scope.addCommand = true;
                break;
              }
            }
            if (exit)
              break;
          }
        }
      };
      $scope.checkClient = function(client){
        var exist = false;
        for (var i = 0; i < _.size($scope.sheet.clients); ++i){
          if (client.id === $scope.sheet.clients[i].id){
            exist = true;
            break;
          }
        }
        return exist;
      };
      $scope.searchDetailNote = function(index, id){
        var pos = -1;
        for (var i = 0; i < _.size($scope.sheet.sheetDetails[index].sheetDetailNotes); i++){
          if ($scope.sheet.sheetDetails[index].sheetDetailNotes[i].id === id){
            pos = i;
            break;
          }
        }
        return pos;
      };
      $scope.createClient = function(){
        if ($scope.client.id!==undefined){
          return $scope.sheet.addClients([$scope.client.id], $scope.tabs[$scope.tabActive].commands[$scope.processing].sheetDetailNote.commandText).then(function (){
            var id = $scope.tabs[$scope.tabActive].commands[$scope.processing].sheetDetailNote.id;
            $scope.tabs[$scope.tabActive].commands[$scope.processing].sheetDetailNote.$destroy().$then(function(){
              $scope.tabs[$scope.tabActive].commands.splice($scope.processing, 1);
              if (!$scope.checkClient($scope.client))
                $scope.sheet.clients.push($scope.client);
              var index = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
              var indexAux = $scope.searchDetailNote(index, id);
              if (indexAux !== -1)
                $scope.sheet.sheetDetails[index].sheetDetailNotes.splice(indexAux, 1);
              $scope.hideSecondPanel($scope);
              if ($scope.tabs[$scope.tabActive].commands.length == 0)
                $scope.addCommand = true;
              $scope.sheet.$refresh();
            });
          });
        }
        else{
          return $scope.client.$save().$then(function(_client){
            $scope.sheet.addClients([_client.id], $scope.tabs[$scope.tabActive].commands[$scope.processing].sheetDetailNote.commandText).then(function (){
              var id = $scope.tabs[$scope.tabActive].commands[$scope.processing].sheetDetailNote.id;
              $scope.tabs[$scope.tabActive].commands[$scope.processing].sheetDetailNote.$destroy().$then(function(){
                $scope.tabs[$scope.tabActive].commands.splice($scope.processing, 1);
                if (!$scope.checkClient(_client))
                  $scope.sheet.clients.push(_client);
                var index = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
                var indexAux = $scope.searchDetailNote(index, id);
                if (indexAux !== -1)
                  $scope.sheet.sheetDetails[index].sheetDetailNotes.splice(indexAux, 1);
                $scope.hideSecondPanel($scope);
                if ($scope.tabs[$scope.tabActive].commands.length == 0)
                  $scope.addCommand = true;
                $scope.sheet.$refresh();
              });
            });
          });
        }
      };
    }
  }
});

projectApp.directive('overviewPanelCommandProject', function ($compile) {
  return {
    restrict: 'E',
    scope :{
      tabs:"=ngTabs",
      sheet:"=ngSheet",
      tabActive:"=ngTabActive"
    },
    template:'<div ng-init="filterNotes()">'+
             '  <div class="wrap-charts">'+
             '    <div class="div-chart-client">'+
             '      <label class="label-chart-client"><strong>CUSTOMERS ({{totalClient}})</strong></label>'+
             '      <canvas id="pie" class="chart chart-pie" data="dataClient" colours="colorClient" legend="true" labels="labelsClient"></canvas> '+
             '    </div>'+
             '    <div class="div-chart-service">'+
             '      <label class="label-chart-service"><strong>SERVICES ({{totalService}})</strong></label>'+
             '      <canvas id="pie" class="chart chart-pie" data="dataService" colours="colorService" legend="true" labels="labelsService"></canvas> '+
             '    </div>'+
             '  <div class="footer-overview">'+
             '    <p class="footer-text-overview"><span translate>Total other notes</span>: {{noLabel}}</p>'+
             '    <ul class="list-tab-notes" ng-show="noLabel > 0">'+
             '      <li ng-show="cont > 0" ng-repeat="cont in noLabelCont track by $index">'+
             '        <i class="fa fa-angle-right"></i><span>{{tabs[$index].title}}: {{cont}}</span>'+
             '      </li>'+
             '    </ul>'+
             '  </div>'+
             '  </div>'+
             '</div>',
    controller: function ($scope, $element, $attrs) {
       $scope.labelsClient = ['Identified', 'In Notes'];
       $scope.dataClient = [0, 0];
       $scope.colorClient = ['#97BBCD','#DCDCDC'];
       $scope.labelsService = ['Flight', 'Lodging', 'Car Rental', 'Processed'];
       $scope.dataService = [0, 0, 0];
       $scope.colorService = ['#97BBCD','#DCDCDC','#46BFBD', '#4D5360'];
       $scope.totalClient = 0;
       $scope.totalService = 0;
       
       $scope.filterNotes = function(){
         $scope.dataCont = [0,0,0,0];
         $scope.noLabel = 0;
         $scope.noLabelCont = [];

         for (var i = 0; i < $scope.tabs.length; i++){
           $scope.noLabelCont.push(0);
           for (var j = 0; j < $scope.tabs[i].commands.length; j++){
             if ($scope.tabs[i].commands[j].sheetDetailNote.typeCommand !== "" && $scope.tabs[i].commands[j].sheetDetailNote.typeCommand !== null
              && $scope.tabs[i].commands[j].sheetDetailNote.typeCommand !== undefined){
               if ($scope.tabs[i].commands[j].sheetDetailNote.typeCommand === '#name')
                 $scope.dataCont[0]++;
               else if ($scope.tabs[i].commands[j].sheetDetailNote.typeCommand === '#flight')
                 $scope.dataCont[1]++;
               else if ($scope.tabs[i].commands[j].sheetDetailNote.typeCommand === '#lodging')
                 $scope.dataCont[2]++;
               else if ($scope.tabs[i].commands[j].sheetDetailNote.typeCommand === '#carRental')
                 $scope.dataCont[3]++;
             }
             else if ($scope.tabs[i].commands[j].sheetDetailNote.commandText !== ""){
               $scope.noLabel++;
               $scope.noLabelCont[i]++;
             }
           }
         }
         $scope.totalClient = _.size($scope.sheet.clients) + $scope.dataCont[0];
         $scope.totalService = _.size($scope.sheet.servicesList) + $scope.dataCont[1] + $scope.dataCont[2] + $scope.dataCont[3];
         $scope.dataClient = [_.size($scope.sheet.clients), $scope.dataCont[0]];
         $scope.dataService = [$scope.dataCont[1], $scope.dataCont[2], $scope.dataCont[3], _.size($scope.sheet.servicesList)];
       };
    },
    link: function  ($scope, $element, $attrs) {

    }
  }

});
projectApp.directive('commandsProject', function ($compile,MessageUtils,gettextCatalog, SheetDetailNote, Client, Sheet, SheetDetail) {
  return {
    restrict: 'E',
    scope :{
      tab:"=ngTab",
      sheet:"=ngSheet",
      openFirst:"=ngOpenFirst",
      openSecond:"=ngOpenSecond",
      showRightPanel:"=showRightPanel",
      hideRightPanel:"=hideRightPanel",
      tabActive:"=ngTabActive",
      tabs:"=ngTabs",
      showSpinner:"=ngShowSpinner",
      moveItems:"=ngMoveItems",
      actionNote:"=ngActionNote",
      indexMoveTab:"=ngMoveTab",
      showListTab:"=ngShowListTab",
      addCommandNote:"=ngAddCommandNote"
    },
    template:'<div ng-init="checkCommand()" class="container-notes container-notes-sheet">'+
             '  <div ng-class="{\'active\': (openSecond==true && processingNote==$index),\'note-selected\':isInDrag($index) == true}" class="note" ng-repeat="command in tab.commands track by $index">'+
             '    <div>'+
             '      <ul id="panelup-{{$index}}-{{sheet.id}}" ng-show="$index==indexShowPanelUp && openSecond===false" class="dropdown-menu panel-command-help panel-command-help-sheet" >'+
             '        <li class="title">'+
             '        <span class="header_help"><strong>tab</strong>&nbsp; or &nbsp;<strong>↑</strong> <strong>↓</strong>&nbsp; to navigate <strong class="left_margin">↵</strong>&nbsp; to select <strong class="left_margin">esc</strong>&nbsp; to dismiss</span> '+
             '        </li>'+
             '        <li ng-class="{\'active\':lastWord==key || indexCommand==$index}" ng-mouseover="setContent(key, indexShowPanelUp, false)" ng-click="setContent(key, indexShowPanelUp, true)" class="command" ng-repeat="(key, value) in matchCommands track by $index" >'+
             '        <span><strong>{{key}}</strong>{{value | translate}}</span>'+
             '        </li>'+
             '      </ul>'+
             '      <div ng-class="{\'checkbox-notes\':isCheckbox==true}" class="containerIconCheck">'+
             '        <div class="icon-drag-select"><input ng-click="addMoveItems($index)" class="checkItem" id="checkItem-{{$index}}" type="checkbox"/></div>'+
             '        <span ng-class="{\'selected\':command.sheetDetailNote.typeCommand==\'#name\' && command.sheetDetailNote.processed===true}" class="icon-span-processed-sheet glyphicon glyphicon-user"></span>'+
             '        <span ng-class="{\'selected\':command.sheetDetailNote.typeCommand==\'#flight\'}" class="icon-span-processed-sheet glyphicon glyphicon-plane"></span>'+
             '        <span ng-class="{\'selected\':command.sheetDetailNote.typeCommand==\'#lodging\'}" class="icon-span-processed-sheet glyphicon glyphicon-home"></span>'+
             '        <i ng-class="{\'selected\':command.sheetDetailNote.typeCommand==\'#carRental\'}" class="icon-center-processed-sheet fa fa-car"></i>'+
             '        <span ng-class="{\'selected\':(command.sheetDetailNote.classification==\'notes\' || (command.sheetDetailNote.typeCommand==\'#name\' && command.sheetDetailNote.processed==false))}" class="icon-span-processed-sheet glyphicon glyphicon-comment"></span>'+
             '      </div>'+
             '      <input id="note-{{$index}}-{{sheet.id}}" ui-keydown="{\'tab\':\'moveCommand($event)\',38:\'cancelMove($event)\'}" class="input-command-sheet" ng-model="command.sheetDetailNote.commandText"'+
             '       ng-keyup="handlePanelCommand($event, $index)" type="text" ng-class="{\'active\': command.sheetDetailNote.processed===true || command.sheetDetailNote.classification==\'services\',\'note-selected\':isInDrag($index) == true}"'+
             '       ng-blur="editContent($index, false)" ng-focus="((openSecond===true && processingNote!==$index) ? hideSecondPanel() : 0)"></div>'+
             '      <ul id="panel-{{$index}}-{{sheet.id}}" ng-show="$index==indexShowPanel && openSecond===false" class="dropdown-menu panel-command-help panel-command-help-sheet" >'+
             '        <li ng-class="{\'active\':lastWord==key || indexCommand==$index}" ng-mouseover="setContent(key, indexShowPanel, false)" ng-click="setContent(key, indexShowPanel, true)" class="command" ng-repeat="(key, value) in matchCommands track by $index" >'+
             '        <span><strong>{{key}}</strong>{{value | translate}}</span>'+
             '        </li>'+
             '        <li class="title">'+
             '        <span class="header_help"><strong>tab</strong>&nbsp; or &nbsp;<strong>↑</strong> <strong>↓</strong>&nbsp; to navigate <strong class="left_margin">↵</strong>&nbsp; to select <strong class="left_margin">esc</strong>&nbsp; to dismiss</span> '+
             '        </li>'+
             '      </ul>'+
             '      <i ng-show="deletingCommand==-1 && openSecond===false && moveItems <= 0" ng-click="showFirstHelp($index)" class="icon-drag-drop fa fa-ellipsis-v"></i>'+
             '      <div id="cog-{{$index}}" class="move-notes-div">'+
             '        <div ng-click="removeCommand($index)" class="delete-inside-menu"><span><i class="icon-delete-inside-menu fa fa-trash-o"></i></span><span class="text-delete-inside-menu" translate>Delete</span></div>'+
             '        <div class="move-inside-menu"><span class="text-move-inside-menu" translate>Move selected to...</span></div>'+
             '        <ul ng-show="tabs.length > 2" class="tabs-list">'+
             '          <li ng-click="moveCommandNote($index, $parent.$index); showListTab=false" ng-show="$index != 0 && $index != tabActive" ng-repeat="tabTitle in tabs track by $index"><span class="icon-li-move"><i class="fa fa-folder-open-o"></i></span><span class="text-li-move">{{tabTitle.title}}</span></li>'+
             '        </ul>'+
             '        <div ng-click="createTab()" class="create-inside-menu"><span class="icon-create-inside-menu"><i class="fa fa-plus"></i></span><span class="text-create-inside-menu" translate>Create new...</span></div>'+
             '      </div>'+
             '      <span ng-show="command.sheetDetailNote.typeCommand==\'#name\' && deletingCommand==-1 && openSecond===false  && moveItems <= 0" class="icon-details icon-arrow-right" ng-click="showSecondPanel($index)"></span>'+
             '      <span ng-class="{\'active\': (openSecond===true && processingNote==$index)}" class="icon-details icon-arrow-left"'+
             '       ng-show="(openSecond==true && processingNote==$index)" ng-click="hideSecondPanel()"></span>'+
             '      <i ng-show="deletingCommand==$index" class="icon-details fa fa-spinner"></i>'+
             '  </div>'+
             '  <label ng-show="tab.commands.length!=0 && tabActive!=0" ng-click="addCommand(-1); setFocus(-1)" class="col-md-12 addCommand" translate>Add another note to the sheet</label>'+
             '  <label ng-show="tab.commands.length==0 && tabActive!=0" ng-click="addCommand(-1); setFocus(-1)" class="col-md-12 addCommand" translate>Add one note to the sheet</label>'+
             '</div>',
    controller: function ($scope, $element, $attrs) {
      $('.top-sheet-command-panel').click(function(e){
        var target = $(e.target);
        if (!target.is('.container-notes-sheet .fa-ellipsis-v') &&
          !target.is('.container-notes-sheet .move-notes-div div') &&
          !target.is('.container-notes-sheet .move-notes-div div span') &&
          !target.is('.container-notes-sheet .move-notes-div .tabs-list') &&
          !target.is('.container-notes-sheet .move-notes-div .tabs-list li') &&
          !target.is('.container-notes-sheet .move-notes-div .tabs-list li span') && 
          !target.is('.container-notes-sheet .move-notes-div .tabs-list div')){
          $element.find('.container-notes-sheet').find('.move-notes-div').css('display','none');
        }
      });
      $scope.$watch('addCommandNote', function(){
        if ($scope.addCommandNote == true){
          $scope.addCommand(-1);
          $scope.addCommandNote = false;
          $scope.$apply();
        }
      });
      $scope.$watch('actionNote', function(){
        if ($scope.actionNote != ""){
          if ($scope.actionNote == "cancel"){
            $scope.cancelMoveNote();
          }
          else if ($scope.actionNote == "delete"){
            $scope.showSpinner = true;
            if ($scope.dragItems.length == 1){
              $scope.removeCommand($scope.dragItems[0]);
            }
            else{
              $scope.indexDrag = 0;
              $scope.removeAllCommand();
            }
          }
          else if ($scope.actionNote == "move"){
            $scope.showSpinner = true;
            if ($scope.dragItems.length > 0){
              if ($scope.dragItems.length > 1){
                $scope.indexDrag = 0;
                $scope.moveAllCommands($scope.indexMoveTab);
              }
              else{
                $scope.moveCommandNote($scope.indexMoveTab, $scope.dragItems[0]);
              }
            }
            else{
              $scope.moveCommandNote($scope.indexMoveTab, $scope.movingCommand);
              $scope.movingCommand = -1;
            }
          }
          $scope.actionNote = "";
        }
      });
      $scope.cancelMoveNote = function(){
        $('.checkItem').attr('checked', false);
        $scope.isCheckbox = false;
        $scope.moveItems = 0;
        $scope.dragItems = [];
        $scope.showListTab = false;
      };
      $scope.createTab = function(){
        $element.find('.container-notes-sheet').find('.move-notes-div').css('display','none');
        $scope.$parent.createTab();
      };
      $scope.removeAllCommand = function(){
        var sheet_detail_note_ids = "";
        for (var i = 0; i < $scope.dragItems.length; ++i){
          sheet_detail_note_ids += $scope.tab.commands[$scope.dragItems[i]].sheetDetailNote.id;
          sheet_detail_note_ids += ",";
        }
        if (sheet_detail_note_ids.length > 0){
          return $scope.auxSheetDetailNote.deleteSheetDetailNote(sheet_detail_note_ids).then(function(response){
            var index2 = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
            $scope.sheet.sheetDetails[index2].sheetDetailNotes = SheetDetailNote.$search({sheet_detail_id: $scope.sheet.sheetDetails[index2].id}).$then(function(){
              $scope.actTabs(-1, index2, -1);
              $scope.moveItems = 0;
              $scope.dragItems = [];
              $scope.isCheckbox = false;
              $scope.showSpinner = false;
              $scope.showListTab = false;
            });
          });
        }
      };
      $scope.findSelected = function(){
        var pos = -1;
        for (var i = 0; i < $scope.tab.commands.length; ++i){
          if ($scope.tab.commands[i].selected){
            pos = i;
            break;
          }
        }
        return pos;
      };
      $scope.showFirstHelp = function(_command){
        var aux = $('#cog-'+_command).css('display');
        if (aux === 'block'){
          $element.find('.container-notes-sheet').find('#cog-'+_command).css('display', 'none');
          $element.find('.container-notes-sheet').find('#link-'+_command).css('display', 'none');
          $scope.movingCommand = -1;
        }
        else{
          $element.find('.container-notes-sheet').find('.move-notes-div').css('display','none');
          $element.find('.container-notes-sheet').find('#cog-'+_command).css('display', 'block');
          $scope.movingCommand = _command;
        }
      };
      $scope.checkCommand = function(){
        if ($scope.tab.commands.length === 0 && $scope.tabActive!==0){
          $scope.addCommand(-1);
        }
      };
      $scope.commandsList = {
                        '#name':{
                          'helpMessage':' - FirstName LastName [Email] - Add a client to the note',
                          'minParam': 2,
                          'minParamError': 'Type at least one value after the hashtag command.',
                          'classification': 'clients',
                          'icon':'glyphicon-user'
                        },
                        '#flight':{
                          'helpMessage':' - Origin Destination Date Quantity - Add a flight to the note',
                          'minParam': 2,
                          'minParamError': 'Type at least one value after the hashtag command.',
                          'classification': 'services',
                          'icon':'glyphicon-plane'
                        },
                        '#lodging':{
                          'helpMessage':' - City CheckIn CheckOut Quantity - Add a lodging to the note',
                          'minParam': 2,
                          'minParamError': 'Type at least one value after the hashtag command.',
                          'classification': 'services',
                          'icon':'glyphicon-home'
                        },
                        '#carRental':{
                          'helpMessage':' - City Pick-up-date Drop-off-date - Add a car rental to the note',
                          'minParam': 2,
                          'minParamError': 'Type at least one value after the hashtag command.',
                          'classification': 'services',
                          'icon':'fa-car'
                        }
      };
      $scope.indexCommand = -1;
      $scope.showPanel = false;
      $scope.indexShowPanel = -1;
      $scope.deletingCommand = -1;
      $scope.dragItems = [];
      $scope.showSpinner = false;
      $scope.indexDrag = 0;
      $scope.indexShowPanelUp = -1;
      $scope.isCheckbox = false;
      $scope.movingCommand = -1;
      $scope.auxSheetDetailNote = SheetDetailNote.$build({});
      $scope.handlePanelCommand = function(event, _command){
        if (event.which == 27){
          $scope.hidePanel();
        }
        else if (event.which === 38 || event.which === 40){
          if ($scope.showPanel){
            if (event.which === 38){
              if ($scope.indexCommand !== 0 && $scope.indexCommand !== -1)
                $scope.indexCommand--;
              else if ($scope.indexShowPanelUp != -1 && $scope.indexCommand == -1)
                $scope.indexCommand = _.size($scope.matchCommands) - 1;
              event.preventDefault();
            }
            if (event.which === 40){
              if ($scope.indexCommand !== _.size($scope.matchCommands) - 1 && $scope.indexShowPanel != -1)
                $scope.indexCommand++;
              else if ($scope.indexShowPanelUp != -1 && $scope.indexCommand != -1 && $scope.indexCommand !== _.size($scope.matchCommands) - 1)
                $scope.indexCommand++;
            }
          }
          else{
            if (event.which == 38)
             _command -= 1;
            else if (event.which == 40)
              _command += 1;
            $element.find(".container-notes").find("#note-"+_command+"-"+$scope.sheet.id).focus();
          }
        }
        else if (event.which === 13){
          if ($scope.showPanel){
            if ($scope.indexCommand !== -1){
              var keys = Object.keys($scope.matchCommands).sort();
              $scope.setContent(keys[$scope.indexCommand], _command, true);
              $scope.indexCommand = -1;
            }
            else{
              var tokens = $scope.tab.commands[_command].sheetDetailNote.commandText.split(' ');
              if (tokens[0][0] === '#' && tokens.length < $scope.commandsList[tokens[0]].minParam){
                $scope.$parent.showError($scope.commandsList[tokens[0]].minParamError);
              }
              else{
                $scope.addCommand(_command);
                $scope.setFocus(_command);
                $scope.hidePanel();
              }
            }
          }
          else{
            var tokens = $scope.tab.commands[_command].sheetDetailNote.commandText.split(' ');
            if (tokens[0][0] === '#' && tokens.length < $scope.commandsList[tokens[0]].minParam){
              $scope.$parent.showError($scope.commandsList[tokens[0]].minParamError);
            }
            else{
              $scope.addCommand(_command);
              $scope.setFocus(_command);
            }
          }
        }
        else{
          $scope.filterCommand(_command);
        }
      };
      $scope.sheetDetailNoteCmp = function(_command){
        if ($scope.tab.commands[_command].sheetDetailNote.typeCommand === $scope.tab.commands[_command].sheetDetailNoteCmp.typeCommand && 
          $scope.tab.commands[_command].sheetDetailNote.commandText === $scope.tab.commands[_command].sheetDetailNoteCmp.commandText && 
          $scope.tab.commands[_command].sheetDetailNote.typeCommandId === $scope.tab.commands[_command].sheetDetailNoteCmp.typeCommandId && 
          $scope.tab.commands[_command].sheetDetailNote.processed === $scope.tab.commands[_command].sheetDetailNoteCmp.processed && 
          $scope.tab.commands[_command].sheetDetailNote.classification === $scope.tab.commands[_command].sheetDetailNoteCmp.classification)
          return true;
        return false;
      }
      $scope.editContent = function(_command, add){
        var tokens = $scope.tab.commands[_command].sheetDetailNote.commandText.split(' ');
        if (tokens[0][0] === '#' && tokens.length < $scope.commandsList[tokens[0]].minParam){
          $scope.$parent.showError($scope.commandsList[tokens[0]].minParamError);
        }
        else{
          if (tokens[0][0] === '#' && $scope.tab.commands[_command].sheetDetailNote.typeCommand !== tokens[0]){
            $scope.tab.commands[_command].sheetDetailNote.typeCommand = tokens[0];
            $scope.tab.commands[_command].sheetDetailNote.classification = $scope.commandsList[tokens[0]].classification;
          }
          else if (tokens[0][0] !== '#' && !$scope.tab.commands[_command].sheetDetailNote.processed){
            $scope.tab.commands[_command].sheetDetailNote.typeCommand = "";
            $scope.tab.commands[_command].sheetDetailNote.classification = "notes";
          }
          $scope.saveSheetDetailNote(_command);
          $scope.hidePanel();
          add ? ($scope.addCommand(_command), $scope.setFocus(_command)) : "";
        }
      };
      $scope.saveSheetDetailNote = function(_command){
        var update = false;
          if ($scope.tab.commands[_command].sheetDetailNote.id === undefined){
            $scope.tab.commands[_command].sheetDetailNote.sheetDetailId = $scope.tab.note.id;
            update = true;
          }
          if (!$scope.sheetDetailNoteCmp(_command)){
            var index = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
            if (update){
              $scope.tab.commands[_command].sheetDetailNote.$save().$then(function(_sheet_detail_note){
                if (_sheet_detail_note.classification === 'services' || _sheet_detail_note.classification === 'clients'){
                  $scope.sheet.sheetDetails[index].sheetDetailNotes.push($scope.tab.commands[_command].sheetDetailNote);
                }
                $scope.tab.commands[_command].sheetDetailNoteCmp = angular.copy(_sheet_detail_note);
              });
            }
            else{
              $scope.tab.commands[_command].sheetDetailNote.$save().$then(function(_sheet_detail_note){
                var indexAux = $scope.searchDetailNote(index,_sheet_detail_note.id);
                if (indexAux != -1) $scope.sheet.sheetDetails[index].sheetDetailNotes[indexAux] = _sheet_detail_note;
                else if (_sheet_detail_note.classification == 'services' || _sheet_detail_note.classification == 'clients'){
                  $scope.sheet.sheetDetails[index].sheetDetailNotes.push($scope.tab.commands[_command].sheetDetailNote);
                }
                $scope.tab.commands[_command].sheetDetailNoteCmp = angular.copy(_sheet_detail_note);
              });
            }
          }
      };
      $scope.removeCommand = function(_command){
       if (_command < $scope.tab.commands.length && _command > -1){
         if (!$scope.showSpinner){ 
           $scope.deletingCommand = _command;
           $scope.showFirstHelp(_command);
         }
         if ($scope.tab.commands[_command].sheetDetailNote.id !== undefined){
           var id = $scope.tab.commands[_command].sheetDetailNote.id;
           return $scope.tab.commands[_command].sheetDetailNote.$destroy().$then(function(){
             $scope.tab.commands.splice(_command, 1);
             if ($scope.tab.commands.length === 0)
               $scope.addCommand(-1);
             var index = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
             var indexAux = $scope.searchDetailNote(index, id);
             if (indexAux !== -1)
               $scope.sheet.sheetDetails[index].sheetDetailNotes.splice(indexAux, 1);
             $scope.deletingCommand = -1;
             if ($scope.showSpinner){
               $scope.moveItems = 0;
               $scope.dragItems = [];
               $scope.isCheckbox = false;
               $scope.showSpinner = false;
               $scope.showListTab = false;
             }
             $('.checkItem').attr('checked', false);
           });
         }
         else{
           $scope.tab.commands.splice(_command, 1);
           $scope.deletingCommand = -1;
           if ($scope.tab.commands.length === 0)
             $scope.addCommand(-1);
           if ($scope.showSpinner){
             $scope.moveItems = 0;
             $scope.dragItems = [];
             $scope.isCheckbox = false;
             $scope.showSpinner = false;
             $scope.showListTab = false;
           }
           $('.checkItem').attr('checked', false);
         }
       }
       else{
         if ($scope.showSpinner){
           $scope.dragItems = [];
           $scope.isCheckbox = false;
           $scope.moveItems = 0;
           $scope.showSpinner = false;
           $scope.showListTab = false;
         }
       }
      };
      $scope.searchDetailNote = function(index, id){
        var pos = -1;
        for (var i = 0; i < _.size($scope.sheet.sheetDetails[index].sheetDetailNotes); i++){
          if ($scope.sheet.sheetDetails[index].sheetDetailNotes[i].id === id){
            pos = i;
            break;
          }
        }
        return pos;
      };
      $scope.moveCommand = function($event){
        if ($scope.showPanel){
          if ($scope.indexCommand === _.size($scope.matchCommands) - 1 || $scope.indexCommand === -1)
            $scope.indexCommand = 0;
          else $scope.indexCommand++;
          $event.preventDefault();
        }
      };
      $scope.setContent = function(content, _command, filter){
        var contentInput = $scope.tab.commands[_command].sheetDetailNote.commandText.replace(/^\s+|\s+$/gm,'');
        var start = document.getElementById('note-'+_command+"-"+$scope.sheet.id).selectionStart;
        var pre = contentInput.substring(0,start);
        var post = contentInput.substring(start,contentInput.length);
        var preAux = pre.substring(0, pre.lastIndexOf(" ")+1) + content;
        $element.find(".container-notes").find("#note-"+_command+"-"+$scope.sheet.id).focus();
        $scope.tab.commands[_command].sheetDetailNote.commandText = preAux + (post === "" ? post : " " + post);
        if (filter)
          $scope.filterCommand(_command);
        else $scope.indexCommand = -1;
      };
      $scope.filterCommand = function(_command){
        var content = $scope.tab.commands[_command].sheetDetailNote.commandText.replace(/^\s+|\s+$/gm,'');
        var start = document.getElementById('note-'+_command+"-"+$scope.sheet.id).selectionStart;
        var pre = content.substring(0,start);
        var post = content.substring(start,content.length);
        var arrayPrePost = pre.split(" ");
        $scope.lastWord = arrayPrePost[arrayPrePost.length-1];
        var pattern = new RegExp(/^#{1,2}\w*$/g);
        if (pattern.test($scope.lastWord)){
          $scope.matchCommands = {};
          angular.forEach($scope.commandsList, function(value, key){
            if (key.search($scope.lastWord) !== -1){
              $scope.matchCommands[key] = value.helpMessage;
            }
          });
          $scope.indexShowPanel = _command;
          var outter = $('#commandsProjectContainer-'+$scope.sheet.id);
          var inner = $('#note-'+_command+'-'+$scope.sheet.id);
          var panel = $('#panel-'+_command+'-'+$scope.sheet.id);
          if (!$scope.showPanel){
            $scope.showPanel = true;
            setTimeout(function(){
              var offsetOutter = outter.offset();
              var offsetInner = inner.offset();
              if ((outter.height() - (offsetInner.top - offsetOutter.top)) < panel.height()){
                $scope.indexShowPanelUp = _command;
                $scope.indexShowPanel = -1;
                $('#panelup-'+_command+'-'+$scope.sheet.id).css('bottom','26px');
                $('#panelup-'+_command+'-'+$scope.sheet.id).css('top','auto');
              }
              else{
                $scope.indexShowPanel = _command;
                $scope.indexShowPanelUp = -1;
              }
            });
          }
          else{
            if ($scope.indexShowPanelUp != -1){
              $scope.indexShowPanel = -1;
              $('#panelup-'+_command+'-'+$scope.sheet.id).css('bottom','26px');
              $('#panelup-'+_command+'-'+$scope.sheet.id).css('top','auto');
            }
          }
        }
        else if ($scope.lastWord === ""){
          $scope.hidePanel();
        }
      };
      $scope.hidePanel = function(){
        $scope.showPanel = false;
        $scope.indexShowPanel = -1;
        $scope.indexCommand = -1;
        $scope.indexShowPanelUp = -1;
      };
      $scope.addCommand = function(_command){
        if (_command === -2 || _command === -1){
          $scope.tab.commands.push(new $scope.$parent.noteInfo());
          _command = $scope.tab.commands.length == 0 ? 0 : $scope.tab.commands.length - 1;
        }
        else{
          _command += 1;
          $scope.tab.commands.splice(_command, 0, new $scope.$parent.noteInfo());
        }
        $scope.tab.commands[_command].sheetDetailNote = SheetDetailNote.$build({
          typeCommand: "",
          commandText: "",
          typeCommandId: 0,
          processed: false,
          classification: "notes",
          sheetDetailId: $scope.tab.note.id,
        });
        $scope.tab.commands[_command].sheetDetailNoteCmp = SheetDetailNote.$build({});
        $scope.tab.commands[_command].client = Client.$build({});
        $scope.tab.commands[_command].selected = false;
        $scope.saveSheetDetailNote(_command);
      };
      $scope.setFocus = function(_command){
        setTimeout(function(){
          if (_command === -1)
            _command = $scope.tab.commands.length - 1;
          else _command += 1;
          $element.find(".container-notes").find("#note-"+_command+"-"+$scope.sheet.id).focus();
        });
      };
      $scope.cancelMove = function($event){
        if ($scope.showPanel){
          $event.preventDefault();
        }
      };
      $scope.showSecondPanel = function(_command){
        $scope.processingNote = _command;
        $scope.textNote = $scope.tab.commands[_command].sheetDetailNote.commandText;
        $scope.showRightPanel($scope);
      };
      $scope.hideSecondPanel = function(){
        $scope.processingNote = -1;
        $scope.textNote = "";
        $scope.hideRightPanel($scope);
      };
      $scope.checkEmptyNotes = function(_tab){
        var index = _tab > 0 ? (_tab - 1) : 0;
        if ($scope.sheet.sheetDetails[index].sheetDetailNotes.length <= 0){
          if ($scope.tabs[_tab].commands.length > 0)
            $scope.tabs[_tab].commands = [];
        }
      };
      $scope.moveCommandNote = function(_tab, _command){
        
        if (_command >= 0 && _command < $scope.tab.commands.length){
          if (!$scope.showSpinner){
            $scope.deletingCommand = _command;
            $scope.showFirstHelp(_command);
          }
          var sheet_detail_id = $scope.tabs[_tab].note.id;
          var sheet_detail_note_ids = $scope.tab.commands[_command].sheetDetailNote.id + ",";
          return $scope.auxSheetDetailNote.updateSheetDetail(sheet_detail_id, sheet_detail_note_ids).then(function(response){
            var index = _tab > 0 ? (_tab - 1) : 0;
            $scope.sheet.sheetDetails[index].sheetDetailNotes = SheetDetailNote.$search({sheet_detail_id: $scope.sheet.sheetDetails[index].id}).$then(function(){
              var index2 = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
              $scope.sheet.sheetDetails[index2].sheetDetailNotes = SheetDetailNote.$search({sheet_detail_id: $scope.sheet.sheetDetails[index2].id}).$then(function(){
                $scope.actTabs(index, index2, _tab);
                $scope.deletingCommand = -1;
                if ($scope.showSpinner){
                  $scope.dragItems = [];
                  $scope.isCheckbox = false;
                  $scope.moveItems = 0;
                  $scope.showSpinner = false;
                  $scope.showListTab = false;
                }
              });
            });
          });
        }
      };
      $scope.isInDrag = function(_command){
        var exist = false;
        if ($scope.dragItems.indexOf(_command) >= 0)
          exist = true;
        return exist;
      };
      $scope.addMoveItems = function(_command){
        if (!$scope.isInDrag(_command)){
          $scope.dragItems.push(_command);
          $scope.tab.commands[_command].selected = true;
          $scope.moveItems += 1;
        }
        else{
          var pos = $scope.dragItems.indexOf(_command);
          $scope.dragItems.splice(pos, 1);
          $scope.tab.commands[_command].selected = false;
          $scope.moveItems -= 1;
        }
        if ($scope.moveItems > 0){
          $scope.isCheckbox = true;
        }
        else{
          $scope.isCheckbox = false;
          $scope.showListTab = false;
          $scope.dragItems = [];
        }
      };
      $scope.moveAllCommands = function(_tab){
        var sheet_detail_id = $scope.tabs[_tab].note.id;
        var sheet_detail_note_ids = "";
        var empty_ones = 0;
        for (var i = 0; i < $scope.dragItems.length; ++i){
          sheet_detail_note_ids += $scope.tab.commands[$scope.dragItems[i]].sheetDetailNote.id;
          sheet_detail_note_ids += ",";
        }
        if (sheet_detail_note_ids.length > 0){
          return $scope.auxSheetDetailNote.updateSheetDetail(sheet_detail_id, sheet_detail_note_ids).then(function(response){
            var index = _tab > 0 ? (_tab - 1) : 0;
            $scope.sheet.sheetDetails[index].sheetDetailNotes = SheetDetailNote.$search({sheet_detail_id: $scope.sheet.sheetDetails[index].id}).$then(function(){
              var index2 = $scope.tabActive > 0 ? ($scope.tabActive - 1) : 0;
              $scope.sheet.sheetDetails[index2].sheetDetailNotes = SheetDetailNote.$search({sheet_detail_id: $scope.sheet.sheetDetails[index2].id}).$then(function(){
                $scope.actTabs(index, index2, _tab);
                $scope.moveItems = 0;
                $scope.dragItems = [];
                $scope.isCheckbox = false;
                $scope.showSpinner = false;
                $scope.showListTab = false;
                $('.checkItem').attr('checked', false);
              });
            });
          });
        }
      };
      $scope.moveEmptyNotes = function(_tab, empty_ones){
        for (var i = 0; i < empty_ones; ++i){
          $scope.tabs[_tab].commands.push(new $scope.$parent.noteInfo());
          $scope.tabs[_tab].commands[$scope.tabs[_tab].commands.length - 1].sheetDetailNote = SheetDetailNote.$build({
            typeCommand: "",
            commandText: "",
            typeCommandId: 0,
            processed: false,
            classification: "notes",
            sheetDetailId: $scope.tabs[_tab].note.id,
          });
          $scope.tabs[_tab].commands[$scope.tabs[_tab].commands.length - 1].sheetDetailNoteCmp = SheetDetailNote.$build({});
          $scope.tabs[_tab].commands[$scope.tabs[_tab].commands.length - 1].client = Client.$build({});
          $scope.tabs[_tab].commands[$scope.tabs[_tab].commands.length - 1].selected = false;
        }
        $scope.moveItems = 0;
        $scope.dragItems = [];
        $scope.isCheckbox = false;
        $scope.showSpinner = false;
        $scope.showListTab = false;
      };
      $scope.actTabs = function(index, index2, tab){
        if (index != -1){
          $scope.tabs[tab].commands = [];
          if (_.size($scope.sheet.sheetDetails[index].sheetDetailNotes) > 0){
            for (var i = 0; i < _.size($scope.sheet.sheetDetails[index].sheetDetailNotes); i++){
              $scope.tabs[tab].commands.push(new $scope.$parent.noteInfo());
              $scope.tabs[tab].commands[i].sheetDetailNote = $scope.sheet.sheetDetails[index].sheetDetailNotes[i];
              $scope.tabs[tab].commands[i].sheetDetailNoteCmp = angular.copy($scope.sheet.sheetDetails[index].sheetDetailNotes[i]);
              $scope.tabs[tab].commands[i].client = Client.$build({});
            }
          }
        }
        $scope.tab.commands = [];
        if (_.size($scope.sheet.sheetDetails[index2].sheetDetailNotes) > 0){
          for (var i = 0; i < _.size($scope.sheet.sheetDetails[index2].sheetDetailNotes); i++){
            $scope.tab.commands.push(new $scope.$parent.noteInfo());
            $scope.tab.commands[i].sheetDetailNote = $scope.sheet.sheetDetails[index2].sheetDetailNotes[i];
            $scope.tab.commands[i].sheetDetailNoteCmp = angular.copy($scope.sheet.sheetDetails[index2].sheetDetailNotes[i]);
            $scope.tab.commands[i].client = Client.$build({});
          }
        }
        else{
          $scope.addCommand(-1);
        }
      };
      $scope.findNotes = function(id){
        var pos = -1;
        for (var i = 0; i < $scope.tab.commands.length; ++i){
          if ($scope.tab.commands[i].sheetDetailNote.id == id){
            pos = i;
            break;
          }
        }
        return pos;
      };
    }
  }
});