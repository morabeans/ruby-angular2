'use strict';
/* global $ */

var projectApp = angular.module('projectApp');
projectApp.directive('tSort', function () {
  return {
    restrict: 'A',
    scope: {
      defaultCol: '@defaultSort',
      collection: '=collection'
    },
    controller: [
      '$scope',
      function ($scope) {
        $scope.sort = {
          column: '',
          descending: false
        };
        $scope.sort.column = $scope.defaultCol;
        this.getSort = function () {
          return $scope.sort.column + ' ' + ($scope.sort.descending ? 'DESC' : 'ASC');
        };
        this.changeSorting = function (column) {
          if ($scope.sort.column === column) {
            $scope.sort.descending = !$scope.sort.descending;
          } else {
            $scope.sort.column = column;
            $scope.sort.descending = false;
          }
        };
        this.selectedCls = function (element) {
          $('th[c-sort]', element.parent()).removeClass().addClass('sorting');
          $('th[c-sort=\'' + $scope.sort.column + '\']', element.parent()).removeClass().addClass('sorting_' + ($scope.sort.descending ? 'desc' : 'asc'));
        };
        this.refresh = function () {
          $scope.collection.$refresh({ 'order_by': this.getSort() });
        };
      }
    ]
  };
});
projectApp.directive('cSort', function () {
  return {
    require: '^?tSort',
    link: function (scope, element, attrs, tSortCtrl) {
      var clickingCallback = function () {
        scope.$apply(function () {
          tSortCtrl.changeSorting(attrs.cSort);
          tSortCtrl.refresh();
          tSortCtrl.selectedCls(element);
        });
      };
      element.bind('click', clickingCallback);
      element.addClass('sorting');
    }
  };
});
