'use strict';
var projectApp = angular.module('projectApp');

projectApp.directive('client', function ($compile,$window,$document,Client, $timeout,$state,  MessageUtils, $stateParams, Country) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:'<div class="padding-20">'+
                                '<div class="tabbable tabbable-client tabs-left row-fluid">'+
                                  '<ul class="nav nav-tabs pull-left nav-tabs-client">'+
                                      '<li  ng-repeat="tabs in TABS" ng-class="{\'active\': tabActive==$index, \'disabled\':client.id==undefined }" >'+
                                          '<a ng-click="setTabH($index)" >{{ TABS_TITLES[$index] | translate}}</a>'+
                                      '</li>'+
                                  '</ul>'+
                                
                                  '<div class="tab-content pull-left">'+
                                      '<div class="bar-background"></div>'+
                                      '<tabset>'+
                                        '<tab disabled="client.id==undefined" ng-repeat="tab in TABS_V[TABS[tabActive]]" ng-click="setDirective($index)">'+
                                          '<tab-heading ng-click="activeAndDesactiveEditButtonInTabs(\'{{ TABS_TITLES_V[TABS[tabActive]][$index]}}\')">'+
                                            '<i class="fa fa-user"></i><span>{{ TABS_TITLES_V[TABS[tabActive]][$index] | translate }}</span>'+
                                          '</tab-heading>'+
                                        '</tab>'+
                                      '</tabset>'+
                                      '<div id="tab-content-directive" class="row-fluid">'+
                                      '</div>'+
                                  '</div>'+
                              '</div>'+
                  '</div>',
        controller : function($scope, $element, $attrs){
          $scope.tabActive=0;
          

          $scope.TABS =[
            "-General-Information",
            "-Passport-Visa",
            "-Preferences",
            "-Loyalty-Programs",
            "-History"
          ];
          $scope.TABS_TITLES =[
            "General Information",
            "Passport/Visa",
            "Preferences",
            "Loyalty Programs",
            "History"
          ];
          $scope.TABS_V ={
            "-General-Information":['-Basic-Information','-Professional-Information', '-Social-Media', '-Special-Information'],
            "-Passport-Visa":['-Passport','-Visa'],
            "-Preferences":['-Interests','-Hotel', '-Flight'],
            "-Loyalty-Programs":['-Loyalty-Programs'],
            "-History":['-Travels']
          };
          $scope.TABS_TITLES_V ={
            "-General-Information":['Personal Information','Professional Information', 'Social Media', 'Special Information'],
            "-Passport-Visa":['Passport','Visa'],
            "-Preferences":['Interests','Hotel', 'Flight'],
            "-Loyalty-Programs":['Loyalty Programs'],
            "-History":['Travels']
          };

            $scope.creating=($scope.client.id==undefined);

            $scope.countries =  Country.$search({});
            $scope.client_preferences_data={};
            $scope.educations = [];
            $scope.professions = [];
            $scope.catAllergy = [];
            $scope.catDisability = [];
            $scope.allergies = [];
            $scope.disabilities = [];
            $scope.hotel_services = [];
            $scope.tripStyleOptions = {};
            

            /*START STATUS OF LOAD SECTIONS */
            $scope.statusLoadBasicInformation = false;
            $scope.statusLoadProfessionalInformation = false;
            $scope.statusLoadSocialMedia = false;
            $scope.statusLoadSpecialInformation = false;
            $scope.statusLoadPassports = false;
            $scope.statusLoadVisas = false;
            $scope.statusLoadTripStylesAndGroups =false;
            $scope.statusLoadHotelPreferences =false;
            $scope.statusLoadFlyPreferences =false;
            $scope.statusLoadLoyaltyPrograms =false;
            /*END STATUS OF LOAD SECTIONS */
          
            
          /* START FUNCTIONALITY FOR EDIT BUTTON*/
            
            $scope.editButton= true;

            $scope.enabledEditFields = function(){
                $scope.editButton= false;
            };

            $scope.showButtonEdit = function(){
                return $scope.editButton;
            };

            $scope.activeAndDesactiveEditButtonInTabs = function(val){
                                
                if($scope.client.id!==undefined)
                    $scope.editButton = true;

            };

            //monitoreo el valor del clientid
            $scope.$watch('client.id', function() {
                if($scope.client.id==undefined){
                    $scope.editButton = false;
                } else {
                  $scope.editButton = true;
                }  
            }, true);

        
          /* END FUNCTIONALITY FOR EDIT BUTTON*/


          /*START Metodos utiles en las directivas */

           $scope.findIndexByKeyValue = function(array, key, value) {
                  for (var i = 0; i < array.length; i++) {
                    if (array[i][key] == value) {
                      return i;
                    }
                  }
              return null;

           }

          /* END Metodos utiles en las directivas*/
                
          $scope.setTabH= function(tab){
            if($scope.client.id==undefined && tab!=0)
              $scope.tabActive=0;
            else 
              $scope.tabActive=tab;        
          };

          $scope.setDirective  =function(tab){
            if($scope.client.id!=undefined || ($scope.tabActive==0 && tab==0)){
              var directive = "client"+$scope.TABS[$scope.tabActive]+$scope.TABS_V[$scope.TABS[$scope.tabActive]][tab];
              $element.find("#tab-content-directive").html($compile('<'+directive+' ng-client="client"></'+directive+'>')($scope));
            }

          };

          

          if ($stateParams.clientId!==undefined)
                $scope.client.$pk = $stateParams.clientId;


         $scope.$watch('tabActive', function() {
          
          if($scope.tabActive !== undefined){
                $scope.setDirective(0); 
                if($scope.client.id==undefined){
                    $scope.editButton = false;
                } else {
                  $scope.editButton = true;
                }  
          }

         }, true);     

        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});


projectApp.directive('clientGeneralInformationBasicInformation', function ($compile,$window,$document, $timeout,$state, Client, ClientPreferences, MessageUtils, CustomerDate, Address, Phone,  ActionType) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:'<div>'+
                    '<div class="row">'+
                     '<div class="col-md-2">'+
                        '<div class="containerAvatar ">'+
                          '<div class="avatarThumbAvatar">'+
                            '<span ng-show="client.avatar" ng-click="viewImage(client)" class="icon-size-fullscreen"></span>'+
                            '<img ng-file-select="uploadAvatar(client)" ng-model="client.avatarSelect"   ng-multiple="false" ng-accept="\'image/*\'" tabindex="0"  src="{{client.avatarUrlThumb}}" />'+
                            '<div ng-show="uploadingAvatar"  class="progress progress-striped active">'+
                              '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: {{progressUploadAvatar}}%">'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<form name="ClientA">'+
                      '<div class="col-md-10">'+
                          '<div class="col-md-12">'+
                          '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;" ng-click="enabledEditFields()" ng-show="showButtonEdit()" translate>Edit</label>'+
                        '</div>  '+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>CI</label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.identifier}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<input type="text" class="form-control"  ng-model="clientView.identifier" ng-enter="findClient()" ng-blur="findClient()" >'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group" ng-class="{\'has-error\':ClientA.email.$error.email}">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Email</label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.email}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<input type="email" name="email" class="form-control"  ng-model="clientView.email"  >'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht"  translate>First Name</label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.firstName}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<input type="text" class="form-control"  ng-model="clientView.firstName">'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Middle Name</label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.middleName}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<input type="text" class="form-control"  ng-model="clientView.middleName">'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Last Name</label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.lastName}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<input type="text" class="form-control"  ng-model="clientView.lastName">'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Second Last Name </label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.secondLastName}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<input type="text" class="form-control" ng-model="clientView.secondLastName">'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Birth date</label>'+
                            '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.birthDate}}</label>'+
                            '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                              '<div sb-date-select sb-select-class="form-control "  class="prsht-sb-date-select" ng-model="clientView.birthDate"></div>'+
                            '</div>'+
                         '</div>'+
                        '</div>'+
                        '<div class="col-md-6">'+
                          '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Sex</label>'+
                            '<div class="col-md-7">'+
                              '<select ng-disabled="showButtonEdit()" class="form-control nopaddingH" name="sex_{{$index}}" ng-model="clientView.sex">'+
                                '<option value="M" translate>Male</option>'+
                                '<option value="F" translate>Female</option>'+
                              '</select>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="col-md-12 cont-buttons-client">'+
                         ' <div class="col-md-12">'+
                            '<button  class="btn blue pull-right " loading-click="savePersonalBasicDetails()" ng-show="!showButtonEdit()" translate>Save</button>' +
                            '<button class="btn default pull-right" ng-click="undoPersonalBasicDetails()"  ng-show="!showButtonEdit()" translate>Cancel</button>'+
                        '</div>'  +
                        '</div>'  +
                      '</div>'+
                      '</form>'+
                    '</div>'+


                    '<div class="col-md-6" style="overflow:none; margin-bottom:10px; height:auto;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                          '<div class="caption" >'+
                            '<i class="fa fa-phone-square"></i><translate>Telephones</translate>'+
                            '<span class="badge badge-info badge-sheet">{{client.phones.length}} </span>'+
                            '</div>'+
                            '<div class="tools">'+
                              '<div class="option" ng-click="addPhone()">'+
                                '<i class="fa fa-phone-square"></i>'+
                                '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                '<translate>Add</translate>'+
                              '</div>'+
                              '<div class="option" ng-click="phonesCollapse = !phonesCollapse">'+
                                '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !phonesCollapse, \'glyphicon-chevron-up\': phonesCollapse}" class="glyphicon "></span>'+
                             ' </div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="portlet-body sheet-table sheet-table-client nopadding" style="height: auto;" collapse="phonesCollapse" >'+

                            '<div class="btn-toolbar">'+
                             ' <loading-message ng-model="client"></loading-message>'+
                            '</div>'+
                            '<div class="table-responsive">'+
                              '<table name="tablesPhones" class="table-project table table-bordered table-condensed flip-content ">'+
                                '<thead>'+
                                  '<tr>'+
                                    
                                    '<th translate>Number</th>'+
                                    '<th translate>Type</th>'+
                                    '<th translate>Country</th>'+
                                    '<th translate>Actions</th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                  '<tr ng-repeat="phone in client.phones" >'+
                                  
                                    '<td ng-click="editPhone(phone)">{{phone.country.dialCode}} {{phone.number}}</td>'+
                                    '<td ng-click="editPhone(phone)">{{phone.type}}</td>'+
                                    '<td ng-click="editPhone(phone)">{{phone.country.name | translate}}</td>'+
                                    '<td class="text-center">'+
                                      '<a href="javascript:;" loading-click="removePhone(phone)" loading-icon="true" class="btn btn-table red ">'+
                                        '<span class="glyphicon glyphicon-trash">'+
                                        '</span>'+
                                     ' </a>'+
                                    '</td>'+
                                  '</tr>'+
                                '</tbody>'+
                              '</table>'+
                            '</div>' +
                            '<form name="formPhone">'+
                            '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowPhone">'+
                              '<div class="col-md-12">'+
                                '<div class="form-group hidden">'+
                                  '<label class="col-md-5 control-label lb-client-bg-wht" translate>Importance</label>'+
                                  '<div class="col-md-7">'+
                                    '<select class="form-control nopaddingH" name="importance_{{$index}}" ng-model="phone.importance">'+
                                      '<option value="IMPORTANCE_1" >1</option>'+
                                      '<option value="IMPORTANCE_2" >2</option>'+
                                      '<option value="IMPORTANCE_3" >3</option>'+
                                    '</select>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-3 control-label lb-client-bg-wht"><translate>Country</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-9">'+
                                    '<select required="" class="form-control"  ng-model="phone.countryId" ng-options="country.id as country.name | translate for country in countries"></select>'+
                                  '</div>'+
                                '</div>'+
                              '</div> ' +
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-3 control-label lb-client-bg-wht"><translate>Number</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-3">'+
                                    '<span class="form-control" style="border:none; padding-left:0; padding-right:0;  font-size: 13.5px; float:right; text-align:right;">{{countrySelected}}</span>'+
                                  '</div>'+
                                    '<div class="col-md-6">'+
                                  '<input required="" style="padding:0;" type="text" class="form-control " ui-mask="(999) 999-9999" ng-model="phone.number" numbers-only="numbers-only" maxlength="12" model-view-value="true">'+
                                  '</div>'+
                                '</div>'+
                              '</div> ' +
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-3 control-label lb-client-bg-wht"  ><translate>Type</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-9">'+
                                  '<select required="" class="form-control nopaddingH" name="type_{{$index}}" ng-model="phone.type">'+
                                      '<option value="trabajo" translate>Work</option>'+
                                      '<option value="Personal" translate>Personal</option>'+
                                  '</select>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+ 
                              '<div class="col-md-12 cont-buttons-client">'+
                                '<div class="col-md-12">'+
                                  '<button class="btn blue pull-right " loading-click="saveNewPhone()" ng-disabled="!formPhone.$valid" translate>Save</button> '+
                                  '<button class="btn default pull-right" ng-click="undoNewPhone()"  translate>Cancel</button>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '</form>'+
                             '</div>'+
                         ' </div>'+
                        '</div>'+
                      


                    '<div class="col-md-6" style="overflow:none; margin-bottom:10px; height:auto;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                         ' <div class="caption" >'+
                            '<i class="fa fa-calendar"></i><translate>Customer Dates</translate>'+
                            '<span class="badge badge-info badge-sheet">'+
                              '{{client.customerDates.length}} </span>'+
                           ' </div>'+
                            '<div class="tools">'+
                              '<div class="option" ng-click="addCustomerDates()">'+
                                '<i class="fa fa-calendar"></i>'+
                                '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                '<translate>Add</translate>'+
                             ' </div>'+
                              '<div class="option" ng-click="customerDatesCollapse = !customerDatesCollapse">'+
                                '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !customerDatesCollapse, \'glyphicon-chevron-up\': customerDatesCollapse}" class="glyphicon "></span>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="portlet-body sheet-table sheet-table-client nopadding" style="height: auto;" collapse="customerDatesCollapse" >'+
                            '<div class="btn-toolbar">'+
                              '<loading-message ng-model="client"></loading-message>'+
                            '</div>'+
                            '<div class="table-responsive">'+
                              '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                                '<thead>'+
                                  '<tr>'+
                                    '<th translate>Type</th>'+
                                    '<th translate>Date</th>'+
                                    '<th translate>Actions</th>'+
                                    '<th ></th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                  '<tr ng-repeat="customer_dates in client.customerDates" >'+
                                    '<td ng-click="editCustomerDates(customer_dates)">{{customer_dates.typeDate}}</td>'+
                                    '<td ng-click="editCustomerDates(customer_dates)">{{customer_dates.date}}</td>'+
                                    '<td ng-click="editCustomerDates(customer_dates)">{{getActionTypesString(customer_dates.actionTypes)}}</td>'+
                                    
                                    '<td class="text-center">'+
                                     ' <a href="javascript:;" loading-click="removeCustomerDates(customer_dates)" loading-icon="true" class="btn btn-table red ">'+
                                        '<span class="glyphicon glyphicon-trash">'+
                                        '</span>'+
                                      '</a>'+
                                    '</td>'+
                                  '</tr>'+
                               ' </tbody>'+
                              '</table>'+
                            '</div>' +
                            '<form name="formCustomerDate">'+
                            '<div class="row-fluid container-detail-passport" style="padding:0px" ng-show="newOrEditShowCustomerDate">'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>Type</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<select required="" class="form-control nopaddingH" name="typeDate_{{$index}}" ng-model="customerDate.typeDate">'+
                                      '<option value="BIRTHDAY" translate>Birthday</option>'+
                                      '<option value="ANNIVERSARY" translate>Anniversary</option>'+
                                    '</select>'+
                                  '</div>'+
                               ' </div>'+
                              '</div>'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>Date</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<div required="" sb-date-select sb-select-class="form-control "  class="prsht-sb-date-select"  ng-model="customerDate.date"></div>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-md-12">'+
                                '<div class="">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>Action type</translate></label>'+
                                  '<div class="col-md-8">'+
                                    '<div class="action-types" ng-dropdown-multiselect="" options="actionTypes" selected-model="customerDate.actionTypes" extra-settings="newActionTypesSettings"></div>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-md-12 cont-buttons-client">'+
                                '<div class="col-md-12">'+
                                  '<button class="btn blue pull-right " ng-disabled="!formCustomerDate.$valid" loading-click="saveNewCustomerDate()" translate>Save</button> '+
                                  '<button class="btn default pull-right" ng-click="undoNewCustomerDate()"  translate>Cancel</button>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '</form>'+
                          '</div>'+
                        '</div>'+
                     ' </div>'+

                      '<div class="col-md-12" style="overflow:auto; margin-bottom:10px;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                          '<div class="caption" >'+
                            '<i class="fa fa-building-o"></i><translate>Addresses</translate>'+
                            '<span class="badge badge-info badge-sheet">'+
                              '{{client.addresses.length}} </span>'+
                            '</div>'+
                            '<div class="tools">'+
                              '<div class="option" ng-click="addAddress()">'+
                                '<i class="fa fa-building-o"></i>'+
                                '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                '<translate>Add</translate>'+
                              '</div>'+
                             ' <div class="option" ng-click="addressCollapse = !addressCollapse">'+
                                '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !addressCollapse, \'glyphicon-chevron-up\': addressCollapse}" class="glyphicon "></span>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                         ' <div class="portlet-body sheet-table sheet-table-client nopadding" collapse="addressCollapse" >'+
                           ' <div class="btn-toolbar">'+
                              '<loading-message ng-model="client"></loading-message>'+
                            '</div>'+
                            '<div class="table-responsive">'+
                              '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                                '<thead>'+
                                  '<tr>'+
                                    '<th translate>Address</th>'+
                                    '<th translate>Country</th>'+
                                    '<th translate>City</th>'+
                                    '<th >ZIP</th>'+
                                    '<th></th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                  '<tr ng-repeat="address in client.addresses" >'+
                                    '<td ng-click="editAddress(address)">{{address.address1}}</td>'+
                                    '<td ng-click="editAddress(address)">{{address.country.name}}</td>'+
                                    '<td ng-click="editAddress(address)">{{address.city}}</td>'+
                                    '<td ng-click="editAddress(address)">{{address.zip}}</td>'+
                                    '<td class="text-center">'+
                                     ' <a href="javascript:;" loading-click="removeAddress(address)" loading-icon="true" class="btn btn-table red ">'+
                                        '<span class="glyphicon glyphicon-trash">'+
                                        '</span>'+
                                      '</a>'+
                                   ' </td>'+
                                  '</tr>'+
                                '</tbody>'+
                              '</table>'+
                            '</div>' +
                            '<form name="formAddress">'+
                            '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowAddress">'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>Address</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<input required="" type="text" class="form-control"  ng-model="address.address1"  >'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-md-12 hidden">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Address line 2</label>'+
                                  '<div class="col-md-8">'+
                                    '<input type="text" class="form-control"  ng-model="address.address2"  >'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>Country</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<select  required="" class="form-control"  ng-model="address.countryId" ng-options="country.id as country.name for country in countries"></select>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                             ' <div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>State/Province/Region</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<input required="" type="text" class="form-control"  ng-model="address.state"  >'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht"><translate>City</translate><span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<input required="" type="text" class="form-control"  ng-model="address.city"  >'+
                                  '</div>'+
                               ' </div>'+
                              '</div>'+
                              '<div class="col-md-12">'+
                                '<div class="form-group">'+
                                  '<label class="col-md-4 control-label lb-client-bg-wht">ZIP<span class="required" aria-required="true">*</span></label>'+
                                  '<div class="col-md-8">'+
                                    '<input required="" type="text" class="form-control"  ng-model="address.zip"  >'+
                                 ' </div>'+
                                '</div>'+
                              '</div>'+
                               '<div class="col-md-12 cont-buttons-client">'+
                                '<div class="col-md-12">'+
                                  '<button class="btn blue pull-right " loading-click="saveNewAddress()" ng-disabled="!formAddress.$valid" translate>Save</button> '+
                                  '<button class="btn default pull-right" ng-click="undoNewAddress()"  translate>Cancel</button>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '</form>'+
                            '</div>'+
                        '</div>'+
                      '</div>'+
                  '</div>',
        controller : function($scope, $element, $attrs, $state, Client, Phone, CustomerDate, Address){

          $scope.clientView = angular.copy($scope.client);
          $scope.countries = $scope.$parent.countries;
          

          if ($state.current.data.creating) {
            $scope.client.avatarUrlThumb="/images/f6da8af4.avatar-150.jpg";
            $scope.tempData={
                starHover:0
            };
          }

      
        /*##### Informacion General, Datos personales###############*/
         $scope.customerDate = CustomerDate.$build({ clientId : $scope.client.id });
         $scope.address = Address.$build({ clientId : $scope.client.id }); 
         $scope.phone = Phone.$build({ clientId : $scope.client.id });

         $scope.savePersonalBasicDetails = function(){
            $scope.client.identifier = $scope.clientView.identifier;
            $scope.client.firstName = $scope.clientView.firstName;
            $scope.client.middleName = $scope.clientView.middleName;
            $scope.client.email = $scope.clientView.email;
            $scope.client.lastName = $scope.clientView.lastName;
            $scope.client.secondLastName = $scope.clientView.secondLastName;
            $scope.client.birthDate = $scope.clientView.birthDate;
            $scope.client.sex = $scope.clientView.sex;
            return $scope.client.$save().$then(function (_client) {
               
               if ($state.current.data.creating) {

                $scope.client.clientPreferences = ClientPreferences.$build({
                  clientId : _client.id,
                  stars:$scope.tempData.starHover
                });

                $scope.client.clientPreferences.$save().$then(function(){
                  if ($state.current.data.creating) {
                      $state.go('clients.edit', { clientId: _client.id });
                  }else{
                      //$scope.client=_client;
                  }
                });

              }

              $scope.$parent.editButton = true;
            },function(_client){
              MessageUtils.error({ 'error': _client.errors });
            });

         };


        $scope.undoPersonalBasicDetails = function(){
          
          $scope.clientView.identifier = $scope.client.identifier;
          $scope.clientView.firstName = $scope.client.firstName;
          $scope.clientView.middleName = $scope.client.middleName;
          $scope.clientView.email = $scope.client.email;
          $scope.clientView.lastName = $scope.client.lastName;
          $scope.clientView.secondLastName = $scope.client.secondLastName;
          $scope.clientView.birthDate = $scope.client.birthDate;
          $scope.clientView.sex = $scope.client.sex;
          
          if($scope.client.id!=undefined){
            $scope.$parent.editButton = true;
          }
          
         };

        
        $scope.uploadAvatar = function(client){
            if(client.avatarSelect.length>0){
            $scope.uploadingAvatar=true;
            $scope.progressUploadAvatar=0;
             UploadFile.upload(client.avatarSelect).success(function(data, status, headers, config){
              client.avatarId = data['temp_file']['id'];
            }).progress(function (evt) {
              $scope.progressUploadAvatar = parseInt(100.0 * evt.loaded / evt.total);
            });
            }
        };


        $scope.newActionTypes = [];
        $scope.newActionTypesSettings = {externalIdProp: ''};

       $scope.addPhone= function(){
        if($scope.client.id!=undefined){
          $scope.newOrEditShowPhone=true;
          $scope.phone = Phone.$build({
            clientId : $scope.client.id
          });
            $scope.phoneCollapse=false;
          }else{
            $scope.savePersonalBasicDetails();
          }
       };

       $scope.editPhone = function(phone){
          $scope.phone = phone;

          if($scope.phone.country!=null)
                $scope.phone.countryId = $scope.phone.country.id;
          $scope.newOrEditShowPhone=true;

       };

       $scope.removePhone = function(phone){
        if(phone.id == $scope.phone.id ){
            $scope.phone = Phone.$build({
              clientId : $scope.client.id
              });
          }
        return phone.$destroy().$then(function(_phone){
          $scope.client.phones.$remove(_phone);
        });
       };

       $scope.saveNewPhone = function(){
        return $scope.phone.$save().$then(function (_phone) {
            $scope.client.phones.$add(_phone);
            $scope.newOrEditShowPhone=false;
        });
       };

       $scope.undoNewPhone = function(){
        $scope.newOrEditShowPhone=false;
        $scope.phone = Phone.$build({
          clientId : $scope.client.id
          });
       };

        $scope.addCustomerDates = function(){
        if($scope.client.id!=undefined){
          $scope.newActionTypes = [];
          $scope.newOrEditShowCustomerDate=true;
          $scope.customerDate = CustomerDate.$build({
            clientId : $scope.client.id
          });
            $scope.customerDatesCollapse=false;
          }else{
            $scope.savePersonalBasicDetails();
          }
       };

       $scope.editCustomerDates = function(customerDate){
          $scope.customerDate = customerDate;
          //$scope.customerDate.actionTypes = customerDate.actionTypes;
          $scope.newOrEditShowCustomerDate=true;

       };

       $scope.removeCustomerDates = function(customerDate){
        if(customerDate.id == $scope.customerDate.id ){
            $scope.customerDate = CustomerDate.$build({
              clientId : $scope.client.id
              });
          }
        return customerDate.$destroy().$then(function(_customerDate){
          $scope.client.customerDates.$remove(_customerDate);
        });
       };


       $scope.saveNewCustomerDate = function(){
        $scope.newArrayActionTypes = [];
        angular.forEach($scope.customerDate.actionTypes, function(value, key) {
          $scope.newArrayActionTypes.push(value.id);
        });

        return $scope.customerDate.$save().$then(function (_customerDate) {
            _customerDate.addActionType($scope.newArrayActionTypes).then(function(customerDateB){
                _customerDate.actionTypes.length=0;
              for(var j=0; j< customerDateB.data.customer_date.action_types.length; j++){
                    $scope.actionType = ActionType.$build({
                        id :customerDateB.data.customer_date.action_types[j].id,
                        label :customerDateB.data.customer_date.action_types[j].label
                    });
                    $scope.actionType.$pk = customerDateB.data.customer_date.action_types[j].id;
                    _customerDate.actionTypes.$add($scope.actionType);
               }
                $scope.client.customerDates.$add(_customerDate);
              });
            $scope.newOrEditShowCustomerDate=false;
        });
       };


       $scope.undoNewCustomerDate = function(){
        $scope.newOrEditShowCustomerDate=false;
        $scope.customerDate = CustomerDate.$build({
          clientId : $scope.client.id
          });
       };

      
       $scope.getActionTypesString = function(array){
        var string = "";
        angular.forEach(array, function(value, key) {
          string+=((key>0)?", ":"")+value.label;
        });
        return string;
       };

        /*Direccioness*/
        $scope.addAddress = function(){
          if($scope.client.id!=undefined){
            $scope.newOrEditShowAddress=true;
            $scope.address = Address.$build({
              clientId : $scope.client.id
            });
              $scope.addressCollapse=false;
          }else{
            $scope.savePersonalBasicDetails();
          }
       };

       $scope.editAddress = function(address){
          $scope.address = address;
          if($scope.address.country!=null)
                $scope.address.countryId = $scope.address.country.id;
          $scope.newOrEditShowAddress=true;
       };

       $scope.removeAddress = function(address){
        if(address.id == $scope.address.id ){
            $scope.address = Address.$build({
              clientId : $scope.client.id
              });
          }
        return address.$destroy().$then(function(_address){
          $scope.client.addresses.$remove(_address);
        });
       };

       $scope.saveNewAddress = function(){
        return $scope.address.$save().$then(function (_address) {
            $scope.client.addresses.$add(_address);
            $scope.newOrEditShowAddress=false;
        });
       };

       $scope.undoNewAddress = function(){
        $scope.address = Address.$build({
          clientId : $scope.client.id
          });
          $scope.newOrEditShowAddress=false;
       };



       //monitoreo el valor del id de pais   
        $scope.$watch('phone.countryId', function() {
          $scope.countrySelected = null;
          if  ($scope.phone.countryId!==undefined){
            $scope.index_result = $scope.findIndexByKeyValue($scope.countries, "id", $scope.phone.countryId)
            if($scope.index_result!==null){
              $scope.countrySelected = $scope.countries[$scope.index_result].dialCode; 
            }  
          } 

        }, true);

        $scope.findIndexByKeyValue = function(array, key, value) {
          return $scope.$parent.findIndexByKeyValue(array, key, value);
        }


        $scope.enabledEditFields = function(){
            return $scope.$parent.enabledEditFields();
        };

        $scope.showButtonEdit = function(){
            return $scope.$parent.showButtonEdit();
        };

        $scope.loadCustomerDates = function(){
          
           $scope.actionTypes = ActionType.$search({});

           if($scope.client.id!=undefined) {
            
            Client.getClientCustomerDates($scope.client.id).then(function(_date){
             
             for(var i = 0; i< _date.data.clients.length; i++){ 
                  $scope.customerDate = CustomerDate.$build({
                    id :_date.data.clients[i].id,
                    date: _date.data.clients[i].date,
                    typeDate :_date.data.clients[i].type_date
                  });
                 
                 $scope.customerDate.$pk = _date.data.clients[i].id;

                 for(var j=0; j< _date.data.clients[i].action_types.length; j++){
                    $scope.actionType = ActionType.$build({
                        id :_date.data.clients[i].action_types[j].id,
                        label :_date.data.clients[i].action_types[j].label
                    });
                    $scope.actionType.$pk = _date.data.clients[i].action_types[j].id;
                    $scope.customerDate.actionTypes.$add($scope.actionType);
                 }
                  
                $scope.client.customerDates.$add($scope.customerDate);
              }
            }); 

            $scope.$parent.statusLoadBasicInformation =true;
         };
     
     }

       
       if($scope.$parent.statusLoadBasicInformation==false){ 
            $scope.loadCustomerDates();
        }
     


        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});


projectApp.directive('clientGeneralInformationProfessionalInformation', function ($compile,$window,$document,Client, $timeout,$state, Education, Profession) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template: '<div class="row">'+
                    '<div class="col-md-12">'+
                      '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;" ng-click="enabledEditFields()" ng-show="showButtonEdit()" translate>Edit</label>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Education Level</label>'+
                        '<div class="col-md-7" >'+
                          '<select ng-disabled="showButtonEdit()" class="form-control" ng-model="clientView.education.id" ng-options="education.id as education.level  for education in $parent.educations"></select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Profession</label>'+
                        '<div class="col-md-7" >'+
                          '<select  ng-disabled="showButtonEdit()" class="form-control" ng-model="clientView.profession.id" ng-options="profession.id as profession.name  for profession in $parent.professions"></select>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Company</label>'+
                        '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.company}}</label>'+
                        '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                          '<input type="text" class="form-control"  ng-model="clientView.company">'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Office</label>'+
                        '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.office}}</label>'+
                        '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                          '<input type="text" class="form-control"  ng-model="clientView.office">'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Work Phone</label>'+
                        '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.telephoneOffice}}</label>'+
                        '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                          '<input type="text" class="form-control "  ng-model="clientView.telephoneOffice" numbers-only="numbers-only" maxlength="12" model-view-value="true">'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Work Address</label>'+
                        '<label class="col-md-7 control-label lb-client-bg-wht" ng-show="showButtonEdit()">{{clientView.workingAddress}}</label>'+
                        '<div class="col-md-7" ng-show="!showButtonEdit()">'+
                          '<input type="text" class="form-control"  ng-model="clientView.workingAddress">'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-12 cont-buttons-client">'+
                      '<div class="col-md-12">'+
                        '<button class="btn blue pull-right " loading-click="saveProfesionalDetails()" ng-show="!showButtonEdit()" translate>Save</button> '+
                        '<button class="btn default pull-right" ng-click="undoProfesionalDetails()" ng-show="!showButtonEdit()" translate>Cancel</button>'+
                      '</div>'+
                   ' </div>'+
                  '</div>',
        controller : function($scope, $element, $attrs){
 
        
        $scope.clientView = angular.copy($scope.client);


        $scope.saveProfesionalDetails = function(){
          if($scope.clientView.education!=null)
            $scope.client.educationId = $scope.clientView.education.id;
          if($scope.clientView.profession!=null)
            $scope.client.professionId = $scope.clientView.profession.id;
          $scope.client.company = $scope.clientView.company;
          $scope.client.office = $scope.clientView.office;
          $scope.client.telephoneOffice = $scope.clientView.telephoneOffice;
          $scope.client.workingAddress = $scope.clientView.workingAddress;
          
          return $scope.client.$save().$then(function (_client) {
            $scope.$parent.editButton = true;
            if ($state.current.data.creating) {
              $state.go('clients.edit', { clientId: _client.id });
            }
            //}else{
              //$scope.client=_client;
            //}
          },function(_client){
            Message.error({ 'error': _client.errors });
          });
        };

        $scope.undoProfesionalDetails = function(){
          if($scope.client.education)
            $scope.clientView.education.id = $scope.client.education.id;
              else 
                $scope.clientView.education = undefined;
              
              if($scope.client.profession)
                $scope.clientView.profession.id = $scope.client.profession.id;
          else 
            $scope.clientView.profession=undefined;
          
          $scope.clientView.company = $scope.client.company;
          $scope.clientView.office = $scope.client.office;
          $scope.clientView.telephoneOffice = $scope.client.telephoneOffice;
          $scope.clientView.workingAddress = $scope.client.workingAddress;
          $scope.$parent.editButton = true;
         };

         $scope.loadProfessionalInformation = function(){
          $scope.$parent.educations = Education.$search({});
          $scope.$parent.professions = Profession.$search({});

          Client.getProfessionalInformation($scope.client.id).then(function(_professional){
            $scope.client.profession = _professional.data.clients[0].profession;
            $scope.client.professionId = _professional.data.clients[0].profession_id;
            $scope.client.education = _professional.data.clients[0].education;
            $scope.client.educationId = _professional.data.clients[0].education_id;
            $scope.client.company = _professional.data.clients[0].company;
            $scope.client.office = _professional.data.clients[0].office;
            $scope.client.telephoneOffice = _professional.data.clients[0].telephone_office;
            $scope.client.workingAddress = _professional.data.clients[0].working_address;
            $scope.clientView = angular.copy($scope.client);
            $scope.$parent.statusLoadProfessionalInformation = true;
          });

         };

         $scope.enabledEditFields = function(){
            return $scope.$parent.enabledEditFields();
        };

        $scope.showButtonEdit = function(){
            return $scope.$parent.showButtonEdit();
        };

        if($scope.$parent.statusLoadProfessionalInformation==false){ 
            $scope.loadProfessionalInformation();
        }


        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});





projectApp.directive('clientGeneralInformationSocialMedia', function ($compile,$window,$document,Client, $timeout,$state, SocialMedia) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div class="col-md-12" style="overflow:auto; margin-bottom:10px;">'+
                    '<div class="portlet box gray">'+
                      '<div class="portlet-title">'+
                        '<div class="caption" >'+
                         ' <i class="fa fa-share-alt"></i><translate>Social Media</translate>'+
                          '<span class="badge badge-info badge-sheet"> {{client.socialMedias.length}} </span>'+
                          '</div>'+
                          '<div class="tools">'+
                            '<div class="option" ng-click="addSocialMedia()">'+
                              '<i class="fa fa-share-alt"></i>'+
                              '<span aria-hidden="true" class="icon-plus blue"></span>'+
                              '<translate>Add</translate>'+
                            '</div>'+
                            '<div class="option" ng-click="socialMediaCollapse = !socialMediaCollapse">'+
                              '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !socialMediaCollapse, \'glyphicon-chevron-up\': socialMediaCollapse}" class="glyphicon "></span>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="socialMediaCollapse" >'+
                          '<div class="btn-toolbar">'+
                            '<loading-message ng-model="client"></loading-message>'+
                          '</div>'+
                          '<div class="table-responsive">'+
                            '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                              '<thead>'+
                                '<tr>'+
                                  '<th translate>Social</th>'+
                                  '<th translate>Type Social</th>'+
                                  '<th></th>'+
                                '</tr>'+
                              '</thead>'+
                              '<tbody>'+
                                '<tr ng-repeat="social_media in client.socialMedias" >'+
                                  '<td ng-click="editSocialMedia(social_media)">{{social_media.name}}</td>'+
                                  '<td ng-click="editSocialMedia(social_media)">{{social_media.url}}</td>'+
                                  '<td class="text-center">'+
                                    '<a href="javascript:;" loading-click="removeSocialMedia(social_media)" loading-icon="true" class="btn btn-table red ">'+
                                      '<span class="glyphicon glyphicon-trash">'+
                                      '</span>'+
                                    '</a>'+
                                 ' </td>'+
                                '</tr>'+
                              '</tbody>'+
                            '</table>'+
                         '</div>' +
                          '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowSocialMedia">'+
                            '<div class="col-md-12">'+
                              '<div class="form-group">'+
                                '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Social</label>'+
                                '<div class="col-md-8">'+
                                  '<input type="text" class="form-control"  ng-model="socialMedia.name"  >'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-md-12">'+
                              '<div class="">'+
                                '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Type Social</label>'+
                                '<div class="col-md-8">'+
                                  '<select ng-model="socialMedia.url" class="form-control" ng-options="social.url as social.name for social in listSocialMedias"></select>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-md-12 cont-buttons-client">'+
                              '<div class="col-md-12">'+
                                '<button class="btn blue pull-right " loading-click="saveNewSocialMedia()" translate>Save</button> '+
                                '<button class="btn default pull-right" ng-click="undoNewSocialMedia()"  translate>Cancel</button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '</div>'+
                   '</div>',
        controller : function($scope, $element, $attrs){
     
        $scope.clientView = angular.copy($scope.client);

        $scope.listSocialMedias = [ // Taken from https://gist.github.com/unceus/6501985
         {name: 'Facebook',  url: 'http://facebook.com'},
         {name: 'Instagram', url: 'http://instagram.com'},
         {name: 'Twitter', url: 'http://twitter.com'},
         {name: 'linkedin', url: 'http://linkedin.com'}
        ];
   
         $scope.addSocialMedia = function(){
          if($scope.client.id!=undefined){
            $scope.newOrEditShowSocialMedia=true;
            $scope.socialMedia = SocialMedia.$build({
              clientId : $scope.client.id
            });
              $scope.socialMediaCollapse=false;
          }else{
            //$scope.$parent.savePersonalBasicDetails();
          }
         };


         $scope.editSocialMedia = function(socialMedia){
            $scope.socialMedia = socialMedia;
            $scope.newOrEditShowSocialMedia=true;
         };

         $scope.removeSocialMedia = function(socialMedia){

          if(socialMedia.id == $scope.socialMedia.id ){
              $scope.socialMedia = SocialMedia.$build({
                clientId : $scope.client.id
                });
            }
          return socialMedia.$destroy().$then(function(_socialMedia){
            $scope.client.socialMedias.$remove(_socialMedia);
          });
         };


         $scope.saveNewSocialMedia = function(){
          return $scope.socialMedia.$save().$then(function (_socialMedia) {
              $scope.client.socialMedias.$add(_socialMedia);
              $scope.newOrEditShowSocialMedia=false;
          });
         };
         

         $scope.undoNewSocialMedia = function(){
          $scope.newOrEditShowSocialMedia=false;
           $scope.socialMedia = SocialMedia.$build({
              clientId : $scope.client.id
              });
         }

         $scope.loadSocialMedias = function(){
            if($scope.client.id!=undefined){
            Client.getSocialMedias($scope.client.id).then(function(_social){
             for (var i= 0; i< _social.data.clients.length; i++){ 
              $scope.socialMedia = SocialMedia.$build({
                id : _social.data.clients[i].id,
                clientId:_social.data.clients[i].client_id,
                name : _social.data.clients[i].social_media.name,
                url : _social.data.clients[i].social_media.url
              });
               $scope.socialMedia.$pk = _social.data.clients[i].id;
               $scope.client.socialMedias.$add($scope.socialMedia);
             }
            });


            $scope.$parent.statusLoadSocialMedia =true;
          }
         };

        

        if($scope.$parent.statusLoadSocialMedia==false){ 
            $scope.loadSocialMedias();
        }


        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});


projectApp.directive('clientGeneralInformationSpecialInformation', function ($compile,$window,$document,Client, $timeout,$state, $stateParams, SocialMedia, SpecialCategory, ClientSpecialComment, SpecialSubCategory, SpecialSubCategoryDetail) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div>'+
                    '<div class="col-md-6" style="overflow:none; margin-bottom:10px; height:auto;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                          '<div class="caption" >'+
                            '<i class="fa fa-medkit"></i><translate>Allergies</translate>'+
                          '<span class="badge badge-info badge-sheet">'+
                            '{{$parent.allergies.length}} </span>'+
                          '</div>'+
                          '<div class="tools">'+
                            '<div class="option" ng-click="addAllergy()">'+
                              '<i class="fa fa-medkit"></i>'+
                              '<span aria-hidden="true" class="icon-plus blue"></span>'+
                              '<translate>Add</translate>'+
                            '</div>'+
                            '<div class="option" ng-click="allergiesCollapse = !allergiesCollapse">'+
                              '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !allergiesCollapse, \'glyphicon-chevron-up\': allergiesCollapse}" class="glyphicon "></span>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="portlet-body sheet-table sheet-table-client nopadding" style="height: auto;" collapse="allergiesCollapse" >'+

                          '<div class="btn-toolbar">'+
                            '<loading-message ng-model="client"></loading-message>'+
                          '</div>'+
                          '<div class="table-responsive">'+
                            '<table name="tablesPhones" class="table-project table table-bordered table-condensed flip-content ">'+
                              '<thead>'+
                                '<tr>'+
                                  '<th translate>Type</th>'+
                                  '<th translate>Detail</th>'+
                                  '<th translate>Comment</th>'+
                                 ' <th ></th>'+
                                '</tr>'+
                              '</thead>'+
                              '<tbody>'+
                                '<tr ng-repeat="allergy in $parent.allergies" >'+
                                  '<td ng-click="editAllergy(allergy)">{{allergy.specialSubCategoryDetail.specialSubCategory.name | translate}}</td>'+
                                  '<td ng-click="editAllergy(allergy)">{{allergy.specialSubCategoryDetail.name | translate}}</td>'+
                                  '<td style="overflow: hidden;max-width:25px;text-overflow:ellipsis;white-space: nowrap;" ng-click="editAllergy(allergy)">{{allergy.comment}}</td>'+
                                  '<td class="text-center">'+
                                    '<a href="javascript:;" loading-click="removeAllergy(allergy)" loading-icon="true" class="btn btn-table red ">'+
                                      '<span class="glyphicon glyphicon-trash">'+
                                      '</span>'+
                                    '</a>'+
                                  '</td>'+
                                '</tr>'+
                              '</tbody>'+
                            '</table>'+
                          '</div>'+
                          '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowAllergy">'+
                            '<div class="col-md-12">'+
                              '<div class="form-group">'+
                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Type</label>'+
                                '<div class="col-md-7">'+
                                  '<i ng-show="categoryAllergyLoading" class="fa fa-spinner"></i><span ng-show="categoryAllergyLoading" translate>LOADING</span>'+
                                  '<select ng-show="!categoryAllergyLoading" class="form-control" ng-change="checkAllergyEdition()" ng-model="clientAllergyComment.specialSubCategoryId" ng-options="subAllergyCategory.id as subAllergyCategory.name | translate for subAllergyCategory in subAllergyCategories">'+
                                  '</select>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-md-12">'+
                              '<div class="form-group">'+
                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Detail</label>'+
                                '<div class="col-md-7">'+
                                  '<i ng-show="subcategoryAllergyLoading" class="fa fa-spinner"></i><span ng-show="subcategoryAllergyLoading" translate>LOADING</span>'+
                                  '<select ng-show="showSubAllergyCategory" class="form-control" ng-change="checkAllergyEdition()" ng-model="clientAllergyComment.specialSubCategoryDetailId" ng-options="subAllergyCategoryDetail.id as subAllergyCategoryDetail.name | translate for subAllergyCategoryDetail in subAllergyCategoryDetails"></select>'+
                                '</div>'+
                              '</div>'+
                            '</div>  '+
                            '<div class="col-md-12">'+
                              '<label class="col-md-4 control-label lb-client-bg-wht" translate>Comment</label>'+
                              '<div class="col-md-8">'+
                                '<textarea maxlength="255" ng-change="checkAllergyEdition()" ng-model="clientAllergyComment.comment" name="comment" class="form-control inputComment" rows="3"></textarea>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-md-12 cont-buttons-client">'+
                              '<div class="col-md-12">'+
                                '<button ng-disabled="cantEditAllergy" class="btn blue pull-right " loading-click="saveNewAllergy()" translate>Save</button> '+
                                '<button class="btn default pull-right" ng-click="undoNewAllergy()"  translate>Cancel</button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6" style="overflow:none; margin-bottom:10px; height:auto;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                          '<div class="caption" >'+
                            '<i class="fa fa-wheelchair"></i><translate>Disabilities</translate>'+
                          '<span class="badge badge-info badge-sheet">'+
                            '{{$parent.disabilities.length}} </span>'+
                          '</div>'+
                          '<div class="tools">'+
                            '<div class="option" ng-click="addDisability()">'+
                              '<i class="fa fa-wheelchair"></i>'+
                              '<span aria-hidden="true" class="icon-plus blue"></span>'+
                              '<translate>Add</translate>'+
                            '</div>'+
                            '<div class="option" ng-click="disabilitiesCollapse = !disabilitiesCollapse">'+
                              '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !disabilitiesCollapse, \'glyphicon-chevron-up\': disabilitiesCollapse}" class="glyphicon "></span>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '<div class="portlet-body sheet-table sheet-table-client nopadding" style="height: auto;" collapse="disabilitiesCollapse" >'+
                          '<div class="btn-toolbar">'+
                            '<loading-message ng-model="client"></loading-message>'+
                          '</div>'+
                          '<div class="table-responsive">'+
                            '<table name="tablesPhones" class="table-project table table-bordered table-condensed flip-content ">'+
                              '<thead>'+
                               ' <tr>'+
                                  '<th translate>Type</th>'+
                                  '<th translate>Detail</th>'+
                                  '<th translate>Comment</th>'+
                                  '<th ></th>'+
                               ' </tr>'+
                              '</thead>'+
                              '<tbody>'+
                                '<tr ng-repeat="disability in $parent.disabilities" >'+
                                  '<td ng-click="editDisability(disability)">{{disability.specialSubCategoryDetail.specialSubCategory.name | translate }}</td>'+
                                  '<td ng-click="editDisability(disability)">{{disability.specialSubCategoryDetail.name | translate}}</td>'+
                                  '<td style="overflow: hidden;max-width:25px;text-overflow:ellipsis;white-space: nowrap;" ng-click="editDisability(disability)">{{disability.comment}}</td>'+
                                  '<td class="text-center">'+
                                   ' <a href="javascript:;" loading-click="removeDisability(disability)" loading-icon="true" class="btn btn-table red ">'+
                                      '<span class="glyphicon glyphicon-trash">'+
                                      '</span>'+
                                    '</a>'+
                                  '</td>'+
                                '</tr>'+
                              '</tbody>'+
                           ' </table>'+
                          '</div>'+
                          '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowDisability">'+
                            '<div class="col-md-12">'+
                              '<div class="form-group">'+
                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Type</label>'+
                                '<div class="col-md-7">'+
                                  '<i ng-show="categoryDisabilityLoading" class="fa fa-spinner"></i><span ng-show="categoryDisabilityLoading" translate>LOADING</span>'+
                                  '<select ng-show="!categoryDisabilityLoading" ng-change="checkDisabilityEdition()" class="form-control" ng-model="clientDisabilityComment.specialSubCategoryId" ng-options="subDisabilityCategory.id as subDisabilityCategory.name | translate for subDisabilityCategory in subDisabilityCategories"></select>'+
                                '</div>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-md-12">'+
                              '<div class="form-group">'+
                                '<label class="col-md-5 control-label lb-client-bg-wht" translate>Detail</label>'+
                                '<div class="col-md-7">'+
                                  '<i ng-show="subcategoryDisabilityLoading" class="fa fa-spinner"></i><span ng-show="subcategoryDisabilityLoading" translate>LOADING</span>'+
                                  '<select ng-show="showSubDisabilityCategory" ng-change="checkDisabilityEdition()" class="form-control" ng-model="clientDisabilityComment.specialSubCategoryDetailId" ng-options="subDisabilityCategoryDetail.id as subDisabilityCategoryDetail.name | translate for subDisabilityCategoryDetail in subDisabilityCategoryDetails"></select>'+
                                '</div>'+
                              '</div>'+
                            '</div> ' +
                            '<div class="col-md-12">'+
                              '<label class="col-md-4 control-label lb-client-bg-wht" translate>Comment</label>'+
                              '<div class="col-md-8">'+
                                '<textarea maxlength="255" ng-change="checkDisabilityEdition()" ng-model="clientDisabilityComment.comment" name="comment" class="form-control inputComment" rows="3"></textarea>'+
                              '</div>'+
                            '</div>'+
                            '<div class="col-md-12 cont-buttons-client">'+
                              '<div class="col-md-12">'+
                                '<button ng-disabled="cantEditDisability" class="btn blue pull-right " loading-click="saveNewDisability()" translate>Save</button> '+
                                '<button class="btn default pull-right" ng-click="undoNewDisability()"  translate>Cancel</button>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                    '</div>'+
                    '</div>',
        controller : function($scope, $element, $attrs){
     
        $scope.clientView = angular.copy($scope.client);
        $scope.categoryAllergyLoading = true;
        $scope.categoryDisabilityLoading = true;
        
        
        $scope.addAllergy = function(){
          $scope.categoryAllergyLoading = true;
          $scope.subcategoryAllergyLoading = false;
          $scope.showSubAllergyCategory = false;

          SpecialSubCategory.$search({ category_id: $scope.$parent.catAllergy }).$then(function(_subAllergyCategories){
            $scope.subAllergyCategories = _subAllergyCategories;
            $scope.categoryAllergyLoading = false;
          });
          if($scope.client.id!=undefined){
            $scope.newOrEditShowAllergy=true;
            $scope.clientAllergyComment = ClientSpecialComment.$build({
              clientId: $scope.client.id,
              specialCategoryId: $scope.$parent.catAllergy
            });
              $scope.allergyCollapse=false;
              $scope.cantEditAllergy = true;
            }else{
              //$scope.$parent.savePersonalBasicDetails();
            }
        };


        $scope.$watch('clientAllergyComment.specialSubCategoryId', function(){
          $scope.cantEditAllergy = true;
          $scope.showSubAllergyCategory = false;
          if ($scope.clientAllergyComment!=null){
            if ($scope.clientAllergyComment.specialSubCategoryId != null){
              $scope.subcategoryAllergyLoading = true;
              SpecialSubCategoryDetail.$search({
                category_id: $scope.$parent.catAllergy, sub_category_id: $scope.clientAllergyComment.specialSubCategoryId
              }).$then(function(_subAllergyCategories){
                $scope.subAllergyCategoryDetails = _subAllergyCategories;
                $scope.subcategoryAllergyLoading = false;
                  $scope.showSubAllergyCategory = true;
              });
            }
            else{
              $scope.subcategoryAllergyLoading = false;
            }
          }
        });

        $scope.checkAllergyEdition = function(){
          if ($scope.clientAllergyComment.clientId != undefined &&
            $scope.clientAllergyComment.specialCategoryId != undefined
            && $scope.clientAllergyComment.specialSubCategoryId != undefined &&
            $scope.clientAllergyComment.specialSubCategoryDetailId != undefined &&
            ($scope.clientAllergyComment.comment != undefined && $scope.clientAllergyComment.comment != "")){

            $scope.cantEditAllergy = false;
          }
          else{
            $scope.cantEditAllergy = true;
          }
        };

        $scope.saveNewAllergy = function(){
          return $scope.clientAllergyComment.$save().$then(function(_clientAllergy){
            $scope.$parent.allergies.$add(_clientAllergy);
            $scope.newOrEditShowAllergy = false;
          });
        };

        $scope.undoNewAllergy = function(){
          $scope.newOrEditShowAllergy=false;
          $scope.clientAllergyComment = ClientSpecialComment.$build({
            clientId : $scope.client.id
            });
         };

         $scope.removeAllergy = function(allergy){
          if ($scope.clientAllergyComment != null && allergy.id == $scope.clientAllergyComment.id ){
            $scope.clientAllergyComment = ClientSpecialComment.$build({
              clientId : $scope.client.id
              });
          }
          return allergy.$destroy().$then(function(_allergy){
            $scope.$parent.allergies.$remove(_allergy);
          });
         };

        $scope.editAllergy = function(allergy){
          $scope.categoryAllergyLoading = true;
            $scope.subcategoryAllergyLoading = true;
            $scope.showSubAllergyCategory = false;
          SpecialSubCategory.$search({ category_id: $scope.$parent.catAllergy }).$then(function(_subAllergyCategories){
            $scope.subAllergyCategories = _subAllergyCategories;
            $scope.clientAllergyComment = allergy;
            $scope.categoryAllergyLoading = false;
            $scope.subcategoryAllergyLoading = false;
            $scope.showSubAllergyCategory = true;
            $scope.newOrEditShowAllergy = true;
            $scope.checkAllergyEdition();
          });
         
         };

        $scope.addDisability = function(){
          $scope.categoryDisabilityLoading = true;
          $scope.subcategoryDisabilityLoading = false;
          $scope.showSubDisabilityCategory = false;

            SpecialSubCategory.$search({ category_id: $scope.$parent.catDisability }).$then(function(_subDisabilityCategories){
            $scope.subDisabilityCategories = _subDisabilityCategories;
            $scope.categoryDisabilityLoading = false;
          });
          if($scope.client.id!=undefined){
            $scope.newOrEditShowDisability=true;
            $scope.clientDisabilityComment = ClientSpecialComment.$build({
              clientId: $scope.client.id,
              specialCategoryId: $scope.$parent.catDisability
            });
              $scope.disabilityCollapse=false;
              $scope.cantEditDisability = true;
            }else{
              //$scope.$parent.savePersonalBasicDetails();
            }
        };

        $scope.$watch('clientDisabilityComment.specialSubCategoryId', function(){
          $scope.cantEditDisability = true;
          $scope.showSubDisabilityCategory = false;
          if ($scope.clientDisabilityComment!=null){
            if ($scope.clientDisabilityComment.specialSubCategoryId != null){
              $scope.subcategoryDisabilityLoading = true;
              SpecialSubCategoryDetail.$search({
                category_id: $scope.$parent.catDisability, sub_category_id: $scope.clientDisabilityComment.specialSubCategoryId
              }).$then(function(_subDisabilityCategories){
                $scope.subDisabilityCategoryDetails = _subDisabilityCategories;
                $scope.subcategoryDisabilityLoading = false;
                  $scope.showSubDisabilityCategory = true;
              });
            }
            else{
              $scope.subcategoryDisabilityLoading = false;
            }
          }
        });

        $scope.checkDisabilityEdition = function(){
          if ($scope.clientDisabilityComment.clientId != undefined &&
            $scope.clientDisabilityComment.specialCategoryId != undefined
            && $scope.clientDisabilityComment.specialSubCategoryId != undefined &&
            $scope.clientDisabilityComment.specialSubCategoryDetailId != undefined &&
            ($scope.clientDisabilityComment.comment != undefined && $scope.clientDisabilityComment.comment != "")){

            $scope.cantEditDisability = false;
          }
          else{
            $scope.cantEditDisability = true;
          }
        };

        $scope.saveNewDisability = function(){
          return $scope.clientDisabilityComment.$save().$then(function(_clientDisability){
            $scope.$parent.disabilities.$add(_clientDisability);
            $scope.newOrEditShowDisability = false;
          });
        };

        $scope.undoNewDisability = function(){
          $scope.newOrEditShowDisability=false;
          $scope.clientDisabilityComment = ClientSpecialComment.$build({
            clientId : $scope.client.id
            });
         };

         $scope.removeDisability = function(disability){
          if ($scope.clientDisabilityComment != null && disability.id == $scope.clientDisabilityComment.id ){
              $scope.clientDisabilityComment = ClientSpecialComment.$build({
                clientId : $scope.client.id
                });
            }
          return disability.$destroy().$then(function(_disability){
            $scope.$parent.disabilities.$remove(_disability);
          });
         };

        $scope.editDisability = function(disability){
          $scope.categoryDisabilityLoading = true;
            $scope.subcategoryDisabilityLoading = true;
            $scope.showSubDisabilityCategory = false;
          SpecialSubCategory.$search({ category_id: $scope.$parent.catDisability }).$then(function(_subDisabilityCategories){
            $scope.subDisabilityCategories = _subDisabilityCategories;
            $scope.clientDisabilityComment = disability;
            $scope.categoryDisabilityLoading = false;
            $scope.subcategoryDisabilityLoading = false;
            $scope.showSubDisabilityCategory = true;
            $scope.newOrEditShowDisability = true;
            $scope.checkDisabilityEdition();
          });
        }

        $scope.loadSpecialInformation = function(){
            if($scope.client.id!=undefined){
            SpecialCategory.$search({}).$then(function(_categories){
              $scope.$parent.catAllergy = _categories[0].id;
              $scope.$parent.catDisability = _categories[1].id;
              $scope.$parent.allergies = ClientSpecialComment.$search({
                  client_id: $stateParams.clientId, category_id: $scope.$parent.catAllergy
              });
              $scope.$parent.disabilities = ClientSpecialComment.$search({
                  client_id: $stateParams.clientId, category_id: $scope.$parent.catDisability
              });
              $scope.$parent.statusLoadSpecialInformation =true;
            });
          }
         };

        

        if($scope.$parent.statusLoadSpecialInformation==false){ 
            $scope.loadSpecialInformation();
        }

      
        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});

projectApp.directive('clientPassportVisaPassport', function ($compile,$window,$document,Client, $timeout,$state, UploadFiles, Passport, gettextCatalog, MessageUtils) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div class="row-fluid" style="overflow:auto; margin-bottom:10px;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                          '<div class="caption" >'+
                            '<i class="fa fa-book"></i><translate>Passport</translate>'+
                            '<span class="badge badge-info badge-sheet">'+
                              '{{client.passports.length}} </span>'+
                            '</div>'+
                            '<div class="tools">'+
                              '<div class="option" ng-click="addPassport()">'+
                                '<i class="fa fa-book"></i>'+
                                '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                '<translate>Add Passport</translate>'+
                              '</div>'+
                              '<div class="option" ng-click="client.$refresh()">'+
                                '<span aria-hidden="true" class="glyphicon glyphicon-refresh" ></span>'+
                              '</div>'+
                              '<div class="option" ng-click="passportCollapse = !passportCollapse">'+
                                '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !passportCollapse, \'glyphicon-chevron-up\': passportCollapse}" class="glyphicon "></span>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="passportCollapse" >'+
                            
                            '<div class="btn-toolbar">'+
                              '<loading-message ng-model="client"></loading-message>'+
                            '</div>'+
                            '<div class="table-responsive">'+
                              '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                                '<thead>'+
                                  '<tr>'+
                                    '<th translate>Full Name</th>'+
                                    '<th translate>Passport Number</th>'+
                                   ' <th translate>Expiration Date</th>'+
                                    '<th translate>Issuing Country</th>'+
                                    '<th translate>Actions</th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                  '<tr ng-repeat="passport in client.passports" >'+
                                    '<td ng-click="editPassport(passport)">{{passport.fullName}}</td>'+
                                   ' <td ng-click="editPassport(passport)"><span >{{passport.passportNumber}}</span></td>'+
                                    '<td ng-click="editPassport(passport)"><span >{{passport.expirationDate}}</span></td>'+
                                    '<td ng-click="editPassport(passport)"><span >{{passport.country.name}}</span></td>'+
                                    '<td class="text-center" >'+
                                     ' <a href="javascript:;" loading-click="removePassport(passport)" loading-icon="true" class="btn btn-table red ">'+
                                        '<span class="glyphicon glyphicon-trash">'+
                                        '</span>'+
                                      '</a>'+
                                    '</td>'+
                                  '</tr>'+
                                '</tbody>'+
                              '</table>'+
                            '</div>'+
                            '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowPassport">'+
                              '<div class="col-md-12">'+
                                '<br>'+
                                '<br>'+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Upload Image</label>'+
                                    '<div class="col-md-8" style="margin-bottom:5px;">'+
                                      '<div   ngf-drop   ng-model="imagePassport" ngf-change="fileSelectedPassport($file)" class="drop-box"   ngf-drag-over-class="dragover" ngf-multiple="false"  name="file" accept="image/*">'+
                                        '<i class="icon-cloud-upload"></i>'+
                                        '<translate>Drop image here to upload, or</translate> '+
                                        '<a  ngf-select  ngf-multiple="false" ngf-change="fileSelectedPassport($file)" type="file" ng-model="imagePassport" name="file" accept="image/*" translate>browse</a>.'+
                                      '</div>'+
                                                          '<div class="text-center">'+
                                                              '<img-view  ngf-image="passport" ></img-view>'+
                                                          '</div>'+
                                   ' </div>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Full Name</label>'+
                                    '<div class="col-md-8">'+
                                      '<input type="text" class="form-control"  ng-model="passport.fullName"  >'+
                                   ' </div>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Passport Number</label>'+
                                    '<div class="col-md-8">'+
                                      '<input type="text" class="form-control"  ng-model="passport.passportNumber"  >'+
                                    '</div>'+
                                  '</div>'+
                                '</div>  '+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Expiration Date</label>'+
                                    '<div class="col-md-8">'+
                                      '<div sb-date-select sb-select-class="form-control "  class="prsht-sb-date-select" max="2030-12-31" ng-model="passport.expirationDate "></div>'+
                                    '</div>'+
                                  '</div>'+
                                '</div> ' +
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Issuing Country</label>'+
                                    '<div class="col-md-8">'+
                                      '<select  class="form-control"  ng-model="passport.countryId" ng-options="country.id as country.name for country in countries"></select>'+
                                    '</div>'+
                                  '</div>'+
                               ' </div>'+
                                '<div class="col-md-12 cont-buttons-client">'+
                                  '<div class="col-md-12">'+ 
                                       '<button class="btn blue pull-right" loading-click="savePassport()" >{{labelSavePassport}}</button> '+
                                       '<button class="btn default pull-right" ng-click="undoPassport()"  translate>Cancel</button>'+
                               ' </div>'+
                               ' </div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                      '</div>',
        controller : function($scope, $element, $attrs){
     
        $scope.countries = $scope.$parent.countries;

        $scope.passport = Passport.$build({
            clientId : $scope.client.id,
            image:null
        });
           
        $scope.fileSelectedPassport = function(file){
              if (file != null) {
                  UploadFiles.uploadFile(file).success(function(data, status, headers, config){
                  $scope.passport.imageId = data['temp_file']['id'];
                  if($scope.passport.id){
                    $scope.passport.$save().$then(function (_passport) {
                        $scope.imageView = _passport;
                        },function(_errors){
                             MessageUtils.error(_errors);
                        });
                 }
                }).progress(function (evt) {
                      $scope.progressUploadPassport = parseInt(100.0 * evt.loaded / evt.total);
                });
              }
          };


        $scope.addPassport = function(){
          if($scope.client.id!=undefined){
              $scope.passport = Passport.$build({
              clientId : $scope.client.id,
              image:null
              });
            $scope.newOrEditShowPassport=true;
              $scope.labelSavePassport = gettextCatalog.getString('Save');
            }else{
            //$scope.$parent.savePersonalBasicDetails();
          }
        };


        $scope.editPassport = function(passport){
          $scope.passport = passport;
          if($scope.passport.country!=null)
                  $scope.passport.countryId = $scope.passport.country.id;
          $scope.labelSavePassport = gettextCatalog.getString('Save');
          $scope.newOrEditShowPassport=true;

        };


        $scope.savePassport = function(){
          return $scope.passport.$save().$then(function (_passport) {
              $scope.client.passports.$add(_passport);
              $scope.newOrEditShowPassport=false;
          });
        };

        $scope.undoPassport = function(){
          $scope.passport = Passport.$build({
              clientId : $scope.client.id
            });
          $scope.newOrEditShowPassport=false;
        };


        $scope.removePassport = function(passport){
           if(passport.id == $scope.passport.id ){
              $scope.passport = Passport.$build({
                clientId : $scope.client.id,
                image:null
                });
            }
          return passport.$destroy().$then(function(_passport){
            $scope.client.passports.$remove(_passport);
          });
        };

          $scope.loadPassports = function(){
            if($scope.client.id!=undefined){
              $scope.client.passports = Passport.$search({client_id: $scope.client.id});
              $scope.$parent.statusLoadPassports =true;
           }

         };

        

        if($scope.$parent.statusLoadPassports==false){ 
            $scope.loadPassports();
        }


        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});


projectApp.directive('clientPassportVisaVisa', function ($compile,$window,$document,Client, $timeout,$state, UploadFiles, Visa, gettextCatalog, MessageUtils) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div class="row-fluid" style="overflow:auto; margin-bottom:10px">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                          '<div class="caption" >'+
                            '<i class="fa fa-book"></i><translate>Visa</translate>'+
                            '<span class="badge badge-info badge-sheet">'+
                              '{{client.visas.length}} </span>'+
                            '</div>'+
                            '<div class="tools">'+
                              '<div class="option" ng-click="addVisa()">'+
                                '<i class="fa fa-book"></i>'+
                                '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                '<translate>Add Visa</translate>'+
                              '</div>'+
                              '<div class="option" ng-click="client.$refresh()">'+
                                '<span aria-hidden="true" class="glyphicon glyphicon-refresh" ></span>'+
                              '</div>'+
                              '<div class="option" ng-click="visaCollapse = !visaCollapse">'+
                                '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !visaCollapse, \'glyphicon-chevron-up\': visaCollapse}" class="glyphicon "></span>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="visaCollapse">'+
                            
                            '<div class="btn-toolbar">'+
                              '<loading-message ng-model="client"></loading-message>'+
                            '</div>'+
                            '<div class="table-responsive">'+
                              '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                                '<thead>'+
                                  '<tr>'+
                                    '<th translate>Full Name</th>'+
                                    '<th translate>Visa Number</th>'+
                                    '<th translate>Expiration Date</th>'+
                                    '<th translate>Issuing Country</th>'+
                                    '<th translate>Actions</th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                  '<tr ng-repeat="visa in client.visas" >'+
                                    '<td ng-click="editVisa(visa)">{{visa.fullName}}</td>'+
                                    '<td ng-click="editVisa(visa)"><span >{{visa.visaNumber}}</span></td>'+
                                    '<td ng-click="editVisa(visa)"><span >{{visa.expirationDate}}</span></td>'+
                                    '<td ng-click="editVisa(visa)"><span >{{visa.country.name}}</span></td>'+
                                    '<td  class="text-center">'+
                                      '<a href="javascript:;" loading-click="removeVisa(visa)" loading-icon="true" class="btn btn-table red ">'+
                                        '<span class="glyphicon glyphicon-trash">'+
                                        '</span>'+
                                      '</a>'+
                                    '</td>'+
                                  '</tr>'+
                               ' </tbody>'+


                              '</table>'+
                            '</div>'+
                            '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowVisa">'+
                              
                              '<div class="col-md-12">'+
                                '<br>'+
                                '<br>'+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Upload Image</label>'+
                                    '<div class="col-md-8" style="margin-bottom:5px;">'+
                                      '<div   ngf-drop   ng-model="imageVisa" ngf-change="fileSelectedVisa($file)" class="drop-box"   ngf-drag-over-class="dragover" ngf-multiple="false"  name="file" accept="image/*">'+
                                        '<i class="icon-cloud-upload"></i>'+
                                        '<translate>Drop image here to upload, or</translate> '+
                                        '<a  ngf-select  ngf-multiple="false" ngf-change="fileSelectedVisa($file)" type="file" ng-model="imageVisa" name="file" accept="image/*" translate>browse</a>.'+
                                      '</div>'+
                                                          '<div class="text-center">'+
                                                              '<img-view  ngf-image="visa" ></img-view>'+
                                                          '</div>'+
                                    '</div>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                   ' <label class="col-md-4 control-label lb-client-bg-wht"  translate>Full Name</label>'+
                                    '<div class="col-md-8">'+
                                      '<input type="text" class="form-control"  ng-model="visa.fullName"  >'+
                                    '</div>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Visa Number</label>'+
                                    '<div class="col-md-8">'+
                                      '<input type="text" class="form-control"  ng-model="visa.visaNumber"  >'+
                                    '</div>'+
                                  '</div>'+
                                '</div> '+
                                '<div class="col-md-12">'+
                                 ' <div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Expiration Date</label>'+
                                   ' <div class="col-md-8">'+
                                      '<div sb-date-select sb-select-class="form-control "  class="prsht-sb-date-select" max="2030-12-31" ng-model="visa.expirationDate"></div>'+
                                    '</div>'+
                                  '</div>'+
                                '</div> ' +
                                '<div class="col-md-12">'+
                                  '<div class="form-group">'+
                                    '<label class="col-md-4 control-label lb-client-bg-wht"  translate>Issuing Country</label>'+
                                    '<div class="col-md-8">'+
                                      '<select  class="form-control"  ng-model="visa.countryId" ng-options="country.id as country.name for country in countries"></select>'+
                                    '</div>'+
                                  '</div>'+
                               ' </div>'+
                                   '<div class="col-md-12 cont-buttons-client">'+
                                      '<div class="col-md-12">'+ 
                                        '<button class="btn blue pull-right" loading-click="saveVisa()" >{{labelSaveVisa}}</button> '+
                                        '<button class="btn default pull-right" ng-click="undoVisa()"  translate>Cancel</button>'+
                                      '</div>'+
                                    '</div>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                     '</div>',
        controller : function($scope, $element, $attrs){
     
        $scope.countries = $scope.$parent.countries;

          $scope.visa = Visa.$build({
            clientId : $scope.client.id,
            image:null
          });
           
        $scope.fileSelectedVisa = function(file){
          if (file != null) {
              UploadFiles.uploadFile(file).success(function(data, status, headers, config){
              $scope.visa.imageId = data['temp_file']['id'];
              if($scope.visa.id){
                $scope.visa.$save().$then(function (_visa) {
                    $scope.imageView = _visa;
                    },function(_errors){
                         MessageUtils.error(_errors);
                    });
            }
            }).progress(function (evt) {
                  $scope.progressUploadVisa = parseInt(100.0 * evt.loaded / evt.total);
            });
          }
       };


        $scope.addVisa = function(){
          if($scope.client.id!=undefined){
              $scope.visa = Visa.$build({
              clientId : $scope.client.id,
              image:null
              });
              $scope.visaCollapse=false;
              $scope.newOrEditShowVisa=true;
              $scope.labelSaveVisa = gettextCatalog.getString('Save');
            }else{
            //$scope.$parent.savePersonalBasicDetails();
          }
        };


        $scope.editVisa = function(visa){
          $scope.visa = visa;
          if($scope.visa.country!=null)
                  $scope.visa.countryId = $scope.visa.country.id;
          $scope.labelSaveVisa = gettextCatalog.getString('Save');
          $scope.newOrEditShowVisa=true;

        };

        $scope.saveVisa = function(){
          return $scope.visa.$save().$then(function (_visa) {
              $scope.client.visas.$add(_visa);
              $scope.newOrEditShowVisa=false;
          });
        };

        $scope.undoVisa = function(){
          $scope.visa =Visa.$build({
              clientId : $scope.client.id
            });
          $scope.newOrEditShowVisa=false;
        };

        $scope.removeVisa = function(visa){
           if(visa.id == $scope.visa.id ){
              $scope.visa = Visa.$build({
                clientId : $scope.client.id,
                image:null
                });
            }
          return visa.$destroy().$then(function(_visa){
            $scope.client.visas.$remove(_visa);
          });
        };

        $scope.loadVisas = function(){
           if($scope.client.id!=undefined){

           $scope.client.visas = Visa.$search({client_id: $scope.client.id});
           $scope.$parent.statusLoadVisas =true;

         }
         
        };

        

        if($scope.$parent.statusLoadVisas==false){ 
            $scope.loadVisas();
        }


        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});



projectApp.directive('clientPreferencesInterests', function ($compile,$window,$document,Client, $timeout,$state, TripStyle, TripGroup) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div>'+
                    '<h4 class="title-section" translate>What do you like?</h4>'+
                    '<div class="choices">'+
                      '<div class="btn blue" ng-repeat="tripStyle in $parent.tripStyleOptions.tripStyles" ng-click="manageStyle($index)" ng-class="{\'no-active\': !inStyles(tripStyle.id)}">'+
                        '<i class="fa" ng-class="inStyles(tripStyle.id) ? \'fa-check-square-o\' : \'fa-square-o\'" fa-check-square-o></i>'+
                        '{{tripStyle.name | translate }}'+
                      '</div>'+
                     '<loading-message ng-model="$parent.tripStyleOptions.tripStyles"></loading-message>'+
                    '</div>'+
                    
                    '<h4 class="title-section" translate>With whom you share?</h4>'+
                    '<div class="choices">'+
                      '<div class="btn blue" ng-repeat="tripGroup in $parent.tripStyleOptions.tripGroups" ng-click="manageGroup($index)" ng-class="{\'no-active\' : !inGroups(tripGroup.id)}">'+
                        '<i class="fa" ng-class="inGroups(tripGroup.id) ? \'fa-check-square-o\' : \'fa-square-o\'" fa-check-square-o></i>'+
                        '{{tripGroup.name | translate}}'+
                      '</div>'+
                    '</div>'+
                    '<loading-message ng-model="$parent.tripStyleOptions.tripGroups"></loading-message>'+
                    '</div>',
        controller : function($scope, $element, $attrs){
     
 
        $scope.inStyles = function (id) {
            var res = false;
            if (id && $scope.client.tripStyleIds.indexOf(id) >= 0) {
                res = true;
            }
            return res;
        };

        $scope.inGroups = function (id) {
            var res = false;
            if (id && $scope.client.tripGroupIds.indexOf(id) >= 0) {
                res = true;
            }
            return res;
        };
        
        $scope.manageStyle = function (index) {
            var tripStyle = $scope.$parent.tripStyleOptions.tripStyles[index];
            var pos = $scope.client.tripStyleIds.indexOf(tripStyle.id);
            if (pos < 0) {
                $scope.client.addStyle(tripStyle.id);
                $scope.client.tripStyleIds.push(tripStyle.id);
            } else {
                $scope.client.deleteStyle(tripStyle.id);
                $scope.client.tripStyleIds.splice(pos,1);
            };
        };
        
        $scope.manageGroup = function (index) {
            var tripGroup = $scope.$parent.tripStyleOptions.tripGroups[index];
            var pos = $scope.client.tripGroupIds.indexOf(tripGroup.id);
            if (pos < 0) {
                $scope.client.addGroup(tripGroup.id);
                $scope.client.tripGroupIds.push(tripGroup.id);
            } else {
                $scope.client.deleteGroup(tripGroup.id);
                $scope.client.tripGroupIds.splice(pos,1);
            }
        };
       

        $scope.loadTripStylesAndGroups = function(){
            $scope.client.tripGroupIds = [];
            $scope.client.tripStyleIds = [];
            
            $scope.$parent.tripStyleOptions.tripStyles = TripStyle.$search({});
            $scope.$parent.tripStyleOptions.tripGroups = TripGroup.$search({});
            

            Client.getTripStyles($scope.client.id).then(function(_styles){
                for(var i =0; i< _styles.data.clients.length; i++){
                  $scope.client.tripStyleIds.push(_styles.data.clients[i].trip_style_id);
                }
              });

             Client.getTripGroups($scope.client.id).then(function(_groups){
                for(var i =0; i< _groups.data.clients.length; i++){
                  $scope.client.tripGroupIds.push(_groups.data.clients[i].trip_group_id);
                }
              });
            $scope.$parent.statusLoadTripStylesAndGroups =true;
        };

        

        if($scope.$parent.statusLoadTripStylesAndGroups==false){ 
          $scope.loadTripStylesAndGroups();
        }

        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});



projectApp.directive('clientPreferencesHotel', function ($compile,$window,$document,Client, ClientPreferences,$timeout,$state, HotelService, HotelOption, PillowType, BedType, BedSheetType, RoomType, TripType) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  ' <div>'+
                   ' <h4 class="title-section" translate>General Information</h4>'+
                    '<div ng-show="!loadPreferencesHotel">'+
                     '<loading-message ng-model="client.hotelOptions"></loading-message>'+
                    '</div>'+
                   '<div ng-show="loadPreferencesHotel">'+

                   '<div class="row">'+
                          '<div class="col-md-12">'+
                            '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;" '+
                            'ng-click="enabledEditFields()" '+
                            'ng-show="showButtonEdit()"'+
                            'translate>Edit</label>'+
                          '</div>'+
                        '<div class="col-md-6">'+
                            '<div class="form-group">'+
                            '<label class="col-md-5 control-label lb-client-bg-wht" translate>Smoker</label>'+
                            '<div class="col-md-7">'+
                              '<input type="checkbox" ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.smoker">'+
                          '</div>'+
                      '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Hotel Stars</label>'+
                        '<div class="col-md-7 hotel-stars" ng-show="!showButtonEdit()">'+
                            '<span style="margin-right:10px;" ng-class="{\'selected\':tempData.starHover==0}" ng-mouseover="tempData.starHover=0" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=0" class="glyphicon glyphicon-ban-circle"></span>'+

                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=1,\'glyphicon-star-empty\':(tempData.starHover<1 || !clientView.clientPreferences.stars) }" ng-mouseover="tempData.starHover=1" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=1" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=2,\'glyphicon-star-empty\':(tempData.starHover<2 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=2" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=2" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=3,\'glyphicon-star-empty\':(tempData.starHover<3 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=3" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=3" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=4,\'glyphicon-star-empty\':(tempData.starHover<4 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=4" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=4" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=5,\'glyphicon-star-empty\':(tempData.starHover<5 || !clientView.clientPreferences.stars)}" ng-mouseover="tempData.starHover=5" ng-mouseleave="tempData.starHover=clientView.clientPreferences.stars" ng-click="clientView.clientPreferences.stars=5" class="glyphicon "></span>'+
                        '</div>'+

                         '<div class="col-md-7 hotel-stars" ng-show="showButtonEdit()">'+
                            '<span style="margin-right:10px;" ng-class="{\'selected\':tempData.starHover==0}" class="glyphicon glyphicon-ban-circle"></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=1,\'glyphicon-star-empty\':(tempData.starHover<1 || !clientView.clientPreferences.stars) }" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=2,\'glyphicon-star-empty\':(tempData.starHover<2 || !clientView.clientPreferences.stars)}"  class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=3,\'glyphicon-star-empty\':(tempData.starHover<3 || !clientView.clientPreferences.stars)}"  class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=4,\'glyphicon-star-empty\':(tempData.starHover<4 || !clientView.clientPreferences.stars)}" class="glyphicon "></span>'+
                            '<span ng-class="{\'selected glyphicon-star\':tempData.starHover>=5,\'glyphicon-star-empty\':(tempData.starHover<5 || !clientView.clientPreferences.stars)}" class="glyphicon "></span>'+
                        '</div>'+


                    '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                      '<div class="form-group">'+
                        '<label class="col-md-12 control-label lb-client-bg-wht" ><translate>Facilities and Services</translate>:</label>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-9">'+
                      '<label class="tag-various" ng-repeat="hotel_service_tag in tempData.arrayFacilities">{{hotel_service_tag.name | translate}}</label>'+
                      '<label ng-class="{\'text-center\':tempData.arrayFacilities.length>0,\'text-left\':tempData.arrayFacilities.length==0}" class="label-edit-facilitiesAndService" ng-show="!tempData.editingFacilities" href="javascript:;" ><span ng-disabled="showButtonEdit()" ng-click="editFacilitiesAndServices()" class="btn btn-default"><i class="fa fa-edit"></i> <translate>Edit</translate></span> '+
                      '</label>'+
                    '</div>'+
                    '<div class="col-md-12" style="margin-bottom: 15px;">'+
                        '<div class="form-group">'+
                            '<div class="col-md-12">'+

                                '<div ng-show="tempData.editingFacilities" class="container-facilities-and-services">'+
                                   ' <div class="col-md-6" ng-repeat="hotel_service in $parent.hotel_services">'+
                                        '<label>'+
                                          '<input type="checkbox" checklist-model="tempData.arrayFacilities" checklist-value="hotel_service"> {{hotel_service.name | translate}}'+
                                      '</label>'+
                                  '</div>'+
                                 ' <div class="col-md-12 cont-buttons-client">'+
                                      '<div class="col-md-12">'+
                                        '<button class="btn blue pull-right " ng-click="closeClientPreferenceFacilitiesAndServices()" translate>Close</button>'+
                                        '<button class="btn default pull-right" ng-click="undoClientPreferenceFacilitiesAndServices()"  translate>Cancel</button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Bed Type</label>'+
                        '<div class="col-md-7">'+
                          '<select ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.bedTypeId" class="form-control" ng-options="bed.id as bed.name | translate for bed in $parent.client_preferences_data.bed_types"></select>'+
                      '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Bed Sheets</label>'+
                        '<div class="col-md-7">'+
                          '<select ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.bedSheetTypeId" class="form-control" ng-options="sheet.id as sheet.name | translate for sheet in $parent.client_preferences_data.bed_sheet_types"></select>'+
                      '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="col-md-6">'+
                      '<div class="form-group">'+
                        '<label class="col-md-5 control-label lb-client-bg-wht" translate>Pillow Type</label>'+
                        '<div class="col-md-7">'+
                          '<select ng-disabled="showButtonEdit()" ng-model="clientView.clientPreferences.pillowTypeId" class="form-control" ng-options="pillow.id as pillow.name | translate for pillow in $parent.client_preferences_data.pillow_types"></select>'+
                      '</div>'+
                    '</div>'+
                    '</div>'+
                   ' <div ng-show="!showButtonEdit()" class="col-md-12 cont-buttons-client" style="border:none;">'+
                      '<div class="col-md-12">'+
                        '<button class="btn blue pull-right " loading-click="saveClientPreference()" translate>Save</button>'+
                        '<button class="btn default pull-right" ng-click="undoClientPreference()"  translate>Cancel</button>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '<div class="row-fluid" style="overflow:auto; margin-bottom:10px;">'+
                    '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                            '<div class="caption" >'+
                                '<i class="fa fa-h-square"></i><translate>Prefered Hotels</translate>'+
                                '<span class="badge badge-info badge-sheet">'+
                                    '{{client.hotelOptions.length}} </span>'+
                                '</div>'+
                                '<div class="tools">'+
                                    '<div class="option" ng-click="addHotelOption()">'+
                                        '<i class="fa fa-h-square"></i>'+
                                        '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                        '<translate>Add Hotel Option</translate>'+
                                    '</div>'+
                                    '<div class="option" ng-click="client.$refresh()">'+
                                        '<span aria-hidden="true" class="glyphicon glyphicon-refresh" ></span>'+
                                    '</div>'+
                                    '<div class="option" ng-click="hotelOptionCollapse = !hotelOptionCollapse">'+
                                        '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !hotelOptionCollapse, \'glyphicon-chevron-up\': hotelOptionCollapse}" class="glyphicon "></span>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="portlet-body sheet-table sheet-table-client nopadding" collapse="hotelOptionCollapse" >'+

                                '<div class="btn-toolbar">'+
                                    '<loading-message ng-model="client"></loading-message>'+
                               ' </div>'+
                                '<div class="table-responsive">'+
                                    '<table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th translate>Hotel Name</th>'+
                                                '<th translate>Trip Type</th>'+
                                                '<th translate>Room Type</th>'+
                                                '<th translate>Comments</th>'+
                                                '<th></th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody>'+
                                            '<tr ng-repeat="hotel_option in client.hotelOptions" >'+
                                                '<td ng-click="editHotelOption(hotel_option)"><span >{{hotel_option.name}}</span></td>'+
                                                '<td ng-click="editHotelOption(hotel_option)"><span >{{hotel_option.tripType.name | translate }}</span></td>'+
                                                '<td ng-click="editHotelOption(hotel_option)"><span >{{hotel_option.roomType.name | translate }}</span></td>'+
                                                '<td ng-click="editHotelOption(hotel_option)"><span >{{hotel_option.comments}}</span></td>'+
                                                '<td class="text-center" >'+
                                                  '<a href="javascript:;" loading-click="removeHotelOption(hotel_option)" loading-icon="true" class="btn btn-table red ">'+
                                                    '<span class="glyphicon glyphicon-trash">'+
                                                    '</span>'+
                                                '</a>'+
                                            '</td>'+
                                        '</tr>'+
                                    '</tbody>'+
                                '</table>'+
                                '<loading-message ng-model="client.hotelOptions"></loading-message>'+
                            '</div>'+
                            '<form name="formHotelOption">'+
                            '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowHotelOption">'+
                                '<div class="col-md-6">'+
                                    '<div class="form-group">'+
                                        '<label class="col-md-4 control-label" ><translate>Hotel Name</translate><span class="required" aria-required="true">*</span></label>'+
                                        '<div class="col-md-8">'+
                                            '<input required="" type="text" class="form-control"  ng-model="hotelOption.name"  >'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6">'+
                                    '<div class="form-group">'+
                                        '<label class="col-md-4 control-label"  ><translate>Trip Type</translate><span class="required" aria-required="true">*</span></label>'+
                                        '<div class="col-md-8">'+
                                            '<select required="" ng-model="hotelOption.tripTypeId" class="form-control" ng-options="trip.id as trip.name | translate for trip in $parent.client_preferences_data.trip_types"></select>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-6">'+
                                    '<div class="form-group">'+
                                        '<label class="col-md-4 control-label"  ><translate>Room Type</translate><span class="required" aria-required="true">*</span></label>'+
                                        '<div class="col-md-8">'+
                                            '<select required="" ng-model="hotelOption.roomTypeId" class="form-control" ng-options="trip.id as trip.name | translate for trip in $parent.client_preferences_data.room_types"></select>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                        '<label class="col-md-2 control-label"  translate>Comments</label>'+
                                        '<div class="col-md-10">'+
                                            '<textarea maxlength="255"  ng-model="hotelOption.comments" name="comment" class="form-control " rows="3"></textarea>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 cont-buttons-client">'+
                                    '<div class="col-md-12">'+
                                        '<button class="btn blue pull-right " loading-click="saveHotelOption()" ng-disabled="!formHotelOption.$valid" translate>Save</button> '+
                                        '<button class="btn default pull-right" ng-click="undoHotelOption()"  translate>Cancel</button>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '</form>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '</div>'+
                    '</div>',
        controller : function($scope, $element, $attrs){
     
       $scope.clientView = angular.copy($scope.client);
       $scope.loadPreferencesHotel=true;

    
       $scope.editFacilitiesAndServices = function(){
            $scope.tempData.editingFacilities=true;
       };
       
       $scope.closeClientPreferenceFacilitiesAndServices = function(){
              $scope.tempData.editingFacilities=false;
       };
       
       $scope.undoClientPreferenceFacilitiesAndServices = function(){
              if($scope.client.clientPreferences.id==undefined){
                $scope.tempData.arrayFacilities=[];
              }else{
                $scope.tempData.arrayFacilities= angular.copy($scope.client.clientPreferences.hotelServices);
              }
       };

  
      $scope.hotelOption = HotelOption.$build({
        clientId : $scope.client.id
      });


      $scope.addHotelOption = function(){

        if($scope.client.id != undefined){
          $scope.newOrEditShowHotelOption=true;
          $scope.hotelOption = HotelOption.$build({
            clientId : $scope.client.id
          });
        }else{
        //$scope.$parent.savePersonalBasicDetails();
        }
      };

      $scope.saveHotelOption = function(){

        return $scope.hotelOption.$save().$then(function (_hotelOption) {
          $scope.client.hotelOptions.$add(_hotelOption);
          $scope.newOrEditShowHotelOption=false;
        });
      };

      $scope.undoHotelOption = function(){
          $scope.hotelOption =HotelOption.$build({
              clientId : $scope.client.id
            });
          $scope.newOrEditShowHotelOption=false;
        };



        $scope.editHotelOption = function(_hotelOption){
          $scope.hotelOption = _hotelOption;
          $scope.newOrEditShowHotelOption=true;
       };


       $scope.removeHotelOption = function(hotelOption){
        if(hotelOption.id == $scope.hotelOption.id ){
            $scope.hotelOption = HotelOption.$build({
              clientId : $scope.client.id
              });
          }
        return hotelOption.$destroy().$then(function(_hotelOption){
          $scope.client.hotelOptions.$remove(_hotelOption);
        });
      };

      $scope.saveClientPreference = function(){
        
        $scope.tempData.editingFacilities=false;

        $scope.client.clientPreferences.smoker = $scope.clientView.clientPreferences.smoker;
        $scope.client.clientPreferences.stars = $scope.clientView.clientPreferences.stars;
        $scope.client.clientPreferences.bedTypeId = $scope.clientView.clientPreferences.bedTypeId;
        $scope.client.clientPreferences.bedSheetTypeId = $scope.clientView.clientPreferences.bedSheetTypeId;
        $scope.client.clientPreferences.pillowTypeId = $scope.clientView.clientPreferences.pillowTypeId;

        return $scope.client.clientPreferences.$save().$then(function(_preference){
        $scope.$parent.editButton = true;

          if($scope.tempData.arrayFacilities != $scope.clientView.clientPreferences.hotelServices){
            $scope.newArrayPreferencesFacilities = [];
          angular.forEach($scope.tempData.arrayFacilities, function(value, key) {
            $scope.newArrayPreferencesFacilities.push(value.id);
          });
          
          $scope.client.clientPreferences.addFacilitiesService($scope.newArrayPreferencesFacilities).then(function(_result){
              $scope.client.clientPreferences.hotelServices = $scope.tempData.arrayFacilities = _result.data.client_preferences.hotel_services;
          });
          }


        });
       };

    $scope.undoClientPreference = function(){

      $scope.clientView.clientPreferences.smoker = $scope.client.clientPreferences.smoker;
      $scope.clientView.clientPreferences.stars = $scope.client.clientPreferences.stars;
      $scope.clientView.clientPreferences.bedTypeId = $scope.client.clientPreferences.bedTypeId;
      $scope.clientView.clientPreferences.bedSheetTypeId = $scope.client.clientPreferences.bedSheetTypeId;
      $scope.clientView.clientPreferences.pillowTypeId = $scope.client.clientPreferences.pillowTypeId;
      $scope.$parent.editButton = true;

        $scope.tempData.starHover = $scope.client.clientPreferences.stars;
        if($scope.client.clientPreferences.id==undefined){
          $scope.tempData.arrayFacilities=[];
        }else{
          $scope.tempData.arrayFacilities= $scope.client.clientPreferences.hotelServices;
        }
     };

        $scope.loadHotelPreferences = function(){
            $scope.loadPreferencesHotel=false;
            $scope.$parent.hotel_services = HotelService.$search({});
            $scope.$parent.client_preferences_data.pillow_types = PillowType.$search({});
            $scope.$parent.client_preferences_data.bed_types    = BedType.$search({});
            $scope.$parent.client_preferences_data.bed_sheet_types = BedSheetType.$search({});  
            $scope.$parent.client_preferences_data.trip_types   = TripType.$search({});
            $scope.$parent.client_preferences_data.room_types   = RoomType.$search({});
    
          if($scope.client.id!=undefined){
          
             if(!$scope.$parent.statusLoadFlyPreferences){

                  ClientPreferences.$search({client_id: $scope.client.id}).$then(function(_preference){
                    $scope.client.clientPreferences = _preference[0]
                    $scope.clientView = angular.copy($scope.client);
                    $scope.tempData.arrayFacilities=angular.copy($scope.clientView.clientPreferences.hotelServices);
                    $scope.tempData.starHover = $scope.clientView.clientPreferences.stars;
                    $scope.tempData.labelOrigin = $scope.clientView.clientPreferences.originLabel;
                  }); 

              }

          $scope.client.hotelOptions = HotelOption.$search({client_id: $scope.client.id }).$then(function(_result){ $scope.loadPreferencesHotel=true; });


          $scope.$parent.statusLoadHotelPreferences =true;

         }

        };

        $scope.enabledEditFields = function(){
            return $scope.$parent.enabledEditFields();
        };

        $scope.showButtonEdit = function(){
            return $scope.$parent.showButtonEdit();
        };

        if ($state.current.data.creating) {
          $scope.tempData={
            editingFacilities:false,
            arrayFacilities:[],
            starHover: 0
          };
        } else{
          $scope.tempData={
            editingFacilities:false,
            arrayFacilities:$scope.clientView.clientPreferences.id==undefined ? [] : angular.copy($scope.clientView.clientPreferences.hotelServices),
            starHover: $scope.clientView.clientPreferences.stars==undefined ? 0 : angular.copy($scope.clientView.clientPreferences.stars)
          };

           
        }

      
        if($scope.$parent.statusLoadHotelPreferences==false){ 
          $scope.loadHotelPreferences();
        }

        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});


projectApp.directive('clientPreferencesFlight', function ($compile,$window,$document,Client, ClientPreferences,$timeout,$state, gettextCatalog, SeatingPreference, SpecialAssistance, AirPort) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div>'+
                   '<h4 class="title-section" translate>Flight Information</h4>'+
                   '<div ng-show="!loadPreferencesFlight">'+
                    '<loading-message ng-model="$parent.client_preferences_data.seating_preferences"></loading-message>'+
                   '</div>'+
                   '<div ng-show="loadPreferencesFlight">'+

                    '<div class="col-md-12">'+
                      
                      '<div class="form-group">'+
                        
                          '<div class="col-md-12">'+
                              '<label style="text-decoration: underline; cursor: pointer; color: #3b5998; text-align: right; float: right;"'+ 
                              'ng-click="enabledEditFields()" '+
                              'ng-show="showButtonEdit()"'+
                              'translate>Edit</label>'+
                          '</div>'+

                          '<label class="col-md-3 control-label lb-client-bg-wht" style="width:24% !important;" translate>Home Airport</label>'+

                          '<label class="col-md-9 control-label lb-client-bg-wht"  '+
                                'ng-show="showButtonEdit()">{{tempData.labelOrigin}}</label>'+


                          '<div class="col-md-9" style="width:76% !important;" ng-show="!showButtonEdit()">'+
                              
                              '<div class="input-icon right input-search-airPort">'+
                                  '<i class="fa fa-search"></i>'+
                                  '<script type="text/ng-template" id="customTemplateAirPort.html">'+
                                      '<a><span bind-html-unsafe="match.label | typeaheadHighlight:query"></span></a>'+
                                  '</script>'+
                                  '<input typeahead-ng-model class="  form-control"  typeahead-input-formatter="submitSearchOrigin($model)" type="text" ng-model="airportPreference" placeholder="{{tempData.labelOrigin}}" typeahead="airport as (airport.city+\',  \'+airport.countryName+\' - \'+airport.name+\'(\'+airport.fs+\')\')   for airport in searchAirport($viewValue)" typeahead-loading="loadingLocations" class="form-control" typeahead-template-url="customTemplateAirPort.html">'+
                                  '<i ng-show="loadingLocations" class="glyphicon glyphicon-refresh pull-right"></i>'+
                              '</div>'+

                            '<div form-input form-error="service.errors.origin" ng-class="{\'has-error\': service.errors.origin}">'+
                            '</div>'+

                        '</div>'+

                      '</div>'+

                    '</div>'+

                    '<div class="col-md-6">'+
                      
                      '<div class="form-group">'+
                        
                        '<label class="col-md-6 control-label lb-client-bg-wht" translate>Seating Preference</label>'+
                        
                        '<div class="col-md-6">'+
                          '<select ng-disabled="showButtonEdit()" class="form-control" ng-model="clientView.clientPreferences.seatingPreferenceId" ng-options="seating.id as seating.name | translate for seating in $parent.client_preferences_data.seating_preferences"></select>'+
                        '</div>'+

                      '</div>'+

                    '</div> '+

                    '<div class="col-md-6">'+
                      
                      '<div class="form-group">'+
                        
                        '<label class="col-md-6 control-label lb-client-bg-wht" translate>Special Assistance</label>'+
                        
                        '<div class="col-md-6">'+
                         ' <select ng-disabled="showButtonEdit()"  class="form-control" ng-model="clientView.clientPreferences.specialAssistanceId" ng-options="special.id as special.name | translate for special in $parent.client_preferences_data.special_assistances"></select>'+
                        '</div>'+

                      '</div>'+

                    '</div> '+

                    '<div ng-show="!showButtonEdit()" class="col-md-12 cont-buttons-client" style="border:none;">'+
                      '<div class="col-md-12">'+
                        '<button class="btn blue pull-right " loading-click="saveClientFlyPreference()" translate>Save</button>'+
                        '<button class="btn default pull-right" ng-click="undoClientFlyPreference()"  translate>Cancel</button>'+
                      '</div>'+
                    '</div>'+

                    '</div>'+
                    '</div>',
        controller : function($scope, $element, $attrs){
     
       
        $scope.clientView = angular.copy($scope.client);
        $scope.loadPreferencesFlight=true;
 
        //Inicio Preferencias de Vuelo

        //monitoreo el valor del aeropuerto de origen   
      $scope.$watch('tempData.labelOrigin', function() {
        if($scope.tempData !== undefined){

            if ($scope.tempData.labelOrigin===null){
              $scope.tempData.labelOrigin = gettextCatalog.getString('Search AirPort');
            }

            if ($scope.tempData.labelOrigin !==undefined){
              if ($scope.tempData.labelOrigin.length===0){
                $scope.tempData.labelOrigin = gettextCatalog.getString('Search AirPort');
              }

            }

          }

       }, true);


      $scope.saveClientFlyPreference = function(){
            $scope.client.clientPreferences.seatingPreferenceId = $scope.clientView.clientPreferences.seatingPreferenceId;
            $scope.client.clientPreferences.specialAssistanceId = $scope.clientView.clientPreferences.specialAssistanceId;
            $scope.client.clientPreferences.origin = $scope.clientView.clientPreferences.origin;
            $scope.client.clientPreferences.originLabel = $scope.clientView.clientPreferences.originLabel;
              
              //actualizo el valor del aeropuerto original que se encuentra ahora en bd
            $scope.tempData.airPortOriginId = $scope.clientView.clientPreferences.origin;
            $scope.tempData.airPortOriginLabel = $scope.clientView.clientPreferences.originLabel;

            return $scope.client.clientPreferences.$save().$then(function(_preference){
            $scope.$parent.editButton = true;
            });
           };

        $scope.undoClientFlyPreference = function(){

          $scope.clientView.clientPreferences.seatingPreferenceId = $scope.client.clientPreferences.seatingPreferenceId;
          $scope.clientView.clientPreferences.specialAssistanceId = $scope.client.clientPreferences.specialAssistanceId;
          $scope.clientView.clientPreferences.origin = $scope.tempData.airPortOriginId;
          $scope.clientView.clientPreferences.originLabel = $scope.tempData.airPortOriginLabel;
          $scope.tempData.labelOrigin = $scope.tempData.airPortOriginLabel;
          $scope.$parent.editButton = true;
        };


        $scope.searchAirport = function(val){
          $scope.airport = AirPort.$build();
            return $scope.airport.searchAirPort(val).then(function(_airports){
              return _airports.data.air_ports;
          });
        };


      $scope.submitSearchOrigin = function(airport){
             
             if(airport!==undefined){
              
              $scope.tempData.labelOrigin = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
              $scope.clientView.clientPreferences.origin= airport.id;
              $scope.clientView.clientPreferences.originLabel = airport.city+",  "+airport.countryName+" - "+airport.name+"("+airport.fs+")";
             }
      };


        $scope.loadFlyPreferences = function(){
               $scope.loadPreferencesFlight=false;
               $scope.$parent.client_preferences_data.special_assistances    = SpecialAssistance.$search({});
               $scope.$parent.client_preferences_data.seating_preferences = SeatingPreference.$search({}).$then(function(_result){$scope.loadPreferencesFlight=true;});
         

               if($scope.client.id!=undefined){

                if(!$scope.$parent.statusLoadHotelPreferences){

                  ClientPreferences.$search({client_id: $scope.client.id}).$then(function(_preference){
                    $scope.client.clientPreferences = _preference[0]
                    $scope.clientView = angular.copy($scope.client);
                    $scope.tempData.arrayFacilities=angular.copy($scope.clientView.clientPreferences.hotelServices);
                    $scope.tempData.starHover = $scope.clientView.clientPreferences.stars;
                    $scope.tempData.labelOrigin = $scope.clientView.clientPreferences.originLabel;
                  }); 

                }

                $scope.$parent.statusLoadFlyPreferences =true;

            }
        };

        $scope.enabledEditFields = function(){
            return $scope.$parent.enabledEditFields();
        };

        $scope.showButtonEdit = function(){
            return $scope.$parent.showButtonEdit();
        };


        if ($state.current.data.creating) {
          $scope.tempData={
              labelOrigin: gettextCatalog.getString('Search AirPort'),
              airPortOriginId:$scope.clientView.clientPreferences.origin==undefined ? [] : angular.copy($scope.clientView.clientPreferences.origin),
              airPortOriginLabel:$scope.clientView.clientPreferences.originLabel==undefined ? [] : angular.copy($scope.clientView.clientPreferences.originLabel),
            };
        } else {
          $scope.tempData={
            labelOrigin:$scope.clientView.clientPreferences.id==undefined ? [] : angular.copy($scope.clientView.clientPreferences.originLabel),
            airPortOriginId:$scope.clientView.clientPreferences.origin==undefined ? [] : angular.copy($scope.clientView.clientPreferences.origin),
            airPortOriginLabel:$scope.clientView.clientPreferences.originLabel==undefined ? [] : angular.copy($scope.clientView.clientPreferences.originLabel),
          };
        }
       
        if($scope.$parent.statusLoadFlyPreferences==false){ 
              $scope.loadFlyPreferences();
        }



        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});

projectApp.directive('clientLoyaltyProgramsLoyaltyPrograms', function ($compile,$window,$document,Client, MessageUtils, $timeout,$state, gettextCatalog, ClientLoyaltyProgram, LoyaltyProgram) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div class="row">'+
                      '<div class="col-md-12" style="overflow:none; margin-bottom:10px; height:auto;">'+
                      '<div class="portlet box gray">'+
                        '<div class="portlet-title">'+
                            '<div class="caption" >'+
                              '<i class="fa fa-pencil-square-o"></i><translate>Loyalty Programs</translate>'+
                              '<span class="badge badge-info badge-sheet">'+
                              '{{client.clientLoyaltyPrograms.length}} </span>'+
                            '</div>'+
                            '<div class="tools">'+
                              '<div class="option" ng-click="addClientLoyaltyProgram()">'+
                                '<i class="fa fa-pencil-square-o"></i>'+
                                '<span aria-hidden="true" class="icon-plus blue"></span>'+
                                '<translate>Add</translate>'+
                              '</div>'+
                              '<div class="option" ng-click="clientLoyaltyProgramsCollapse = !clientLoyaltyProgramsCollapse">'+
                                '<span aria-hidden="true" ng-class="{\'glyphicon-chevron-down\': !customerDatesCollapse, \'glyphicon-chevron-up\': clientLoyaltyProgramsCollapse}" class="glyphicon "></span>'+
                              '</div>'+
                            '</div>'+
                          '</div>'+
                          '<div class="portlet-body sheet-table sheet-table-client nopadding" style="height: auto;" collapse="clientLoyaltyProgramsCollapse" >'+
                            '<div class="btn-toolbar">'+
                              '<loading-message ng-model="client"></loading-message>'+
                           '</div>'+
                           '<div class="table-responsive">'+
                              '<table name="tablesLoyaltyProgram" class="table-project table table-bordered table-condensed flip-content ">'+
                                '<thead>'+
                                  '<tr>'+
                                    '<th translate>Program Name</th>'+
                                    '<th translate>Program Number</th>'+
                                    '<th translate>Actions</th>'+
                                  '</tr>'+
                                '</thead>'+
                                '<tbody>'+
                                  '<tr ng-repeat="result in client.clientLoyaltyPrograms" >'+
                                    '<td ng-click="editClientLoyaltyProgram(result)">{{result.loyaltyProgram.name}}</td>'+
                                    '<td ng-click="editClientLoyaltyProgram(result)">{{result.number}}</td>'+
                                    '<td class="text-center">'+
                                      '<a href="javascript:;" loading-click="removeClientLoyaltyProgram(result)" loading-icon="true" class="btn btn-table red ">'+
                                        '<span class="glyphicon glyphicon-trash">'+
                                        '</span>'+
                                      '</a>'+
                                    '</td>'+
                                  '</tr>'+
                                '</tbody>'+
                              '</table>'+
                            '</div>'+
                           '<div class ="loyalty-program">'+
                             '<form name="form.loyalty">'+
                                '<div class="row-fluid container-detail-passport" ng-show="newOrEditShowClientLoyaltyProgram">'+
                                  '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                      '<label class="col-md-5 control-label lb-client-bg-wht"><translate>Program Name</translate><span class="required" aria-required="true">*</span></label>'+
                                      '<div class="col-md-7">'+
                                        '<ui-select required="true" ng-model="clientViewLoyaltyProgram.id" theme="bootstrap" ng-disabled="disabled" reset-search-input="false" title="Selecciona">'+
                                          '<ui-select-match placeholder="{{ \'Select\' | translate}}">{{$select.selected.name}}</ui-select-match>'+
                                            '<ui-select-choices repeat="loyaltyProgram.id as loyaltyProgram in client_data.loyalty_programs | filter: $select.search"'+
                                            'refresh="refreshLoyaltyPrograms($select.search)"'+
                                            'refresh-delay="0">'+
                                            '<div ng-bind-html="loyaltyProgram.name | highlight: $select.search"></div>'+
                                          '</ui-select-choices>'+
                                        '</ui-select>'+
                                      '</div>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="col-md-12">'+
                                    '<div class="form-group">'+
                                      '<label class="col-md-5 control-label lb-client-bg-wht"><translate>Program Number</translate> <span class="required" aria-required="true">*</span></label>'+
                                      '<div class="col-md-7">'+
                                        '<input type="text" required="true" class="form-control" ng-model="clientViewLoyaltyProgram.number">'+
                                      '</div>'+
                                    '</div>'+
                                  '</div>'+
                                  '<div class="col-md-12 cont-buttons-client">'+
                                    '<div class="col-md-12">'+
                                      '<button class="btn blue pull-right " loading-click="saveNewClientLoyaltyProgram()" ng-disabled="form.loyalty.$pristine ||disabledButton"translate>Save</button> '+
                                      '<button class="btn default pull-right" ng-click="undoNewClientLoyaltyProgram()"  translate>Cancel</button>'+
                                    '</div>'+
                                  '</div>'+
                                '</div>'+
                              '</form>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                  '</div>'+
                  '</div>',
        controller : function($scope, $element, $attrs){
     
       
        $scope.clientView = angular.copy($scope.client);
        $scope.clientViewLoyaltyProgram = {};
        $scope.client_data = {};
        $scope.form = {};

       $scope.addClientLoyaltyProgram= function(){
        if($scope.client.id!=undefined){
          $scope.newOrEditShowClientLoyaltyProgram=true;
          $scope.clientLoyaltyProgram = ClientLoyaltyProgram.$build({
            clientId : $scope.client.id
          });
          $scope.clientViewLoyaltyProgram = $scope.clientLoyaltyProgram; 
          $scope.clientLoyaltyProgramsCollapse=false;
          $scope.client_data.loyalty_programs.length = 0;
          }else{
            //$scope.savePersonalBasicDetails();
          }
       }; 

       $scope.editClientLoyaltyProgram = function(clientLoyaltyProgram){
          $scope.clientViewLoyaltyProgram.id = clientLoyaltyProgram.loyaltyProgram.id; 
          $scope.clientViewLoyaltyProgram.number = clientLoyaltyProgram.number; 
          $scope.refreshLoyaltyPrograms(clientLoyaltyProgram.loyaltyProgram.name);
          $scope.clientLoyaltyProgram = clientLoyaltyProgram;
          $scope.newOrEditShowClientLoyaltyProgram=true;

       };

      $scope.removeClientLoyaltyProgram = function(_clientLoyaltyProgram){
          return _clientLoyaltyProgram.$destroy().$then(function(_client_loyalty_program){
              $scope.client.clientLoyaltyPrograms.$remove(_client_loyalty_program);
          });
      };

      $scope.saveNewClientLoyaltyProgram = function(){
        
        if ($scope.validateUniqueClientLoyaltyProgram($scope.clientViewLoyaltyProgram.id)){    
          $scope.clientLoyaltyProgram.LoyaltyProgramId = $scope.clientViewLoyaltyProgram.id; 
          $scope.clientLoyaltyProgram.number = $scope.clientViewLoyaltyProgram.number; 
          $scope.client.clientLoyaltyPrograms.$add($scope.clientLoyaltyProgram);
        }

        return $scope.clientLoyaltyProgram.$save().$then(function (_client_loyalty_program) {
            $scope.client_data.loyalty_programs = []; 
            $scope.newOrEditShowClientLoyaltyProgram=false;
            $scope.form.loyalty.$setPristine();
        });
        
       };

       
       $scope.validateUniqueClientLoyaltyProgram = function(id){
        var response = true;
        for (var i=0; i<$scope.client.clientLoyaltyPrograms.length; i++) {
            if (id==$scope.client.clientLoyaltyPrograms[i].loyaltyProgram.id) 
            {
              $scope.clientLoyaltyProgram.LoyaltyProgramId = $scope.clientViewLoyaltyProgram.id; 
              $scope.clientLoyaltyProgram.number = $scope.clientViewLoyaltyProgram.number; 
              $scope.client.clientLoyaltyPrograms[i] = $scope.clientLoyaltyProgram;
              response=false;
              break;
            }
          }       
          return response;
       }


      $scope.undoNewClientLoyaltyProgram = function(){
        $scope.newOrEditShowClientLoyaltyProgram=false;
        $scope.clientViewLoyaltyProgram.id = undefined;
        $scope.clientViewLoyaltyProgram.number = undefined;
        $scope.clientLoyaltyProgram = ClientLoyaltyProgram.$build({
          clientId : $scope.client.id
          });
       };


        //monitoreo el valor del programa de lealtad   
        $scope.$watch('clientViewLoyaltyProgram.id', function() {
          
          if  ($scope.clientViewLoyaltyProgram!==undefined){

             if  ($scope.clientViewLoyaltyProgram.id!==undefined &&
                  $scope.clientViewLoyaltyProgram.number!==undefined &&
                  $scope.clientViewLoyaltyProgram.id.length!==0 &&
                  $scope.clientViewLoyaltyProgram.number.length!==0)     
                {
                     $scope.disabledButton=false;   
             }
             else {
                    $scope.disabledButton=true;
             }

          } 

        }, true);


         //monitoreo el valor del numero programa de lealtad   
        $scope.$watch('clientViewLoyaltyProgram.number', function() {
          
          if  ($scope.clientViewLoyaltyProgram!==undefined){

             if  ($scope.clientViewLoyaltyProgram.id!==undefined &&
                  $scope.clientViewLoyaltyProgram.number!==undefined &&
                  $scope.clientViewLoyaltyProgram.id.length!==0 &&
                  $scope.clientViewLoyaltyProgram.number.length!==0)     
                {
                     $scope.disabledButton=false;   
             }
             else {
                    $scope.disabledButton=true;
             }

          } 

        }, true);


      $scope.findIndexByKeyValue = function(array, key, value) {
       return $scope.$parent.findIndexByKeyValue(array, key, value);
      }



      $scope.refreshLoyaltyPrograms = function(name) {
                $scope.loyaltyPrograms = LoyaltyProgram.$build();
                return $scope.loyaltyPrograms.searchLoyaltyPrograms(name).then(function(response) {
                    return $scope.client_data.loyalty_programs = response.data.loyalty_programs;
                });
      };




        $scope.loadLoyaltyPrograms = function(){
            $scope.client.clientLoyaltyPrograms = ClientLoyaltyProgram.$search({client_id: $scope.client.id});
            $scope.$parent.statusLoadLoyaltyPrograms=true;
        };

               
        if($scope.$parent.statusLoadLoyaltyPrograms==false){ 
              $scope.loadLoyaltyPrograms();
        }



        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});


projectApp.directive('clientHistoryTravels', function ($compile,$window,$document,Client, MessageUtils, $timeout,$state, gettextCatalog, $modal) {
    return {
        restrict: 'E',
        scope: { client:"=ngClient" },
        replace:true,
        template:  '<div class="row">'+
                  '<div class="col-md-12" style="overflow:none; margin-bottom:10px; height:auto;">'+
            '<div class="portlet box gray">'+
                '<div class="portlet-title">'+
                  '<div class="caption" >'+
                      '<translate>History</translate>'+
                      '<span class="badge badge-info badge-sheet">'+
                  '{{client.sheets2.length}} </span>'+
                  '</div>'+
                '</div>'+
                '<div class="portlet-body sheet-table sheet-table-client nopadding" style="height: auto;" collapse="clientLoyaltyProgramsCollapse" >'+
                  '<div class="btn-toolbar">'+
                      '<loading-message ng-model="client"></loading-message>'+
                  '</div>'+
                  '<div class="table-responsive">'+
                      '<table name="tablesHistory" class="table-project table table-bordered table-condensed flip-content ">'+
                    '<thead>'+
                      '<tr>'+
                        '<th translate>ID</th>'+
                        '<th translate>Name</th>'+
                        '<th translate>Persons</th>'+
                            '<th translate>Status</th>'+
                            '<th translate>Begin At</th>'+
                            '<th translate>Ends At</th>'+
                      '</tr>'+
                    '</thead>'+
                    '<tbody>'+
                      '<tr ng-repeat="sheet in client.sheets2" ng-click="showSheet(sheet.id)">'+
                        '<td>{{sheet.ref}}</td>'+
                        '<td>{{sheet.title}}</td>'+
                        '<td>{{sheet.clients_count}}</td>'+
                            '<td>{{sheet.status}}</td>'+
                            '<td>{{sheet.begins_at}}</td>'+
                            '<td>{{sheet.ends_at}}</td>'+
                      '</tr>'+
                    '</tbody>'+
                  '</table>'+
                  '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>',
        controller : function($scope, $element, $attrs){
     
       
        $scope.clientView = angular.copy($scope.client);
        $scope.client.getSheets().then(function (data) {
              $scope.client.sheets2 = data.data.clients;
        });

        $scope.load = function () {
            $scope.client.sheets = $scope.client.getSheets();
        };

        $scope.showSheet = function (id) {
            var modalInstance = $modal.open({
                controller: 'ClientSheetCtrl',
                $scope: $scope,
                size:'lg',
                resolve: {
                    sheetId: function () {
                        return id;
                    }
                },
                template: '<div class="modal-scrollable">'+
                    '  <div class="modal-header"> '+
                    '    <a ng-click="close()" type="button" class="pull-right close-white" data-dismiss="modal" aria-label="Close"><i class="glyphicon glyphicon-remove"></i></a>'+
                    '    <h5 >{{sheet.title}}</h5>'+
                    '  </div>'+
                    '  <div class="modal-body2">'+
                    '    <loading-message ng-model="sheet"></loading-message>'+
                    '    <div class="col-sm-12">'+
                    '      <span class="titleCollapse-less pull-right" ng-click="titleCollapse = !titleCollapse" ng-show="titleCollapse" translate>more...</span> '+
                    '      <div class="container-titles-sheet pull-right" collapse="titleCollapse">'+
                    '        <div class="sheet-title-container-b">'+
                    '          <span><translate>Status:</translate></span><strong>{{ sheet.status | uppercase | translate }}</strong>      '+
                    '        </div>'+
                    '        <div class="sheet-title-container-b separador-left">'+
                    '          <span><translate>Created at:</translate></span><strong>{{sheet.createdAt|date:"yyyy-MM-dd"}}</strong>      '+
                    '        </div>'+
                    '        <div class="sheet-title-container-b">'+
                    '          <span><translate>Created by:</translate></span><strong>{{sheet.agent.name}} {{sheet.agent.lastName}}</strong>     '+
                    '          <span class="titleCollapse-less" ng-click="titleCollapse = !titleCollapse" ng-show="!titleCollapse" translate>hide...</span> '+
                    '        </div>'+
                    '      </div>'+
                    '    </div>'+
                    '    <div class="col-sm-12">'+
                    '      <tabset>'+
                    '        <tab>'+
                    '          <tab-heading>'+
                    '            <i class="fa fa-calendar"></i><span translate>Timeline</span>'+
                    '          </tab-heading>'+
                    '          <!-- Timeline -->'+
                    ''+
                    '          <div class="row">'+
                    '            <div class="col-sm-12">    '+
                    '              <timeline>'+
                    '                <ul class="timeline">'+
                    '                  <div ng-repeat="(date, events) in sheet.timeline">'+
                    '                    <li class="timeline-yellow">'+
                    '               <div class="timeline-time">'+
                    '           <span class="date">{{date}}</span>'+
                    '           <span class="time">&nbsp;</span>'+
                    '               </div>'+
                    '                    </li>'+
                    '                    <li ng-repeat="event in events" ng-class="timelineClass(event.color,$parent.$last,$last)">'+
                    '                      <div class="timeline-time">'+
                    '           <span class="date hidden-xs">&nbsp</span>'+
                    '           <span class="time hidden-xs">&nbsp</span>'+
                    '               </div>'+
                    '               <div class="timeline-icon">'+
                    '           <i ng-class="event.icon"></i>'+
                    '               </div>'+
                    '               <div class="timeline-body">'+
                    '           <h2>'+
                    '                          <span ng-hide="event.title">{{event.origin_label}} - {{event.destination_label}}</span>'+
                    '                          <span ng-show="event.title">{{event.title}}</span>'+
                    '                        </h2>'+
                    '         </div>'+
                    '                    </li>'+
                    '                  </div>'+
                    '                </ul>'+
                    '              </timeline>'+
                    '            </div>'+
                    '          </div>'+
                    '        </tab>'+
                    '        <tab>'+
                    '          <tab-heading>'+
                    '            <i class="fa fa-tags"></i><span translate>Tickets And Services</span>'+
                    '          </tab-heading>'+
                    ''+
                    '          <div class="row">'+
                    '            <div class="col-sm-12">'+
                    '              <div class="portlet box gray">'+
                    '                <div class="portlet-title">'+
                    '                  <div class="caption" >'+
                    '                    <i class="icon-note"></i><translate>Tickets and Services</translate>'+
                    '                    <span class="badge badge-info badge-sheet">'+
                    '                      {{sheet.servicesList.length}} </span>'+
                    '                  </div>'+
                    '                </div>'+
                    '                <div class="portlet-body sheet-table sheet-table-client nopadding" collapse="serviceCollapse">'+
                    '                  <div class="btn-toolbar">'+
                    '                    <loading-message ng-model="sheet" one-time-loading="true"></loading-message>'+
                    '                    <loading-message ng-model="sheet.airServices"></loading-message>'+
                    '                    <loading-message ng-if="!sheet.airServices.$pending || sheet.airServices.$pending.length == 0"  ng-model="sheet.accomodationServices"></loading-message>'+
                    '                  </div>'+
                    '                  <div class="table-responsive" ng-show="(sheet.servicesList && sheet.servicesList.length > 0) || note_command_services.length > 0">'+
                    '                    <table class="table-project table table-bordered table-condensed flip-content ">'+
                    '                      <thead>'+
                    '                        <tr>'+
                    '                          <th class="col-sm-1"translate>Kind</th>'+
                    '                          <th class="col-sm-3"translate>Date</th>'+
                    '                          <th translate>Description</th>'+
                    '                        </tr>'+
                    '                      </thead>'+
                    '                      <tbody>'+
                    '                        <tr ng-repeat="service in sheet.servicesList | orderBy:\'date\'">'+
                    '                          <td ng-click="editService(service)">{{ service.type | translate | capitalize }}'+
                    '                          </td>'+
                    '                          <td ng-if="service.type==\'air\'">'+
                    '                            {{ service.date}}'+
                    '                          </td>'+
                    '                          <td ng-if="service.type==\'accomodation\'" ng-click="editService(service)">'+
                    '                            {{ service.initDate }} -- {{service.endDate}}'+
                    '                          </td>'+
                    '                          <td ng-if="service.type==\'air\'" ng-click="editService(service)">'+
                    '                            <strong translate>Origin</strong>: {{service.originLabel}} /'+
                    '                            <strong translate>Destination</strong>: {{service.destinationLabel}}'+
                    '                          </td>'+
'                          <td ng-if="service.type==\'accomodation\'" ng-click="editService(service)">'+
                    '                            {{service.city}} / {{service.accomodationType}}'+
                    '                          </td>'+
                    '                        </tr>'+
                    '                        <tr  class="no-processed-command" ng-repeat="note in note_command_services">'+
                    '                          <td  colspan="5">{{note.command_text}}</td>'+
                    '                        </tr>'+
                    '                      </tbody>'+
                    '                    </table>'+
                    '                  </div>'+
                    '                  <p ng-show="(!sheet.servicesList || sheet.servicesList.length==0) && note_command_services.length==0" translate>'+
                    '                    There aren\'t any services added yet'+
                    '                  </p>'+
                    '                </div>'+
                    '              </div>'+
                    '            </div>'+
                    '          </div>    '+
                    '        </tab>'+
                    '        <tab>'+
                    '          <tab-heading>'+
                    '            <i class="fa fa-users"></i><span translate>Clients</span>'+
                    '          </tab-heading>'+
                    '          <div class="row-fluid">'+
                    '            <div class="portlet box gray">'+
                    '              <div class="portlet-title">'+
                    '                <div class="caption" >'+
                    '                  <i class="fa fa-users"></i><translate>Clients</translate>'+
                    '                  <span class="badge badge-info badge-sheet">'+
                    '                    {{sheet.clients.length}} </span>'+
                    '                </div>'+
                    '              </div>'+
                    '              <div class="portlet-body sheet-table sheet-table-client nopadding" collapse="clientCollapse">'+
                    '                '+
                    '                <div class="btn-toolbar">'+
                    '                  <loading-message ng-model="sheet"></loading-message>'+
                    '                </div>'+
                    '                <div class="table-responsive">'+
                    '                  <table name="tablesClients" class="table-project table table-bordered table-condensed flip-content ">'+
                    '                    <thead>'+
                    '                      <tr>'+
                    '                        <th translate>ID</th>'+
                    '                        <th translate>Name</th>'+
                    '                        <th translate>Email</th>'+
                    '                      </tr>'+
                    '                    </thead>'+
                    '                    <tbody>'+
                    '                      <tr ng-repeat="client in sheet.clients" >'+
                    '                        <td ng-click="addDetailClient(sheet,client,null)">'+
                    '                          <span >{{ client.id }}</span>'+
                    '                        </td>'+
                    '                        <td ng-click="addDetailClient(sheet,client,null)">'+
                    '                          <span >{{ client.firstName }}&nbsp;{{ client.middleName }}&nbsp;{{ client.lastName }}&nbsp;{{ client.secondLastName }}</span>'+
                    '                        </td>'+
                    '                        <td ng-click="addDetailClient(sheet,client,null)">'+
                    '                          <span >{{ client.email }}</span>'+
                    '                        </td>'+
                    '                      </tr>'+
                    '                      <tr class="no-processed-command" ng-repeat="note in note_command_clients">'+
                    '                        <td  colspan="5">{{note.command_text}}</td>'+
                    '                      </tr>'+
                    '                    </tbody>'+
                    ''+
                    '                    '+
                    '                  </table>'+
                    '                </div>'+
                    '              </div>'+
                    '            </div>'+
                    '          </div>'+
                    '        </tab>'+
                    '      </tabset>'+
                    '    </div>'+
                    '  </div>'+
                    '</div>'
            });
        };



        /*$scope.loadLoyaltyPrograms = function(){
            $scope.client.clientLoyaltyPrograms = ClientLoyaltyProgram.$search({client_id: $scope.client.id});
            $scope.$parent.statusLoadLoyaltyPrograms=true;
        };

               
        if($scope.$parent.statusLoadLoyaltyPrograms==false){ 
              $scope.loadLoyaltyPrograms();
        }*/



        },
        link : function ($scope, $element, $attrs) {
          
        }
    }

});

projectApp.controller('ClientSheetCtrl', [
    '$scope',
    '$modalInstance',
    'sheetId',
    'Sheet',
    function ($scope, $modalInstance, sheetId, Sheet) {
        var today = function() {
            var now = new Date();
            var day = now.getDate() < 10 ? '0'+now.getDate() : now.getDate();
            var month = (now.getMonth()+1) < 10 ? '0'+(now.getMonth()+1) : (now.getMonth()+1);
            var year = now.getFullYear();
            return year+'-'+month+'-'+day;
        };

        var getColor = function(date) {
            var res = 'timeline-sp1';
            if (new Date(date) < new Date(today())) {
                res = 'timeline-blue';
            } else if (new Date(date) > new Date(today())){
                res = 'timeline-sp2';
            }
            return res;
        };
        
        var addEvent = function (element, date) {
            if ($scope.sheet.timeline[date] == undefined){
                $scope.sheet.timeline[date] = [element];
            } else {
                $scope.sheet.timeline[date] = $scope.sheet.timeline[date].concat(element);
            };
        };

        function buildTimeline() {
            $scope.sheet.timeline = {};
            addEvent({
                title: 'Hoy',
                badgeClass: 'warning',
                badgeIconClass: 'fa fa-thumb-tack',
                color: 'timeline-sp1',
                icon: 'fa fa-thumb-tack'
            }, today());
            $.map($scope.sheet.airServices, function(elem){
                var newElem =   {
                    type: 'air',
                    passengers: elem.passengers,
                    origin_label: elem.originLabel,
                    destination_label: elem.destinationLabel,
                    badgeClass: 'info',
                    badgeIconClass: 'fa fa-plane',
                    color: getColor(elem.date),
                    icon: 'fa fa-plane'
                };
                addEvent(newElem,elem.date);
            });
            $.map($scope.sheet.accomodationServices, function(elem){
                var elementIn =  {
                    type: 'in',
                    people_number: elem.peopleNumber,
                    title: 'Check In: '+elem.accomodationType+' - '+elem.city,
                    badgeClass: 'success',
                    badgeIconClass: 'fa fa-h-square',
                    color: getColor(elem.initDate),
                    icon: 'fa fa-h-square'
                };
                var elementOut =  {
                    type: 'out',
                    people_number: elem.peopleNumber,
                    title: 'Check Out: '+elem.accomodationType+' - '+elem.city,
                    badgeClass: 'danger',
                    badgeIconClass: 'fa fa-h-square',
                    color: getColor(elem.endDate),
                    icon: 'fa fa-h-square'
                };
                addEvent(elementIn,elem.initDate);
                addEvent(elementOut,elem.endDate);
            });
        };
        
        function generateList() {
            $scope.sheet.servicesList =
                $.map( $scope.sheet.airServices, function(elem) {
                    elem.type = 'air'; 
                    return elem; }
                ).concat(
                    $.map( $scope.sheet.accomodationServices, function(elem) { 
                        elem.type = 'accomodation'; 
                        elem.date = elem.initDate; 
                        return elem; }
                         ));
        };
        
        $scope.sheet = Sheet.$find(sheetId).$then(function () {
            buildTimeline();            
            generateList();
        });

        $scope.close = function () {
            $modalInstance.close();
        };

        $scope.timelineClass = function (colorClass, lastDate, lastEvent) {
            var res = colorClass;
            
            if (lastDate && lastEvent) {
                res = res+' timeline-noline';
            }
            
            return res;
        };
    }
    ]);    