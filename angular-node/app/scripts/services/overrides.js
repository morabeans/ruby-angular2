'use strict';
var overrideServices = angular.module('overrideServices', ['ngResource']);

overrideServices.factory('BaseModel', [
  'restmod',
  'inflector',
  function(restmod, inflector) {
    return restmod.mixin({
      'Model.encodeUrlName': function(_name) {
        return inflector.parameterize(_name, '_');
      }
    });
}]);
