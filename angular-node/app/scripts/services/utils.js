'use strict';
var utilServices = angular.module('utilServices', ['ngResource']);
utilServices.factory('MessageUtils', [
    'toaster',
    function (toaster) {
        return {
            error: function (data, stat) {
                if (data.error) {
                    toaster.pop('error', 'Error', data.error);
                } else if (data.errors) {
                    for (var key in data.errors) {
                        var message = '';
                        for (var i = 0; i < data.errors[key].length; i++) {
                            message += data.errors[key];
                            if (i < data.errors[key].length - 1) {
                                message += ', ';
                            }
                        }
                        toaster.pop('error', 'Error', key + ': ' + message);
                    }
                } else {
                    toaster.pop('error', 'Error', stat + ' ocurred');
                }
            },
            success: function (msg) {
                toaster.pop('success', 'Exito', msg);
            },
            getStringError: function(data){
                var messageFinal = '';
                if (data) {
                    for (var key in data) {
                        var message = '';
                        for (var i = 0; i < data[key].length; i++) {
                            message += data[key];
                            if (i < data[key].length - 1) {
                                message += ', ';
                            }
                        }
                        messageFinal+=' '+key+': '+message+' <br>';   
                    }
                }
                return messageFinal;
            }
        };
    }
                    ]);

utilServices.factory('UploadFiles', [
    'Upload',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (Upload, API_PREFIX, MODULE_PREFIX) {
        return {
            upload: function (files) {
                if(files.length>0){
                    return Upload.upload({
                        url: API_PREFIX + MODULE_PREFIX + '/upload',
                        method: 'POST',
                        headers: {},
                        data: {},
                        file: files[0],
                        fileFormDataName: 'tempfile[file]',
                        formDataAppender: function(fd, key, val) {
                            if (angular.isArray(val)) {
                                angular.forEach(val, function(v) {
                                    fd.append('tempfile['+key+']', v);
                                });
                            } else {
                                fd.append('tempfile['+key+']', val);
                            }
                        }
                    });
                }
            },
            uploadFile: function (file) {
                if(file!=null){
                    return Upload.upload({
                        url: API_PREFIX + MODULE_PREFIX + '/upload',
                        method: 'POST',
                        headers: {},
                        data: {},
                        file: file,
                        fileFormDataName: 'tempfile[file]',
                        formDataAppender: function(fd, key, val) {
                            if (angular.isArray(val)) {
                                angular.forEach(val, function(v) {
                                    fd.append('tempfile['+key+']', v);
                                });
                            } else {
                                fd.append('tempfile['+key+']', val);
                            }
                        }
                    });
                }
            }
        };
    }
]);
