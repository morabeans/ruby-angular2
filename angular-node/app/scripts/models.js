'use strict';
var projectApp = angular.module('projectApp');
projectApp.factory('Agency', function (restmod) {
    return restmod.model('agencies');
});
projectApp.factory('Client', [
    'DATE_FORMAT',
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (DATE_FORMAT, restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/clients').mix({
            // hotel_options: { hasMany: 'HotelOption' },
            // client_preferences: { hasOne: 'ClientPreference' },
            passports: { hasMany: 'Passport' },
            visas: { hasMany: 'Visa' },
            customerDates: { hasMany: 'CustomerDate'},
            addresses: { hasMany: 'Address'},
            socialMedias: { hasMany: 'SocialMedia'},
            phones: { hasMany: 'Phone'},
            hotelOptions: { hasMany: 'HotelOption' },
            clientPreferences:{hasOne:'ClientPreferences'},
            clientLoyaltyPrograms:{hasMany:'ClientLoyaltyProgram'}
            
        }).mix({
            $extend: {
                Record: {
                    searchClients: function (clientName) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/search_clients.json?client_name='+clientName);
                    },
                    importClients: function (clients) {
                        return $http.post(API_PREFIX + MODULE_PREFIX + '/import_clients.json',clients);
                    },
                    countClientByAgency: function(){
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/count_clients_by_agency.json');
                    },
                    getSheets: function(){
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + this.id + '/sheets.json');
                    },
                    addStyle: function (tripStyleId) {
                        return $http.put(API_PREFIX + MODULE_PREFIX + '/clients/' + this.id + '/trip_style/' + tripStyleId + '.json');
                    },
                    deleteStyle: function (tripStyleId) {
                        return $http.delete(API_PREFIX + MODULE_PREFIX + '/clients/' + this.id + '/trip_style/' + tripStyleId + '.json');
                    },
                    addGroup: function (tripGroupId) {
                        return $http.put(API_PREFIX + MODULE_PREFIX + '/clients/' + this.id + '/trip_group/' + tripGroupId + '.json');
                    },
                    deleteGroup: function (tripGroupId) {
                        return $http.delete(API_PREFIX + MODULE_PREFIX + '/clients/' + this.id + '/trip_group/' + tripGroupId + '.json');
                    }

                },
                Model: {
                    getBasicInformation: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_basic_data.json');
                    },
                    getClientPhones: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_phones.json');
                    },
                    getClientAddresses: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_addresses.json');
                    },
                    getClientCustomerDates: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_customer_dates.json');
                    },
                    getProfessionalInformation: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_professional_details.json');
                    },
                    getSocialMedias: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_social_medias.json');
                    },
                    getSpecialComments: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/general_information_special_comments.json?');
                    },
                    getPassports: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/passports.json');
                    },
                    getVisas: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/visas.json');
                    },
                    getTripStyles: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/preferences_trip_styles.json');
                    },
                    getTripGroups: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/preferences_trip_groups.json');
                    },
                    getHotelPreferences: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/preferences_hotel.json');
                    },
                    getFlyPreferences: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/preferences_fly.json');
                    },
                    getLoyaltyPrograms: function (clientId) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/clients/' + clientId + '/loyalty_programs.json');
                    }                    
                    
             }
            }
        }).mix(function () {
            this.on('after-request-error', function (response) {
                //Message.error({ 'error': 'No se pudo guardar el cliente' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        }).mix({
            birthDate: {
                decode: 'date',
                param: DATE_FORMAT
            },
            sex: { init: 'M' }
        });
    }
]);

projectApp.factory('Company', function (restmod) {
    return restmod.model('companies');
});

projectApp.factory('Provider', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX', 
    'MODULE_PREFIX',
    function (restmod, MessageUtils, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/providers').mix({
            $extend: {
                Record: {
                    searchProviders: function (providerName) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/search_providers.json?provider_name='+providerName);
                    }
                }
            }
            
        }).mix(function () {
            this.on('after-request-error', function (response) {
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });

    }

]);



//projectApp.factory('Service', [
//'DATE_FORMAT',
//'restmod',
//'MessageUtils',
//'$http',
//'API_PREFIX',
//'MODULE_PREFIX',
//function (DATE_FORMAT, restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
//return restmod.model('/services').mix(function () {
//this.on('after-request-error', function (response) {
//if (response.data.errors.status[0] === 'can\'t be confirmed without any clients') {
//Message.error({ 'error': 'Debe indicar los clientes que disfrutarán del servicio para poder confirmarlo' }, response.status);
//}
//else {
//Message.error({ 'error': 'No se pudo guardar el servicio' }, response.status);
//if (response.data.errors) {
//this.errors = response.data.errors;
//}
//}
//});
//this.on('after-request', function () {
//this.errors = {};
//});
//}).mix({
//clients: { hasMany: 'Client' },
//provider: { hasOne: 'Provider' },
//beginsAt: {
//decode: 'date',
//param: DATE_FORMAT
//},
//endsAt: {
//decode: 'date',
//param: DATE_FORMAT
//}
//}).mix({
//$extend: {
//Record: {
//addClients: function (clientIDs) {
//return $http.put(API_PREFIX + MODULE_PREFIX + '/services/' + this.id + '/clients.json', { 'client_ids': clientIDs });
//},
//deleteClient: function (clientID) {
//return $http.delete(API_PREFIX + MODULE_PREFIX + '/services/' + this.id + '/clients/' + clientID + '.json');
//}
//}
//}
//});
//}
//]);
projectApp.factory('Event', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/events').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo obtener los eventos' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('Comment', [
    'restmod',

    function (restmod) {
        return restmod.model().mix({
            $config: {
                name: 'comment',
                plural: 'comments'
            }
        });
    }
]);

projectApp.factory('AirService', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model().mix({
            $config: {
                name: 'air_service',
                plural: 'air_services'
            }
        }).mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el servicio' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('AirPort', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model().mix({
            $config: {
                name: 'air_port',
                plural: 'air_ports'
            }
        }).mix({
            $extend: {
                Record: {
                    searchAirPort: function (airportName) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/search_airports.json?airport_name='+airportName);
                    }
                }
            }
        }).mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo obtener el aeropuerto' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('AccomodationService', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model().mix({
            $config: {
                name: 'accomodation_service',
                plural: 'accomodation_services'
            }
        }).mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el servicio' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);


projectApp.factory('Sheet', [
    'restmod',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/sheets').mix({
            airServices: { hasMany: 'AirService' },
            accomodationServices: { hasMany: 'AccomodationService' },
            clients: { hasMany: 'Client' },
            comments: {hasMany: 'Comment'},
            sheetDetails: {hasMany: 'SheetDetail'},
            vehicleServices: {hasMany: 'VehicleService'},
            agent:{hasOne:'Agent'}
        }).mix({
            $extend: {
                Record: {
                    addClients: function (clientIDs, commandText) {
                        return $http.put(API_PREFIX + MODULE_PREFIX + '/sheets/' + this.id + '/clients.json', { 'client_ids': clientIDs, 'command_text': commandText });
                    },
                    deleteClient: function (clientID) {
                        return $http.delete(API_PREFIX + MODULE_PREFIX + '/sheets/' + this.id + '/clients/' + clientID + '.json');
                    },
                    setContact: function (clientID, isContact) {
                        return $http.post(API_PREFIX + MODULE_PREFIX + '/sheets/' + this.id + '/contacts/', { 'client_id': clientID, 'is_contact': isContact });
                    },
                    confirm: function () {
                        return $http.post(API_PREFIX + MODULE_PREFIX + '/sheets/' + this.id + '/confirm.json');
                    },
                    sheetsWithCoord: function () {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/sheets_with_coord.json');
                    },
                    searchNoteCommands: function(classification){
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/sheets/' + this.id + '/note_commands.json?classification='+classification);
                    },
                    createSheetByNotes: function(sheet, sheet_note, notes_data){
                        return $http.post(API_PREFIX + MODULE_PREFIX + '/create_sheet_by_notes.json', {'sheet': sheet, 'sheet_note': sheet_note, 'notes_data': notes_data});
                    }
                },
                Model: {
                    options: function () {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/sheets/options.json');
                    }
                }
            }
        });
    }
]);


projectApp.factory('TrackedClient', function (restmod) {
    return restmod.model('/connector/pnr');
});
projectApp.factory('Provider', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/providers').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el Proveedor' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('DetailForm', [
    'restmod',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, $http, API_PREFIX, MODULE_PREFIX) {
        var DetailForm = restmod.model('/detail_form').mix({
            $extend: {
                Model: {
                    getSchema: function (kind) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/get_schema.json?kind='+kind);
                    }
                }
            }
        });
        return DetailForm;
    }
]);

projectApp.factory('TempFile', [
    'restmod',

    function (restmod) {
        return restmod.model('/temp_file');
    }
]);


projectApp.factory('ClientPreferences', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX', 
    'MODULE_PREFIX',
    function(restmod, Message,$http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/client_preferences').mix({
            hotelServices: { hasMany: 'HotelService' }
        }).mix(function() {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'Hubo un problema obteniendo las preferencias del cliente' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });

            this.on('after-request', function () { this.errors = {}; });
        }).mix({
            $extend: {
                Record: {
                    addFacilitiesService: function (facilitiesServiceIDs) {
                        return $http.put(API_PREFIX + MODULE_PREFIX + '/client_preferences/' + this.id + '/facilities_services.json', { 'facilities_services_ids': facilitiesServiceIDs });
                    }
                }
            }
        });
    }]);

projectApp.factory('HotelService', ['restmod', 'MessageUtils', function (restmod, Message){
    return restmod.model('/hotel_services').mix(function() {
        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'Hubo un problema obteniendo las preferencias de los servicios de hoteles' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);

projectApp.factory('RoomType', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/room_types').mix(function () {
        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de habitación' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);

projectApp.factory('TripType', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/trip_types').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de almohada' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);


projectApp.factory('HotelOption', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/hotel_options').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron guardar las Opciones de Hoteles' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('PillowType', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/pillow_types').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de almohada' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);

projectApp.factory('BedType', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/bed_types').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de cama' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);

projectApp.factory('BedSheetType', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/bed_sheet_types').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de sábanas' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);


projectApp.factory('SeatingPreference', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/seating_preferences').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener las  preferencias de asiento' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);


projectApp.factory('SpecialAssistance', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/special_assistances').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de asistencia especial' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);

projectApp.factory('Company', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/companies').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener las compañias' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);


projectApp.factory('TypeService', ['restmod', 'MessageUtils', function (restmod, Message) {
    return restmod.model('/type_services').mix(function () {

        this.on('after-request-error', function (response) {
            Message.error({ 'error': 'No se pudieron obtener los tipos de servicios' }, response.status);
            if (response.data.errors) { this.errors = response.data.errors; }
        });

        this.on('after-request', function () { this.errors = {}; });
    });
}]);

projectApp.factory('LoyaltyProgram', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/loyalty_programs').mix({
            $extend: {
                Record: {
                    searchLoyaltyPrograms: function (loyaltyName) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/search_loyaltys.json?loyalty_name='+loyaltyName);
                    }
                }
            }
        }).mix(function () {
            
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron obtener los programas de lealtad' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });
            
            this.on('after-request', function () { this.errors = {}; });
        });
    }]);

projectApp.factory('ClientLoyaltyProgram', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/client_loyalty_programs').mix(function () {
            
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron obtener los programas de lealtad del cliente' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });
            
            this.on('after-request', function () { this.errors = {}; });
        });
    }]);



projectApp.factory('Passport', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/passports').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el Pasaporte' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('Visa', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/visas').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar la Visa' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('CustomerDate', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/customer_dates').mix({
            actionTypes: { hasMany: 'ActionType' }

        }).mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar la fecha' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        }).mix({
            $extend: {
                Record: {
                    addActionType: function (actionTypesIDs) {
                        return $http.put(API_PREFIX + MODULE_PREFIX + '/customer_dates/' + this.id + '/action_types.json', { 'action_type_ids': actionTypesIDs });
                    }
                }
            }
        });
    }
]);

projectApp.factory('Address', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/addresses').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar la direccion' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        }).mix({
            $extend: {
                
                Model: {
                    getAddressGlobal: function (address) {
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/addressGlobal?address='+address);
                    }
                }
            }
        });
    }
]);

projectApp.factory('SocialMedia', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/social_medias').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el social media' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('Phone', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/phones').mix({
            country: { hasOne: 'Country'}
        }).mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el Telefono' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('ActionType', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/action_types').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el tipo de accion' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('Education', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/educations').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el nivel de educacion' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('Profession', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/professions').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar la profesion' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);

projectApp.factory('Agent', [
    'restmod',
    'MessageUtils',
    function (restmod, Message) {
        return restmod.model('/agents').mix(function () {
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudo guardar el agent' }, response.status);
                if (response.data.errors) {
                    this.errors = response.data.errors;
                }
            });
            this.on('after-request', function () {
                this.errors = {};
            });
        });
    }
]);
/*
  projectApp.factory('Agent', [
  'restmod',
  '$http',
  'API_PREFIX',
  'MODULE_PREFIX',
  function (restmod, $http, API_PREFIX, MODULE_PREFIX, MODULE_PREFIX) {
  var Agent =  {
  updateAgent: function (data) {
  return $http.put(API_PREFIX + MODULE_PREFIX + '/avatar_agent.json',data);
  },
  getAvatar: function () {
  return $http.get(API_PREFIX + MODULE_PREFIX + '/avatar_agent.json');
  }
  }
  return Agent;
  }
  ]);*/
projectApp.factory('ClientSpecialComment', ['restmod',
                                           function (restmod){
                                               return restmod.model('/client_special_comments');
                                           }
                                          ]);

projectApp.factory('SpecialCategory', ['restmod',
                                      function (restmod){
                                          return restmod.model('/special_categories');
                                      }
                                     ]);

projectApp.factory('SpecialSubCategory', [
    'restmod',
    function (restmod){
        return restmod.model('/special_sub_categories');
    }
]);

projectApp.factory('SpecialSubCategoryDetail', [
    'restmod',
    function (restmod){
        return restmod.model('/special_sub_category_details');
    }
]);

projectApp.factory('SheetNote', [
    'restmod',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/sheet_notes').mix({
            sheetNoteCommands: { hasMany: 'SheetNoteCommand' }

        }).mix({
            $extend:{
                Record:{
                    updateSheetNote: function(id, title, reason, privacy){
                        $http.put(API_PREFIX + MODULE_PREFIX + '/sheet_notes/' + id + '.json', {'title': title, 'reason': reason, 'public': privacy});
                    }
                }
            }
        });
    }
]);

projectApp.factory('SheetNoteCommand', [
    'restmod',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/sheet_note_commands').mix({
            $extend:{
                Record:{
                    updateSheetNoteCommand: function(id, typeCommand, commandText, typeCommandId, processed, classification){
                        $http.put(API_PREFIX + MODULE_PREFIX + '/sheet_note_commands/' + id + '.json', {'type_command': typeCommand,
                                                                                                        'command_text': commandText, 'type_command_id': typeCommandId, 'processed': processed, 'classification': classification});
                    }
                }
            }
        });
    }
]);

projectApp.factory('SheetDetail', [
    'restmod',
    function (restmod){
        return restmod.model('/sheet_details').mix({
            sheetDetailNotes: {hasMany: 'SheetDetailNote'}
        })
    }
]);
projectApp.factory('SheetDetailNote', [
    'restmod',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, $http, API_PREFIX, MODULE_PREFIX){
        return restmod.model('/sheet_detail_notes').mix({
          $extend:{
            Record:{
              updateSheetDetail: function(sheet_detail_id, sheet_detail_note_ids){
                return $http.put(API_PREFIX + MODULE_PREFIX + '/update_id_sheet_detail.json', {'sheet_detail_id': sheet_detail_id, 'sheet_detail_note_ids':sheet_detail_note_ids});
              },
              deleteSheetDetailNote: function(sheet_detail_note_ids){
                return $http.delete(API_PREFIX + MODULE_PREFIX + '/delete_id_sheet_detail_notes.json?sheet_detail_note_ids='+sheet_detail_note_ids);
              }
            }
          }
        });
    }
]);

projectApp.factory('Country', [
    'restmod',
    function (restmod){
        return restmod.model('/countries');
    }
]);

projectApp.factory('DanaConnect', [
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function ($http, API_PREFIX, MODULE_PREFIX) {
        return {
            signCheck: function(){
                return $http.get(API_PREFIX + MODULE_PREFIX + '/dana_connect/sign_check/',{'headers':['Access-Control-Allow-Origin','*'],
                                                                                           'xsrfCookieName':"PHPSESSID"});
                /*return $http.get('https://secure.danaconnect.com/portal/session/externalSignCheck/',{'headers':['Access-Control-Allow-Origin','*'],
                  'xsrfCookieName':"PHPSESSID"}
                  );*/
            },
            tokenRequest: function(){
                return $http.get(API_PREFIX + MODULE_PREFIX + '/dana_connect/token_request/');
            },
            signIn: function(token){
                return $http({
                    method: 'POST',
                    url: 'https://secure.danaconnect.com/portal/session/externalSignIn/',
                    data: $.param({token: token}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        }
    }]);



projectApp.factory('TripStyle', [
    'restmod',
    function (restmod) {
        return restmod.model('/trip_styles');
    }
]);

projectApp.factory('TripGroup', [
    'restmod',
    function (restmod) {
        return restmod.model('/trip_groups');
    }
]);
projectApp.factory('ReserveSystem', [
    'restmod',
    'MessageUtils',
    '$http',
    function (restmod, Message, $http) {
        return restmod.model('/reserve_systems').mix(function () {
            
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron obtener los sistemas de reserva' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });
            
            this.on('after-request', function () { this.errors = {}; });
        });
    }]);

projectApp.factory('TravelItinerary', [
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    'restmod',
    function ($http, API_PREFIX, MODULE_PREFIX,restmod) {
        return restmod.model().mix({

        }).mix({
            $extend: {
                Record: {
                    getPNRItinerary: function(pnr, backend){
                        return $http.get(API_PREFIX + MODULE_PREFIX + '/connector/pnr.json?pnr='+pnr+'&backend='+backend);
                    }
                }
            }
        });
    }]);
projectApp.factory('Pnr', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/pnrs').mix({
          $extend : {
            Record : {
              getByCode: function(code){
                return $http.get(API_PREFIX + MODULE_PREFIX + '/get_pnr_code.json?code='+code);
              },
              associatePnrSheet: function(sheet_id, code, clients, services){
                return $http.post(API_PREFIX + MODULE_PREFIX + '/associate_pnr_sheet.json', {'sheet_id': sheet_id, 'code':code, 'clients':clients, 'services':services});
              }
            }
          }
        }).mix(function () {
            
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron obtener los PNRs' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });
            
            this.on('after-request', function () { this.errors = {}; });
        });
    }]);
projectApp.factory('PnrClient', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/pnr_clients').mix(function () {
            
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron obtener los clientes PNR' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });
            
            this.on('after-request', function () { this.errors = {}; });
        });
    }]);
projectApp.factory('VehicleService', [
    'restmod',
    'MessageUtils',
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function (restmod, Message, $http, API_PREFIX, MODULE_PREFIX) {
        return restmod.model('/vehicle_services').mix(function () {
            
            this.on('after-request-error', function (response) {
                Message.error({ 'error': 'No se pudieron obtener los servicios de vehiculos' }, response.status);
                if (response.data.errors) { this.errors = response.data.errors; }
            });
            
            this.on('after-request', function () { this.errors = {}; });
        });
    }]);


projectApp.factory('marketing', [
    '$http',
    'API_PREFIX',
    'MODULE_PREFIX',
    function ($http, API_PREFIX, MODULE_PREFIX) {
        MODULE_PREFIX = "/mktg"
        return {
            list: function(){
                return $http.get(API_PREFIX + MODULE_PREFIX + '/mktg_conversations.json');
                
            },
            list2: function(){
                return $http.get(API_PREFIX + MODULE_PREFIX + '/mktg_conversations.json');
            },
            signIn: function(token){
                return $http({
                    method: 'POST',
                    url: 'https://secure.danaconnect.com/portal/session/externalSignIn/',
                    data: $.param({token: token}),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        }
    }]);
