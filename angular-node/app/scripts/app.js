'use strict';
var projectApp = angular.module('projectApp', [
  'ng-token-auth',
  'gettext',
  'ngResource',
  'projectConstants',
  'ngSanitize',
  'ngRoute',
  'restmod',
  'ui.router',
  'ui.bootstrap',
  'ui.utils',
  'utilServices',
  'overrideServices',
  'toaster',
  'project.templates',
  'schemaForm',
  'ngFileUpload',
  'ui.date',
  'uiGmapgoogle-maps',
  'ngAnimate',
  'angularjs-dropdown-multiselect',
  'ui.select',
  'toggle-switch',
  'checklist-model',
  'ngCookies',
  'ipCookie',
  'sbDateSelect',
  'chart.js',
  '720kb.tooltips',
  'ui.mask',
  'ngDraggable'
]);
projectApp.constant({
  'DATE_FORMAT': 'yyyy-MM-dd'
});
projectApp.constant('CONST_SERVICES', {
  KIND: {
    FLIGHT: 'FLIGHT',
    ACCOMMODATION: 'ACCOMMODATION'
  }
});

projectApp.run([
  '$rootScope',
  '$location',
  '$auth',
  'gettextCatalog',
  '$window',
  function ($rootScope, $location, $auth, gettextCatalog,$window) {
    gettextCatalog.debug = true;
    gettextCatalog.setCurrentLanguage('es');
    moment.locale("es");
    $rootScope.isClosedMenu = false;
    // verify if there is a logged in user on each change of state.
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

      if($location.$$path=="/recoverpassword"){
	  return;
      }else
       if( $location.$$path=="/"  ){ 
        $window.location.href = 'http://w3.projecttech.com/eng';
        return;
      }else{
        $auth.validateUser({ config: 'Agent' }).then(
          function(){
 
          },
          function() {
                $location.path('/login');
          });
        }


        });


    $rootScope.$on('auth:logout-success', function() {
      $location.path('/login');
    });
  }
]);

projectApp.config([
    '$authProvider',
    '$stateProvider',
    '$urlRouterProvider',
    'restmodProvider',
    '$locationProvider',
    'datepickerPopupConfig',
    'API_PREFIX',
    'MODULE_PREFIX',
    'uiGmapGoogleMapApiProvider',
    function ($authProvider, $stateProvider, $urlRouterProvider, restmodProvider, $locationProvider, datepickerPopupConfig, API_PREFIX, MODULE_PREFIX, GoogleMapApiProvider) {

    $authProvider.configure([
      {
        Agent: {
          apiUrl: API_PREFIX,
          proxyIf: function() { window.isOldIE(); },
          signOutUrl:          '/agent_auth/sign_out',
          emailSignInPath:     '/agent_auth/sign_in',
          tokenValidationPath: '/agent_auth/validate_token',
          passwordResetPath:   '/agent_auth/password',
          passwordUpdatePath:  '/agent_auth/password'
        }
      }, {
          Client: {
              apiUrl: API_PREFIX,
              tokenValidationPath: '/client_auth/validate_token',
              passwordResetPath:   '/client_auth/password',
              passwordUpdatePath:  '/client_auth/password'
          }
      }
    ]);

    restmodProvider.rebase('AMSApi');
    //make all ajax calls have a '.json' at the end of the url
    restmodProvider.rebase({
      $hooks: {
        'before-request': function (_req) {
            _req.url += '.json';
        }
      }
    });
    restmodProvider.rebase({ $config: { urlPrefix: API_PREFIX + MODULE_PREFIX } });
    restmodProvider.rebase('BaseModel');


    $urlRouterProvider.otherwise('/');
   // $urlRouterProvider.when('/', '/website');
    $locationProvider.html5Mode(true);

    $stateProvider.state('all', {
      abstract: true,
      template: '<ui-view/>'
    });

    $stateProvider.state('login', {
      url: '/login',
      templateUrl: '/views/login.html',
      controller: 'LoginCtrl'
    });

    $stateProvider.state('recoverpassword', {
      url: '/recoverpassword',
      templateUrl: '/views/recover_password.html',
      controller: 'RecoverPasswordCtrl'
    });
            
    $stateProvider.state('website', {
      url: '/website',
      templateUrl: '/views/website.html',
      controller: 'WebsiteCtrl'
      });

    $stateProvider.state('change_password', {
      url: '/change_password',
      templateUrl: '/views/change_password.html',
      controller: 'ChangePasswordCtrl'
    });
        
    $stateProvider.state('danaConnect', {
        url: '/marketing',
        templateUrl: '/views/marketing/dashboard.html',
        controller: 'MarketingCtrl'
    });

        
    //Ficha
    $stateProvider
      .state('base', {
        abstract: true,
        templateUrl: '/views/partials/base.html',
        controller: 'MainCtrl'
      })
      .state('dashboard', {
        parent: 'base',
        url: '/dashboard',
	      templateUrl: '/views/dashboard.html',
	      controller: 'DashboardCtrl'
      })
      .state('clients', {
        parent: 'base',
        url: '/clientes',
        templateUrl: '/views/client/clients.html',
        controller: 'ListClientCtrl'
      })
      .state('clients.create', {
        parent: 'base',
        url: '/clientes',
        templateUrl: '/views/client/client.html',
        controller: 'ClientFormCtrl',
        data: { creating: true },
        resolve:{
           rslv_client:['Client',function(Client) { return Client.$build({});  }]
        }
      }).state('clients.edit', {
        parent: 'base',
        url: '/clientes/:clientId',
        templateUrl: '/views/client/client.html',
        controller: 'ClientFormCtrl',
        data: { creating: false },
        resolve:{
            rslv_client:['Client','$stateParams','Phone', 'Address', function(Client,$stateParams, Phone, Address) { 
            return Client.getBasicInformation($stateParams.clientId).then(function (_client) {
                    return Client.$build({
                      id: $stateParams.clientId,
                      firstName: _client.data.clients[0].first_name,
                      lastName: _client.data.clients[0].last_name,
                      secondLastName: _client.data.clients[0].second_last_name,
                      middleName: _client.data.clients[0].middle_name,
                      email: _client.data.clients[0].email,
                      sex: _client.data.clients[0].sex,
                      birthDate: _client.data.clients[0].birth_date,
                      identifier: _client.data.clients[0].identifier,
                      phones: Phone.$search({client_id:$stateParams.clientId}),
                      addresses: Address.$search({client_id:$stateParams.clientId})
      
                    });

                  });
          }]
        }
      })
      .state('providers', {
        parent: 'base',
        url: '/providers',
        templateUrl: '/views/provider/providers.html',
        controller: 'ListProviderCtrl'
      })
      .state('providers.create', {
        parent: 'base',
        url: '/providers',
        templateUrl: '/views/provider/provider.html',
        controller: 'ProviderFormCtrl',
        data: { creating: true },
        resolve:{
          rslv_companies: ['Company',function(Company) { return Company.$search(); }],
          rslv_type_services: ['TypeService',function(TypeService) { return TypeService.$search(); }]
        }
      }).
        state('providers.edit', {
        parent: 'base',
        url: '/providers/:providerId',
        templateUrl: '/views/provider/provider.html',
        controller: 'ProviderFormCtrl',
        data: { creating: false },
        resolve:{
          rslv_companies: ['Company',function(Company) { return Company.$search(); }],
          rslv_type_services: ['TypeService',function(TypeService) { return TypeService.$search(); }]
        }
      })
      .state('sheet', {
        parent: 'base',
        url: '/fichas',
        templateUrl: '/views/sheet/list.html',
        controller: 'ListSheetCtrl',
        data: { sheetStatus: 'QUOTE' }
      })
      .state('sheetNext', {
        parent: 'base',
        url: '/fichas/proximas',
        templateUrl: '/views/sheet/list.html',
        controller: 'ListSheetCtrl',
        data: { sheetStatus: 'NEXT' }
      })
      .state('sheetActive', {
        parent: 'base',
        url: '/fichas/activas',
        templateUrl: '/views/sheet/list.html',
        controller: 'ListSheetCtrl',
        data: { sheetStatus: 'ACTIVE' }
      })
      .state('sheetRecent', {
        parent: 'base',
        url: '/fichas/recientes',
        templateUrl: '/views/sheet/list.html',
        controller: 'ListSheetCtrl',
        data: { sheetStatus: 'RECENT' }
      })
      .state('sheetClaim', {
        parent: 'base',
        url: '/fichas/reclamos',
        templateUrl: '/views/sheet/list.html',
        controller: 'ListSheetCtrl',
        data: { sheetStatus: 'CLAIM' }
      })
      .state('sheet.create', {
        parent: 'base',
        url: '/fichas/crear',
        templateUrl: '/views/sheet/sheet.html',
        controller: 'SheetCtrl',
        data: { creating: true },
        resolve:{
          rslv_sheet:['Sheet',function(Sheet) { return Sheet.$build({
            status: 'QUOTE',
            title: 'DRAFT-'+(Math.random().toString(16)+"000000000").substr(2,8),
            reason: '',
            public: true
          });
        }]
      }
    })
      .state('sheet.edit', {
        parent: 'base',
        url: '/:sheetId',
        templateUrl: '/views/sheet/sheet.html',
        controller: 'SheetCtrl',
        data: { creating: false },
        resolve:{
          rslv_sheet:['Sheet','$stateParams',function(Sheet,$stateParams) { 
            return Sheet.$find($stateParams.sheetId);
          }]
        
      }
    }).state('profile', {
        parent: 'base',
        url: '/perfil/editar',
        templateUrl: '/views/profile/edit.html',
        controller: 'AgentProfileCtrl'
      }).state('marketing', {
        parent: 'base',
        url: '/marketing/dashboard',
        templateUrl: '/views/marketing/dashboard.html',
        controller: 'MarketingCtrl'
      });
  }
]);

projectApp.filter('getByName', function () {
  return function (input, id) {
    var i = 0, len = input.length;
    for (; i < len; i++) {
      if (input[i].name === id) {
        return input[i];
      }
    }
    return null;
  };
});

projectApp.filter('capitalize', function () {
  return function (input) {
    return !!input ? input.replace(/([^\W_]+[^\s-]*) */g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }) : '';
  };
});

projectApp.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind('keydown keypress', function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEnter);
        });
        event.preventDefault();
      }
    });
  };
});

projectApp.directive('ngBlurSelect', function () {
  return function (scope, element, attrs) {
      var son = $('#'+element.attr('id')+' input[type=text]'); 
      son.bind('blur', function (event) {
          scope.$apply(function () {
              scope.$eval(attrs.ngBlurSelect);
          });
          event.preventDefault();
      });
    };
});


projectApp.filter('slugify', function(){
    return function(input){
        var tittles = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç";
        var original = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc";
        for (var i = 0; i < tittles.length; i++) {
            input = input.replace(tittles.charAt(i), original.charAt(i)).toLowerCase();
        };
        return input;
   };
});
projectApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});
projectApp.filter('nospace', function () {
    return function (value) {
        return (!value) ? '' : value.replace(/ /g, '');
    };
});
