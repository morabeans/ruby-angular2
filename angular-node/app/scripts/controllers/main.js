'use strict';
var projectApp = angular.module('projectApp');
  projectApp.controller('MainCtrl', ['$scope','gettextCatalog','$http',
  function ($scope, gettextCatalog,$http) {
    
    $scope.languages = {
      current: gettextCatalog.currentLanguage,
      available: {
      	'es': 'Spanish',
      	'en': 'English'
      }
    };

    $scope.changeLang = function(lang){
      $scope.languages.current = lang;
      moment.locale(lang);

    };
    
    $scope.$watch('languages.current', function (lang) {
      if (!lang) {
        return;
      }
      $http.defaults.headers.common['Accept-Language'] = lang;
      gettextCatalog.setCurrentLanguage(lang);
    });
    
     
  }

]);

var projectApp = angular.module('projectApp');
  projectApp.controller('DashboardCtrl', ['$scope','$auth','Agency','Event','uiGmapGoogleMapApi','Sheet','Client','gettextCatalog' ,
  '$filter',
  function ($scope, $auth, Agency,Event,uiGmapGoogleMapApi,Sheet,Client,gettextCatalog,$filter ) {
    $auth.validateUser({ config: 'Agent' }).then(function(){
      $scope.events = Event.$search({});
      $scope.agency = Agency.$find($auth.user.agency_id);
      Client.$build().countClientByAgency().then(function(_data){
        $scope.countClient=_data['data']['count'][0]['count_client'];
      });

      },function() {

    });

   
    $scope.markers=[];
    $scope.sheet = Sheet.$build();
    $scope.sheets = [];
    $scope.sheetsCoord={};
    $scope.maxCoord={};
    $scope.getWindowMarker = function(value){
      var html="<div class='window-marker'><span class='title'>"+value.valueHeader.air_destination_location+"</span><span class='name'>("+value.valueHeader.air_destination_name+")</span></div>";

        html+="<div class='portlet-body sheet-table sheet-table-client nopadding container-sheet-marker-google-map'<div class='table-responsive'><table class='table-project table table-bordered table-condensed flip-content '><thead><tr><th>"+gettextCatalog.getString('ID')+"</th><th>"+gettextCatalog.getString('Client')+"</th><th>"+gettextCatalog.getString('Date')+"</th></tr></thead><tbody>"+value.html+"</tbody></table></div></div>";
      return html;
    };
    $scope.getRowHtml = function(value){
      return "<tr><td><a href='/"+value.id+"'><span>"+value.ref+"</span></a></td><td><span>"+(value.clients_name==null?"":$filter('limitTo')(value.clients_name, 20)+(value.clients_name.length>20 ?"...":""))+"</span></td><td><span>"+value.air_date+"</span></td></tr>"
    }
    $auth.validateUser({ config: 'Agent' }).then(function(){
        $scope.sheet.sheetsWithCoord().then(function(_sheets){
          $scope.sheets = _sheets;
          angular.forEach(_sheets.data.sheets, function(value, key) {
            if($scope.sheetsCoord[value.air_destination_id]==undefined){
              $scope.sheetsCoord[value.air_destination_id] ={
                latitude:value.air_destination_latitude,
                longitude:value.air_destination_longitude,
                cant:1,
                sheets:[],
                valueHeader:{
                  air_destination_location:value.air_destination_location,
                  air_destination_name:value.air_destination_name
                },
                html:$scope.getRowHtml(value)
              }
              $scope.sheetsCoord[value.air_destination_id].sheets.push(value);
              $scope.maxCoord = {
                  latitude:value.air_destination_latitude,
                  longitude:value.air_destination_longitude,
                  cant:1
              }
            }else{
              $scope.sheetsCoord[value.air_destination_id].cant++;
              $scope.sheetsCoord[value.air_destination_id].html+=$scope.getRowHtml(value);
            
              $scope.sheetsCoord[value.air_destination_id].sheets.push(value);
              if($scope.maxCoord.cant < $scope.sheetsCoord[value.air_destination_id].cant )
              $scope.maxCoord={
                  latitude:value.air_destination_latitude,
                  longitude:value.air_destination_longitude,
                  cant:$scope.sheetsCoord[value.air_destination_id].cant
              }
            }
          });
          angular.forEach($scope.sheetsCoord, function(valueb, keyb) {
            var marke = {
                latitude: valueb.latitude,
                longitude: valueb.longitude,
                id:keyb,
                icon:"images/7d7998c6.blue_marker.png",
                title: $scope.getWindowMarker(valueb),
                show: false,
                options: {
                    labelContent:valueb.cant,
                    labelAnchor:"10 0",
                    labelClass:"marker-labels",
                    labelVisible:true
                }
              };
            marke.onClick = function() {
                    marke.show = !marke.show;
                };
            this.push(marke);

          },$scope.markers);

          $scope.map = {
          center: {
              latitude: $scope.maxCoord.latitude,
              longitude: $scope.maxCoord.longitude
            },
            zoom: 2,
            bounds: {}
          };
          //console.log($scope.sheetsCoord);
          //console.log($scope.markers);
        });
    });

    $scope.options = {
      scrollwheel: false
    };
  }
]);
