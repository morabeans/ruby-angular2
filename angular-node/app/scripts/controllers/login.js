'use strict';

var projectApp = angular.module('projectApp');

projectApp.controller('LoginCtrl', [
  '$auth',
  '$scope',
  '$state',
  '$rootScope',
  function($auth, $scope, $state, $rootScope) {
    $scope.loginError = false;
      
    $rootScope.$on('auth:login-success', function(ev, user){
      $rootScope.current_user = user;
      $state.go('dashboard');
      
    });
    $rootScope.$on('auth:login-error', function(){
      $scope.loginError = true;
    });
    
    $scope.handleLoginBtnClick = function() {
      $auth.submitLogin($scope.loginForm, { config: 'Agent' });
    };
  }
]);

projectApp.controller('RecoverPasswordCtrl', [
  '$scope', '$auth', '$window','$routeParams', '$rootScope','$state',
    function($scope, $auth, $window, $routeParams, $rootScope, $state) {
        $auth.validateUser({ config: 'Agent' }).then(function(){
            $state.go('change_password');
        });
        
        $scope.submitRecover = function(){
            $scope.success = false;
            $scope.error = false;
            
            $auth.requestPasswordReset($scope.recoverForm, { config: 'Agent' })
                .then(function(resp) { 
	            $scope.showMessage = true;
	            $scope.success = true;
	            $scope.resultMsg = resp.data.message;
                }).catch(function(resp) { 
	            $scope.showMessage = true;
	            $scope.error = true;
	            $scope.resultMsg = resp.data.errors[0];
                });
        };
    }
]);

projectApp.controller('ChangePasswordCtrl', [
    '$scope',
    '$auth',
    '$window',
    '$routeParams',
    '$rootScope',
    '$state',
    'MessageUtils',
    'gettextCatalog',
    function($scope, $auth, $window, $routeParams, $rootScope, $state, Message, gettextCatalog) {
        $scope.savePassword = function(){
	    $auth.updatePassword($scope.recoverForm)
	        .then(function(resp) {
                    if ($auth.user.configName == 'Agent') {
	                $state.go('dashboard').then(function(){
		            Message.success(gettextCatalog.getString("Your password was updated correctly"));
		        });
                    } else {
                        $scope.showMessage = true;
	                $scope.success = true;
	                $scope.resultMsg = gettextCatalog.getString("Your password was updated correctly.")+' '+gettextCatalog.getString("Now, You can log in through the mobile application.");
                    }
	        })
	        .catch(function(resp) {
                    $scope.showMessage = true;
	            $scope.error = true;
	            $scope.resultMsg = gettextCatalog.getString('Check that your password is more than 8 chars long and that both match');
                });
	};
    }]);
