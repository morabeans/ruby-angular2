'use strict';
var alanaApp = angular.module('projectApp');
alanaApp.controller('MarketingCtrl', [
  '$scope',
  '$http' ,
  'MessageUtils',
  function ($scope,$http,Message){

  	$scope.conversations = [];
  	$http.get("http://localhost:3000/api/v1/mktg/conversations_dashboard.json ")
  	.success(function(data){
  		console.log(data);
  		$scope.conversations = data.conversations;
  	})
  	.error(function(err){
  		console.log(err);
  	});

}]);