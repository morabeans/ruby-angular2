'use strict';
var projectApp = angular.module('projectApp');
projectApp.controller('MenuCtrl', [
  '$scope',
  '$state',
  '$rootScope',
  function ($scope, $state,$rootScope) {

  	$scope.closeOpenMenu = function(){

  		$rootScope.isClosedMenu=!$rootScope.isClosedMenu;
  	};
  	$scope.getState= function(){
  		return $rootScope.isClosedMenu;
  	}

    $scope.state = $state;
  }
]);