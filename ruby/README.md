# README

## API

__Request:__

```
GET /api/v1/agencies
```

__Response:__

```
{
  "agencies": [
    {
      "id": 1,
      "code": "PO",
      "name": "Protocolo"
    },
    {
      "id": 2,
      "code": "GT",
      "name": "Global Tours"
    }
  ]
}
```

__Request:__

```
GET /api/v1/agencies/1
```

__Response:__

```
{
  "agency": {
    "id": 1,
    "code": "PO",
    "name": "Protocolo"
  }
}
```

__Request:__

```
GET /api/v1/providers
```

__Response:__

```
{
  "providers": [
    {
      "id": 1,
      "name": "JW Marriott Caracas"
    },
    {
      "id": 2,
      "name": "American Airlines"
    },
  ]
}
```

__Request:__

```
GET /api/v1/providers/1
```

__Response:__

```
{
  "provider": {
    "id": 1,
    "name": "JW Marriott Caracas"
  }
}
```


__Request:__

```
GET /api/v1/clients
```

__Response:__

```
{
  "clients": [
    {
      "id": 1,
      "identifier": "1234567890",
      "passport": "123456",
      "first_name": "John",
      "middle_name": "Richard",
      "last_name": "Doe",
      "second_last_name": "Roo",
      "sex": "M",
      "birth_date": "1970-01-15"
    },
    {
      "id": 2,
      "identifier": "1234567891",
      "passport": "1234567",
      "first_name": "Jane",
      "middle_name": "Mary",
      "last_name": "Doe",
      "second_last_name": "Watson",
      "sex": "F",
      "birth_date": "1980-12-24"
    }
  ]
}
```

__Request:__

```
GET /api/v1/clients/1
```

__Response:__

```
{
  "client": {
    "id": 1,
    "identifier": "1234567890",
    "passport": "123456",
    "first_name": "John",
    "middle_name": "Richard",
    "last_name": "Doe",
    "second_last_name": "Roo",
    "sex": "M",
    "birth_date": "1970-01-15"
  }
}
```

__Request:__

```
GET /api/v1/sheets
```

__Response:__

```
{
  "sheets": [
    {
      "id": 1,
      "ref": "PO-1",
      "title": "Doe's family vacations",
      "status": "NEXT",
      "public": true,
      "begins_at": "2014-10-21",
      "ends_at": "2014-10-22",
      "clients_count": 2,
      "services_count": 1
    },
    {
      "id": 2,
      "ref": "PO-2",
      "title": "Trip for David",
      "status": "QUOTE",
      "public": true,
      "begins_at": null,
      "ends_at": null,
      "clients_count": 0,
      "services_count": 0
    }
  ],
  "meta": {
    "pages": 1,
    "previous": null,
    "next": null,
    "per_page": 10,
    "total": 7
  }
}
```

__Request:__

```
GET /api/v1/sheets/1
```

__Response:__

```
{
  "sheets": {
    "id": 1,
    "ref": "PO-1",
    "title": "Doe's family vacations",
    "status": "NEXT",
    "public": true,
    "begins_at": "2014-10-21",
    "ends_at": "2014-10-22",
    "clients_count": 2,
    "services_count": 1,
    "clients": [
      {
        "id": 1,
        "identifier": "1234567890",
        "passport": "123456",
        "first_name": "John",
        "middle_name": "Richard",
        "last_name": "Doe",
        "second_last_name": "Roo",
        "sex": "M",
        "birth_date": "1970-01-15"
      },
      {
        "id": 2,
        "identifier": "1234567891",
        "passport": "1234567",
        "first_name": "Jane",
        "middle_name": "Mary",
        "last_name": "Doe",
        "second_last_name": "Watson",
        "sex": "F",
        "birth_date": "1980-12-24"
      }
    ],
    "services": [
      {
        "id": 1,
        "locator": null,
        "kind": "ACCOMMODATION",
        "begins_at": "2014-10-21",
        "ends_at": "2014-10-22",
        "amount": "10000.0",
        "currency": "VEF",
        "status": "CONFIRMED",
        "provider": {
          "id": 1,
          "name": "JW Marriott Caracas"
        }
      }
    ]
  }
}
```


__Request:__

```
GET /api/v1/sheets/1/services
```

__Response:__

```
{
  "services": [
    {
      "id": 1,
      "locator": null,
      "kind": "ACCOMMODATION",
      "begins_at": "2014-10-21",
      "ends_at": "2014-10-22",
      "amount": "10000.0",
      "currency": "VEF",
      "status": "CONFIRMED",
      "provider": {
        "id": 1,
        "name": "JW Marriott Caracas"
      }
    }
  ]
}
```

__Request:__

```
GET /api/v1/sheets/1/services/1
```

__Response:__

```
{
  "service": {
    "id": 1,
    "locator": null,
    "kind": "ACCOMMODATION",
    "begins_at": "2014-10-21",
    "ends_at": "2014-10-22",
    "amount": "10000.0",
    "currency": "VEF",
    "status": "CONFIRMED",
    "provider": {
      "id": 1,
      "name": "JW Marriott Caracas"
    }
  }
}
```