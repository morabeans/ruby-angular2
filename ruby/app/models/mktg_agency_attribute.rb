class MktgAgencyAttribute < ActiveRecord::Base
  belongs_to :agency
  belongs_to :mktg_cfg_agency_attribute

  validates :agency_id, presence: true
  validates :mktg_cfg_agency_attribute_id, presence: true
  validates :value, presence: true, length: { minimum: 5 }

	def self.by_agency(agency_id)

  	    MktgAgencyAttribute.where("agency_id = " + agency_id)					

  	end

end
