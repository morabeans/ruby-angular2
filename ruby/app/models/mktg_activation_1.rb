class MktgActivation < ActiveRecord::Base
  belongs_to :client
  belongs_to :mktg_conversation
  belongs_to :agent

  def self.create_activation(params)

  mktg_conversation_id = params[:mktg_conversation_id]
	client_id = params[:client_id]
	agent_id =  params[:agent_id]
	status = params[:status]
	fields = params[:fields]
	comments = params[:comments]
  parameters_fields = params[:parameters_fields]    # 201510 - amx 

 puts "-----------------------------------"
 puts fields
	mktg_conversation = MktgConversation.find(mktg_conversation_id)
	
	# Begin Dana communication

   
	client = Savon.client do
  		wsdl Project::Application.config.danawsdl
  		basic_auth [Project::Application.config.danauser, Project::Application.config.danapassword]
	end

	array_entry = Array.new
	
	fields.each do |field|
		#key = field.keys.first
		key = field["name"]
		mktg_conversation.table_fields.each do |table_code|
			
			if  "#{table_code["name"]}".eql?("#{key}")
      			
      			hash = {:key => "#{table_code["code"]}", :value =>  field["value"]}
      			array_entry.push hash

      			break;
   			end 
   			
		end	
	end
hash_entry = {:entry=> array_entry}


array_values = Array.new
array_values.push hash_entry
hash_values = {:values => array_values}
puts "Hash values"
puts hash_values

hash_message = { :idConversation => mktg_conversation.dana_id , :tableCode => mktg_conversation.table_code , :values => hash_values  }


#response = client.call(:start_conversation_project , idProject: 58836)
response = client.call(:start_conversation_with_data , message: hash_message)
result = response.xpath("//return").first.inner_text
	
# End Dana communication

   MktgActivation.create(client_id:client_id, 
   						 mktg_conversation_id:mktg_conversation_id, 
   						 datesend: Date.current, 
   						 agent_id: agent_id, 
   						 status:status,
   						 requestws:hash_message.to_s, 
   						 responsews: result,
   						 comments:comments)

  end


 def self.by_client(client_id)
   MktgActivation.where ("client_id = "+ client_id)
 end

 def self.by_agent(agent_id)
   MktgActivation.where ("agent_id = "+ agent_id)
 end

 def self.by_conversation(conversation_id)
   MktgActivation.where ("mktg_conversation_id = "+ conversation_id)
 end


  
end
