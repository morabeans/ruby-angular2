class MktgCatalog < ActiveRecord::Base
  belongs_to :agency

  validates :agency_id, presence: true
  validates :name, presence: true, length: { minimum: 2 }


  def self.by_agency(agency_id)

  	    MktgCatalog.where("agency_id = " + agency_id)					

  	end

  

end
