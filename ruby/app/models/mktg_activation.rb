class MktgActivation < ActiveRecord::Base
  belongs_to :client
  belongs_to :mktg_conversation
  belongs_to :agent
  
 
  def self.create_document(params)
    
    array_field = Array.new
    mktg_conversation_id = params[:mktg_conversation_id]
    msg_parameters = params[:msg_parameters]
    mktg_conversation = MktgConversation.find(mktg_conversation_id)
    msg_field = JSON.parse(mktg_conversation.msg_field.to_json)
    field = JSON.parse(params[:fields].to_json)
    
    
 #
 #   msg_parameters.each do |parameter|
 #     
 #     key = parameter.keys.first
 #     msg_field["title"].gsub!("#{key}", parameter[key])
 #     msg_field["body"].gsub!("#{key}", parameter[key])
 #     
 #   end


   msg_parameters.each do |parameter|
     
     key = parameter["name"]
     msg_field["title"].gsub!("#{key}", parameter["value"])
     msg_field["body"].gsub!("#{key}", parameter["value"])
     
   end
    
    
    hash = {:body => msg_field["body"]}
    array_field.push hash
    hash = {:title => msg_field["title"]}
    array_field.push hash
    
        
    field.each do |f|
      #key = f.keys.first
      key = f["name"]
      hash = {:"#{key}" => f["value"]}
      array_field.push hash
    end
    
    params[:fields] = array_field
      
    self.create_activation(params)
    
  end
  
  def self.create_activation(params)

  mktg_conversation_id = params[:mktg_conversation_id]
	client_id = params[:client_id]
	agent_id =  params[:agent_id]
	status = params[:status]
	fields = params[:fields]
	comments = params[:comments]
  

 mktg_conversation = MktgConversation.find(mktg_conversation_id)
     
puts params
# Begin Dana communication

   
	client = Savon.client do
  		wsdl Project::Application.config.danawsdl
  		basic_auth [Project::Application.config.danauser, Project::Application.config.danapassword]
	end

	array_entry = Array.new
	
	fields.each do |field|
		key = field.keys.first
		
		mktg_conversation.table_fields.each do |table_code|
		
			if  "#{table_code["name"]}".eql?("#{key}")
      			
      			hash = {:key => "#{table_code["code"]}", :value =>  field[key]}
      			array_entry.push hash

      			break;
   			end 
   			
		end	
	end
hash_entry = {:entry=> array_entry}


array_values = Array.new
array_values.push hash_entry
hash_values = {:values => array_values}


hash_message = { :idConversation => mktg_conversation.dana_id , :tableCode => mktg_conversation.table_code , :values => hash_values  }


#response = client.call(:start_conversation_project , idProject: 58836)
response = client.call(:start_conversation_with_data , message: hash_message)
result = response.xpath("//return").first.inner_text
	
# End Dana communication

   MktgActivation.create(client_id:client_id, 
   						 mktg_conversation_id:mktg_conversation_id, 
   						 datesend: Date.current, 
   						 agent_id: agent_id, 
   						 status:status,
   						 requestws:hash_message.to_s, 
   						 responsews: result,
   						 comments:comments)

  end


 def self.by_client(client_id)
   MktgActivation.where ("client_id = "+ client_id)
 end

 def self.by_agent(agent_id)
   MktgActivation.where ("agent_id = "+ agent_id)
 end

 def self.by_conversation(conversation_id)
   MktgActivation.where ("mktg_conversation_id = "+ conversation_id)
 end


  
end
