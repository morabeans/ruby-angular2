class MktgCfgAgencyAttribute < ActiveRecord::Base
	has_many :mktg_agency_attributes

	validates  :attribute_name, presence: true,uniqueness: true
	


end
