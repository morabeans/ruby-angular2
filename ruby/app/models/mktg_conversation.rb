class MktgConversation < ActiveRecord::Base
  belongs_to :mktg_catalog
  has_many :mktg_activations

  validates  :conversation_alias, presence: true,uniqueness: true
  validates  :conversation_type,  presence: true , inclusion: %w(TEMPLATE INSTANCE DOCUMENT)


  scope :select_show ,-> { select("id, conversation_alias , url_preview, languages_supported , description")}
 
 

	def self.show_all
  		MktgConversation.select(:id, :mktg_catalog_id , :dana_id, :conversation_alias, :conversation_type, :url_preview, :languages_supported, :description )
  	end

  	def self.show_public(id)

  	     	    MktgConversation.find_by_sql [ "SELECT       		"\
									"	mk01.id , 					"\
									"	mk01.conversation_alias,	"\
									"	mk01.url_preview , 			"\
									"	mk01.languages_supported,	"\
									"	mk01.description , 			"\
									"	( SELECT array_to_json(array_agg (h)) FROM (  "\
									"										  SELECT hstore_to_json(hstore(unnest(mk00.table_fields)::hstore->'name' ,'value')) h "\
									"										  FROM mktg_conversations as mk00						"\
									"										  WHERE id ="+id+" ) as valores) as table_fields_name			"\
									"FROM mktg_conversations as mk01 "\
									"WHERE mk01.id = " + id ]
												

  	end


    def self.show_conversations_dashboard(params)

           where_alias = " true "

           where_catalogo = " true "

           where_type = " true "

            if not params[:conversation_alias].nil?
              where_alias = " UPPER(mk01.conversation_alias) LIKE '%"+params[:conversation_alias].upcase+"%'"  
            end  

             if not params[:mktg_catalog_id].nil?
              where_catalogo = " UPPER(mk01.mktg_catalog_id)  = "+params[:mktg_catalog_id]  
            end  

            if not params[:conversation_type].nil?
              where_type = " UPPER(mk01.conversation_type)  = '"+params[:conversation_type]+"'"  
            end  

            where = where_alias + " AND " + where_catalogo + " AND " + where_type

              MktgConversation.find_by_sql [ "SELECT          "\
                  " mk01.id ,           "\
                  " mk01.conversation_alias,  "\
                  " mk01.url_preview ,      "\
                  " mk01.languages_supported, "\
                  " mk01.description ,       "\
                  " mk01.conversation_type ,    "\
                  " mk02.name "\
                  "FROM mktg_conversations as mk01 , mktg_catalogs as mk02  "\
                  "WHERE mk01.mktg_catalog_id = mk02.id AND " + where ]
                        

    end

    def self.show_dashboard(params)

           where_alias = " true "

           where_catalogo = " true "

           where_type = " true "

            if not params[:conversation_alias].nil?
              where_alias = " UPPER(mk00.conversation_alias) LIKE '%"+params[:conversation_alias].upcase+"%'"  
            end  

             if not params[:mktg_catalog_id].nil?
              where_catalogo = " UPPER(mk00.mktg_catalog_id)  = "+params[:mktg_catalog_id]  
            end  

            if not params[:conversation_type].nil?
              where_type = " UPPER(mk00.conversation_type)  = '"+params[:conversation_type]+"'"  
            end  

            where = where_alias + " AND " + where_catalogo + " AND " + where_type

              MktgConversation.find_by_sql [ "SELECT          "\
                  " mk00.id ,           "\
                  " mk00.conversation_alias , "\
                  " mk00.conversation_type ,    "\
                  " mk02.name  ,"\
                  " (select count(mkt01.id) from mktg_activations as mkt01 where mkt01.mktg_conversation_id = mk00.id ) sent , "\
                  " (select count(mkt01.id) from mktg_activations as mkt01 where mkt01.mktg_conversation_id = mk00.id and mkt01.responsews = 'OK') sucessful , "\
                  " mk00.languages_supported, "\
                  " mk00.description        "\
                  "FROM mktg_conversations as mk00 , mktg_catalogs as mk02  "\
                  "WHERE mk00.mktg_catalog_id = mk02.id AND " + where ]

    end


  	def self.show_detail(id)

  	    MktgConversation.find(id)					

  	end

  	def self.to_activate(id)

  	     	    MktgConversation.find_by_sql [ "SELECT  					"\
  	     	    					"	'ACTIVATION' as id , 				"\
									"	mk01.id as mktg_conversation_id , 	"\
									"	mk01.conversation_alias , 			"\
									"	1 as client_id	,					"\
									"	1 as agent_id 	,					"\
									"	mk01.languages_supported,			"\
									"	( SELECT array_to_json(array_agg (h)) FROM (  "\
									"										  SELECT hstore_to_json(hstore(unnest(mk00.table_fields)::hstore->'name' ,'value')) h "\
									"										  FROM mktg_conversations as mk00						"\
									"										  WHERE id ="+id+" ) as valores) as table_fields_name			"\
									"FROM mktg_conversations as mk01		"\
									"WHERE mk01.id = " + id ]
												

  	end

  	def self.to_activate_alias(conversation_alias)


			MktgConversation.where ("UPPER(conversation_alias) = '"+conversation_alias.upcase+"'")
			
  	end

  	def self.show_all_detail(id)

  	    MktgConversation.find(id)

  	end


	def self.by_catalog(catalog_id)

  	    MktgConversation.select_show.where("mktg_catalog_id = "+catalog_id)
  
  	end

  	def self.by_alias(conversation_alias)

  	    MktgConversation.select_show.where("UPPER(conversation_alias) LIKE '%"+conversation_alias.upcase+"%'")
  
  	end
    
    def self.create_template(params) 
      
      params[:conversation_type] = "TEMPLATE"
      params[:msg_field] = nil
      params[:msg_parameters] = []
      
      
      
      return MktgConversation.new(params)
      
    end
    
     def self.create_document(params) 
      
      params[:conversation_type] = "DOCUMENT"
      
      return MktgConversation.new(params)
      
    end

end
