var miAplicacion = angular.module('miAplicacion', []);
miAplicacion.controller('Controlador', function($scope,$http) {

        $scope.lista = [];
        $scope.lista_activations = [];
        $scope.lista_var = [];
         $scope.lista_activations_msg = [];


        $scope.agregar = function() {
            $scope.lista.push({code: $scope.code, name: $scope.name});
            $scope.code = '';
            $scope.name = '';
           
        };

        $scope.agregarCampo = function() {
            $scope.lista_activations.push({name: $scope.name, value: $scope.value});
            $scope.name = '';
            $scope.value = '';
           
        };

        $scope.agregarvar = function() {
            $scope.lista_var.push({var: $scope.var});
            $scope.var = '';
                       
        };
    
        $scope.agregarMsgCampo = function() {
            $scope.lista_activations_msg.push({value: $scope.msg_value, name: $scope.msg_name});
            $scope.msg_value = '';
            $scope.msg_name = '';
           
        };




        $scope.eliminar = function() {
            var lista = $scope.lista;
            $scope.lista = [];
            angular.forEach(lista, function(item) {
                if (!item.seleccionado)
                    $scope.lista.push(item);
            });
        };


        $scope.guardar = function() {
        
            data = {
               mktg_catalog_id: $scope.mktg_catalog_id ,
               dana_id : $scope.dana_id ,
               table_code : $scope.table_code ,
               table_fields : $scope.lista ,
               conversation_alias : $scope.conversation_alias ,
               conversation_type : $scope.conversation_type ,
               url_preview : $scope.url_preview ,
               languages_supported : $scope.languages_supported ,
               description : $scope.description
            };

            $http.post("http://localhost:3000/api/v1/mktg/mktg_conversations.json",data)

            .success(function(data,status,header,config)
             {
               console.log(data);
             })
            .error(function(error,status,header,config)
             {
                  console.log(error);
             });
        };


        $scope.activar = function() {
        
        data = {
               client_id: $scope.client_id ,
               mktg_conversation_id : $scope.mktg_conversation_id ,
               agent_id : $scope.agent_id ,
               status : $scope.status ,
               comments : $scope.comments ,
               fields : $scope.lista_activations 
            };

            $http.post("http://localhost:3000/api/v1/mktg/create_activations.json",data)

            .success(function(data,status,header,config)
             {
               console.log($scope.lista_activations )
               console.log(data.fields);
               console.log(data);
             })
            .error(function(error,status,header,config)
             {
                  console.log(error);
             });
        };

        $scope.guardarDocument = function() {
        
            data = {
               mktg_catalog_id: $scope.mktg_catalog_id ,
               dana_id : $scope.dana_id ,
               table_code : $scope.table_code ,
               table_fields : $scope.lista ,
               conversation_alias : $scope.conversation_alias ,
               conversation_type : $scope.conversation_type ,
               url_preview : $scope.url_preview ,
               languages_supported : $scope.languages_supported ,
               description : $scope.description ,
               msg_field : { title: $scope.msg_title , body: $scope.msg_body } ,
               msg_parameters : $scope.lista_var
            };

            $http.post("http://localhost:3000/api/v1/mktg/conversations_document.json",data)

            .success(function(data,status,header,config)
             {
               console.log(data);
             })
            .error(function(error,status,header,config)
             {
                  console.log(error);
             });
        };

      $scope.activarDocument = function() {
        
        data = {
               client_id: $scope.client_id ,
               mktg_conversation_id : $scope.mktg_conversation_id ,
               agent_id : $scope.agent_id ,
               status : $scope.status ,
               comments : $scope.comments ,
               fields : $scope.lista_activations , 
               msg_parameters : $scope.lista_activations_msg
            };
            console.log(data);
            $http.post("http://localhost:3000/api/v1/mktg/create_document.json",data)

            .success(function(data,status,header,config)
             {
               console.log($scope.lista_activations )
               console.log(data.fields);
               console.log(data);
             })
            .error(function(error,status,header,config)
             {
                  

                   console.log(data);
             });
        };

    });



