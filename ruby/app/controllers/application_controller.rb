class ApplicationController < ActionController::Base
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_current_agency
  before_action :set_current_locale

  respond_to :json

  def default_url_options
    { host: ENV["DOMAIN"] }
  end

  before_filter :cors_preflight_check
  after_filter :cors_set_access_control_headers

  # For all responses in this controller, return the CORS access control headers.

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE, HEAD'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    if request.method == :options
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept'
      headers['Access-Control-Max-Age'] = '1728000'
      render :text => '', :content_type => 'text/plain'
    end
  end

  
  def current_user
    if current_agent
      current_user = current_agent
    else
      current_user = current_client
    end
  end
  
  private
    def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) << :agency_id
      devise_parameter_sanitizer.for(:sign_up) << :name
      devise_parameter_sanitizer.for(:sign_up) << :is_owner
    end

    def set_current_agency
      @current_agency = current_agent.agency if current_agent
    end

    def errors_for(model)
      { errors: model.errors }
    end

    def set_current_locale
      I18n.locale = http_accept_language.compatible_language_from(I18n.available_locales)
    end
end
