class Api::V1::Mktg::MktgCatalogsController < ApplicationController
	before_action :set_mktg_catalog, only: [:show, :edit, :update]
	#before_action :authenticate_client!
	respond_to :json

  # GET /mktg_catalogs.json
  def index
    @mktg_catalogs = MktgCatalog.all
    json_result = { catalogs: @mktg_catalogs}
    render json: json_result , status: :ok
  end

   # GET /mktg_catalogs/page/1.json
  def index
    @mktg_catalogs = MktgCatalog.all


    json_result = { catalogs: @mktg_catalogs.paginate(params[:page])}
    render json: json_result , status: :ok
  end


  
  # GET /mktg_catalogs/1.json
  def show
  	    
    	if MktgCatalog.exists?(params[:id])
          @mktg_catalog = MktgCatalog.find(params[:id])
          json_result = {catalog: @mktg_catalog}
          code_result = :ok
          
        else
          json_result = {catalog: []}
        	code_result = :not_found
      	end
        
        render json: json_result , status: code_result  	
  end

  # GET /mktg_catalogs/new
  def new
    @mktg_catalog = MktgCatalog.new
  end

  # GET /mktg_catalogs/1/edit
  def edit
  end

  # POST /mktg_catalogs.json
  def create
        
     @mktg_catalog = MktgCatalog.new(mktg_catalog_params)

      if @mktg_catalog.save
        json_result = {catalog: @mktg_catalog}
        code_result = :created
      else
        json_result = {error: @mktg_catalog.errors }
        code_result = :unprocessable_entity 
      end
   
      render json: json_result , status: code_result
 
    
  end

  # PATCH/PUT /mktg_catalogs/1.json
  def update
        
    if MktgCatalog.exists?(params[:id]) 
         
          if @mktg_catalog.update(mktg_cfg_agency_attribute_params)
           json_result =  {catalog: @mktg_catalog}
           code_result = :ok 
          else
           json_result =  {error: @mktg_catalog.errors}
           code_result = :unprocessable_entity 
          end
      else
          json_result = {catalog: []}
        	code_result = :not_found
      end
      
    render json: json_result , status: code_result
    
  end

 

  def agency
  	
  	@mktg_catalog = MktgCatalog.by_agency(params[:agency_id])
  	render json:@mktg_catalog

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mktg_catalog
      @mktg_catalog = MktgCatalog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mktg_catalog_params
      params.require(:mktg_catalog).permit(:agency_id, :name,:page)
    end
end
