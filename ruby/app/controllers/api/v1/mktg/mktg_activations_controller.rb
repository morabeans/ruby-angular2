class Api::V1::Mktg::MktgActivationsController < ApplicationController
	before_action :set_mktg_activation, only: [:show, :edit, :update, :destroy]
	#before_action :authenticate_client!
	respond_to :json

  # GET /mktg_activations.json
  def index
    @mktg_activations = MktgActivation.all
    json_result = { activations: @mktg_activations}
    render json: json_result , status: :ok
    
  end

  
  # GET /mktg_activations/1.json
  def show
  	  	    	
        if MktgActivation.exists?(params[:id])
          @mktg_activation = MktgActivation.find(params[:id])
          json_result = {activations: @mktg_activation}
          code_result = :ok          
        else
          json_result = {activations: []}
        	code_result = :not_found
      	end
        
        render json: json_result , status: code_result
  end

  # GET /mktg_activations/new
  def new
    @mktg_activation = MktgActivation.new
  end

  # GET /mktg_activations/1/edit
  def edit
  end

  
 # POST /mktg_activations.json
  def create
    
    @mktg_activation = MktgActivation.new(mktg_activation_params)

      if @mktg_activation.save
        json_result = {activations: @mktg_activation}
        code_result = :created
      else
        json_result = {error: @mktg_activation.errors }
        code_result = :unprocessable_entity 
      end
   
       render json: json_result , status: code_result
       
  end

  # POST /create_activations.json
  def create_activation

    @mktg_activation = MktgActivation.create_activation(params)
 
      if @mktg_activation.save
        json_result = {activations: @mktg_activation}
        code_result = :created
      else
        json_result = {error: @mktg_activation.errors }
        code_result = :unprocessable_entity 
      end
   
       render json: json_result , status: code_result
       
  end
  
  
  # POST /create_document.json
  def create_document

    @mktg_activation = MktgActivation.create_document(params)
 
      if @mktg_activation.save
        json_result = {activations: @mktg_activation}
        code_result = :created
      else
        json_result = {error: @mktg_activation.errors }
        code_result = :unprocessable_entity 
      end
   
       render json: json_result , status: code_result
       
  end

 
# GET /activations_agent/1.json
  def by_agent
    @mktg_activations = MktgActivation.by_agent(params[:agent_id])
    json_result = {activations: @mktg_activation}
    render json: json_result , status: :ok
  end

  # GET /activations_conversation/1.json
  def by_conversation
    @mktg_activations = MktgActivation.by_conversation(params[:conversation_id])
    json_result = {activations: @mktg_activation}
    render json: json_result , status: :ok
  end


  # GET /activations_client/1.json
  def by_client
    @mktg_activations = MktgActivation.by_client(params[:client_id])
    json_result = {activations: @mktg_activation}
    render json: json_result , status: :ok
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mktg_activation
       if MktgActivation.exists?(params[:id])
        @mktg_activation = MktgActivation.find(params[:id])
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mktg_activation_params
      params.require(:mktg_activation).permit(
                            :client_id ,
                            :mktg_conversation_id,
                            :datesend,
                            :agent_id ,
                            :status ,
                            :requestws , 
                            :responsews,
                            :comments 
                           )
    end
  
end
