class Api::V1::Mktg::MktgCfgAgencyAttributesController < ApplicationController
	before_action :set_mktg_cfg_agency_attribute, only: [:show, :edit, :update]
	#before_action :authenticate_client!
	before_action :upperletter ,  only: [:create, :edit, :update]
  respond_to :json

  # GET /mktg_cfg_agency_attributes.json
  def index
    @mktg_cfg_agency_attributes = MktgCfgAgencyAttribute.all
    json_result = { cfg_attributes: @mktg_cfg_agency_attributes}
    render json: json_result , status: :ok
    
    
  end

  
  # GET /mktg_cfg_agency_attributes/1.json
  def show
  	 	
    	if MktgCfgAgencyAttribute.exists?(params[:id])
          @mktg_cfg_agency_attribute = MktgCfgAgencyAttribute.find(params[:id])
          json_result = {cfg_attribute: @mktg_cfg_agency_attribute}
          code_result = :ok
          
        else
          json_result = {cfg_attribute: []}
        	code_result = :not_found
      	end
        
        render json: json_result , status: code_result
  	
  end

  # GET /mktg_cfg_agency_attributes/new
  def new
    @mktg_cfg_agency_attribute = MktgCfgAgencyAttribute.new
  end

  # GET /mktg_cfg_agency_attributes/1/edit
  def edit
  end

  # POST /mktg_cfg_agency_attributes.json
  def create
    @mktg_cfg_agency_attribute = MktgCfgAgencyAttribute.new(mktg_cfg_agency_attribute_params)

      if @mktg_cfg_agency_attribute.save
        json_result = {cfg_attribute: @mktg_cfg_agency_attribute}
        code_result = :created
      else
        json_result = {error: @mktg_cfg_agency_attribute.errors }
        code_result = :unprocessable_entity 
      end
   
       render json: json_result , status: code_result
  end

  # PATCH/PUT /mktg_cfg_agency_attributes/1.json
  def update
  

       if MktgCfgAgencyAttribute.exists?(params[:id]) 
         
          if @mktg_cfg_agency_attribute.update(mktg_cfg_agency_attribute_params)
           json_result =  {cfg_attribute: @mktg_cfg_agency_attribute}
           code_result = :ok 
          else
           json_result =  {error: @mktg_cfg_agency_attribute.errors}
           code_result = :unprocessable_entity 
          end
      else
          json_result = {cfg_attribute: []}
        	code_result = :not_found
      end
      
    render json: json_result , status: code_result
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mktg_cfg_agency_attribute

      if MktgCfgAgencyAttribute.exists?(params[:id])
        @mktg_cfg_agency_attribute = MktgCfgAgencyAttribute.find(params[:id])
      end
    end

    def upperletter
        params[:attribute_name].upcase! 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mktg_cfg_agency_attribute_params
      params.require(:mktg_cfg_agency_attribute).permit(:attribute_name, :attribute_type)
    end
end
