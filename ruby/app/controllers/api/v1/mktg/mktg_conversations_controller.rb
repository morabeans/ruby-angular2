class Api::V1::Mktg::MktgConversationsController < ApplicationController
  before_action :set_mktg_conversation, only: [:show, :edit, :update]
	#before_action :authenticate_client!
	respond_to :json

  # GET /mktg_conversations.json
  def index

    
    @mktg_conversations = MktgConversation.show_all
    json_result = { conversations: @mktg_conversations}
    render json: json_result , status: :ok

  end

  
  # GET /conversations_show/1.json
  def show_public
  	  	    	
  	
    if MktgConversation.exists?(params[:id])
          @mktg_conversation = MktgConversation.show_public(params[:id])
          json_result = { conversation: @mktg_conversation }
          code_result = :ok
          
        else
          json_result = {conversation: []}
        	code_result = :not_found
      	end
        
        render json: json_result , status: code_result
        
  end


 # GET /conversations_show_detail/1.json
  def show_detail
      
    if MktgConversation.exists?(params[:id])
          @mktg_conversation = MktgConversation.show_detail(params[:id])
          json_result = { conversation: @mktg_conversation }
          code_result = :ok
          
        else
          json_result = {conversation: []}
        	code_result = :not_found
      	end
        
        render json: json_result , status: code_result
    
  end

   # GET /conversations_dashboard.json
  def show_dashboard
      
    @mktg_conversations = MktgConversation.show_dashboard(params)
    json_result = { conversations: @mktg_conversations}
    render json: json_result , status: :ok
    
    
  end

  # GET /conversations_catalog/1.json
  def by_catalog
              
    @mktg_conversation = MktgConversation.by_catalog(params[:catalog_id])
    json_result = { conversation: @mktg_conversation }
    render json: json_result , status: :ok
    
  end


# GET /conversations_alias/alias.json
  def by_alias
              
    @mktg_conversation = MktgConversation.by_alias(params[:alias])
    json_result = { conversation: @mktg_conversation }
    render json: json_result , status: :ok
    
  end

# GET /parameters_activate/1.json
  def to_activate
              
    @mktg_conversation = MktgConversation.to_activate(params[:id])
    json_result = { conversation: @mktg_conversation }
    render json: json_result , status: :ok
    
  end

  

# GET /parameters_activate_alias/1.json
  def to_activate_alias
    
    @mktg_conversation = MktgConversation.new          
    mktg_conversation = MktgConversation.to_activate_alias(params[:alias]).first
    
    
        if not mktg_conversation.nil?
        @mktg_conversation = MktgConversation.to_activate(mktg_conversation.id.to_s)
          json_result = { conversation: @mktg_conversation }
          code_result = :ok
        else
          json_result = { conversation: @mktg_conversation.errors }
          code_result = :unprocessable_entity
        end
        render json: json_result , status: code_result
  end


  # GET /mktg_conversations/new
  def new
    @mktg_conversation = MktgConversation.new
  end

  # GET /mktg_conversations/1/edit
  def edit
  end

  # POST /mktg_conversations.json
  def create
  	
     
    @mktg_conversation = MktgConversation.new(mktg_conversation_params)

      if @mktg_conversation.save
        json_result = {conversation: @mktg_conversation}
        code_result = :created
      else
        json_result = {error: @mktg_conversation.errors }
        code_result = :unprocessable_entity 
      end
   
      render json: json_result , status: code_result
      
  end
  
  
  # POST /mktg_conversations_template.json
  def create_template
  	
     
    @mktg_conversation = MktgConversation.create_template(mktg_conversation_params)

      if @mktg_conversation.save
        json_result = {conversation: @mktg_conversation}
        code_result = :created
      else
        json_result = {error: @mktg_conversation.errors }
        code_result = :unprocessable_entity 
      end
   
      render json: json_result , status: code_result
      
  end
  
  
  # POST /conversations_document.json
  def create_document
  	
     
    @mktg_conversation = MktgConversation.create_document(mktg_conversation_params)

      if @mktg_conversation.save
        json_result = {conversation: @mktg_conversation}
        code_result = :created
      else
        json_result = {error: @mktg_conversation.errors }
        code_result = :unprocessable_entity 
      end
   
      render json: json_result , status: code_result
      
  end

   # PATCH/PUT /mktg_conversations/1.json
  def update
    
    if MktgConversation.exists?(params[:id]) 
         
          if @mktg_conversation.update(mktg_conversation_params)
           json_result =  {conversation: @mktg_conversation}
           code_result = :ok 
          else
           json_result =  {error: @mktg_conversation.errors}
           code_result = :unprocessable_entity 
          end
      else
          json_result = {conversation: []}
        	code_result = :not_found
      end
      
    render json: json_result , status: code_result
    
  end

 

  def agency
  	
  	@mktg_conversation = MktgConversation.byAgency(params[:agency_id])
  	render json:@mktg_conversation

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    
     def set_mktg_conversation
      if MktgConversation.exists?(params[:id])
        @mktg_conversation = MktgConversation.find(params[:id])
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mktg_conversation_params
      params.require(:mktg_conversation).permit(
      											:mktg_catalog_id, 
      											:dana_id, 
      											:table_code, 
                            :conversation_alias, 
      											:conversation_type,
      											:url_preview,
      											:languages_supported,
      											:description,
                            msg_field: [:title , :body] ,
                            msg_parameters: [:var] , 
                            table_fields: [:code,:name] )
    end
end

