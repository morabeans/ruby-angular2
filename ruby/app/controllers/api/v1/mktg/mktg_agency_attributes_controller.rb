class Api::V1::Mktg::MktgAgencyAttributesController < ApplicationController
	before_action :set_mktg_agency_attribute, only: [:show, :edit, :update]
	#before_action :authenticate_client!
	respond_to :json

  # GET /mktg_agency_attributes.json
  def index
    @mktg_agency_attributes = MktgAgencyAttribute.all
    json_result = { agency_attributes: @mktg_agency_attributes}
    render json: json_result , status: :ok
  end

  
  # GET /mktg_agency_attributes/1.json
  def show
  		
        if MktgAgencyAttribute.exists?(params[:id])
          @mktg_agency_attribute = MktgAgencyAttribute.find(params[:id])
          json_result = {agency_attributes: @mktg_agency_attribute}
          code_result = :ok
          
        else
          json_result = {agency_attributes: []}
        	code_result = :not_found
      	end
        
        render json: json_result , status: code_result

  	
  end

  # GET /mktg_agency_attributes/new
  def new
    @mktg_agency_attribute = MktgAgencyAttribute.new
  end

  # GET /mktg_agency_attributes/1/edit
  def edit
  end

  # POST /mktg_agency_attributes.json
  def create
   
    
    @mktg_agency_attribute = MktgAgencyAttribute.new(mktg_agency_attribute_params)

      if @mktg_agency_attribute.save
        json_result = {cfg_attribute: @mktg_agency_attribute}
        code_result = :created
      else
        json_result = {error: @mktg_agency_attribute.errors }
        code_result = :unprocessable_entity 
      end
   
       render json: json_result , status: code_result
  end

  # PATCH/PUT /mktg_agency_attributes/1.json
  def update
    
    if MktgAgencyAttribute.exists?(params[:id]) 
         
          if @mktg_agency_attribute.update(mktg_agency_attribute_params)
           json_result =  {agency_attribute: @mktg_agency_attribute}
           code_result = :ok 
          else
           json_result =  {error: @mktg_agency_attribute.errors}
           code_result = :unprocessable_entity 
          end
      else
          json_result = {agency_attribute: []}
        	code_result = :not_found
      end
      
    render json: json_result , status: code_result
    
  end

  
  def agency
  	@mktg_agency_attribute = MktgAgencyAttribute.by_agency(params[:agency_id])
  	render json:@mktg_agency_attribute

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mktg_agency_attribute
      if MktgAgencyAttribute.exists?(params[:id])
        @mktg_agency_attribute = MktgAgencyAttribute.find(params[:id])
      end  
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mktg_agency_attribute_params
      params.require(:mktg_agency_attribute).permit(:agency_id, :mktg_cfg_agency_attribute_id, :value)
    end
end
