module ControllerMacros
  def login_agent(user = FactoryGirl.create(:agent))
    if user.nil?
      allow(controller).to receive(:current_agent).and_return(nil)
    else
      allow(controller).to receive(:current_agent).and_return(user)
    end
  end
end
