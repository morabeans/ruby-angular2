# spec/models/client.rb
require 'rails_helper'

RSpec.describe Client, :type => :model do

  #tests

  describe "attribute first_name" do
    
    context "valid first_name" do

      it "is valid with a first_name not empty and not nil" do
        client = build(:client)
        expect(client).to be_valid
      end

    end
    

    context "invalid first_name" do

      it "is invalid with a first_name in nil" do
        client = build(:client, first_name: nil)
        expect(client).not_to be_valid
      end

      it "is invalid with a first_name empty" do
        client = build(:client, first_name: '')
        expect(client).not_to be_valid
      end

    end

  end

  describe "attribute last_name" do  

    context "valid lastname" do

      it "is valid with a last_name not empty and not nil" do
        client = build(:client)
        expect(client).to be_valid
      end

    end

    context "invalid lastname" do

      it "is invalid with a lastname in nil" do
        client = build(:client, last_name: nil)
        expect(client).not_to be_valid
      end
  
      it "is invalid with a lastname empty" do
        client = build(:client, last_name: '')
        expect(client).not_to be_valid
      end
    end
  end

  describe "attribute email" do 
  
    context "valid email" do

      it "is valid with a emial not empty and not nil" do
       client = build(:client)
       expect(client).to be_valid
      end
    
    end

    context "invalid email" do

      it "is invalid with a email in nil" do
       client = build(:client, email: nil)
       expect(client).not_to be_valid
      end
    
      it "is invalid with a email empty" do
       client = build(:client, email: '')
       expect(client).not_to be_valid
      end

      it "is invalid a email without format" do
       client = build(:client, email: 'ojedaojeda.com')
       expect(client).not_to be_valid
      end

    end

  end



end