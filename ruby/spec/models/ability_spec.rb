# coding: utf-8
require "spec_helper"
require "cancan/matchers"
require 'rails_helper'

describe "Abilities" do
  describe "as agent" do
    before(:each) do
      @agent = Agent.new(:id => 1, :agency_id => 1, :is_owner => false)
      @ability = Ability.new(@agent)
    end
    let(:client) { create(:client_agency1) }
    let(:oclient) { create(:client_agency2)}
    let(:oagent) { Agent.new(:id => 2, :agency_id => 2, :is_owner => false) }
    
    # Begin: Agent Block    
    it "acces to ActionType" do
      # Access
      @ability.should be_able_to(:index, ActionType.new)
      @ability.should be_able_to(:show, ActionType.new)
      # Not Access
      @ability.should_not be_able_to(:create, ActionType.new) 
      @ability.should_not be_able_to(:update, ActionType.new) 
      @ability.should_not be_able_to(:destroy, :action_type) 
    end
    
    it "acces to Address" do
      # Access
      @ability.should be_able_to(:index, Address.new)
      @ability.should be_able_to(:show, Address.new(:client_id => client.id))
      @ability.should be_able_to(:create, Address.new(:client_id => client.id))
      @ability.should be_able_to(:update, Address.new(:client_id => client.id))
      @ability.should be_able_to(:destroy, Address.new(:client_id => client.id))
      # Not Access
      @ability.should_not be_able_to(:show, Address.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:create, Address.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:update, Address.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:destroy, Address.new(:client_id => oclient.id))      
    end
    
    it "acces to Airports" do
      # Access
      @ability.should be_able_to(:index, AirPort.new)
      @ability.should be_able_to(:show, AirPort.new)
      # Not Access
      @ability.should be_able_to(:search_airports, AirPort.new)
      @ability.should_not be_able_to(:create, AirPort.new)
      @ability.should_not be_able_to(:update, AirPort.new)
      @ability.should_not be_able_to(:destroy, AirPort.new)
    end
    
    it "acces to BedSheetType" do
      # Access
      @ability.should be_able_to(:index, BedSheetType.new)
      @ability.should be_able_to(:show, BedSheetType.new)
      # Not Access
      @ability.should_not be_able_to(:create, BedSheetType.new)
      @ability.should_not be_able_to(:update, BedSheetType.new)
      @ability.should_not be_able_to(:destroy, BedSheetType.new)
    end
    
    it "acces to BedType" do
      # Access
      @ability.should be_able_to(:index, BedType.new)
      @ability.should be_able_to(:show, BedType.new)
      # Not Access
      @ability.should_not be_able_to(:create, BedType.new)
      @ability.should_not be_able_to(:update, BedType.new)
      @ability.should_not be_able_to(:destroy, BedType.new)
    end
    
    it "acces to Client" do
      # Access
      @ability.should be_able_to(:index, Client.new())
      @ability.should be_able_to(:count_clients_by_agency, Client.new())
      @ability.should be_able_to(:search_client, Client.new())
      @ability.should be_able_to(:show, client)
      @ability.should be_able_to(:create, client)
      @ability.should be_able_to(:update, client)
      @ability.should be_able_to(:destroy, client)
      @ability.should be_able_to(:sheets, client)
      @ability.should be_able_to(:add_trip_style, client)
      @ability.should be_able_to(:delete_trip_style, client)
      @ability.should be_able_to(:add_trip_group, client)
      @ability.should be_able_to(:delete_trip_group, client)
      @ability.should be_able_to(:general_information_basic_data, client)
      @ability.should be_able_to(:general_information_phones, client)
      @ability.should be_able_to(:general_information_addresses, client)
      @ability.should be_able_to(:general_information_customer_dates, client)
      @ability.should be_able_to(:general_information_professional_details, client)
      @ability.should be_able_to(:general_information_social_medias, client)
      @ability.should be_able_to(:general_information_special_comments, client)
      @ability.should be_able_to(:passports, client)
      @ability.should be_able_to(:visas, client)
      @ability.should be_able_to(:preferences_fly, client)
      @ability.should be_able_to(:preferences_hotel, client)
      @ability.should be_able_to(:preferences_trip_groups, client)
      @ability.should be_able_to(:preferences_trip_styles, client)
      @ability.should be_able_to(:loyalty_programs, client)
      # Not Access
      @ability.should_not be_able_to(:show, oclient)
      @ability.should_not be_able_to(:create, oclient)
      @ability.should_not be_able_to(:update, oclient)
      @ability.should_not be_able_to(:destroy, oclient)
      @ability.should_not be_able_to(:sheets, oclient)
      @ability.should_not be_able_to(:sheets, oclient)
      @ability.should_not be_able_to(:add_trip_style, oclient)
      @ability.should_not be_able_to(:delete_trip_style, oclient)
      @ability.should_not be_able_to(:add_trip_group, oclient)
      @ability.should_not be_able_to(:delete_trip_group, oclient)
      @ability.should_not be_able_to(:general_information_basic_data, oclient)
      @ability.should_not be_able_to(:general_information_phones, oclient)
      @ability.should_not be_able_to(:general_information_addresses, oclient)
      @ability.should_not be_able_to(:general_information_customer_dates, oclient)
      @ability.should_not be_able_to(:general_information_professional_details, oclient)
      @ability.should_not be_able_to(:general_information_social_medias, oclient)
      @ability.should_not be_able_to(:general_information_special_comments, oclient)
      @ability.should_not be_able_to(:passports, oclient)
      @ability.should_not be_able_to(:visas, oclient)
      @ability.should_not be_able_to(:preferences_fly, oclient)
      @ability.should_not be_able_to(:preferences_hotel, oclient)
      @ability.should_not be_able_to(:preferences_trip_groups, oclient)
      @ability.should_not be_able_to(:preferences_trip_styles, oclient)
      @ability.should_not be_able_to(:loyalty_programs, oclient)
    end
        
    it "acces to ClientLoyaltyProgram" do
      # Access
      @ability.should be_able_to(:index, ClientLoyaltyProgram.new)
      @ability.should be_able_to(:show, ClientLoyaltyProgram.new(:client_id => client.id))
      @ability.should be_able_to(:create, ClientLoyaltyProgram.new(:client_id => client.id))
      @ability.should be_able_to(:update, ClientLoyaltyProgram.new(:client_id => client.id))
      @ability.should be_able_to(:destroy, ClientLoyaltyProgram.new(:client_id => client.id))
      # Not Access
      @ability.should_not be_able_to(:show, ClientLoyaltyProgram.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:create, ClientLoyaltyProgram.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:update, ClientLoyaltyProgram.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:destroy, ClientLoyaltyProgram.new(:client_id => oclient.id))      
    end
    
    # it "acces to ClientPreference" do
    #   # Access
    #   @ability.should be_able_to(:index, ClientPreference.new)
    #   @ability.should be_able_to(:show, ClientPreference.new(:client_id => client.id))
    #   @ability.should be_able_to(:create, ClientPreference.new(:client_id => client.id))
    #   @ability.should be_able_to(:update, ClientPreference.new(:client_id => client.id))
    #   @ability.should be_able_to(:destroy, ClientPreference.new(:client_id => client.id))
    #   # Not Access
    #   @ability.should_not be_able_to(:show, ClientPreference.new(:client_id => oclient.id))
    #   @ability.should_not be_able_to(:create, ClientPreference.new(:client_id => oclient.id))
    #   @ability.should_not be_able_to(:update, ClientPreference.new(:client_id => oclient.id))
    #   @ability.should_not be_able_to(:destroy, ClientPreference.new(:client_id => oclient.id))      
    # end

    it "acces to ClientSpecialComment" do
      # Access
      @ability.should be_able_to(:index, ClientSpecialComment.new)
      @ability.should be_able_to(:show, ClientSpecialComment.new(:agency_id => 1))
      @ability.should be_able_to(:create, ClientSpecialComment.new(:agency_id => 1))
      @ability.should be_able_to(:update, ClientSpecialComment.new(:agency_id => 1))
      @ability.should be_able_to(:destroy, ClientSpecialComment.new(:agency_id => 1))
      # Not Access
      @ability.should_not be_able_to(:show, ClientSpecialComment.new(:agency_id => 2))
      @ability.should_not be_able_to(:create, ClientSpecialComment.new(:agency_id => 2))
      @ability.should_not be_able_to(:update, ClientSpecialComment.new(:agency_id => 2))
      @ability.should_not be_able_to(:destroy, ClientSpecialComment.new(:agency_id => 2))      
    end
    
    it "acces to Country" do
      # Access
      @ability.should be_able_to(:index, Country.new)
      @ability.should be_able_to(:show, Country.new)
      # Not Access
      @ability.should_not be_able_to(:create, Country.new)
      @ability.should_not be_able_to(:update, Country.new)
      @ability.should_not be_able_to(:destroy, Country.new)
    end

    it "acces to Education" do
      # Access
      @ability.should be_able_to(:index, Education.new)
      @ability.should be_able_to(:show, Education.new)
      # Not Access
      @ability.should_not be_able_to(:create, Education.new)
      @ability.should_not be_able_to(:update, Education.new)
      @ability.should_not be_able_to(:destroy, Education.new)
    end
    
    it "acces to HotelOption" do
      # Access
      @ability.should be_able_to(:index, HotelOption.new)
      @ability.should be_able_to(:show, HotelOption.new(:client_id => client.id))
      @ability.should be_able_to(:create, HotelOption.new(:client_id => client.id))
      @ability.should be_able_to(:update, HotelOption.new(:client_id => client.id))
      @ability.should be_able_to(:destroy, HotelOption.new(:client_id => client.id))
      # Not Access
      @ability.should_not be_able_to(:show, HotelOption.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:create, HotelOption.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:update, HotelOption.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:destroy, HotelOption.new(:client_id => oclient.id))      
    end

    # it "acces to HotelService" do
    #   # Access
    #   @ability.should be_able_to(:index, HotelService.new)
    #   @ability.should be_able_to(:show, HotelService.new)
    #   # Not Access
    #   @ability.should_not be_able_to(:create, HotelService.new)
    #   @ability.should_not be_able_to(:update, HotelService.new)
    #   @ability.should_not be_able_to(:destroy, HotelService.new)
    # end

    it "acces to LoyaltyProgram" do
      # Access
      @ability.should be_able_to(:index, LoyaltyProgram.new)
      @ability.should be_able_to(:show, LoyaltyProgram.new)
      @ability.should be_able_to(:search_loyalty, LoyaltyProgram.new)
      # Not Access
      @ability.should_not be_able_to(:create, LoyaltyProgram.new)
      @ability.should_not be_able_to(:update, LoyaltyProgram.new)
      @ability.should_not be_able_to(:destroy, LoyaltyProgram.new)
    end
    
    it "acces to Passport" do
      # Access
      @ability.should be_able_to(:index, Passport.new)
      @ability.should be_able_to(:show, Passport.new(:client_id => client.id))
      @ability.should be_able_to(:create, Passport.new(:client_id => client.id))
      @ability.should be_able_to(:update, Passport.new(:client_id => client.id))
      @ability.should be_able_to(:destroy, Passport.new(:client_id => client.id))
      # Not Access
      @ability.should_not be_able_to(:show, Passport.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:create, Passport.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:update, Passport.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:destroy, Passport.new(:client_id => oclient.id))      
    end
    
    it "acces to Phone" do
      # Access
      @ability.should be_able_to(:index, Phone.new)
      @ability.should be_able_to(:show, Phone.new(:client_id => client.id))
      @ability.should be_able_to(:create, Phone.new(:client_id => client.id))
      @ability.should be_able_to(:update, Phone.new(:client_id => client.id))
      @ability.should be_able_to(:destroy, Phone.new(:client_id => client.id))
      # Not Access
      @ability.should_not be_able_to(:show, Phone.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:create, Phone.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:update, Phone.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:destroy, Phone.new(:client_id => oclient.id))      
    end
    
    it "acces to PillowType" do
      # Access
      @ability.should be_able_to(:index, PillowType.new)
      @ability.should be_able_to(:show, PillowType.new)
      # Not Access
      @ability.should_not be_able_to(:create, PillowType.new)
      @ability.should_not be_able_to(:update, PillowType.new)
      @ability.should_not be_able_to(:destroy, PillowType.new)
    end

    it "acces to Profession" do
      # Access
      @ability.should be_able_to(:index, Profession.new)
      @ability.should be_able_to(:show, Profession.new)
      # Not Access
      @ability.should_not be_able_to(:create, Profession.new)
      @ability.should_not be_able_to(:update, Profession.new)
      @ability.should_not be_able_to(:destroy, Profession.new)
    end

    it "acces to RoomType" do
      # Access
      @ability.should be_able_to(:index, RoomType.new)
      @ability.should be_able_to(:show, RoomType.new)
      # Not Access
      @ability.should_not be_able_to(:create, RoomType.new)
      @ability.should_not be_able_to(:update, RoomType.new)
      @ability.should_not be_able_to(:destroy, RoomType.new)
    end

    it "acces to SeatingPreference" do
      # Access
      @ability.should be_able_to(:index, SeatingPreference.new)
      @ability.should be_able_to(:show, SeatingPreference.new)
      # Not Access
      @ability.should_not be_able_to(:create, SeatingPreference.new)
      @ability.should_not be_able_to(:update, SeatingPreference.new)
      @ability.should_not be_able_to(:destroy, SeatingPreference.new)
    end
    
    it "access to Sheet" do
      # Access
      @ability.should be_able_to(:index,Sheet.new)
      @ability.should be_able_to(:show_options,Sheet.new)
      @ability.should be_able_to(:show, Sheet.new(:agent_id => @agent.id))
      @ability.should be_able_to(:create, Sheet.new(:agent_id => @agent.id))
      @ability.should be_able_to(:update, Sheet.new(:agent_id => @agent.id))
      @ability.should be_able_to(:destroy, Sheet.new(:agent_id => @agent.id))
      @ability.should be_able_to(:show, Sheet.new(:agency_id => @agent.agency_id, :public => true))
      @ability.should be_able_to(:create, Sheet.new(:agency_id => @agent.agency_id, :public => true))
      @ability.should be_able_to(:update, Sheet.new(:agency_id => @agent.agency_id, :public => true))
      @ability.should be_able_to(:destroy, Sheet.new(:agency_id => @agent.agency_id, :public => true))
      # Not Acces
      @ability.should_not be_able_to(:show, Sheet.new(:agent_id => oagent.id))
      @ability.should_not be_able_to(:create, Sheet.new(:agent_id => oagent.id))
      @ability.should_not be_able_to(:update, Sheet.new(:agent_id => oagent.id))
      @ability.should_not be_able_to(:destroy, Sheet.new(:agent_id => oagent.id))
      @ability.should_not be_able_to(:show, Sheet.new(:agency_id => @agent.agency_id, :public => false))
      @ability.should_not be_able_to(:create, Sheet.new(:agency_id => @agent.agency_id, :public => false))
      @ability.should_not be_able_to(:update, Sheet.new(:agency_id => @agent.agency_id, :public => false))
      @ability.should_not be_able_to(:destroy, Sheet.new(:agency_id => @agent.agency_id, :public => false))
    end

    # Things About Sheet
    # it "acces same agency public sheet" do
    #   expect { @ability.should be_able_to(:show, Sheet.new(:agent_id => 2, :agency_id =>1, :public => true)) }
    # end

    # it "not acces same agency private sheet" do
    #   expect { should_not be_able_to(:show, Sheet.new(:agent_id => 2, :agency_id =>1, :public => false)) }
    # end
    it "acces to SpecialAssistance" do
      # Access
      @ability.should be_able_to(:index, SpecialAssistance.new)
      @ability.should be_able_to(:show, SpecialAssistance.new)
      # Not Access
      @ability.should_not be_able_to(:create, SpecialAssistance.new)
      @ability.should_not be_able_to(:update, SpecialAssistance.new)
      @ability.should_not be_able_to(:destroy, SpecialAssistance.new)
    end
    
    it "acces to SpecialCategory" do
      # Access
      @ability.should be_able_to(:index, SpecialCategory.new)
      @ability.should be_able_to(:show, SpecialCategory.new)
      # Not Access
      @ability.should_not be_able_to(:create, SpecialCategory.new)
      @ability.should_not be_able_to(:update, SpecialCategory.new)
      @ability.should_not be_able_to(:destroy, SpecialCategory.new)
    end

    it "acces to SpecialSubCategory" do
      # Access
      @ability.should be_able_to(:index, SpecialSubCategory.new)
      @ability.should be_able_to(:show, SpecialSubCategory.new)
      # Not Access
      @ability.should_not be_able_to(:create, SpecialSubCategory.new)
      @ability.should_not be_able_to(:update, SpecialSubCategory.new)
      @ability.should_not be_able_to(:destroy, SpecialSubCategory.new)
    end

    it "acces to SpecialSubCategoryDetail" do
      # Access
      @ability.should be_able_to(:index, SpecialSubCategoryDetail.new)
      @ability.should be_able_to(:show, SpecialSubCategoryDetail.new)
      # Not Access
      @ability.should_not be_able_to(:create, SpecialSubCategoryDetail.new)
      @ability.should_not be_able_to(:update, SpecialSubCategoryDetail.new)
      @ability.should_not be_able_to(:destroy, SpecialSubCategoryDetail.new)
    end

    it "acces to TripGroup" do
      # Access
      @ability.should be_able_to(:index, TripGroup.new)
      @ability.should be_able_to(:show, TripGroup.new)
      # Not Access
      @ability.should_not be_able_to(:create, TripGroup.new)
      @ability.should_not be_able_to(:update, TripGroup.new)
      @ability.should_not be_able_to(:destroy, TripGroup.new)
    end

    it "acces to TripStyle" do
      # Access
      @ability.should be_able_to(:index, TripStyle.new)
      @ability.should be_able_to(:show, TripStyle.new)
      # Not Access
      @ability.should_not be_able_to(:create, TripStyle.new)
      @ability.should_not be_able_to(:update, TripStyle.new)
      @ability.should_not be_able_to(:destroy, TripStyle.new)
    end

    it "acces to TripType" do
      # Access
      @ability.should be_able_to(:index, TripType.new)
      @ability.should be_able_to(:show, TripType.new)
      # Not Access
      @ability.should_not be_able_to(:create, TripType.new)
      @ability.should_not be_able_to(:update, TripType.new)
      @ability.should_not be_able_to(:destroy, TripType.new)
    end

    it "acces to TypeService" do
      # Access
      @ability.should be_able_to(:index, TypeService.new)
      @ability.should be_able_to(:show, TypeService.new)
      # Not Access
      @ability.should_not be_able_to(:create, TypeService.new)
      @ability.should_not be_able_to(:update, TypeService.new)
      @ability.should_not be_able_to(:destroy, TypeService.new)
    end
    
    it "acces to Visa" do
      # Access
      @ability.should be_able_to(:index, Visa.new)
      @ability.should be_able_to(:show, Visa.new(:client_id => client.id))
      @ability.should be_able_to(:create, Visa.new(:client_id => client.id))
      @ability.should be_able_to(:update, Visa.new(:client_id => client.id))
      @ability.should be_able_to(:destroy, Visa.new(:client_id => client.id))
      # Not Access
      @ability.should_not be_able_to(:show, Visa.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:create, Visa.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:update, Visa.new(:client_id => oclient.id))
      @ability.should_not be_able_to(:destroy, Visa.new(:client_id => oclient.id))      
    end
  # End: Agent Block
  end
  
  describe "as owner" do
    before(:each) do
      @agent = Agent.new(:id => 1, :agency_id => 1, :is_owner => true)
      @ability = Ability.new(@agent)
    end

    # Begin: Owner Block
    it "not acces same agency private sheet" do
      @ability.should be_able_to(:show, Sheet.new(:agent_id => 2, :agency_id =>1, :public => false)) 
    end
    
    # End: Owner Block
  end
end
