# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application

cors_origins = ENV.fetch('RAILS42_CORS_ORIGINS', '*')

require 'rack/cors'
use Rack::Cors do
  # TODO: secure
  allow do
        origins '*'

        resource '*',
          :headers => :any,
          :methods => [:get, :post, :delete, :put, :options, :head],
          :expose => ['access-token', 'expiry', 'token-type', 'uid', 'client'],
          :max_age => 1728000
      end
end
